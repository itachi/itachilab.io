title: node命令行工具开发
keywords: node命令行工具开发
savedir: articles
categories:
  - nodejs
tags:
  - knowledge
date: 2016-06-23 16:32:32
updated: 2016-06-23 16:32:32
urlkey: node-cli
---

## 1. 命令行主程序

建于 `bin` 目录，代码第一行为 `shebang`，比如 `#!/usr/bin/env node`，表明使用 node 执行

## 2. package.json 添加 bin 字段，指明命令与文件映射关系

```js
"bin": {
	"itacmd": "bin/cmd.js"
}
```

## 3. 编写主程序

可使用第三方库辅助开发

* [commander](https://github.com/tj/commander.js)
* [commander api](http://tj.github.io/commander.js/)
* [shelljs](https://github.com/shelljs/shelljs)
* [chalk](https://github.com/chalk/chalk)
* [node-optimist](https://github.com/substack/node-optimist)
* [yargs](https://github.com/yargs/yargs)

## 4. 发布，安装

```bash
npm publish
npm i xxx -g
```

也可以目录下运行 `npm link` 链接到全局，直接全局调用