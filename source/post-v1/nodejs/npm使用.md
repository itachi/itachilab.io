title: npm使用
keywords: npm使用
savedir: articles
categories:
  - nodejs
date: 2015-11-28 13:50:39
updated: 2015-11-28 13:50:39
urlkey: npm
---

## 指定库安装

```bash
npm --registry=http://xxx.com install -(g) xxx
```

## 修改默认库

```bash
npm config set registry http://xxx.com
```

## 删除库（执行完会自动恢复到 https://registry.npmjs.org/）

```bash
npm config delete registry
```

## 配置

```bash
npm config set <key> <value> [--global]
npm config get <key>
npm config delete <key>
npm config list
npm config edit
npm get <key>
npm set <key> <value> [--global]
npm config get userconfig ----- 获取当前用户文件路径
npm config get globalconfig ---------- 获取全局文件路径
npm config ls -l ----- 如果查看npm的所有配置属性。注意：以;开头的为注释。
```

## 常用命令

```bash
npm install xxx 安装模块
npm install xxx@1.1.1 安装1.1.1版本的xxx
npm install xxx -g 将模块安装到全局环境中。
npm uninstall xxx (-g) 卸载模块
npm update xxx 更新
npm ls 查看安装的模块及依赖
npm ls -g 查看全局安装的模块及依赖
npm root 查看当前包的安装路径
npm root -g 查看全局的包的安装路径
npm whoami 查看当前npm帐户
npm cache clean 清理缓存
npm view moudleName dependencies 查看包的依赖关系
npm view moduleNames 查看node模块的package.json文件夹
npm view moduleName labelName 查看package.json文件夹下某个标签的内容
npm view moduleName repository.url 查看包的源文件地址
npm view moduleName engines 查看包所依赖的Node的版本
npm help folders 查看npm使用的所有文件夹
npm rebuild moduleName 用于更改包内容后进行重建
npm outdated 检查包是否已经过时，此命令会列出所有已经过时的包，可以及时进行包的更新
```