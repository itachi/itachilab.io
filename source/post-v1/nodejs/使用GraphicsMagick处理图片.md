title: 使用GraphicsMagick处理图片
keywords: 使用GraphicsMagick处理图片
savedir: articles
categories:
  - nodejs
tags:
  - experience
date: 2017-12-27 17:16:34
updated: 2017-12-27 17:16:34
urlkey: graphicsmagick-nodejs
---

## 简介

ImageMagick是一个免费的创建、编辑、合成图片的软件。它可以读取、转换、写入多种格式的图片。图片切割、颜色替换、各种效果的应用，图片的旋转、组合，文本，直线，多边形，椭圆，曲线，附加到图片伸展旋转。其全部源码开放，可以自由使用，复制，修改，发布。它遵守GPL许可协议。它可以运行于大多数的操作系统。而GraphicsMagick是从 ImageMagick 5.5.2分支出来的更小更高效的版本，GraphicsMagick的命令与ImageMagick基本是一样的

## 参考

* [官网](http://www.graphicsmagick.org/index.html)
* [gm](https://aheckmann.github.io/gm/docs.html#manipulation)
* [github](https://github.com/aheckmann/gm)


## node.js中的使用

根据操作系统从[官网](http://www.graphicsmagick.org/index.html)下载软件，这里以window为例，[下载地址](http://78.108.103.11/MIRROR/ftp/GraphicsMagick/windows/)

安装node包

```bash
npm i gm -S
```

使用

```js
var fs = require('fs')
  , gm = require('gm');

// resize and remove EXIF profile data
gm('/path/to/my/img.jpg')
.resize(240, 240)
.noProfile()
.write('/path/to/resize.png', function (err) {
  if (!err) console.log('done');
});
```

在window下会报找不到convert包的错误，需要指定本地软件安装目录
```
gm('img.png')
.options({
	appPath: 'C:\\GraphicsMagick-1.3.23-Q8\\'
})
```

## 使用举例

### 获取gif每一帧

```
fs.readdir(gifdir, (err, dir) => {
	if(err) throw new Error(err)
	var count = 0
	if(dir.length){
		dir.forEach((item, index) => {
			var gif = path.join(gifdir, item) + '[0]'
			var dist = path.join(gifdist, item)
			gm(gif)
			.options({
				appPath: 'C:\\GraphicsMagick-1.3.23-Q8\\'
			})
			.resize(35, 35)
			.write(dist, err => {
				if(err){
					console.log(err)
				}else{
					count++
					console.log(count)
				}
			})
		})
	}
})
```

### 合并图片

```
fs.readdir(gifdist, (err, dir) => {
	if(err) throw new Error(err)
	if(dir.length){
		dir = dir.map(item => path.join(gifdist, item))
		var gif = dir.shift()
		var dist = path.join(gifdir, 'face.gif')
		gm(gif)
		.options({
			appPath: 'C:\\GraphicsMagick-1.3.23-Q8\\'
		})
		.append(...dir)
		.write(dist, err => {
			if(err){
				console.log(err)
			}else{
				console.log('success')
			}
		})
	}
})
```