title: tap点透问题
keywords: tap点透问题
savedir: articles
categories:
  - resolution
date: 2017-12-26 16:54:57
updated: 2017-12-26 16:54:57
urlkey: taphack
---

## 产生条件

如果绑定tap方法的dom元素（或者使用touchstart事件绑定）在tap方法触发后发生移动（隐藏，移走，删掉），则它底下同一位置的dom元素会触发click事件、或者有浏览器认为可以被点击有交互反应的dom元素（例如input的focus事件），这个称为“点透”现象。

## 产生原因

1. click事件在移动端会有300ms延迟（因为需要检测双击事件，移动端有click300毫秒延迟的原因）
2. 对于zepto，zepto的tap事件是绑定在document.body上的，tap事件执行（冒泡之后）之前，click事件已经被"执行"，只是被延迟了而已，所以在tap事件用preventDefault也无济于事
3. 对于touchstart，事件执行的顺序是touchstart > touchend > click。 而click事件有300ms的延迟，当touchstart事件把元素隐藏之后，隔了300ms，浏览器触发了click事件，但是此时元素不见了，所以该事件被派发到了元素下方的元素身上

## 解决方案

1. 使用click替代touchstart，但要解决300ms问题
2. 底下的元素也使用tap事件，即上下两个元素使用同一种事件（tap/click）
3. 去掉300ms延迟，比如使用fastclick库