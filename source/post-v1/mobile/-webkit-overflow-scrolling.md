title: -webkit-overflow-scrolling
keywords: -webkit-overflow-scrolling
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-26 16:26:13
updated: 2015-12-26 16:26:13
urlkey: -webkit-overflow-scrolling
---

-webkit-overflow-scrolling 用来控制元素在移动设备上是否使用滚动回弹效果，仅在ios上有效

兼容安卓和IOS的写法如下

```css
overflow:auto; /* android */ 
-webkit-overflow-scrolling: touch; /* ios */ 
```

在IOS环境下，每次手动改变scrollTop后整个容器变成空白，但手指触摸一下内容就出来了，scrollTop是成功改变了的，只是内容绘制出错，解决思路，在手动设置scrollTop前，先关闭惯性滚动，待设置完成后重新开启

```js
el.WebKitOverflowScrolling = 'auto';
el.scrollTop = 500;
el.WebKitOverflowScrolling = 'touch';
```