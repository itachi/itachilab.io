title: 淘宝lib-flexible实践
keywords: 淘宝lib-flexible实践
savedir: articles
categories:
  - dom
date: 2017-03-16 16:01:34
updated: 2017-03-16 16:01:34
urlkey: lib-flexible
---

## 原理

### js 设置 viewport，动态设置 scale

一般 device-width 的计算公式为：设备的物理分辨率/(devicePixelRatio * scale)，而设计稿的尺寸一般是按照设备的物理分辨率来定，为了让 device-width 和设计稿大小保持一致，动态设置 scale 为 1 / devicePixelRatio

### 动态设置 font-size

规定 1rem 等于设计稿宽度十份一，即750的设计稿 font-size 为75px

```js
document.documentElement.style.fontSize = document.documentElement.clientWidth / 10 + 'px';
```

### css换算

各元素的css尺寸=设计稿标注尺寸/设计稿横向分辨率/10

### 适配流程

1. 设计师按宽度750px（iPhone 6）做设计稿，在750px的设计稿上做标注，输出标注图。同时等比放大1.5倍生成宽度1125px的设计稿，在1125px的稿子里切图
2. 开发人员使用750px标注图完成iPhone 6（375pt）的界面开发，切图资源用的是1125px的设计稿的切图
3. 调试阶段，基于iPhone 6的界面效果，分别向上向下调试iPhone 6 plus（414pt）和iPhone 5S及以下（320pt）的界面效果。由此完成大中小三屏适配

## 实践

### 引入flexible.js，页面不需要手动设置 viewport

```html
<head>
    <title>淘宝弹性布局方案lib-flexible实践</title>
    <script src="js/lib/flexible.js"></script>
    <link rel="stylesheet" href="css/index.css"/>
</head>
```

### 字体使用 px 单位，利用 data-dpr 属性适配

```css
[data-dpr="2"] p {
    font-size: 16px;
}

[data-dpr="3"] p {
    font-size: 24px;
}
```

### css转换

```css
@font-size-base: 75;
.btn {
    width: 414rem/@font-size-base;
    height: 80rem/@font-size-base;
}
```

## 注意事项

### 不能与响应式布局兼容

lib-flexible 在适配的时候会修改 viewport 的 initial-scale，导致 viewport 的 width 不等于 device-width，比如以下本想适配桌面设备的媒体查询也会被 iphone6 plus 应用到，因为 lib-flexible 的作用，导致页面的 width 与实际物理分辨率的宽相等，也就是1242个像素，完全达到了该媒介查询的范围

```css
@media only screen and (min-width: 1024px) {
    body {
        border: 10px solid #ccc;
    }
}
```

### 0.5px边框

lib-flexible 在适配的时候会缩放网页，导致css代码中的 1px 等于物理分辨率的 1px，所以直接设置 1px 的 border 就可以实现 0.5px边框的效果，但 lib-flexible 只适配了 iphone 设备，安卓设备的 dpr 都默认为1，导致在安卓设备下，1px的边框问题依然存在

解决方案

```css
.setTopLine(@c: #C7C7C7) {
  & {
    position: relative;
  }

  &:before {
    content: " ";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 1px;
    border-top: 1px solid @c;
  }

  [data-dpr="1"] &:before {
    transform-origin: 0 0;
    transform: scaleY(0.5);
  }
}
```

### 雪碧图

用不了