title: 服务器包含技术-SSI
keywords: 服务器包含技术-SSI
savedir: articles
categories:
  - development
tags:
  - knowledge
date: 2015-12-01 00:08:22
updated: 2015-12-01 00:08:22
urlkey: ssi
---

## 简介

服务器端包含(Server Side Includes)，通常简称为SSI,是HTML页面中的指令，在页面被提供时由服务器进行运算，以对现有HTML页面增加动态生成的内容，而无须通过CGI程序提供其整个页面，或者使用其他动态技术。

## 使用

要使服务器允许SSI，必须在 httpd.conf 文件或 .htaccess 文件中有如下配置：

```
Options Includes
```

> 注意：在多数配置中，多个 Options 指令会互相覆盖，所以如果要确保起作用。可以针对需要SSI的特定目录中使用 Options

并不是所有文件中的SSI指令都会被解析，所以，必须告诉 Apache 应该解析哪些文件。有两种方法，

### 方法一：

解析带有特定文件后缀的任何文件（以shtml为例）

```
AddType text/html .shtml
AddOutputFilter INCLUDES .shtml
```

添加inducles
```

Options FollowSymLinks Indexes Includes
```

### 方法二：

使用 XBitHack 指令，告诉 Apache 解析有执行权限的文件中的SSI指令。然后用 chmod 使文件变成可执行的

```
XBitHack on
chmod +x pagename.html
```

## 基本SSI指令

格式：

```
<!--#element attribute=value attribute=value ... -->
```

其格式很象 HTML 的注释，因此如果没有正确配置SSI，它会被浏览器忽略，但在 HTML 代码中仍然可见。而如果正确配置了SSI，则此指令会被其结果替代。

### 应用举例

今天的日期

```
<!--#echo var="DATE_LOCAL" -->
```

echo 元素仅仅是反馈一个变量的值。标准变量有许多，其中包含对CGI程序有效的所有的环境变量

包含一个CGI程序的输出

```
<!--#include virtual="/cgi-bin/counter.pl" -->
```

文件的修改日期

```
This document last modified <!--#flastmod file="index.html" -->
```