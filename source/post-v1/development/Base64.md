title: Base64网页性能优化
keywords: Base64网页性能优化
savedir: articles
categories:
  - development
tags:
  - knowledge
date: 2015-12-26 16:48:05
updated: 2015-12-26 16:48:05
urlkey: base64
---
Base64是网络上最常见的用于传输8 Bit字节代码的编码方式之一，可用于在 HTTP 环境下传递较长的标识信息。在网络中，通过 HTTP 传输的文件还可以通过 base64 对数据进行编码进行传输，例如图片，字体文件，背景图片

## 使用

1. 使用工具对图片等文件进行 base64 编码，得到一串该文件的base64编码格式，推荐工具：http://www.pjhome.net/web/html5/encodeDataUrl.htm
2. 将以上得到的整串编码应用到图片原来的位置，原图片可以不存在网站目录
3. 样式编写
```css
.dot {
    background-image: url(data:image/gif;base64,R0lGODlhBAABAIABAMLBwfLx8SH5BAEAAAEALAAAAAAEAAEAAAICRF4AOw==);
   *background-image: url(http://www.zhangxinxu.com/wordpress/wp-content/themes/default/images/zxx_dotted.gif);    // IE6~IE7
}
```

## 使用base64:URL的优缺点

### 优点

* 减少了HTTP请求
* 某些文件可以避免跨域的问题
* 没有图片更新要重新上传，还要清理缓存的问题

### 缺点

* 兼容性：IE6/IE7浏览器是不支持，需要单独设置（兼容性问题使没有文件上传以及无需更新缓存的优点不存在了）
* 增加了CSS文件的尺寸，base64编码图片本质上是将图片的二进制大小以一些字母的形式展示，例如一个1024字节的图片，base64编码后至少1024个字符，这个大小会被完全嵌入到CSS文件中（不过幸运的是也可以被gzip了，而图片文件被gzip效果不明显）。
* 维护工作：图片完成后还需要base64编码，增加了一定的工作量，后期维护不方便

## 实际应用价值

在 web 页面制作的时候，如果需要一种这样的图片：这类图片不能与其他图片以 CSS Sprite 的形式存在，体积小，基本上很少被更新，大规模使用，比如 loading 图片，一些修饰背景图片（虚线）等。使用 base64 可以起到一定的优化作用
