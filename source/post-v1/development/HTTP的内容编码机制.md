title: HTTP的内容编码机制
keywords: HTTP的内容编码机制
savedir: articles
categories:
  - development
tags:
  - knowledge
  - http
date: 2017-12-25 10:30:41
updated: 2017-12-25 10:30:41
urlkey: http-content-encoding
---

## 内容编码机制

内容编码目的是优化传输内容大小，通俗地讲就是进行压缩。一般经过 gzip 压缩过的文本响应，只有原始大小的 1/4。对于文本类响应是否开启了内容压缩，是我们做性能优化时首先要检查的重要项目；而对于 JPG / PNG 这类本身已经高度压缩过的二进制文件，不推荐开启内容压缩，效果微乎其微还浪费 CPU

内容编码针对的只是传输正文。在 HTTP/1 中，头部始终是以 ASCII 文本传输，没有经过任何压缩。这个问题在 HTTP/2 中得以解决，详见：[HTTP/2 头部压缩技术介绍](https://imququ.com/post/header-compression-in-http2.html)。

## Content-Encoding

Accept-Encoding 和 Content-Encoding 是 HTTP 中用来对__「采用何种编码格式传输正文」__进行协定的一对头部字段。它的工作原理是这样：浏览器发送请求时，通过 Accept-Encoding 带上自己支持的内容编码格式列表；服务端从中挑选一种用来对正文进行编码，并通过 Content-Encoding 响应头指明选定的格式；浏览器拿到响应正文后，依据 Content-Encoding 进行解压。当然，服务端也可以返回未压缩的正文，但这种情况不允许返回 Content-Encoding

一般内容编码格式有gzip和deflate，但一般都是gzip，很少看到 `Content-Encoding: deflate`