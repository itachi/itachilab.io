title: jsonp跨域
keywords: jsonp跨域
savedir: articles
categories:
  - request
tags:
  - summary
date: 2015-10-03 17:35:44
updated: 2015-10-03 17:35:44
urlkey: jsonp
---

Ajax直接请求普通文件存在跨域无权限访问的问题，不管静态页面、动态网页、web服务、WCF ，而Web页面上调用js文件时则不受是否跨域的影响，而且，凡是拥有 src 这个属性的标签都拥有跨域的能力，比如`<script>`、`<img>`、`<iframe>` 。如果想通过纯web端跨域访问数据就可以利用这点，那就是在远程服务器上设法把数据装进js格式的文件里，供客户端调用和进一步处理,具体实现方法是web客户端通过与调用脚本一模一样的方式，来调用跨域服务器上动态生成的js格式文件，利用 JSON 这种数据格式的各种优势，服务器端可以把客户端需要的数据封装成 JSON 格式，方便其处理。客户端在对 JSON 文件调用成功之后，也就获得了自己所需的数据，剩下的就是按照自己需求进行处理和展现了，这种获取远程数据的方式看起来非常像 AJAX，但其本质并不一样。这种获取远程数据的方式逐渐形成了一种非正式传输协议，人们把它称作 JSONP，该协议的一个要点就是允许用户传递一个 callback 参数给服务端，然后服务端返回数据时会将这个 callback 参数作为函数名来包裹住 JSON 数据，这样客户端就可以随意定制自己的函数来自动处理返回数据了。服务器端的工作是利用客户端特定的一个参数（回调函数名）来动态生成js代码（用回调函数包住数据）

所以，ajax 和 jsonp 本质上是不同的东西。ajax 的核心是通过 XmlHttpRequest 异步获取数据并更新处理，而 jsonp 的核心则是动态添加`<script>`标签来调用服务器提供的js脚本，ajax 与 jsonp 的区别不在于是否跨域，ajax 通过服务端代理一样可以实现跨域，jsonp本身也不排斥同域的数据获取。跨域不一定要用到 ajax，而 ajax 一般使用 jsonp 来实现跨域

> 注意：使用 JSONP 来跨域要求请求类型只能是 get，如果是 post 类型的跨域，可以用 iframe跨域等其它方式

## 实现代码（假设服务器端接受回调函数名的参数为callback）

### 服务器端（PHP）

```php
$arr=array('data'=>'show me'); 
$result=json_encode($arr); 
$callback=$_GET['callback']; 
echo $callback."($result)";
```

### 客户端

```js
var custom_callback = function(data){
     alert(data.txt);
};
var JSONP=document.createElement("script");
JSONP.type="text/javascript";
JSONP.src="http://xxx.com?var=1&callback=custom_callback";
document.getElementsByTagName('head')[0].appendChild(script);
```

或

```html
<script type="text/javascript">
var custom_callback = function(data){
     alert(data.txt);
};
</script>
<script type="text/javascript" src="http://xxx.com?var=1&callback=custom_callback"></script>
```

## 在jQuery中使用

如果使用jQuery，则可以给 callback 参数赋一个问号，jquery 会传递一个随机的函数名并同步到回调函数，用户只需要实现该匿名函数的具体功能即可

```js
$.getJSON("http://xxx.com?var=1&callback=?", 
function(data){
     alert(data.txt);
});
```

或

```js
$.get('http://xxx.com?callback=?', {var: 1}, function (data){alert(data.txt);}, 'jsonp');  
```

或

```js
$.ajax({
     url:"http://xxx.com?var=1",
     dataType:'jsonp',
     data:'',
     jsonp:'callback', 
     success:function(data) {
          alert(data.txt);
     },
     timeout:3000
});
```