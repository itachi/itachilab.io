title: 通过Doctype启用浏览器模式
keywords: 通过Doctype启用浏览器模式
savedir: articles
categories:
  - html
tags:
  - knowledge
date: 2015-12-26 16:46:36
updated: 2015-12-26 16:46:36
urlkey: doctype
---

为了即能解析那些满足Web标准的网页，又能解析那些过去20年来遗留下来的传统的网页，现代浏览器一般都实现了多种网页解析的模型,这些解析模型一般通过Doctype来触发，另外还可以通过其它方式来触发。

## 适用于text/html类型的常见模式

对text/html类型的内容来说，模式的选择取决于对文档的doctype探测。在IE8及更高版本的浏览器中，模式还取决于一些其它因素

### 怪癖模式（Quirks Mode）

以前，不同的浏览器实现了不同的怪癖模式，比如IE6/7/8/9里的怪癖模式就是IE5.5模式，其它浏览器的怪癖模式则是对近乎标准模式的一种修改。最近各浏览器已经开始在它们的怪癖模式里应用相同的行为，尤其要提到的是IE10的怪癖模式已不再模仿IE5.5，而是与其它浏览器的怪癖模式保持一致。目前WHATWG正在制定怪癖模式的标准。

### 标准模式（Standards Mode）

标准模式中，这些浏览器将尝试以各自实现的程度规范正确地处理文档。不同的浏览器遵循不同的阶段，所以标准模式也不是一个单一目标。HTML标准中称这种模式为 “非怪癖模式”（no quirks mode）

### 近乎标准模式（Almost Standards Mode）

Firefox, Safari, Chrome, Opera (从7.5开始), IE8, IE9 and IE10 还有一个称为近乎标准模式（the Almost Standards mode）的模式，这种模式以传统的方式设定表格的垂直高度，而没有遵从CSS2的标准。事实上，相对于较新的浏览器的标准模式，它们的标准模式更接近于近乎标准模式。然而，你还是应该用标准模式，而不要用近乎标准模式。HTML标准中称这种模式为有限怪癖。

## 适用于application/xhtml+xml类型的模式（XML模式）

在Firefox, Safari, Chrome, Opera 和 IE9 里，如果HTTP响应中的Content-type是application/xhtml+xml时会触发XML模式，需要强调的是触发条件是HTTP的Content-type，不是HTML中的<meta...>元素，也不是HTML的doctype。在XML模式下，这些浏览器将以各自实现的程度规范正确地处理XML文档。  
IE6/7/8不支持application/xhtml+xml，Mac版的IE5也不支持。

## 微软额外提供的一些IE-Specific模式

这些模式并不适用于HTML5，也不适用于其它不支持这些模式的浏览器，它们通过配置来激活，还可以通过"X-UA-Compatibleas"的HTTP头，或者html里的meta元素进行激活。

### Internet Explorer 5 Quirks

除了互操作性怪癖模式外，IE10还有一个称为Internet Explorer 5 Quirks的模式，它模仿了IE5.5的行为，这种模式在IE6/7/8/9中称为怪癖模式。

### Internet Explorer 7 Standards
IE8/9/10使用该模式模拟IE7的标准模式。

### Internet Explorer 8 Standards
IE9/10使用该模式模拟IE8的标准模式。

### Internet Explorer 8 Almost Standards
IE9/10使用该模式模拟IE8的近乎标准模式行为。在界面上的开发者工具箱中，这个模式与IE8标准模式没什么区别。

### Internet Explorer 9 Standards
IE9/10使用该模式模拟IE9的标准模式。

### Internet Explorer 9 Almost Standards
IE10使用该模式模拟IE9的近乎标准模式行为。在界面上的开发者工具箱中，这个模式与IE9标准模式没什么区别。

### Internet Explorer 9 XML
IE10使用该模式模拟IE9的XML模式行为。在界面上的开发者工具箱中，这个模式与IE9标准模式没什么区别。

## Google额外提供的一些IE-Specific模式

当安装了Google Chrome Frame后，IE6/7/8/9（不包括Windows8以及2013年2月的Windows7上的IE10）还可提供以下这些额外的模式。

### Chrome Quirks
此模式相当于Google Chrome上的怪癖模式。

### Chrome Standards
此模式相当于Google Chrome上的标准模式。

### Chrome Almost Standards
此模式相当于Google Chrome上的近乎标准模式。

## 模式的影响

除IE外，对于text/html类型的模式主要影响样式表布局以及样式系统。例如，表格内不会继承样式表；在IE和Opera的老版本中的怪癖模式下，盒模型变成了IE5.5的盒模型。在近乎标准模式中（在所有包含此模式的浏览器中），仅含有图片的单元格高度的计算方式与标准模式不同。
在XML模式中，选择器对查询条件的大小写有不相同的处理行为，另外，对于那些没有实现更多的样式表规范的老版本浏览器中，并不包含对body元素的特殊规则。  
另外，不同模式对内容及脚本也可能会有不同的解析方式

## doctype嗅探（也叫doctype转换）

现代浏览器使用doctype嗅探来决定text/html文档的引擎模式。这意味着模式的选择是基于HTML文档开始的文档类型声明（或缺少）。（这不适于使用XML文档类型的文档。）

## 选择Doctype

### text/html
标准模式，最前沿的验证可用<!DOCTYPE html>，标准模式，更稳定的验证目标可用<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">，故意要使用怪癖模式可以直接不用doctype

不推荐任何的XHTML doctype，因为XHTML被用作text/html被认为是有害的。如果你选择使用XHTML doctype，那么请注意XML声明会使IE6触发怪癖模式（IE7不会）。

### application/xhtml+xml
对application/xhtml+xml的简单指南是绝不使用doctype。该方式下的网页不是“严格一致”的XHTML1.0，但这并不重要。

## IE8/9/10相关的问题

IE8/9/10除了使用doctype嗅探外还接受其它方式来转换模式，模式的选择取决于来自几个方面的数据：doctype、meta元素、HTTP头、来自微软的定期下载数据、局域网域、用户所做设置、局域网管理员所做设置、父框架的模式（如果有）和地址栏兼容视图按钮被用户触发。  
如果出现下列情况，IE8/9大体上会像其他浏览器一样使用doctype嗅探，而IE10则会严格地使用doctype进行嗅探：

* 作者没有设置X-UA-Compatible HTTP头
* 作者没有设置X-UA-Compatible meta标签
* 域名没有被微软添加到黑名单中
* 局域网管理员没有把该站点放置到黑名单上
* 用户没有按下兼容视图按钮(或以其他方式添加到某个特定的用户黑名单中) (Metro IE10 没有此按钮, 但是桌面里IE10的兼容视图也会影响到Metro模式下的IE10)
* 该站点不在局域网域中
* 用户没有选择以IE7模式显示所有站点
* 页面没有通过frame嵌入到兼容模式的页面中

## 总结
你应当在你HTML文档（所有以text/html类型处理的内容）的源代码顶部加上<!DOCTYPE html>  
如果你还想确保使用IE8/IE9/IE10的用户不做任何操作就可以让网页以IE7的形式显示，你可以在你的服务器上为所有text/html的响应添加HTTP头"X-UA-Compatible: IE=Edge"，还可以在HTML文档的head内的最上方加上<meta http-equiv="X-UA-Compatible" content="IE=Edge">