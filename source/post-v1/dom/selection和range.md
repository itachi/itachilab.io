title: selection和range
keywords: selection和range
savedir: articles
categories:
  - dom
date: 2015-12-05 12:30:10
updated: 2015-12-05 12:30:10
urlkey: selection-range
---

## 简介

Range 是指HTML文档中任意一段内容，可以只包含文字。也可以包含HTML代码内容，起始点和结束点位置任意，可以是一样的（即空Range），当用户选择了页面上的某一段文字后，可以把这个选择转为 Range。也可以直接手动创建 Range

## 三种range

* __W3C Range__：标准化的 range，不支持IE6/7，基本上其是将 Range 作为包含 DOM 的文档片段
* __Selection__：类似于 W3C Range 对象，也是基于 DOM 树的。能够直接将用户选择任何内容变成完全 W3C Range 对象，该对象不支持IE
* __TextRange__：IE专有，跟上面两个有很大区别，因为其是基于字符串的（事实上，Text Range 包含的字符串是很难一下子跳变成 DOM 节点的）

## 标准 Range 对象

2级DOM中的 `Range` 模块为 DOM 元素提供了 `createRange()` 方法，调用它可创建新的 `Range` 对象，该对象提供了大量的方法和属性来表示和操作文档的连续范围区域

一个 `Range` 对象具有两个边界点，开始点和结束点。每个边界点由一个节点和偏移量指定。该节点通常是 Element 节点、Document 节点或 Text 节点。对于 Element 节点和 Document 节点，偏移量指该节点的子节点。偏移量为 0，说明边界点位于该节点的第一个子节点之前。偏移量为 1，说明边界点位于该节点的第一个子节点之后，第二个子节点之前。但如果边界节点是 Text 节点，偏移量则指的是文本中两个字符之间的位置。

[参考手册](http://www.w3school.com.cn/xmldom/dom_range.asp)

### 为`Range`对象设置范围

#### 方法一：

用 `selectNode()` 或者 `selectNodeContents()` 方法，这两个方法只接收一个 DOM 节点，`selectNode()` 方法选择全部节点，包括它的孩子，而 `selectNodeContents()` 选择的节点只是它的孩子

```html
<p id="p1"><b>Hello</b> World</p>
<script>
var oRange1 = document.createRange();
var oRange2 = document.createRange();
var oP1 = document.getElementById("p1");
oRange1.selectNode(oP1);
oRange2.selectNodeContents(oP1);
</script>
```

oRange1 选中的是 `<p id="p1"><b>Hello</b> World</p>`，oRange2 则是 `<b>Hello</b> World`

#### 方法二：
使用 `setStart()` 和 `setEnd()` 两个方法，这两个方法有两个参数：一个是一个节点(node)引用和一个偏移(offset)。

```js
function useRanges() {
    var oRange1 = document.createRange();
    var oRange2 = document.createRange();
    var oP1 = document.getElementById("p1");
    var iP1Index = -1;
    for (var i = 0; i < oP1.parentNode.childNodes.length; i++) {
        if (oP1.parentNode.childNodes[i] == oP1) {
            iP1Index = i;
            break;
        }
    }
    oRange1.setStart(oP1.parentNode, iP1Index);
    oRange1.setEnd(oP1.parentNode, iP1Index + 1);
    oRange2.setStart(oP1, 0);
    oRange2.setEnd(oP1, oP1.childNodes.length);
}
```

设置复杂的范围时，只能用这种方法，比如设置 `<p id="p1"><b>Hello</b> World</p>` 中的 `llo` 开始到从 `World` 中的 `Wo` 结束，代码如下

```js
var oP1 = document.getElementById("p1");
var oHello = oP1.firstChild.firstChild;
var oWorld = oP1.lastChild;
var oRange = document.createRange();
oRange.setStart(oHello, 2);
oRange.setEnd(oWorld, 3);
```

> 注意：这里用 `oP1.firstChild.firstChild` 来引用起始节点，因为文本 Hello 实际上是容器 p1 的孙子节点  

![image](~@img/selection-range/001.png)


### Range对象和[DocumentFragment](http://www.w3school.com.cn/xmldom/dom_documentfragment.asp)对象

当创建一个 Range 时，会自动产生一个 DocumentFragment，该 Fragment 会自动添加一些元素以保证 Range 的正确性，比如上面的例子，设置的结果是 `llo</b>wo`，很明显是不正确的结构，这时 Fragment 会自动补齐标签，变成 `<b>llo</b> Wo`

### 常用方法

#### deleteContents()
这个方法将删除 Range 选中的部分，比如在上面的例子中执行这个方法，结果会变成 `<p><b>He</b>rld</p>`（依然会自动补齐标签）

#### extractContents()
将选中的 Range 从 DOM 树中移到一个 Fragment 中，并返回此 Fragment

```js
var oRange = document.createRange();
var oFragment = oRange.extractContents();
document.body.appendChild(oFragment);
```

#### cloneContents()
类似于 `extractContents()`，但是不是删除，而是克隆，利用这个方法可以得到选择区域的html内容

```js
var oRange = document.createRange();
var oFragment = oRange.cloneContents();
var testDiv = document.createElement("div");
testDiv.appendChild(oFragment);
var selectHtml = testDiv.innerHTML;
```

#### insertNode()
插入一个节点到 Range 中（从起始点插入）  
在例子2中插入：`<span style="color: red">Inserted text</span>`

```js
var oP1 = document.getElementById("p1");
var oHello = oP1.firstChild.firstChild;
var oWorld = oP1.lastChild;
var oRange = document.createRange();
var oSpan = document.createElement("span");
oSpan.style.color = "red";
oSpan.appendChild(document.createTextNode("Inserted text"));
oRange.setStart(oHello, 2);
oRange.setEnd(oWorld, 3);
oRange.insertNode(oSpan);
```

结果会变成

```html
<p id="p1"><b>He<span style="color: red">Inserted text</span>llo</b> World</p>
```

#### surroundContents()
接收一个 node，将 Range 用该 node 包围起来

```js
var oP1 = document.getElementById("p1");
var oHello = oP1.firstChild.firstChild;
var oWorld = oP1.lastChild;
var oRange = document.createRange();
var oSpan = document.createElement("span");
oSpan.style.backgroundColor = "yellow";
oRange.setStart(oHello, 2);
oRange.setEnd(oWorld, 3);
oRange.surroundContents(oSpan);
```

#### collapse()
只有一个布尔型的参数，默认为 false，表示将 Range 折叠到尾部,o true 时折叠到 Range 边界的首部，另外，可以用 collapsed 属性来得到 true 或者 false 从而判断该 Range 是否已经折叠

```js
var oP1 = document.getElementById("p1");
var oP2 = document.getElementById("p2");
var oRange = document.createRange();
oRange.setStartAfter(oP1);
oRange.setStartBefore(oP2);
alert(oRange.collapsed); //outputs "true"
```

上面的代码输为 true。虽然没有用 collapse 方法，但该 Range 开始位置和结束位置重合了

#### compareBoundaryPoints()
Range边界的比较

#### cloneRange()
返回一个当前 Range 的副本，当然，它也是 Range 对象

```js
var oNewRange = oRange.cloneRange();
```

#### detach()
清除 Range 所占的系统资源，虽然不清除，GC（垃圾收集器）也会将其收集

## selection 对象

代表了当前激活选中区，即高亮文本块，和/或文档中用户可执行某些操作的其它元素，标准浏览器用 `window.getSelection()` 获取，IE下用 `document.selection.createRange()` 获取。IE下的 selection 对象可[参考这里](http://www.hbcms.com/main/dhtml/objects/obj_selection.html)，标准的 selection 对象可[参考这里](https://developer.mozilla.org/en/DOM/Selection) 

### 获取当前选择内容（selection对象）

```js
var userSelection;
if (window.getSelection) { //现代浏览器
    userSelection = window.getSelection();
} else if (document.selection) { //IE浏览器 考虑到Opera，应该放在后面
    userSelection = document.selection.createRange();
}
```

上面的 `userSelection` 是个 `Selection` 对象，而在IE下则是 `Text Range` 对象

### 打印selection的内容

```js
var oBtn = document.getElementById("button");
oBtn.onclick = function() {
    var text;
    if (!(text = userSelection.text)) {
        text = userSelection;
    }
    alert(text);
};
```

Selection 对象包含用户选择的文本内容，从 Microsoft Text Range 对象上获得同样的信息需要使用 userSelection.text

### 从`Selection`对象创建`Range`对象

```js
var userSelection, rangeObject;
if (window.getSelection) {
    //现代浏览器
    userSelection = window.getSelection();
} else if (document.selection) {
    //IE浏览器 考虑到Opera，应该放在后面
    userSelection = document.selection.createRange();
}

//Range对象
rangeObject = userSelection;
if (userSelection.getRangeAt) {
    //现代浏览器
    rangeObject = userSelection.getRangeAt(0);
}
```

通过 Selection 对象的 `getRangeAt()` 方法就可以得到 W3C Range 对象。此方法可以返回给定索引值的 range 对象。通常情况下，在 JavaScript 中第一个 Range 的索引值是0。

## TextRange 对象

IE下的 `Range` 对象，通过对 body, button 或 textarea 元素或带有 type 文本的 input 元素应用 `createTextRange` 方法来获取

[参考手册](http://www.hbcms.com/main/dhtml/objects/obj_textrange.html)

### 常用属性

* `htmlText`：返回一个DocumentFragment
* `text`：返回当前范围包含的文本

### 常用方法

#### createTextRange
创建一个 TextRange 对象

#### move
折叠给定文本范围并将空范围移动给定单位数（可理解为移动光标）。第一个参数是字符串，指定移动的单位，有 character、word、sentence、textedit 可选，第二个参数为数值，指定移动偏移量，默认是1，正数为向后移动，负数为向前移动，如果单位是 textedit 时移动到范围的结束点，则不需要第二个参数。

#### moveStart`和`moveEnd
更改范围的开始位置和结束位置同，用法和 move 一样。默认情况下开始点为容器第一个字符、结束点为最后一个字符

#### select
选择当前文本范围内的文本，显示光标也必须利用它来实现，因为所谓的"光标"可以理解为边界重合的范围

#### collapse
将插入点移动到当前范围的开始或结尾，带一个布尔值的参数，默认是true，移到开始点

#### parentElement
获取给定文本范围的父元素

#### `findText("searchString"[,searchScope,flags])`  
从开始点到结束点浏览文本范围，搜索一个不区分大小写的字符串匹配。如果在范围中发现一个实例，范围的开始点和结束点就放到这个文本中，方法返回 true；否则返回false,开始点和结束点都不动。方法仅搜索显示文本，而任何标记或属性都不会被搜索。
可选参数flag用来设置搜索是否区分大小写，或者是否仅匹配整个单词。参数是整数值，它用按位组合的数学方法计算单个值，这些值能容纳一个或多个设置。匹配整个单词的值为2；匹配大小写的值为4等等，具体查看手册

## 应用举例

### 例子一：移动光标

```js
var txtobj = document.getElementById("txtarea");
var moveto = 3;
var rng = txtobj.createTextRange();
rng.move("character",moveto);
rng.select();
```

### 例子二：选择光标前4个字符

```js
var rng = txtobj.createTextRange();
rng.moveStart("character",-4);
rng.select();
return rng.text;
```

### 例子三：选择除第1个字符和最后1个字符的范围

```js
var rng = txtBox.createTextRange();
rng.moveStart("character",1);
rng.moveEnd("character",-1);
rng.select();
```

### 例子四：搜索文本

```js
var rng = txtBox.createTextRange();
var str = "搜索文本";
var num = 0; 
if(document.selection)
num = document.selection.createRange().text.length;
//每次执行，开始点是跳过选择文本后的新开始点，结束点都为末尾 
rng.moveStart("character",num);
rng.moveEnd("character",txtBox.value.length);
if(rng.findText(str))
rng.select(); //搜索到后选择文本
//搜索到最后的范围还是找不到，则提示搜索完毕，并重新恢复rng最初的范围(否则无法执行新搜索)    
if(rng.text!=str){    
    alert("搜索完毕");
    rng = txtBox.createTextRange();
}
上述条件也可以这样写
if(rng.findText(str)){
    rng.select();
    rng.collapse(false);
} else{ 
    alert("搜索完毕");
    rng = txtBox.createTextRange();
}
```

## 参考文档

* [JavaScript标准Selection操作](http://www.cnblogs.com/rainman/archive/2011/02/27/1966482.html)
* [JS Range HTML文档/文字内容选中、库及应用介绍](http://www.cnblogs.com/solove/archive/2011/11/01/2231309.html)
* [JavaScript Range资源](http://www.cnblogs.com/rainman/archive/2010/08/08/1795173.html)