title: SVG-sprite
keywords: SVG-sprite
savedir: articles
categories:
  - dom
date: 2017-12-26 16:48:05
updated: 2017-12-26 16:48:05
urlkey: svg-sprite
---

## SVG Sprite与symbol元素

一个 svg 标签可以包含多个 symbol 元素，每个 symbol 元素可以放置一个图标的内容

```html
<svg>
    <symbol>
        <!-- 第1个图标代码 -->
    </symbol>
    <symbol>
        <!-- 第2个图标代码 -->
    </symbol>
    <symbol>
        <!-- 第3个图标代码 -->
    </symbol>
</svg>
```

## SVG中的use元素

svg 中元素是可以重复调用的以达到代码复用的功能，具体是使用 use 元素实现的，use 元素可以有自己的坐标，以及支持 transform 变换，甚至可以use其他use元素，可以调用其他SVG文件的元素，只要在一个文档中

```html
<svg>
  <defs>
    <g id="shape">
        <rect x="0" y="0" width="50" height="50" />
        <circle cx="0" cy="0" r="50" />
    </g>
  </defs>

  <use xlink:href="#shape" x="50" y="50" />
  <use xlink:href="#shape" x="200" y="50" />
</svg>

<svg width="500" height="110"><use xlink:href="#shape" x="50" y="50" /></svg>
```

## SVG Sprite

页面引入一个由多个 symbol 组成的 svg，在其它地方使用 use 元素调用其中的 symbol，便是 SVG Sprite

```html
<svg class="size"><use xlink:href="#target" /></svg>
```

## svg合成

[http://icomoon.io/app/](http://icomoon.io/app/) 在线工具提供合成 svg sprite 功能