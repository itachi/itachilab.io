title: IE特有三个Meta标签
keywords: IE特有三个Meta标签
savedir: articles
categories:
  - html
tags:
  - knowledge
date: 2015-12-26 16:38:37
updated: 2015-12-26 16:38:37
urlkey: ie-meta
---

## X-UA-Compatible
IE8加入的标签，用来设置页面渲染模式

```html
<meta http-equiv="X-UA-Compatible" content="IE=7" />
```

表示无论页面是否包含 `<!DOCTYPE>` 指令，都像是使用了 Windows Internet Explorer 7 的标准模式。

```html
<meta http-equiv="X-UA-Compatible" content="edge" />
```

通知 Windows Internet Explorer 以最高级别的可用模式显示内容，这实际上破坏了“锁定”模式。

```html
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
```

EmulateIE7 模式通知 Windows Internet Explorer 使用 `<!DOCTYPE>` 指令确定如何呈现内容。标准模式指令以 Windows Internet Explorer 7 标准模式显示，而 Quirks 模式指令以 IE5 模式显示。与 IE7 模式不同，EmulateIE7 模式遵循 `<!DOCTYPE>` 指令。对于多数网站来说，它是首选的兼容性模式。

## MSSmartTagsPreventParsing

在 Microsoft IE 6 中有一个 Smart tag 开关，如果您包含下面标记，访问这将看不到某些相关连接，这样可以避免访问者流失到竞争对手的网站上去。

```html
<meta name="MSSmartTagsPreventParsing" content="True" />
```

## MSThemeCompatible

是否在IE中关闭 xp 的主题

```html
<meta http-equiv=”MSThemeCompatible” Content=”No”>
```

关闭xp 的蓝色立体按钮系统显示

```html
<meta http-equiv="MSThemeCompatible" content="Yes" />
```

打开xp 的蓝色立体按钮系统显示
