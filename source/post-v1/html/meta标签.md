title: meta标签
keywords: meta标签
savedir: articles
categories:
  - html
tags:
  - knowledge
date: 2015-12-26 16:41:20
updated: 2015-12-26 16:41:20
urlkey: metatag
---

meta标签共有两个属性，它们分别是 http-equiv 属性和 name 属性，各自拥有几个参数，每个参数对应一个 content，通过 content 设置值

## name属性

name 属性主要用于描述网页，内容主要是便于搜索引擎机器人查找信息和分类信息用的。

### Keywords(关键字)

keywords 用来告诉搜索引擎你网页的关键字是什么

```html
<meta name ="keywords" content="science, education">
```

### description(网站内容描述)

description 用来告诉搜索引擎你的网站主要内容

```html
<meta name="description" content="This page is about the meaning of science">
```

### robots(机器人向导)

robots 用来告诉搜索机器人哪些页面需要索引，哪些页面不需要索引。content 的参数有 all、none、index、noindex、follow、nofollow。默认是 all

```html
<meta name="robots" content="none">
```

### author(作者)

标注网页的作者

```html
<meta name="author" content="zsin,zsin@zsin.net">
```

## http-equiv属性

http-equiv 相当于 http 的文件头作用，它可以向浏览器传回一些有用的信息，以帮助正确和精确地显示网页内容

### Expires(期限)

可以用于设定网页的到期时间。一旦网页过期，必须到服务器上重新传输。

```html
<meta http-equiv="expires" content="Fri, 12 Jan 2001 18:18:18 GMT">
```
> 注意：必须使用GMT的时间格式。

### Pragma(cache模式)

禁止浏览器从本地计算机的缓存中访问页面内容。

```html
<meta http-equiv="Pragma" content="no-cache">
```

> 注意：这样设定，访问者将无法脱机浏览。

### Refresh(刷新)

自动刷新并指向新页面。

```html
<meta http-equiv="Refresh" content="2；URL=http://www.zsin.net">
```

> 注意：其中的2是指停留2秒钟后自动刷新到URL网址。

### Set-Cookie(cookie设定)

如果网页过期，那么存盘的cookie将被删除。

```html
<meta http-equiv="Set-Cookie" content="cookievalue=xxx; expires=Friday, 12-Jan-2001 18:18:18 GMT； path=/">
```

> 注意：必须使用GMT的时间格式。

### Window-target(显示窗口的设定)

强制页面在当前窗口以独立页面显示。

```html
<meta http-equiv="Window-target" content="_top">
```

> 注意：用来防止别人在框架里调用自己的页面。

### content-Type(显示字符集的设定)

设定页面使用的字符集。

```html
<meta http-equiv="content-Type" content="text/html; charset=gb2312">
```