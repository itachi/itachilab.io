title: fis
keywords: fis
savedir: articles
categories:
  - automation
tags:
  - fis
date: 2015-11-28 13:23:49
updated: 2015-11-28 13:23:49
urlkey: fis
---

## 安装

```bash
npm install -g fis
```

## 本地服务器

fis的调试服务器依赖于用户本地的 [jre](http://www.java.com/) 和 [php-cgi](http://php.net/downloads.php) 环境，默认端口是8080，如果没有环境，也可以选择 Node 版的本地服务器

* __开启：__ `fis server start`
* __停止：__ `fis server stop`
* __指定端口：__ `fis server start -p [port]`
* __启动 Node 版 fis server：__ `fis server start --type node`
* __目录：__ `fis server open`

## 发布

`fis release` 搭配不同参数，实现不同发布任务

* `-w` : 监听文件
* `-L` : 自动刷新
* `-d` : 部署路径
* `-m` : 加md5戳
* `-l` : 代码检查
* `-t` : 自动化测试
* `-p` : 打包
* `-o` : 压缩
* `-D` : 添加域名

## 部署

支持发布到本地目录或远程机器目录，支持逗号分隔，可以一次性发布到多个地址，搭配 deploy 节点配置可以灵活设置发布的文件

```bash
fis release -d deploy
fis release -d ../output
fis release -d d:/work/output
fis releaes -d remote,qa,rd,output,preview,D:/work/output
```

## 自动化

* __文件监视：__ `fis release --watch || fis release -w`
* __自动刷新：__ `fis release --watch --live || fis release -wL`

## 打包

FIS默认只会进行文件打包，不会对页面中的静态资源引用进行替换，可以通过引入后端静态资源管理来加载打包模块

```js
fis.config.set('pack', {
    '/pkg/lib.js': [
        'js/lib/jquery.js',
        'js/lib/underscore.js',
        'js/lib/backbone.js',
        'js/lib/backbone.localStorage.js',
    ]
})
```

## 拼图

通过 pack 合并的 css 文件会自动 csssprites 处理，没有合并的 css 需要 csssprites，得在 roadmap.path 设置 `useSprite: true`，换言之需要 `-p` 参数时才会执行 csssprites，生成的合并图默认放在 css 同目录下，以 css文 件名加 xyz 命名，比如 index_z.png

```js
fis.config.set('roadmap.path', [{
    reg: /\/static\/.*\.css$/i,
    //配置useSprite表示reg匹配到的css需要进行图片合并
    useSprite: true
}]);
```
需要做合并的图片必须添加 `?__sprite` 识别

```css
background: url(icon.png?__sprite) no-repeat;
```

### background-position

一般情况下不需要设置 `background-position`，当引用的图片已经是合并了几个小图的图片或需要合并的图片向左/右浮动时，也可以设置 `background-position`，另外，需要水平居中的背景图不适合做 csssprites 处理

### background-size

需要图片同倍率缩小或者放大时可以用 `scale < 1` 表示缩小, `scale > 1` 表示放大，当写像素时，必须两个大小同时写才能识别。如 `background-size:10px 15px`，另外，不支持跟 `background-repeat`一起用。设置了 `scale`，则规则不允许写 `background-size`

### background-repeat

支持 `repeat-x`,`repeat-y`，默认是 `no-repeat`，设置了 repeat-x` 的图片会合并成 sprite_x.png，其它对应为 sprite_y.png，sprite_z.png

### 其它设置

开启模板内联 css 处理,默认关闭

```js
htmlUseSprite: true
```

图之间的边距

```js
margin: 10
```
使用矩阵排列方式，默认为线性 `linear`

```js
layout: 'matrix'
```

## 扩展语言 (以Less为例)

1. 安装所需插件 [更多插件](http://fis.baidu.com/docs/advance/plugin-list.html)

```bash
npm install -g fis-parser-less
```

2. 设置扩展名映射

```js
fis.config.merge({
	roadmap : {
	    ext:{
	     less:'css'
	    }
	}
})
// 或
fis.config.set('roadmap.ext.less', 'css');
```

3. 指定编译插件

```js
fis.config.merge({
	modules:{
	    parser:{ 
	      less:['less'],
	    }
	}
})
// 或
fis.config.set('modules.parser.less', 'less');
```

## 三种语言能力

### 资源定位

有效的分离开发路径与部署路径之间的关系，开发过程只需要使用相对路径来定位资源，上线后资源的目录，甚至文件名则通过配置来指定

#### html的资源定位

处理 script、link、style、video、audio、embed 等标签的 src 或 href 属性，原来 url 中的 query 查询信息可以保留

```js
fis.config.merge({
    roadmap : {
        path : [
            {
                //所有image目录下的.png，.gif文件
                reg : /^\/images\/(.*\.(?:png|gif))/i,
                //发布到/static/pic/xxx目录下
                release : '/static/pic/$1',
                //访问url是/oo/static/baidu/xxx
                url : '/oo/static/baidu$&'
            }
        ]
    }
});
```

源码
```html
<img title="百度logo" src="images/logo.gif"/>
```

编译后
```html
<img title="百度logo" src="/static/pic/logo_74e5229.gif"/>
```

#### js的资源定位

如果 js 文件或 html 中的 script 标签内内容需要定位资源，可以用编译函数 `__uri(path)`

源码
```js
var img = __uri('images/logo.gif');
```

编译后
```js
var img = '/images/logo_74e5229.gif';
```

#### css的资源定位

处理 css 文件或 html 的 style 标签内容 中 url() 以及 src 字段

源码
```css
@import url('demo.css');
```

编译后
```css
@import url('/demo_7defa41.css');
```

源码
```css
_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/body-bg.png');
```

编译后
```css
_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/images/body-bg_1b8c3e0.png');
```

### 内容嵌入

提供诸如图片base64嵌入到css、js里，前端模板编译到js文件中，将js、css、html拆分成几个文件最后合并到一起的能力

#### 在html中嵌入资源

嵌入其他文件内容或者 base64 编码，给资源加 `?__inline` 参数来标记

```html
<link rel="import" href="demo.html?__inline">
<img title="百度logo" src="images/logo.gif?__inline"/>
<link rel="stylesheet" type="text/css" href="demo.css?__inline">
<script type="text/javascript" src="demo.js?__inline"></script>
```

#### 在js中嵌入资源

嵌入图片的 base64 编码、嵌入其他js或者前端模板文件的编译内容，使用编译函数 `__inline()` 来实现

```js
__inline('demo.js');
var img = __inline('images/logo.gif');
var css = __inline('a.css');
```

#### 在css中嵌入资源

嵌入其他文件内容或者 base64 编码，给资源加 `?__inline` 参数来标记

```css
@import url('demo.css?__inline');
background: url(images/logo.gif?__inline);
```
> 注意：src="xxx" 被用在ie浏览器支持的 filter 内，该属性不支持 base64 字符串，因此未做处理

### 依赖声明

fis在执行编译的过程中，会扫描这些编译标记，从而建立一张静态资源关系表，它在编译阶段最后会被产出为一份 map.json 文件，这份文件详细记录了项目内的静态资源id、发布后的线上路径、资源类型以及 依赖关系和资源打包等信息。使用fis作为编译工具的项目，可以将这张表提交给后端或者前端框架去运行时根据组件使用情况来 按需加载资源或者资源所在的包，从而提升前端页面运行性能。

## 配置

fis配置系统的接口是 `fis.config.set` 和 `fis.config.merge`，作用一样，比如

```js
fis.config.set('project.charset', 'gbk');
```

等同于

```js
fis.config.merge({
    project : { charset : 'gbk' }
});
```

配置段分为

* __project__：项目配置
* __modules__：配置编译各阶段调用的插件
* __settings__：针对各插件的专门配置
* __roadmap__：设置FIS的编译流程、产出目录结构、资源引用路径等，[详解](http://fis.baidu.com/docs/advance/roadmap.html)
* __pack__：打包配置，fis内置的打包策略与传统的打包概念不同，fis的打包实际上是在建立一个资源表，并将其描述并产出为一份 map.json 文件，用户应该围绕着这份描述文件来设计前后端运行框架，从而实现运行时判断打包输出策略的架构。
* __deploy__：配置发布方式，配合 `--dest` 参数使用

## 内置插件

* [雪碧图 - fis-spriter-csssprites](https://github.com/fex-team/fis-spriter-csssprites#%E4%BD%BF%E7%94%A8)
* [js压缩 - fis-optimizer-uglify-js](https://github.com/fex-team/fis-optimizer-uglify-js)
* [css压缩 - fis-optimizer-clean-css](https://github.com/fex-team/fis-optimizer-clean-css)
* [图片压缩 - fis-optimizer-png-compressor](https://github.com/fex-team/fis-optimizer-png-compressor)
* [包装js - fis-postprocessor-jswrapper](https://github.com/fis-dev/fis-postprocessor-jswrapper)
* [打包 - fis-packager-map](https://github.com/fex-team/fis-packager-map)

## 外置插件

* [自动打包零散资源 - fis-postpackager-simple](https://github.com/hefangshi/fis-postpackager-simple)
* [retina屏的image-set属性支持 - fis-preprocessor-image-set](https://github.com/fouber/fis-preprocessor-image-set)

## 组件系统

```bash
fis install jquery
```

更多详情请参考 [fis-components](https://github.com/fis-components/components)

## 其它问题

[用时间戳代替md5](https://github.com/fex-team/fis/issues/73)