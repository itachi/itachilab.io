title: eslint
keywords: eslint
savedir: articles
categories:
  - quality
date: 2018-09-05 14:16:43
updated: 2018-09-05 14:16:43
urlkey: eslint
---

## 安装

```bash
npm install eslint --save-dev
```

## 运行

```bash
eslint [options] [file|dir|glob]*
```

具体可以通过运行 `eslint -h` 查看所有选项

## 设置配置文件

```bash
eslint --init
```

非全局安装下使用

```bash
./node_modules/.bin/eslint --init
```

会根据提示生成一个 .eslintrc 配置文件，也可以在 package.json 的 `eslintConfig` 字段添加配置，在具体文件中添加特殊注释也可以达到配置的作用

> 如果你在你的主目录（通常 ~/）有一个配置文件，ESLint 在无法找到其他配置文件时会使用它

### parserOptions

设置解析器选项设置想要支持的 JavaScript 语言选项，默认情况下支持 ECMAScript 5 语法，

* ecmaVersion - 使用的 ECMAScript 版本，默认是5，如果要支持es6，不仅要设置为6，还要额外支持新的 ES6 全局变量，使用 `{ "env":{ "es6": true } }`这个设置会同时自动启用 ES6 语法支持
* sourceType - 默认是 script ，改为 module 则支持 ECMAScript 模块
* ecmaFeatures - 这是个对象，表示你想使用的额外的语言特性，包括以下
  * globalReturn 全局作用域下使用 return 语句
  * impliedStrict 启用全局 strict mode
  * jsx 启用 JSX

### parser

ESLint 默认使用Espree作为其解析器，可以通过 parser 配置指定一个不同的解析器，比如 [Babel-ESLint](https://www.npmjs.com/package/babel-eslint)

### env

env 定义了一组预定义的全局变量。可用的环境包括：

* browser - 浏览器环境中的全局变量
* node - Node.js 全局变量和 Node.js 作用域
* commonjs - CommonJS 全局变量和 CommonJS 作用域
* es6 - 启用除了 modules 以外的所有 ECMAScript 6 特性（该选项会自动设置 ecmaVersion 解析器选项为 6）。
* worker - Web Workers 全局变量。
* mocha - 添加所有的 Mocha 测试全局变量。
* jasmine - 添加所有的 Jasmine 版本 1.3 和 2.0 的测试全局变量。
* mongo - MongoDB 全局变量。
* ...

环境可以同时定义多个，还可以使用命令行的 --env 选项来指定环境

```js
{
    "env": {
        "browser": true,
        "node": true
    }
}
```

要在 JavaScript 文件中使用注释来指定环境，格式如下

```js
/* eslint-env node, mocha */
```

### globals

使用 globals 来设置允许使用的全局变量

```js
{
    "globals": {
        "var1": true,
        "var2": false
    }
}
```

要在 JavaScript 文件中用注释指定全局变量，格式如下

```js
/* global var1, var2 */
```

### plugins

通过 plugins 添加第三方插件，注意插件需要npm安装，插件名称可以省略 `eslint-plugin-` 前缀

```js
{
    "plugins": [
        "plugin1",
        "eslint-plugin-plugin2"
    ]
}
```

### rules

`rules` 配置具体规则，一条规则由规则名称和对应的一个数组表示的值，数组第一个值是错误级别，可以使下面的值之一，第二个值开始为具体配置项

* `"off"` or `0` - 关闭规则
* `"warn"` or `1` - 将规则视为一个警告（不会影响退出码）
* `"error"` or `2` - 将规则视为一个错误 (退出码为1)

```js
{
    "rules": {
        "eqeqeq": "off",
        "curly": "error",
        "quotes": ["error", "double"]
    }
}
```

为了在文件注释里配置规则，使用以下格式的注释

```js
/* eslint eqeqeq: "off", curly: "error" */
or
/* eslint eqeqeq: 0, curly: 2 */
```

配置定义在插件中的一个规则的时候，必须使用 插件名/规则ID 的形式。比如

```js
{
    "plugins": [
        "plugin1"
    ],
    "rules": {
        "plugin1/rule1": "error"
    }
}
```

在文件中使用以下格式的块注释来临时禁止规则出现警告

```js
/* eslint-disable */

alert('foo');

/* eslint-enable */
```

### settings

ESLint 支持在配置文件添加共享设置。你可以添加 settings 对象到配置文件，它将提供给每一个将被执行的规则。如果你想添加的自定义规则而且使它们可以访问到相同的信息，这将会很有用，并且很容易配置。

```js
{
    "settings": {
        "sharedData": "Hello"
    }
}
```

### extends

一个配置文件可以被基础配置中的已启用的规则继承

```js
{
    "extends": "eslint:recommended"
}
```

值为 "eslint:recommended" 的 extends 属性启用一系列核心规则，这些规则报告一些常见问题，在 [规则页面](http://eslint.cn/docs/rules/) 中被标记为勾选状态。 这个推荐的子集只能在 ESLint 主要版本进行更新。

> "eslint:all" 启用当前安装的 ESLint 中所有的核心规则。这些规则可以在 ESLint 的任何版本进行更改。

使用一些第三方的配置，比如 `eslint-config-standard`，extends 属性值可以省略包名的前缀 eslint-config-

```js
{
    "extends": "standard"
}
```

一些插件也可以输出一个或多个命名的规则，extends 配置这些规则需要加上 `plugin:` 前缀

```js
{
    "plugins": [
        "react"
    ],
    "extends": [
        "plugin:react/recommended"
    ]
}
```

extends 还可以使用绝对路径或相对路径

```js
{
    "extends": [
        "./node_modules/coding-standard/eslintDefaults.js",
        "./node_modules/coding-standard/.eslintrc-es6",
        "./node_modules/coding-standard/.eslintrc-jsx"
    ],
    "rules": {
        "eqeqeq": "warn"
    }
}
```

## 忽略文件或目录

通过在项目根目录创建一个 `.eslintignore` 文件告诉 ESLint 去忽略特定的文件和目录。 `.eslintignore` 文件是一个纯文本文件，其中的每一行都是一个 glob 模式表明哪些路径应该忽略检测。例如，以下将忽略所有的 JavaScript 文件

```js
**/*.js
```

Globs 匹配使用 [node-ignore](https://github.com/kaelzhang/node-ignore)

> ESLint总是忽略 /node_modules/* 和 /bower_components/* 中的文件

在 package.json 中可以设置 `eslintIgnore` 字段来忽略文件

```js
"eslintIgnore": ["hello.js", "world.js"]
```

## 结果格式

通过 `--format` 指定使用的查看格式，还可以安装第三方格式工具，内建的格式有

* checkstyle
* codeframe
* compact
* html
* jslint-xml
* json
* junit
* stylish
* table
* tap
* unix
* visualstudio

## 使用案例（以vue项目为例）

1. 配置 `.eslintrc.js`，安装需要npm包

```js
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/standard/standard/blob/master/docs/RULES-en.md
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
```

2. 配置 `.eslintignore`

```js
/build/
/config/
/dist/
/*.js
/test/unit/coverage/
/src/assets/
/.vscode/
```

3. 配置一个 `npm script`

```js
"lint": "eslint --ext .js,.vue src test/unit/specs test/e2e/specs"
```

4. 配置 hook

```js
npm install husky@next --save-dev
```

package.json

```js
  "husky": {
    "hooks": {
      "pre-commit": "npm run lint"
    }
  }
```

5. 安装及配置 `eslint-loader`

```js
{
  test: /\.(js|vue)$/,
  loader: 'eslint-loader',
  enforce: 'pre',
  include: [resolve('src'), resolve('test')],
  options: {
    formatter: require('eslint-friendly-formatter'),
    emitWarning: !config.dev.showEslintErrorsInOverlay
  }
}
```

> 作用是构建过程中会对js及vue文件运行eslint并显示结果，不过不会中断构建(可以设置)，除非语法错误

6. vscode安装eslint插件，配置 setting.json

```js
{
  "eslint.autoFixOnSave": true,
  "eslint.run": "onSave",
  "eslint.validate": [
      "javascript",
      "javascriptreact",
      {
        "language": "html",
        "autoFix": true
      }
  ],
  "files.associations": {
    "*.vue": "html"
  }
}
```

> 作用是保存自动fix，并适配vue文件