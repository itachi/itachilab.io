title: Cookie
keywords: Cookie
savedir: articles
categories:
  - storage
tags:
  - summary
date: 2015-09-30 20:34:16
updated: 2015-09-30 20:34:16
urlkey: cookie
---
## cookie 的分类

1. 持久 cookie： 数据保存在磁盘中。
2. 会话 cookie：数据保存在内存中，浏览器关闭后将被清除。定义时没有设置过期时间则为该类型。
3. 第一方 cookie：当前访问站点的 cookie。
4. 第三方 cookie：非当前访问站点的 cookie。假如当前正访问A站点，A站点中有指向B站点的请求，HTTP 请求和响应头也会包含B站点设置的 cookie，相对于A站点即为第三方 cookie。

## cookie 的缺点

1. cookie 会被附加在每个 HTTP 请求中，所以无形中增加了流量。
2. 由于在 HTTP 请求中的 cookie 是明文传递的，所以安全性成问题。（除非用HTTPS）
3. Cookie 的大小限制在 4KB 左右。对于复杂的存储需求来说是不够用的。

## 设置 cookie

cookie 以一个用等号分隔开键和值的字符串保存，设置时赋值给 `document.cookie`，一次设置多个用__分号加空格__隔开，每次赋值不会丢失原来的值，而是添加到已存在的 cookie 字符串，如果要改变一个 cookie 的值，只需重新赋值

> 注意，键或值中不能使用分号、逗号、等号以及空格，必要时可用 `escape()` 函数进行编码，它能将一些特殊符号使用十六进制表示，取值后需要使用 `unescape()` 进行解码

```js
document.cookie="userId=828";
document.cookie="userId=828; userName=hulk";
document.cookie="str="+escape("I love ajax");//相当于document.cookie="str=I%20love%20ajax";
```

## 获取 cookie

cookie 的值由 `document.cookie` 直接获得，返回值为以分号隔开的多个键值对所组成的字符串，这些键值对包括了该域名下的所有 cookie。用户必须自己分析这个字符串，来获取某个键的值

```js
document.cookie="userId=828";
document.cookie="userName=hulk";
var strcookie=document.cookie;
var arrcookie=strcookie.split("; ");
var userId;
for(var i=0; i < arrcookie.length; i++){
	var arr=arrcookie[i].split("=");
	if("userId"==arr[0]){
		userId=arr[1];
		break;
	}
}
alert(userId);
```

## 设置有效期

一般情况下 cookie 都是单会话 cookie，即浏览器关闭后这些 cookie 将会丢失，事实上这些 cookie 仅仅是存储在内存中，而没有建立相应的硬盘文件。如需长期保存，可用 `expires` 指定过期时间

```js
//基本格式
document.cookie="userId=828; expires=GMT_String";//GMT_String是以GMT格式表示的时间字符串

//将date设置为10天以后的时间，最终cookie为十天后过期
var date=new Date();
var expireDays=10;
date.setTime(date.getTime()+expireDays*24*3600*1000);
document.cookie="userId=828; userName=hulk; expire="+date.toGMTString();
```

## 删除 cookie

删除一个 cookie，可以将其过期时间设定为一个已过去的时间，

```js
var date=new Date();
date.setTime(date.getTime()-10000);
document.cookie="userId=828; expire="+date.toGMTString();
```

## 设置路径

默认情况下，如果在某个页面创建了一个 cookie，那么该页面所在目录中的其他页面也可以访问该 cookie。如果这个目录下还有子目录，则在子目录中也可以访问，如果要控制 cookie 可以访问的目录，需要使用 `path` 参数设置 cookie，

```js
document.cookie="userId=320; path=/shop"; //只在shop目录下可用
document.cookie="userId=320; path=/"; //整个网站可用
```

## 设置主机名

主机名是指同一个域下的不同主机，例如：www.google.com 和 gmail.google.com 就是两个不同的主机名。默认情况下，一个主机中创建的 cookie 在另一个主机下是不能被访问的，可以通过 `domain` 参数来实现对其的控制

```js
document.cookie="name=value;domain=.google.com"; //所有google.com下的主机都可以访问该cookie
```

## 设置 Secure

在 Cookie 中标记该变量，表明只有当浏览器和 Web Server 之间的通信协议为加密认证协议（即https）时，浏览器才向服务器提交相应的 Cookie。否则不发送，默认为 false

## cookie加密

Cookie 中的数据以文本的形式存在客户端计算机，考虑它的安全性，最好在将数据存入 Cookie 之前对其进行加密。 加密的方法很多，比较简单一点的有：Base64, md5, sha 等，而相对比较复杂一点的有：DES, TripleDES, RC2, Rijndael 等。

加密的方法很多，使用比较复杂的加密算法，安全性比较高些，但占用服务器资源比较大，会减慢整个网站的访问速度。 所以对 Cookie 加密在考虑三个方面：

1. 安全性
2. Cookie容量
3. 整个网站的性能

## 应用举例

### 判断是否存在某 cookie

```js
if(!!document.cookie.match(/(^|; )cookieName=[^;]+/)){some code;}
```

### 读取cookie

```js
var cookieName=document.cookie.match(/(^|; )cookieName=([^;]+)/)[2];
```