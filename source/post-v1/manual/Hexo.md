title: Hexo
keywords: Hexo
savedir: articles
categories:
  - thirdparty
tags:
  - hexo
date: 2016-01-03 11:58:04
updated: 2016-01-03 11:58:04
urlkey: hexo
---

## 简介

[Hexo](https://hexo.io/) 是基于 nodejs 的静态博客框架，使用 Markdown（或其他渲染引擎）组织文章，支持发布到 git，Heroku 等平台

## 安装

本地需要 [Git](http://git-scm.com/) 和 [Node.js](http://nodejs.org/) 环境

```bash
npm install -g hexo-cli
```

## 初始化

```bash
hexo init <folder>
$ cd <folder>
$ npm install
```

## 目录说明

### _config.yml

网站的配置信息，[各字段说明](https://hexo.io/zh-cn/docs/configuration.html)

### scaffolds

模版文件夹。Hexo 会根据 scaffold 来新建文件

### scripts

脚本文件夹。脚本是扩展 Hexo 最简易的方式，在此文件夹内的 JavaScript 文件会被自动执行

### source

资源文件夹是存放用户资源的地方。除 _posts 文件夹之外，开头命名为 _ (下划线)的文件/文件夹和隐藏的文件将会被忽略。Markdown 和 HTML 文件会被解析并放到 public 文件夹，而其他文件会被拷贝过去

### themes

主题文件夹。Hexo 会根据主题来生成静态页面

### public

项目生成文件夹

## 发布流程

### 新建文件

```basg
hexo new [layout] <title>
```

执行完会在 source 文件夹中生成相应文件，对该文件进行编辑。

> 注意：如果没有设置 `layout` ，默认使用 _config.yml 中的 `default_layout` 参数代替。如果标题包含空格，需要用引号括起来。

Hexo 有三种默认布局：post、page 和 draft，它们分别对应不同的路径，自定义布局和 post 相同，都储存到 _posts 文件夹。

#### 文件名称

默认以标题做为文件名称，也可以编辑 `new_post_name` 参数来改变默认的文件名称，比如设为 `:year-:month-:day-:title.md` 可以通过日期来管理文章

#### 草稿

使用 layout 为 draft 时创建草稿，保存在 `_drafts` 文件夹中，草稿默认不会显示，但可以在执行时加上 `--draft` 参数，或是把 `render_drafts` 参数设为 `true` 来预览草稿。

```bash
hexo new draft <title>
```

通过 `publish` 命令将草稿移动到 `_posts` 文件夹

```bash
hexo publish [layout] <title>
```

#### 模板（Scaffold）

在新建文章时，Hexo 会根据 scaffolds 文件夹内相对应的文件来建立文件，例如：

```bash
hexo new photo "My Gallery"
```
执行这行指令时，Hexo 会尝试在 scaffolds 文件夹中寻找 photo.md，并根据其内容新建文章，模板中使用 `layout` `title` `date` 三个变量，比如

```bash
// 模板
title: {{ title }}
date: {{ date }}
tags:
---

// 生成的文件
title: f3
date: 2015-08-03 17:33:25
tags:
---
```

### 生成文件

```
hexo generate
or
hexo g
```
可带选项 `-d` 表示生成后部署；`-w` 表示监视文件变化

### 本地预览

需要先安装 [hexo-server](https://github.com/hexojs/hexo-server) 本地服务器
```
npm install hexo-server --save
```

启动服务器，访问 `http://localhost:4000` 查看效果，修改文章后刷新可查看效果
```
hexo server
or
hexo s
```

重设端口/IP(默认端口：`4000`，IP：`0.0.0.0`)
```
hexo server -p 5000
hexo server -i 192.168.1.1
```

静态模式：只发布 publish 文件夹内容，不会重新生成再预览
```
hexo server -s
```

启动日记记录
```
hexo server -l
```

使用 [Forever](https://github.com/nodejitsu/forever) 或 [PM2](https://github.com/Unitech/pm2)
```
// 新建 app.js
require('hexo').init({command: 'server'});
```
```
// 需要先安装 Forever 或 pm2
forever start app.js
$ pm2 start app.js -x
```

### 部署（以github 以例）

需要先安装 [hexo-deployer-git](https://github.com/hexojs/hexo-deployer-git)
```
npm install hexo-deployer-git --save
```

`_config.yml` 中添加配置
```
deploy:
  type: git
  repo: <repository url>
  branch: [branch]
  message: [message]
```

正式发布，带选项 `-g` 表示发布前先生成
```
hexo deploy
or
hexo d
```

设置 git 用户名密码

## 其它操作

### 发表草稿

```
hexo publish [layout] <filename>
```

### 渲染文件

```
hexo render <file>
```
可带选项 `-o` 设置输出路径

### 博客迁移

```
hexo migrate <type>
```

### 清除缓存和 public

```
hexo clean
```

### 列出网站资料

```
hexo list <type>
```

### 调试模式

```
hexo --debug
```

## 设置文章信息

每个文章文件最上方以 `---` 分隔的区域用于指定当前文章的信息，支持以下变量
```
layout		布局	
title		标题	
date		建立日期				文件建立日期
updated		更新日期				文件更新日期
comments	开启文章的评论功能		true
tags		标签（不适用于分页）	
categories	分类（不适用于分页）	
permalink	覆盖文章网址
```
以设置分类标签为例
```
categories:
- Diary
tags:
- PS3
- Games
```

也可以使用 JSON	格式
```
"title": "Hello World",
"date": "2013/7/13 20:46:25"
;;;
```

## 标签插件（Tag Plugins）

在文章中可以使用一些标签以达到快速插入某些内容块的目的，比如插入代码
```
{% codeblock %}
alert('Hello World!');
{% endcodeblock %}
```
可用标签列表可查看[官方文档](https://hexo.io/zh-cn/docs/tag-plugins.html)

## 管理资源

source 文件夹除了放文章外，还负责管理资源文件，比如图片、CSS、JS 文件等。Hexo 提供了一种设定：`post_asset_folder`，设置为 true 后，在建立文件时 Hexo 会自动建立一个与文章同名的文件夹，负责放与该文章相关的所有资源，并提供了三个标签以便在文章中引用资源
```
{% asset_path url %}
{% asset_img url [title] %}
{% asset_link url [title] %}
```

## 数据文件

`source/_data` 文件夹用于放置一些通用的数据文件，以便在其它地方重复调用，格式为 YAML 或 JSON 文件，比如
```
// `menu.yml 文件`
Home: /
Gallery: /gallery/
Archives: /archives/

// 模板调用
{% for link in site.data.menu %}
  <a href="{{ link }}">{{ loop.key }}</a>
{% endfor %}
```

## 配置链接

使用 `permalink` 设置链接规则，可以使用以下变量或文章头部定义的信息变量
```
:year		文章的发表年份（4 位数）
:month		文章的发表月份（2 位数）
:i_month	文章的发表月份（去掉开头的零）
:day		文章的发表日期 (2 位数)
:i_day		文章的发表日期（去掉开头的零）
:title		文件名称
:id			文章 ID
:category	分类。如果文章没有分类，则是 default_category 配置信息。
```
比如 `:category/:title` 生成 `foo/bar/hello-world`

## 模板与主题

`themes` 文件夹用于存放主题，每一个子文件夹代表一个主题，通过 `_config.yml` 里的 `theme`` 切换主题

### 主题结构

* ___config.yml__：主题配置文件。修改时会自动更新，无需重启服务器
* __languages__：语言文件夹，用于多语种
* __layout__：存放模板文件，Hexo 根据模板文件的扩展名来决定所使用的模板引擎
* __scripts__：存放插件
* __source__：资源文件夹，文件或文件夹开头名称为 _（下划线线）或隐藏的文件会被忽略掉，其余的会发布到 public 文件夹

### 模板

#### 模板文件

```
模板		用途		回调
index		首页	
post		文章		index
page		分页		index
archive		归档		index
category	分类归档	archive
tag			标签归档	archive
```

#### 布局（Layout）

每个模板都默认使用 layout 布局（`layout.ejs`），可以在文章头部信息指定其他布局，或是设为 false 关闭布局功能

#### 局部模版（Partial）

可使用局部模板功能共享通用模板块，调用时可以传入变量
```
<%- partial('partial/header', {title: 'Hello World'}) %>
```

#### 局部缓存（Fragment Caching）

本功能借鉴于 [Ruby on Rails](http://guides.rubyonrails.org/caching_with_rails.html#fragment-caching)，它储存局部内容，下次便能直接使用缓存内容，可以减少文件夹查询并使生成速度更快。适用于页首、页脚、侧边栏等文件不常变动的位置
```
// 定义
<%- fragment_cache('header', function(){
  return '<header></header>';
});

// 调用
<%- partial('header', {}, {cache: true});
```
> 注意：如果开启了 `relative_link` 参数，请勿使用局部缓存功能，因为相对链接在每个页面可能不同。

### 贡献主题

1. Fork [hexojs/site](https://github.com/hexojs/site)
2. clone 到本地
3. 编辑 `source/_data/themes.yml` 文件，新增主题
4. 在 `source/themes/screenshots` 新增同名的截图，图片必须为 800x500 的 PNG 文件
5. 推送分支。提交合并申请（pull request）

## 插件

Hexo 有强大的插件系统，能轻松扩展功能而不用修改核心模块的源码，[插件列表](https://hexo.io/plugins/)

### 脚本形式（Scripts）

如果代码很简单，可以把 JavaScript 文件放到 `scripts` 文件夹，在启动时就会自动载入

### 包形式（Packages）

插件也可以是 package 包的形式，但文件夹名称开头必须为 `hexo-`，这样 Hexo 才会在启动时载入

### 插件类型

Hexo 共有九种插件，具体查看 [API](https://hexo.io/api/)
```
* Generator
* Renderer
* Helper
* Deployer
* Processor
* Tag
* Console
* Migrator
* Filter
```

### 官方工具插件

* [hexo-fs](https://github.com/hexojs/hexo-fs)：文件 IO
* [hexo-util](https://github.com/hexojs/hexo-util)：工具程式
* [hexo-i18n](https://github.com/hexojs/hexo-i18n)：本地化（i18n）
* [hexo-pagination](https://github.com/hexojs/hexo-pagination)：生成分页资料

### 贡献插件

1. Fork [hexojs/site](https://github.com/hexojs/site)
2. clone 到本地
3. 编辑 `source/_data/plugins.yml` 文件，新增插件
4. 推送分支。提交合并申请（pull request）

## 多语种支持

设置 `new_post_name` 和 `permalink` 参数
```
new_post_name: :lang/:title.md
permalink: :lang/:title/
```
[详细查看文档](https://hexo.io/zh-cn/docs/internationalization.html)

## 备忘

### 描述读取规则

先读 `description` 属性，再读 `<!--more-->` 分隔符，最后读第一个换行前内容

### 文章详情页设置导航

添加属性 `toc: true`

### _config.yml配置

```
permalink: :savedir/:category/:urlkey/
```

### 自定义scaffolds

```
savedir: articles
urlkey: xxx
```

## 博客迁移

[官方文档](https://hexo.io/zh-cn/docs/migration.html)

## vps部署


1. clone项目
2. 安装依赖，运行`hexo g`，生成public目录
3. 配置nginx，访问public目录
4。 新建sh脚本，功能是在项目运行`git pull origin master`，`hexo g`
5. 新建web服务，收到请求后执行上述sh脚本
6. 项目源仓库（比如gitlab）设置webhook，有push更新后发送请求到上述web服务器