title: 制作vscode代码片段
keywords: 制作vscode代码片段
savedir: articles
categories:
  - tool
tags:
  - vscode
date: 2018-11-23 17:19:34
updated: 2018-11-23 17:19:34
urlkey: vscode-snippet
---

一般新建代码片段直接在vscode里直接创建编辑即可，如果代码片段内容过长，编辑比较麻烦（因为body字段是一个数组，需要一行一行添加），可以使用这个方法来制作。[参考文档](https://code.visualstudio.com/docs/extensions/yocode)

## 步骤1

安装 Yeoman 和 VS Code Extension generator

```bash
npm install -g yo generator-code
```

该工具可以生成 vscode 的插件，主题，语言，代码片段快速开发脚手架，这里用到了其中开发代码片段架手架，可以将 TextMate 或 Sublime Text 的代码片段文件转换为 vscode 代码片段包，转换完可以本地添加或发布到市场

## 步骤2 - 制作 Sublime Text 代码片段

新建 .sublime-snippet 文件，格式为

```
<snippet>
  <content><![CDATA[

]]></content>
  <tabTrigger>script-lazyload</tabTrigger>
  <scope>text.html,source.js,source.css</scope>
</snippet>
```

## 步骤3

编辑具体代码内容，可以用 `${1:xxx}` 指定光标及选定内容

## 步骤4 - 转换代码文件

* 来到sublime-snippet文件上层目录，这里以 /snippet/a.sublime-snippet 为例
* 运行 `yo codde` 选择 `New Code Snippets` 选项
* 输入片段文件所在目录名，这里是snippet，
* 输入导出文件夹
* 继续填写相当信息：扩展名，描述等等
* 成功生成一个片段包文件夹

## 步骤5 - 使用

可以整个包复制到 .vscode/extends 下使用，也可以直接复制 snippets/snippets.json 内容，在 vscode 编辑里新建代码片段文件，粘贴内容

> 注意：生成的json内容中，body字段是一个字符串，但vscode的代码片段格式是一个数组，需要修改一下，scope字段也会和sublime稍有不同，也需要修改过来