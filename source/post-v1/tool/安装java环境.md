title: 安装java环境
keywords: 安装java环境
savedir: articles
categories:
  - tool
date: 2015-11-29 23:25:35
updated: 2015-11-29 23:25:35
urlkey: java-environment
---

## window

### 1. 下载JDK1.5并安装

* 下载地址：[国外](http://java.sun.com/javase/downloads/index_jdk5.jsp)
* 下载地址：[国内](http://www.java-cn.com/javatools/tools/jdk/jdk-1_5_0_06-windows-i586-p.exe)

### 2. 测试是否安装成功

cmd 下运行  java，如果出现下面信息，则表示安装成功

![image](~@img/java-environment/img1.png)

### 3. 配置环境变量

* 新建系统变量 `JAVA_HOME`，值为安装目录，比如 `E:\Program Files\Java\jdk1.6.0_18`
* 新建系统变量 `CLASSPATH`，值为 `%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar`
* 编辑环境变量 `Path`，在“变量值”文本框的最前面加入 `%JAVA_HOME%\bin;`

### 4. 测试配置效果

cmd 下运行 javac，如果出现下面信息，则表示配置成功

![image](~@img/java-environment/img2.png)

### 5. 测试

在E盘新建文件 test.java，执行查看结果
```
cmd -> e: -> javac test.java
```

## centos

### 1. 下载tar.gz包，并上传到centos

[下载地址](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

> 注意：如果使用wget下载tar包，解压时会报错，因为在官网下载要同意Oracle的安装协议，用wget的方式，默认是不同意，虽然能下载下来，但是下载下来的文件会有问题，需要在window先下载再上传到服务器

### 2. 解压，设置全局变量

```
cd /usr/local
mkdir java
cd java
tar -zxvf jdk-8u171-linux-x64.tar.gz
mv jdk-8u171-linux-x64 jdk_1.8.0
vi /etc/profile

# java
JAVA_HOME=/usr/local/java/jdk_1.8.0
JRE_HOME=/usr/local/java/jdk_1.8.0/jre
JAVA_CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export JAVA_HOME JRE_HOME JAVA_CLASS_PATH PATH

source /etc/profile
```