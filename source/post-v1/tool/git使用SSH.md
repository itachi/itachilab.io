title: git使用SSH
keywords: git使用SSH
savedir: articles
categories:
  - tool
tags:
  - git
date: 2017-11-13 12:17:21
updated: 2017-11-13 12:17:21
urlkey: git-ssh
---

## 使用 git bash

以下操作在 Git Bash 中进行，具体参考 [Connecting to GitHub with SSH](https://help.github.com/articles/connecting-to-github-with-ssh/)

### 检查是否存在SSH key

```bash
ls -al ~/.ssh
```

如果本机已存在SSH key，在user目录下可以找到.ssh文件夹，里面可以看到 id_rsa.pub 和 id_rsa 密钥文件

### 生成SSH key

```bash
ssh-keygen -t rsa
```

成功后在user/.ssh目录下可以看到生成的密钥文件

### 添加 SSH key 到github，gitlab帐户管理后台

复制 id_rsa.pub，可以打到.ssh/id_rsa.pub复制，也可以使用命令

```bash
clip < ~/.ssh/id_rsa.pub
```

### 使用ssh协议克隆仓库并操作

```bash
git clone git@github.com:xxx/N-blog.git
```

## TortoiseGit

先创建公钥私钥对，公钥导到服务端，打开Puttygen，load生成好的私钥，得到格式化后的私钥，点击保存私钥为xxx.ppk，配置小乌龟，在远端的putty设置为保存的xxx.ppk，或者在克隆时加载putty密钥

![image](~@img/git-ssh/001.png)
![image](~@img/git-ssh/002.png)
![image](~@img/git-ssh/003.png)
![image](~@img/git-ssh/004.png)

> 注意，被Puttygen工具load过的私钥，内容会有变化，这个私钥已经不能用openSSH的方式

## sourcetree

如果是用Puttygen生成的私钥，在option里面的SSH Client需要选择PuTTY/Plink，否则选择OpenSSH

## 使用 SSH key passphrases

如果生成 SSH 时密码不为空，则每次和远程交互都需要输入密码，不杨每次都输入密码，可以生成 SSH 的时候使用空密码或使用 http 协议并记住密码，如果使用 SSH 带密码并不想每次都输入密码，可以参考下面方法

在用户目录下创建 `.profile` 或 `.bashrc` 文件，内容如下

```bash
env=~/.ssh/agent.env

agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }

agent_start () {
    (umask 077; ssh-agent >| "$env")
    . "$env" >| /dev/null ; }

agent_load_env

# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
    agent_start
    ssh-add
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
    ssh-add
fi

unset env
```

这样第一次打开bash或Git shell时会自动开启ssh-agent，第一次输入密码后接下为便可不用输入