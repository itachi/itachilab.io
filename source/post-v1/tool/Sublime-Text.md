title: Sublime Text
keywords: Sublime Text
savedir: articles
categories:
  - tool
tags:
  - summary
date: 2015-11-28 13:32:35
updated: 2015-11-28 13:32:35
urlkey: Sublime
---

## 安装package control（插件管理）

* [官网](http://wbond.net/sublime_packages/package_control/installation)
* 通过命令行安装

```
import urllib2,os; pf='Package Control.sublime-package'; ipp=sublime.installed_packages_path(); os.makedirs(ipp) if not os.path.exists(ipp) else None; urllib2.install_opener(urllib2.build_opener(urllib2.ProxyHandler())); open(os.path.join(ipp,pf),'wb').write(urllib2.urlopen('http://sublime.wbond.net/'+pf.replace(' ','%20')).read()); print('Please restart Sublime Text to finish installation')
```

## 插件列表

* BracketHighlighter
* ConvertToUTF8
* Emmet
* JsFormat
* AndyJS2
* Package Control
* Theme - Soda
* AutoFileName
* SublimeTmpl
* HTMLAttributes

## 命令（Commands）

`Ctrl + Shift + P` 打开指令面板，这个面板指令是读取自所有的 .sublime-commands 文件

格式说明

    { "command" : "clear_bookmarks" , "caption" : "Bookmarks: Clear All" },
    caption：显示在指令面板中的文字
    command：要执行的指令名称
    args：   该指令传入的参数

[指令大全](http://docs.sublimetext.info/en/latest/reference/commands.html)

## 启用vi

模拟模式 Vi 是让开发者能够只用键盘便完成所有的操作的相当经典的编辑器；而 Vim 是改良后的版本，目前仍然被广泛地使用，Sublime Text 透过 Vintage 这个内建的 package，提供了 vi 模拟模式，让你可以使用 vi 的指令模式来操作 Sublime Text。这个 Vintage package 默认是被忽略的，要启用这个模式，选择 `Preferences > Settings - User`，将原本的内容 `"ignored_packages" :  ["Vintage"]` 改为 `"ignored_packages" : []`

一旦这个模式被启用，可以看到 `INSERT MODE` 文字出现在左下角的状态栏里。

Vintage 一开始预设是 insert mode，这时可正常插入字符，按 esc 后进入 command mode，这时可以用 vi 的指令，按 i 可回到 insert mode

Vintage 提供以下这些ctrl按键的快捷键,这些按键会与 Sublime Text 2 原本的一些快捷键冲突,所以默认是关闭，打开方式是：`"vintage_ctrl_keys" : true`

    ctrl + [     Escape
    ctrl + R     复原上一步
    ctrl + Y     往下卷动一行
    ctrl + E     往上卷动一行
    ctrl + F     往下卷动一个页面
    ctrl + B     往上卷动一个页面

## Goto Anything

**ctrl + p** 快速地跳到任何文件，结合以下各种符号，执行不同的操作

* `#`：在文件内执行模糊搜索；
* `@`：搜寻文件内的 symbols（类别名或者是方法名），快捷键是 `Ctrl + R`
* `:`：定位到行数，快捷键是 `Ctrl + G`

## 快捷键

    Ctrl + Shift + P         打开指令面板
    Ctrl + P                 文件跳转
    Ctrl + D                 多重选择
    Ctrl + K D               多重选择中跳过一项
    Ctrl + Shift + up/down   移动整行
    Ctrl + J                 合并多行
    Ctrl + T                 交换文本
    Ctrl + K U               转换文字为大写
    Ctrl + K L               转换文字为小写
    Ctrl + /                 行注释
    Ctrl + Shift + /         块注释
    Ctrl + [                 左缩进
    Ctrl + ]                 右缩进
    Ctrl + L                 选择行
    Ctrl + Alt + up/down     选择多行
    Ctrl + shift + L         转为多行
    Ctrl + shift + D         复制行
    Ctrl + shift + K         删除行
    Ctrl + enter             下一行
    Ctrl + shift + enter     上一行
    Ctrl + shift + G           选中区域增加父级标签
    Ctrl + Alt + Enter        进入emmet的可是化模式
    Ctrl + delete            删到单词末
    Ctrl + back              删到单词首
    Ctrl + K,K               删到行末
    Ctrl + K,backspace       删到行首
    Ctrl + K,A               折叠属性
    Ctrl + K,0               展开所有
    Ctrl + M                 指针到括号末
    Ctrl + shift + M         选择括号内内容
    Ctrl + pageup            上一个文档
    Ctrl + pagedown          下一个文档
    Ctrl + R                 搜索函数
    Ctrl + Tab               文档间切换
    Ctrl + F2                设置书签
    shift + F2               上一个书签
    Ctrl + shift + A         选择标签内内容
    Ctrl + shift + space     选择范围内容（比如类名）
    Alt + F3                 选中所有相同
    Shift + 鼠标右键         选择多列
    Ctrl + W                 关闭当前文件
    Alt + Shift + 数字       多屏视图
    Ctrl + shift + v         粘贴并格式化

## 搜索替换

    Ctrl + F        调出搜索工具
    F3              下一条（或用Enter）
    Shift + F3      上一条（或用Shift + Enter）
    Alt + Enter     选取所有匹配项并关闭搜索工具
    Ctrl + I        调出累加搜寻（Incremental Search）和常规搜索唯一不一样的地方就是按下Enter后搜索面板会关闭

## 宏（Macros）

`Ctrl + Q` 开始记录，再按一次停止，`Ctrl + Shift + Q` 执行宏，要保存宏可用 `Tools > Save Macro `保存到 User/ 目录下

## 目录说明
`Date -> Packages'目录存放所有插件，一个插件对应一个文件夹，可直接把插件复制到这里来安装插件  
`Date -> Packages -> User`目录放用户的自定义配置文件，用以覆盖一些默认配置

## 代码风格
通过`Preferences -> Color Scheme`选择风格，也可以自定义风格文件，比如利用[在线代码配色工具](http://tmtheme-editor.herokuapp.com/)生成`.tmTheme`后缀文件，在 Packages 目录下新建一个目录来保存

## 程序皮肤
通过在线或者`Pagckages`目录添加皮肤文件夹来添加皮肤，在配置文件中设置要使用的皮肤
```
"theme": "Soda Light.sublime-theme"
```

## 代码片段（Snippets）

输入关键字，用 Tab 调出代码块

代码片段保存为 sublime-snippet（XML格式）文件，格式如下

    <snippet>
        <content> <![CDATA[def ${1:method_name}
      $0
    end]]> </content>
        <tabTrigger> def </tabTrigger>
        <scope> source.ruby </scope>
        <description> def … end </description>
    </snippet>

    content           程式码片段的内容；
    tabTrigger        触发自动完成这段程式码的关键字；
    scope             选择哪类的档案会触发这段程式码；
    description       给人类阅读的友善描述；
    $1、$2、$3        按Tab自动移动的位置（Shift + Tab回到上个位置）
    ${1:method_name}  会选取这段文字（占位符的意思）
    $0                是Tab移动的最后一个位置

## 自动补全

输入前几个字符，用 Tab 或 Enter 自动补全代码块，自动补全代码保存为 sublime-completions 文件，格式如下

    {
        "scope" :  "text.html - source - meta.tag, punctuation.definition.tag.begin" ,

        "completions" :
        [
            {  "trigger" :  "a" ,  "contents" :  "<a href=\"$1\">$0</a>"  },
            {  "trigger" :  "abbr" ,  "contents" :  "<abbr>$0</abbr>"  },
            {  "trigger" :  "acronym" ,  "contents" :  "<acronym>$0</acronym>"  }
        ]
    }

    scope        决定哪类档案会使用这份自动补全清单；
    completions  自补补全的的阵列；

## 作用域
```
语言                  作用域
HTML                   text.html
CSS                      source.css
JAVASCRIPT          source.js
JAVA                     source.java
XML                      text.xml
Ruby on Rails         source.ruby.rails
PHP                       source.php
Objective-C           source.objc
Markdown            text.html.markdown
JSP                         text.html.jsp
ActionScript           source.actionscript.2
ActionScript 3         source.actionscript.3
```

## 快捷键定义

快捷键组合设定用 JSON 格式保存在名为 .sublime-keymap 的文件，语法主要是由 keys 跟 command 组成，也可以传参数给命令，还可以传 context 给命令，快捷键会基于插入字符的前后关系或是其他情况而触发，或者是无效

常规

    {  "keys" :  [ "super+shift+n" ],  "command" :  "new_window"  }

带参数

    {  "keys" :  [ "shift+tab" ],  "command" :  "insert" ,  "args" :  { "characters" :  "\t" }  }

带context

    {  "keys" :  [ "escape" ],  "command" :  "clear_fields" ,  "context" :
      [
        {  "key" :  "has_next_field" ,  "operator" :  "equal" ,  "operand" :  true  }
      ]
    }

## 使用经验
### 警告弹窗
删除一些插件后，每次启动会有警告弹窗，可能是删除不完全或还保存对该插件的一些引用，这时可以删了`Data/Settings/Session.sublime_session`文件，再重新启动

### 查看当前文档使用的 `scope`
添加 snippet 时不知道该用哪种 `scope`，可先打开任一该类型的文档，比如 `layout.ejs`，按 `ctrl` + `shift` + `alt` + `p`，在左下角状态栏里会显示