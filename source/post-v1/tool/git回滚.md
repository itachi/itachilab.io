title: git回滚
keywords: git回滚
savedir: articles
categories:
  - tool
tags:
  - git
date: 2017-11-03 12:17:21
updated: 2017-11-03 12:17:21
urlkey: git-rollback
---

## 方法一，删除远程分支再提交

这里以操作dev分支为例

1. 保证当前工作区是干净的，并且和远程分支代码一致

```bash
git pull origin dev
```

2. 恢复到指定的commit

```bash
git reset --hard <commit>
```

3. 删除当前分支的远程分支

```bash
git push origin :dev 
# or
git push origin --delete dev
```

4. 当前分支提交到远程

```bash
git push origin dev
```

## 方法二：强制push远程分支

1. 保证当前工作区是干净的，并且和远程分支代码一致

```bash
git pull origin dev
```

2. 恢复到指定的commit

```bash
git reset --hard <commit>
```

3. 当前分支强制提交到远程

```bash
git push -f origin dev
```

## 方法三：使用revert

1. revert 

```bash
git revert HEAD~3..HEAD
# or
git revert e9d861e e565632 e1eccf9
```

2. 回滚后生成新的commit，再同步到远程

```bash
git push origin dev
```

## 团队操作

但团队有人使用 `git reset` 回滚远程分支后，其它成员使用 `git pull` 后不会覆盖本地分支的代码，`git status` 会提示比远程分支超前了n次提交，这个时候需要手动用远程分支覆盖本地分支

```bash
git reset --hard origin/<branch>
```
> 注意：这个时候不能直接 `git push`，这样会使远程回到回滚前的状态，使得回滚无效

如果回滚影响了其它成员，比如把别人的提交也一起回滚掉了，这个时候该成员需要手动找回自己的提交并还原到远程

```bash
git reflog                       //查看被回滚掉的的commit id,例如aa 和当前HEAD的commit id，例如bb
git checkout my_branch           //先回到自己的分支  
git reset --hard bb              //回到被覆盖的那次提交B1
git checkout -b banch_backup     //拉个分支，用于保存之前因为回退版本被覆盖掉的提交
git checkout my_branch           //拉完分支，迅速回到自己分支
git reset --hard aa              //马上回到自己分支的最前端
git reset --hard origin/master   //本地分支和远程分支保持一致
git checkout master              //切换到master
git merge banch_backup           //再合并一次备份的分支
git push origin master           //再同步到远程
```

git revert 命令的好处就是不会丢掉别人的提交，即使你撤销后覆盖了别人的提交，他更新代码后，可以在本地用 reset 向前回滚，找到自己的代码，然后拉一下分支，再回来合并上去就可以找回被你覆盖的提交了。