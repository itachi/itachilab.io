title: 张学思版TotalCommander快捷键
keywords: 张学思版TotalCommander快捷键
savedir: articles
categories:
  - tool
tags:
  - experience
date: 2016-01-02 18:26:18
updated: 2016-01-02 18:26:18
urlkey: totalcommander-key
---

* 快速目录：ctrl+d
* 历史记录：alt+→/alt+←
* 最近历史：alt+↓
* 上一层：backspace
* 预览：F3 （esc退出）
* 预览窗口：ctrl+q （再次点击关闭）
* 显示所有文件：ctrl+b （再次点击切换回原来）
* 新建文件夹：F7
* 新建文件：shift+F4
* 复制所在路径：ctrl+1
* 复制文件（夹）名：ctrl+2
* 复制文件（夹）路径：ctrl+3
* 缩略图模式：alt+4
* 多列排序：先点击一列，按住ctrl再点另一列
* 显示隐藏文件：ctrl+h
* 选择同类型文件：alt++
* 压缩文件到对面窗口：alt+F5
* 压缩文件到当前窗口：ctrl+alt+F5
* 解压到对面窗口：alt+F9
* 批量命名：ctrl+m
* 刷新：ctrl+r
* 比较文件（默认找对面窗口同名文件）：F10
* 比较同步文件夹：shift+F12
* 选中不同文件：shift+F2