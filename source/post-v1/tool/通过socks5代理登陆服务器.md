title: 通过socks5代理登陆服务器
keywords: 通过socks5代理登陆服务器
savedir: articles
categories:
  - tool
date: 2016-03-29 10:34:17
updated: 2016-03-29 10:34:17
urlkey: socks5-config
---

1. 下载 [proxifier](https://www.proxifier.com/) 并安装，激活码：`MAZDC-M2DMM-FPSWN-LNADE-SM6UN`

2. 获取代理IP，帐户及密码

3. 添加 socks5 代理服务器

![image](~@img/socks5-config/001.png)
![image](~@img/socks5-config/002.png)

4. 添加代理策略，指定哪些通讯走代理通道

![image](~@img/socks5-config/003.png)
![image](~@img/socks5-config/004.png)

> 一般需要通过代理登陆的服务有 ssh、phpMyAdmin、远程桌面等，代理策略可以导出导入

[参考](https://blog.csdn.net/wu_cai_/article/details/80271478)