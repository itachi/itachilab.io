title: Fiddler
keywords: Fiddler
savedir: articles
categories:
  - tool
tags:
  - summary
date: 2015-12-03 23:22:26
updated: 2015-12-03 23:22:26
urlkey: fiddler
---

## 工作原理

Fiddler 以代理web服务器的形式工作，代理地址:127.0.0.1, 端口:8888. 启动 Fiddler 后会自动设置IE代理， 退出后自动注销代理

![image](~@img/fiddler/001.png)
![image](~@img/fiddler/002.png)

能支持HTTP代理的任意程序的数据包都能被 Fiddler 嗅探到，Fiddler 的运行机制其实就是本机上监听8888端口的HTTP代理。 Fiddler 启动的时候默认把IE的代理设为了127.0.0.1:8888，如果其它程序不是默认使用IE的代理的，需要手动设置代理才可以监听数据

> 注意：如果 Fiddler 非正常退出，这时候因为 Fiddler 没有自动注销，可能会造成网页无法访问。解决的办法是重新启动下 Fiddler.

## 捕获HTTPS会话

默认下，Fiddler 不会捕获 HTTPS 会话，需要打开菜单 Tool -> Fiddler Options -> HTTPS tab 完成以下三步设置（后两步选择Yes）

![image](~@img/fiddler/003.png)
![image](~@img/fiddler/004.png)
![image](~@img/fiddler/005.png)

## QuickExec命令行

Fiddler 的左下角有一个命令行工具叫做 QuickExec，允许直接输入命令

![image](~@img/fiddler/006.png)

常见得命令有

* __help__: 打开官方的使用页面介绍，所有的命令都会列出来
* __cls__: 清屏（Ctrl+x 也可以清屏）
* __select__: 选择会话的命令
* __?.png__: 用来选择png后缀的图片
* __bpu__: 截获 request


## 设置Request断点

通过设置断点，可以修改 httpRequest 的任何信息包括 host, cookie 或者表单中的数据

![image](~@img/fiddler/007.png)

设置断点有两种方法

* __第一种__: 菜单 Rules -> Automatic Breakpoint -> Before Requests，这种方法会中断所有的会话，菜单 Rules-> Automatic Breakpoint -> Disabled 用来清除
* __第二种__: 在命令行中输入命令: `bpu www.baidu.com`，这种方法只会中断 www.baidu.com，命令 `bpu` 用来清除中断

### 实例，模拟博客园的登录

1. 打开博客园的登录界面  https://passport.cnblogs.com/login.aspx（该链接是https请求，需要先设置捕获https请求）
2. 打开Fiddler,  在命令行中输入 `bpu http://passport.cnblogs.com/login.aspx`，设置该链接断点，这时再刷新该页面会被 fiddler 中断

![image](~@img/fiddler/008.png)

3. 输入错误的用户名和密码，提交表单，这样同样会被中断，但请求的数据会被 fiddler 捕获
4. Fiddler 已经了中断这次会话，选择被中断的会话，点击 Inspectors tab 下的 WebForms tab ，可以看到发送过去的表单信息

![image](~@img/fiddler/009.png)

5. 在 fiddler WebForms tab下手动修改用户名密码为正确值，然后点击继续运行

![image](~@img/fiddler/010.png)

6. 结果：页面提交了并正确登录

## 设置Response断点

Fiddler 同样能修改 Response 的信息，应用思路和上面一样

![image](~@img/fiddler/011.png)

也有两种方式

* __第一种__: 菜单 Rules -> Automatic Breakpoint -> After Response，这种方法会中断所有的会话，Rules -> Automatic Breakpoint -> Disabled 用于清除中断
* __第二种__: 在命令行中输入命令: `bpafter www.baidu.com`，这种方法只会中断www.baidu.com，命令 `bpafter` 用于清除中断

## 自带编码小工具

点击 Fiddler 工具栏上的 TextWizard, 这个简单的文本编辑器提供编码功能

![image](~@img/fiddler/012.png)

## script系统

fiddler 复杂的脚本系统，可以自己编写脚本去运行

首先先安装 SyntaxView 插件，Inspectors tab -> Get SyntaxView tab -> Download and Install SyntaxView Now

![image](~@img/fiddler/013.png)

安装成功后 Fiddler 就会多了一个 Fiddler Script tab 用来编写脚本

## omposer功能

Fiddler Composer的功能就是用来创建HTTP Request 然后发送。可以自定义一个 Request, 也可以手写一个 Request,还可以在Web会话列表中拖拽一个已有的 Request. 来创建一个新的 HTTP Request，发送后的 Request 会在左边会话列表里出现

![image](~@img/fiddler-composer/001.png)

### Fiddler Composer 比其他工具的优势

能创建发送HTTP Request的工具很多很多。但是Fiddler的功能有如下的优势。

1. 能从"Web会话列表"中 拖拽一个先前捕获到的 Request, 然后稍微修改一下
2. 发送 Request 后，还能设置断点，继续修改 Request.
3. 支持在 Request中上传文件
4. 支持发送多次 Request.

###  Parsed和Raw两种编辑模式

Fiddler Composer 有两种编辑模式

![image](~@img/fiddler-composer/002.png)

* __Parsed模式__: 最常用, 把 Request 分为三个部分，Request line, Request Headesr, Request Body。很容易创建一个 Request
* __Raw模式__: 需要一行一行手动写一个 Request


### 实例: 模拟京东商城的登录

1. 启动 fiddler 监听
2. 打开京东，然后输入用户名和密码，登录。Fiddler 捕获到这个登录的 Request
3. 找出哪个 Request 是用来登录的，然后把它拖拽到 Composer 中
4. 在 Composer 可以看到，登录是使用 POST 方法把用户名和密码发送给服务器。可以修改 Composer 中的 request 内容，比如修改用户名或密码
5. 改好 request 后点击 Execute 按钮发送请求，这样便通过 fiddler 发送了一次京东登录的请求

> 如果按住 Shift 键的同时按 Execute. Fiddler 会自动给这个 Request 下断点

## 手机代理调试

1. Tools-> Fiddler Options->Connections，勾选 "Allow remote computers to connect" 
![image](~@img/Fiddler-remote/001.jpg)

2. Tools-> Fiddler Options->Connections，勾选"Capture HTTPS CONNECTs"，再勾选"Decrypt HTTPS traffic"、"Ignore server certificate errors"
![image](~@img/Fiddler-remote/002.jpg)

3. 手机安装HTTPS证书
假设本机IP为192.168.11.8，手机访问http://192.168.11.8:8888，点"FiddlerRoot certificate" 然后安装证书

> 如果不需要捕获HTTPS请求，可省略2，3步

## 常见问题

### 调试本地网页

默认 Fiddler 是不能嗅探到 localhost 的网站。不过只要在 localhost 后面加个点号，Fiddler 就能嗅探到，比如原本地址是 `http://localhost/Default.aspx`，加个点号后，变成 `http://localhost./Default.aspx` 就可以了

### Response乱码

有时看到 Response 中的HTML是乱码的，这是因为 HTML 被压缩了，可以通过两种方法去解压缩

1. 点击 Response Raw 上方的 "Response is encoded any may need to be decoded before inspection. click here to transform"
2. 选中工具栏中的 Decode，这样会自动解压缩。

![image](~@img/fiddler/014.png)