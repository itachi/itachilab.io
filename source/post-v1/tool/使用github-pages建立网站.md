title: 使用github pages建立网站
keywords: 使用github pages建立网站
savedir: articles
categories:
  - thirdparty
tags:
  - git
date: 2016-01-03 12:02:45
updated: 2016-01-03 12:02:45
urlkey: github-pages
---

使用 [github pages](https://pages.github.com/) 可以为个人或项目设置一个静态网站，用以展示信息。分为 `个人/组织` 和 `项目` 两种类型

## 个人/组织网站

1. 新建一个项目，名字为 `用户名.github.io`
2. 克隆该项目，在 `master` 分支下添加网页文件，提交
3. 通过 `http://用户名.github.io` 可以访问该项目下 `master` 分支。

> 注意：项目名必须为 `用户名.github.io`，文件必须提交在 `master` 分支

## 项目网站

1. 选择一个项目，进入 'Settings' 菜单
2. 找到 `GitHub Pages` 块，选择 `automatic page generator`
3. 设置 `README.md` 内容，选择一款主题
4. 以上操作后会自动在项目下面创建 `gh-pages` 分支，存放了主题的相关文件
5。通过 `http://用户名.github.io/项目名` 即可访问，修改网站内容需要切换到 `gh-pages` 分支下修改

> 注意：也可以不用官司提供的主题，直接手动创建一个 `gh-pages` 分支，在该分支下提交网页文件即可

## 使用第三方工具制作和管理网站

* [Jekyll](http://jekyllrb.com/)
* [Hexo](https://hexo.io/)

## 设置自定义域名

1. 进入网站分支，个人/组织对应 `master` 分支，项目网站对应 `gh-pages` 分支
2. 根目录下创建文本文件，命名为 `CNAME` ,编辑该文件，内容为自定义的域名（不带协议），比如 `xxx.me` 或 `blog.xxx.me`，保存并提交成功后在项目的 `Settings` 里的 `Github Pages` 块可以看到是否生效
3. 设置自定义域名的 DNS，如果是子域名，则配置一个 `CNAME` 记录，如果是顶级域名，则需要配置 `ALIAS`，`ANAME` 或 `A` 纪录

> 注意：如果选择设置 `A` 纪录，则IP设为：192.30.252.153 或 192.30.252.154

## gitlab page

1. fork一个模板项目
2. 修改项目名为username.gitlab.io
3. clone项目，修改，推送，gitlab自动构建
4. 添加自定义域名 settings->pages->New Domain
5. 自定义域名添加DNS记录：A地址->52.167.214.135
6. 二级域名（如果需要）添加CNAME记录：CNAME->username.gitlab.io

参考：[GitLab Pages documentation](https://gitlab.com/help/user/project/pages/index.md)