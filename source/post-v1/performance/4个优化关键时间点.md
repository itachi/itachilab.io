title: 性能优化知识点
keywords: 性能优化知识点
savedir: articles
categories:
  - performance
date: 2017-11-13 16:48:05
updated: 2017-11-13 16:48:05
urlkey: performce-step
---

## 2-5-8原则

当用户访问一个页面：在2秒内得到响应时，会感觉系统响应很快；在2-5秒之间得到响应时，会感觉系统的响应速度还可以；在5-8秒以内得到响应时，会感觉系统的响应速度很慢，但可以接受；而超过8秒后仍然无法得到响应时，用户会感觉系统糟透了

## 4个优化关键时间点

### 开始渲染时间

该时间点表示浏览器开始绘制页面，在此之前页面都是白屏，所以也称为白屏时间。

该时间点可用公式 `Time To Start Render = TTFB（Time To First Byte） + TTDD（Time To Document Download） + TTHE（Time To Head End）` 表示。其中TTFB表示浏览器发起请求到服务器返回第一个字节的时间，TTDD表示从服务器加载HTML文档的时间，TTHE表示文档头部解析完成所需要的时间。在高级浏览器中有对应的属性可以获取该时间点。Chrome可通过 `chrome.loadTimes().firstPaintTime` 获取，IE9+可以通过 `performance.timing.msFirstPaint` 获取，

对于该时间点的优化有：

* 优化服务器响应时间
* 减少html文件大小
* 减少头部资源,脚本尽量放在body中

### DOM Ready

该时间点表示dom解析已经完成，资源还没有加载完成, 这个时候用户与页面的交互已经可用了。用公式 `TimeTo Dom Ready = TTSR(Time To Start Render) + TTDC(Time To Dom Created) + TTST(Time To Script)` 可以表示。TTSR上面已经介绍过了，TTDC表示DOM树创建所耗时间。TTST表示BODY中所有静态脚本加载和执行的时间。在高级浏览器中有 `DOMContentLoaded` 事件对应。

对于该时间点的优化有：

* 减少dom结构的复杂度，节点尽可能少，嵌套不要太深
* 优化关键呈现路径(DOMContentLoaded)

### 首屏时间

该时间点表示用户看到第一屏页面的时间，这个时间点很重要但是很难获取，一般都只能通过模拟获取一个近似时间。一般模拟方法有

* 不断获取屏幕截图，当截图不再变化时，可以视为首屏时间。可参考webPagetest的Speed Index算法；
* 般影响首屏的主要因素是图片的加载，通过页面加载完后判断图片是否在首屏内，找出加载最慢的一张即可视为首屏时间。当然还需考虑其他细节

对于该时间点的优化有：

* 页面首屏的显示尽量不要依赖于js代码，js尽量放到domReady后执行或加载
* 首屏外的图片延迟加载
* 首屏结构尽量简单，首屏外的css可延迟加载

### onload

该时间点是 `window.onload` 事件触发的时间，表示原始文档和所有引用的内容已经加载完成，用户最明显的感觉就是浏览器tab上loading状态结束。

该时间点的优化方式有：

* 减少资源的请求数和文件大小
* 将非初始化脚本放到onLoad之后执行
* 无需同步的脚本异步加载

## DOMContentLoaded

该事件主要是指dom文档加载解析完成，看上去很简单，但是DOMContentLoaded事件的触发与css,js息息相关，现在有专门的名词Critical Rendering Path(关键呈现路径)来描述。

在不支持 `DOMContentLoaded` 事件的浏览器中可以通过模拟获取近似值，主要的模拟方法有：

* 低版本webkit内核浏览器可以通过轮询 `document.readyState` 来实现
* ie中可通过setTimeout不断调用 `documentElement的doScroll` 方法，直到其可用来实现

它的触发时机是：加载完页面，解析完dom树（不包括执行CSS和JS），并设置 `interactive` 和执行每个静态的script标签中的JS，然后触发。
而JS的执行，需要等待位于它前面的CSS加载（如果是外联的话）执行完成，因为JS可能会依赖位于它前面的CSS计算出来的样式。