title: 智能合约
keywords: 智能合约
savedir: articles
categories:
  - blockchain
date: 2018-08-20 23:49:49
updated: 2018-08-20 23:49:49
urlkey: solidity
---

## Solidity源文件

Solidity源文件使用的扩展名为.sol，代码需要声明使用的编译器版本

```
pragma Solidity ^0.4.2;
```

## 结构

合约就像一个类（class），其中包含状态变量（state variable）、函数（function）、函数修改器（function modifier）、事件（event）、结构（structure）和枚举（enum），还支持继承 、多态

