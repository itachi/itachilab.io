title: passport中间件使用
keywords: passport中间件使用
savedir: articles
categories:
  - eggjs
date: 2018-09-17 17:55:11
updated: 2018-09-17 17:55:11
urlkey: passport-middleware
---

## 简介

[Passport](http://www.passportjs.org/) 是一个Node.js认证中间件，为任何类express应用程序提供认证服务，通过使用不同的策略来实现用户名密码、OAuth、OpenID等认证功能

## 认证

通过 `passport.authenticate()` 并指定一种策略名称来实现一种认证中间件，默认情况下如果认证失败，会有一个 `401 Unauthorized` 的响应状态，后面的中间件不会继续执行，如果认证成功，会继续执行后面的中间件，并且 `req.user` 可以获取认证的用户信息

```js
app.post('/login',
  passport.authenticate('local'),
  function(req, res) {
    res.redirect('/users/' + req.user.username);
  });
```

可以配置成功或失败的跳转链接来覆盖默认的响应行为

```js
app.post('/login',
  passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login' }));
```

### 使用Flash信息

如果需要在认证成功或失败后有信息提示，可以使用flash选项配置

```js
passport.authenticate('local', { failureFlash: 'Invalid username or password.' });
passport.authenticate('local', { failureFlash: true });
passport.authenticate('local', { successFlash: 'Welcome!' });
```

> 注意：该功能是基于 `req.flash()` 方法，需要 [connect-flash](https://github.com/jaredhanson/connect-flash) 中间件支持

### 关闭session

默认情况下认证成功后会创建一个登录session，如果应用使用证书等其它方式验证登录，可以关闭session功能

```js
app.get('/api/users/me',
  passport.authenticate('basic', { session: false }),
  function(req, res) {
    res.json({ id: req.user.id, username: req.user.username });
  });
```

### 自定义回调

如果内建的认证处理无法满足自身需求，可以自定义认证回调，这种情况下 `authenticate()` 方法需要在路由中间件中使用，需要调用 `req.login()` 创建session并发送响应

```js
app.get('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/users/' + user.username);
    });
  })(req, res, next);
});
```

## 策略

使用 Passport 认证之前需要先配置一个策略，使用 `use()` 函数加入一个策略，以下代码以配置用户名密码认证为例

```js
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));
```

配置一个策略时需要传入一个验证回调，当验证一个请求的时候，会使用请求中包含的凭证作为参数（在这个例子是用户名和密码），并调用验证回调，如果验证成功，需要使用 `return done(null, user)` 来返回通过认证的用户信息

如果验证不能过，则使用 `false` 来作为参数调用，比如 `return done(null, false)`

另外还可以返回具体的信息表示原因

```js
return done(null, false, { message: 'Incorrect password.' });
```

如果验证过程中发生程序异常，需要传递 `error`，比如 `return done(err)`

## 中间件

在基于 Connect 的应用程序中使用 Passport，需要使用 `passport.initialize()` 中间件初始化 Passport，如果使用了 session，还需要使用 `passport.session()` 中间件

```js
app.use(passport.initialize());
app.use(passport.session());
```

> 注意：在使用 `passport.session()` 之前需要先确保程序已经支持 session，比如 `app.use(express.session({ secret: 'keyboard cat' }))`

## 会话

用户的登录凭证（比如用户名密码）只会在登录请求里传递，一旦登录成功后会建立一个 session，Passport 会将用户信息序列化处理后作为 session 内容，获取用户信息（通过 `req.user` ）会相应反序列化

```js
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});
```

> 注意：调用 `req.login()` 也会触发序列化操作

## 登录

使用 `req.login()` 可以调用登录逻辑，完成后会创建登录 session 并赋值 `req.user`

```js
req.login(user, function(err) {
  if (err) { return next(err); }
  return res.redirect('/users/' + req.user.username);
});
```

> 注意： `passport.authenticate()` 中间件会自动调用该方法，所以该方法一般用于用户注册完后手动登录创建session

## 登出

使用 `req.logout()` 做退出登录操作，会移除 `req.user` 属性并清除登录 session

```js
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});
```

## 判断登录

使用 `req.isAuthenticated()` 方法判断是否登录状态

## OAuth

通过 [passport-oauth](https://github.com/jaredhanson/passport-oauth) 模块提供OAuth支持。先重定向到第三方提供的认证页面，并提供一个回调地址，认证通过后跳转到回调地址并携带一个 code，根据 code 去获取 token，认证函数一般提供了 `accessToken` `profile` 参数，部分还提供 `refreshToken` 方便重新获取 token，`profile` 除了第三方提供的用户信息，还有 passport 提供的一些用户字段

```js
var passport = require('passport')
  , OAuth2Strategy = require('passport-oauth').OAuth2Strategy;

passport.use('provider', new OAuth2Strategy({
    authorizationURL: 'https://www.provider.com/oauth2/authorize',
    tokenURL: 'https://www.provider.com/oauth2/token',
    clientID: '123-456-789',
    clientSecret: 'shhh-its-a-secret'
    callbackURL: 'https://www.example.com/auth/provider/callback'
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOrCreate(..., function(err, user) {
      done(err, user);
    });
  }
));
```

需要定义两个路由，一个用于跳转到认证页面，一个用于接收回调

```js
app.get('/auth/provider', passport.authenticate('provider'));
app.get('/auth/provider/callback',
  passport.authenticate('provider', { successRedirect: '/',
                                      failureRedirect: '/login' }));
```

使用第一个路由作为登录入口

```html
<a href="/auth/provider">Log In with OAuth 2.0 Provider</a>
```

如果第三方服务需要指定范围，则由 `scope` 选项指定

```js
app.get('/auth/provider',
  passport.authenticate('provider', { scope: 'email' })
);
```


## eggjs使用

Egg 在它之上提供了 [egg-passport](https://github.com/eggjs/egg-passport) 插件，实现方法统一封装

### 自动加入初始化中间件

```js
app.config.coreMiddleware.push('passportInitialize');
app.config.coreMiddleware.push('passportSession');
```

### 方法映射

```js
ctx.user ==> req.user
ctx.login ==> req.login
ctx.logout ==> req.logout
ctx.isAuthenticated ==> req.isAuthenticated
```

### 二次封装

暴露 `app.passport` 为封装过的 `Passport` 实例

封装mount方法（魔法糖），方便开发者快速配置路由

```js
mount(strategy, options = {}) {
  options.loginURL = options.loginURL || `/passport/${strategy}`;
  options.callbackURL = options.callbackURL || `/passport/${strategy}/callback`;
  const auth = this.authenticate(strategy, options);
  this.app.get(options.loginURL, auth);
  this.app.get(options.callbackURL, auth);
}
```

通过 `app.passport.authenticate(strategy, options)` 方法生成指定的鉴权中间件

```js
router.post('/login', app.passport.authenticate('local', { successRedirect: '/authCallback' }));
```

通过 `app.passport.verify(async (ctx, user) => {})` 方法定义用户验证逻辑

```js
app.passport.verify(async (ctx, user) => {
  if(user.provider == 'local' && user.username == 'he', && user.password == '123'){
    return user
  } else{
    return null
  }
});
```

使用 `app.passport.doVerify()` 自动调用验证逻辑

```js
app.passport.use(new LocalStrategy({
  passReqToCallback: true,
}, (req, username, password, done) => {
  const user = {
    provider: 'local',
    username,
    password,
  };
  app.passport.doVerify(req, user, done);
}));
```

序列化和反序列化

```js
app.passport.serializeUser(async (ctx, user) => {
  return user
});
app.passport.deserializeUser(async (ctx, user) => {
  return user
});
```