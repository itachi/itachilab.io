title: css动画
keywords: css动画
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-26 16:26:13
updated: 2015-12-26 16:26:13
urlkey: css-animate
---

## Transform

transform 属性实现了一些可用 SVG 实现的同样的功能。可用于内联(inline)元素和块级(block)元素。对元素进行变形，变形规则包括旋转(rotate)、缩放(scale)、扭曲(skew)、移动(translate)、矩阵(matrix)，可以多个变量规则同时使用，用空格隔开

```css
transform: rotate(30deg):
transform: rotate(30deg) translate(100px,20px):
```

translate，scale，skew接收 x，y 两个参数，y 可以省略，scale 如果没有设置 Y 值，则表示 X，Y 两个方向的缩放倍数是一样的，其余两个不设置时默认为 0，值可以为正数也可以是负数，为负数时表示反方向或逆时针。另外还可以对单独一个方向作改变，分别对应 translateX，translateY，translateZ...等属性，部分浏览器还支持3d变形，分别对应 translate3d(x,y,z)，而 rotate 的3d旋转则对应 rotateX，rotateY

```css
transform: translate(100px,20px):
transform: translateX(100px):
transform: scale(2,1.5):
transform: skew(30deg,10deg):
```

元素 transform 时默认基点就是其中心位置，可通过 transform-origin 设置基点，接受 x，y，其中 X 和 Y 的值可以是百分值、em、px，可以是字符参数值 left，center，right，top，bottom，从元素左上角开始算起，比如 `top left` 等价于 `0 0` 或 `0% 0%`，`center top` 等价于 `50% 0`

```css
transform-origin: left top;
```

## Transition

[Transition](http://www.w3.org/TR/css3-transitions/#transition-property-property) 允许 css 的属性值在一定的时间区间内平滑地过渡。并可以通过时间函数控制动画效果，当指定的属性发生变化（通过元素添加类名或hover等方式）时便会执行动画，语法：`transition: property duration animation delay`，property 指定监听的 css 属性，设为 all 表示所有属性，duration 和 delay 单位为 s（秒）或 ms（毫秒），animation 支持 ease(逐渐变慢-默认值)、linear(匀速)、ease-in(加速)、ease-out(减速)、ease-in-out(加速然后减速)、cubic-bezier(自定义时间曲线)，可以设置多个 css 属性的 transition 效果，用逗号隔开

```css
a {
transition: all 0.5s ease-in;
transition: background 0.5s ease-in, color 0.3s ease-out;
}
```

## Animation

使用 Animation 需要先定义 Keyframes，Keyframes 由多个时间点组成，每个时间点对应各自的样式属性，时间点只能用百分比来表示，并且百分符号不能省略，另外，100% 和 0%也可以用 to 和 from 表示，语法如下


```css
@keyframes 'wobble' {
	0% {
		margin-left: 100px;
		background: green;
	}
	40% {
		margin-left: 150px;
		background: orange;
	}
	60% {
		margin-left: 75px;
		background: blue;
	}
	100% {
		margin-left: 100px;
		background: red;
	}
}
```

使用 animation 调用定义好的动画，有 animation 属性的元素会马上执行动画，不需要触发条件，规则如下

```css
animation: name duration timing-function delay iteration-count direction;
```

animation 支持多个动画一起用，用逗号隔开，其中 `animation-iteration-count` 表示动画循环次数，默认值为“1”；`infinite` 为无限次数循环，`animation-direction` 表示动画播放的方向，默认值为 normal，每次循环都是向前播放；另一个值是 alternate，作用是动画播放在第偶数次向前播放，第奇数次向反方向播放。animation-play-state 用来控制元素动画的播放状态，主要有两个值，`running` 和 `paused` 其中 `running` 为默认值（这个属性目前很少内核支持），其它的属性均和 `translate` 一样

```css
.demo1 {
	animation: wobble 0.3s ease-in;
}
```

也可以分开写各个属性，

```css
.demo1 {
	width: 50px;
	height: 50px;
	margin-left: 100px;
	background: blue;
	animation-name:'wobble';
	animation-duration: 10s;
	animation-timing-function: ease-in-out;
	animation-delay: 2s;
	animation-iteration-count: 10;
	animation-direction: alternate;
}
```