title: LESS
keywords: LESS
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-26 16:30:53
updated: 2015-12-26 16:30:53
urlkey: less
---

CSS预处理器定义了一种新的语言，用一种专门的编程语言，为CSS增加了一些编程的特性，将CSS作为目标生成文件，然后开发者就只要使用这种语言进行编码工作。流行的CSS预处理器有 [sass](http://sass-lang.com)、[less](http://lesscss.org)、[stylus](http://learnboost.github.com/stylus)，这里主要介绍 less

## 安装

Less 基于 nodejs 环境，使用 npm 安装

```bash
npm install -g less
```

## 命令行

使用 lessc 调用 Less 编译器来编译 CSS，如果需要压缩 CSS，只需添加 `-x` 选项。如果希望获得更好的压缩效果，还可以通过 `--clean-css` 选项启用 [Clean CSS](https://github.com/GoalSmashers/clean-css) 进行压缩

```bash
lessc styles.less
lessc styles.less > styles.css
```

## 第三方编译工具

* Grunt 下的 [grunt-contrib-less](https://github.com/gruntjs/grunt-contrib-less) 插件
* [koala](http://koala-app.com/)

## 客户端使用

开发阶段时可以在客户端直接使用 less，省去编译的环节

1. 在页面中加入 .less 样式表的链接，并将 rel 属性设置为 "stylesheet/less"

```html
<link rel="stylesheet/less" type="text/css" href="styles.less" />
```

2. 引入 [less.js](https://github.com/less/less.js/archive/master.zip)

```html
<script src="less.js" type="text/javascript"></script>
```

> 注意：样式表要在 less.js 前加载，每个样式表文件单独编译，不共享作用域

3. 在 less.js 前还可以定义全局的 less 对象为 Less.js 设置参数

```js
less = {
	env: "development",
	async: false,
	fileAsync: false,
	poll: 1000,
	functions: {},
	dumpLineNumbers: "comments",
	relativeUrls: false,
	rootpath: ":/a.com/"
};
```

## 语法

### 变量

可以不用在使用前定义，可用作值、属性、字符串

```css
@link-color: #428bca;
.link {
	color: @link-color;
}

@property: color;
.widget {
	@{property}: #0ee;
	background-@{property}: #999;
}

@mySelector: banner;
.@{mySelector} {
	font-weight: bold;
}

@images: "../img";
body {
	background: url("@{images}/white-sand.png");
}
```

### 作用域

变量优先级从当前作用域开始，一层一层往上找

```css
@var: red;
#page {
	@var: white;
	#header {
		color: @var; // white
	}
}
```

### 混合

```css
.a {
	color: red;
}
.mixin-class {
	.a();
	.a;  // 结果一样
}

// 有括号时编译时会过滤掉
.my-other-mixin() {
	background: white;
}
.class {
	.my-other-mixin;
}

// 使用选择器
.my-hover-mixin {
	&:hover {
		border: 1px solid red;
	}
}
button {
  .my-hover-mixin;
}

// 命名空间
#outer {
	.inner {
		color: red;
	}
}
#outer > .inner;
#outer > .inner();  // 用法相同
#outer.inner;  // 用法相同
#outer.inner();  // 用法相同

// 带参数
.border-radius(@radius) {
	-webkit-border-radius: @radius;
	-moz-border-radius: @radius;
	border-radius: @radius;
}
#header {
	.border-radius(4px);
}

// 带默认参数
.border-radius(@radius: 5px) {
	-webkit-border-radius: @radius;
	-moz-border-radius: @radius;
	border-radius: @radius;
}
#header {
	.border-radius;
}
```

### 嵌套

```css
#header {
	color: black;
		.navigation {
		font-size: 12px;
	}
	.logo {
		width: 300px;
	}
}
```

### 运算

任何数字、颜色或者变量都可以参与运算

```css
@base: 5%;
@filler: @base * 2;
@other: @base + @filler;
```

### 函数

Less 内置了多种函数用于转换颜色、处理字符串、算术运算等，[函数手册](http://less.bootcss.com/functions/)

### 导入

可导入 css 文件和 less 文件，除了 css，没有带后缀名或后缀名不是 less 的都以 less 文件处理

```css
@import "this-is-valid.less";
```

## 合并

每个需要合并的属性名后面要添加一个 + 以作标示

```css
.mixin() {
	box-shadow+: inset 0 0 10px #555;
}
.myclass {
	.mixin();
	box-shadow+: 0 0 20px black;
}
```

## & 符号

& 可以引用父选择器

```css
.button {
	&-ok {
		background-image: url("ok.png");
	}
}
.button-ok {
	background-image: url("ok.png");
}
```