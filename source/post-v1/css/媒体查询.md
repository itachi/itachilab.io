title: CSS媒体查询
keywords: CSS媒体查询
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-26 16:28:35
updated: 2015-12-26 16:28:35
urlkey: media-query
---

媒体查询作为 CSS3 标准的一部分，已成为响应式设计的主要实现原理。主流浏览器均支持，IE系列里IE8及以下不支持。[浏览器支持列表](http://caniuse.com/css-mediaqueries)

从 CSS 版本 2 开始，就可以通过媒体类型在 CSS 中获得媒体支持，比如screen 适用于计算机彩色屏幕，print 适用于打印预览模式下查看的内容或者打印机打印的内容

```html
<link rel="stylesheet" type="text/css" href="site.css" media="screen" />
<link rel="stylesheet" type="text/css" href="print.css" media="print" />
```

## 语法

作为 CSS v3 规范的一部分，媒体查询扩展了媒体类型函数，并允许在样式中使用。由一个媒体类型（比如all、screen）和一个或多个检查条件组成。完整可用的媒体类型及查询条件可查看 [W3C官方列表](http://www.w3.org/TR/css3-mediaqueries/)

```css
@media all and (min-width: 800px) { ... }
```

媒体类型是 all 时可以省略，第一个 and 也可以省略

```css
@media (min-width: 800px) { ... }
```

支持 and or not 三种逻辑条件

```css
@media (min-width:800px) and (max-width:1200px) { ... }
@media (min-width:800px) or (orientation:portrait) { ... }
@media (not min-width:800px) { ... }
```

## 使用

常用见的两种使用方式，一种为不同屏幕大小指定完全不同的样式表，另一种方式是在现有样式表中使用媒体查询，定义在其余元素样式表的位置的旁边，具体使用如种方式具体看项目情况而定

有些设备比如iphone不支持 orientation 媒体特性，需要用 width 模拟这些方向

```css
@media (min-width: 321px) { ... } //横屏
@media (max-width: 320px) { ... } //纵屏
@media (orientation: landscape) { ... } //平板横屏
```

## 接合 LESS/SASS

```css
#header {
  width: 400px;
  @media (min-width: 800px) {
    width: 100%;
  }
}
//编译后
#header {
  width: 400px;
}
@media (min-width: 800px) {
  #header {
    width: 100%;
  }
}
```

## 低版本下使用

[Respond.js](https://github.com/scottjehl/Respond/) 是一个极小的增强 Web 浏览器的 JavaScript 库，使得原本不支持 CSS 媒体查询的浏览器能够支持它们。该脚本循环遍历页面上的所有 CSS 引用，并使用媒体查询分析 CSS 规则。然后，该脚本会监控浏览器宽度变化，添加或删除与 CSS 中媒体查询匹配的样式。最终结果是，能够在原本不支持的浏览器上运行媒体查询。该脚本只支持创建响应式设计所需的最小和最大 width 媒体查询。这并不是适用于所有可能 CSS 媒体查询的一个替代。