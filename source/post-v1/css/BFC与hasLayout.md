title: BFC与hasLayout
keywords: BFC与hasLayout
savedir: articles
categories:
  - css
tags:
  - knowledge
date: 2015-12-05 16:49:39
updated: 2015-12-05 16:49:39
urlkey: bfc-hasLayout
---
## Block Formatting Context （块格式化上下文）

W3C CSS 2.1 规范中的一个概念，它决定了元素如何对其内容进行定位，以及与其他元素的关系和相互作用。浮动元素、绝对定位元素、inline-blocks、table-cells、table-captions 以及 'overflow' 值不是 'visible' 的元素，会创建 Block Formatting Context。在创建了 Block Formatting Context 的元素中，其子元素会一个接一个地放置。垂直方向上他们的起点是包含块的顶部，两个相邻的元素之间的垂直距离取决于 'margin' 特性。在 Block Formatting Context 中相邻的块级元素的垂直边距会折叠（collapse）。

## hasLayout

'Layout' 是 IE 的专有概念，它决定了元素如何对其内容进行定位和尺寸计算，与其他元素的关系和相互作用，以及对应用还有使用者的影响。'Layout' 是 IE 浏览器渲染引擎的一个内部组成部分。在 IE 浏览器中，一个元素要么自己对自身的内容进行组织和计算大小， 要么依赖于包含块来计算尺寸和组织内容。为了协调这两种方式的矛盾，渲染引擎采用了 'hasLayout' 属性，属性值可以为 true 或 false。 当一个元素的 'hasLayout' 属性值为 true 时，我们说这个元素有一个布局（layout），或拥有布局。'Layout' 可以被某些 CSS property（特性）不可逆的触发，而某些 HTML 元素本身就具有 layout。'Layout' 在 IE 中可以通过 hasLayout 属性来判断一个元素是否拥有 layout ，如 object.currentStyle.hasLayout 。

默认拥有布局的元素：`<html> <body> <table> <tr> <th> <td> <img> <hr> <input> <button> <select> <textarea> <fieldset> <legend> <iframe> <embed> <object> <applet> <marquee>`

可触发 hasLayout 的 CSS 特性：

* display: inline-block
* height: (除 auto 外任何值)
* width: (除 auto 外任何值)
* float: (left 或 right)
* position: absolute
* writing-mode: tb-rl
* zoom: (除 normal 外任意值)

IE7 还有一些额外的属性(不完全列表)可以触发 hasLayout ：

* min-height: (任意值)
* min-width: (任意值)
* max-height: (除 none 外任意值)
* max-width: (除 none 外任意值)
* overflow: (除 visible 外任意值，仅用于块级元素)
* overflow-x: (除 visible 外任意值，仅用于块级元素)
* overflow-y: (除 visible 外任意值，仅用于块级元素)
* position: fixed

> 注意：IE6 以前的版本（也包括 IE6 及以后所有版本的混杂模式，其实这种混杂模式在渲染方面就相当于 IE 5.5）， 通过设置任何元素的 'width' 或 'height'（非auto）都可以触发 hasLayout ； 但在 IE6 和 IE7 的标准模式中的行内元素上却不行，设置 'display:inline-block' 才可以。

## hasLayout 和 block formatting context 的特点

在触发 hasLayout 的元素和创建了 Block Formatting Context 的元素中，浮动元素参与高度的计算

![image](~@img/bfc-hasLayout/001.png)

与浮动元素相邻的、触发了 hasLayout 的元素或创建了 Block Formatting Context 的元素，都不能与浮动元素相互覆盖

![image](~@img/bfc-hasLayout/002.png)

触发 hasLayout 的元素和创建了 Block Formatting Context 的元素不会与它们的子元素发生外边距折叠

![image](~@img/bfc-hasLayout/003.png)

## 应用举例

### 防止文字环绕

![image](~@img/bfc-hasLayout/004.png)

### 闭合浮动

![image](~@img/bfc-hasLayout/005.png)

### 三栏布局

![image](~@img/bfc-hasLayout/006.png)

## 总结

* 在 IE8(S) 之前的版本中，没有规范中提及的 block formatting context 和 Inline formatting context 概念，必须用 hasLayout 来达到相似的目的。
* 在 IE 中可通过设置 'width'、'height'、'min-width'、'min-height'、'max-width'、'max-height'、'zoom'、'writing-mode' 来触发 hasLayout，而这些特性值的设置不能够使元素创建 block formatting context。
* 在 IE 中很多元素默认就是拥有布局的，如 IPUNT, BUTTON, SELECT, TEXTAREA 等，但是这些元素在标准中会形成 [Inline formatting context](http://www.w3.org/TR/CSS2/visuren.html#inline-formatting)。
* 'table-cell' 和 'table-caption' 既是 hasLayout 的元素，又是可以创建 block formatting context 的元素。

## 兼容性

由于 hasLayout 和 block formatting context 是对一类事物的不同理解，并且他们的启用条件不尽相同，因此如果一个元素设计时，在 IE 早期版本中触发了 hasLayout ，但在其他浏览器中又没有创建 block formatting context，或者相反，一个元素在 IE 早期版本中没有触发 hasLayout ，在其他浏览器中却创建了 block formatting context（如设置了 'overflow:hidden' ），将导致页面布局的重大差异。解决方案是同时启用上述两者以保证各浏览器的兼容，或者相反，两者皆不启用。比如使元素即生成了 block formatting context，又触发了 hasLayout

## 参考
[不能同时在 IE6 IE7 IE8(Q) 中触发 hasLayout 并在其他浏览器中创建 Block Formatting Context 的元素在各浏览器中的表现会有差异](http://w3help.org/zh-cn/causes/RM8002)