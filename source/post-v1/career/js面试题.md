title: js面试题
keywords: js面试题
savedir: articles
categories:
  - js
tags:
  - experience
date: 2017-06-03 17:01:19
updated: 2017-06-03 17:01:19
urlkey: interview-js
---

## 赋值

1. 输出运算结果

```js
(function(){
	var a = b = 3
})()
console.log(b) // 3
console.log(a) // ReferenceError: a is not defined
```

__解答：__`var a = b = 3` 拆解为 `var a = b; b = 3;` b 变成了全局变量

2. 输出运算结果

```js
var z = 1, y = z = typeof y;
console.log(y); // undefined
```

__解答：__js中赋值操作结合律是右至左的，上面代码等价于

```js
var z = 1;
z = typeof y;
var y = z;
console.log(y);
```

## this及作用域

1. 输出运算结果

```js
var myObject = {
    foo: "bar",
    func: function() {
        var self = this;
        console.log(this.foo); // bar
        console.log(self.foo); // bar
        (function() {
            console.log(this.foo); // undefined
            console.log(self.foo); // bar
        }());
    }
};
myObject.func();
```

2. 下面两个函数的返回值是一样的吗？为什么？

```js
function foo1()
{
  return {
      bar: "hello"
  };
}

function foo2()
{
  return
  {
      bar: "hello"
  };
}
```

__解答：__在JS中，如若语句各占独立一行，通常可以省略语句间的分号（;），JS 解析器会根据能否正常编译来决定是否自动填充分号，比如以下代码就不会自动填充分号

```js
var test = 1 + 
2
```

题中第二种情况return被js视为是单独语句，所以自动填充了分号，变成了

```js
function foo2()
{
  return;
  {
      bar: "hello"
  };
}
```

最终返回 undefined

## 数据类型

1. 解释下什么是NaN

NaN 是 Not a Number 的缩写，JavaScript 的一种特殊数值，其类型是 Number，可以通过 isNaN(param) 来判断一个值是否是 NaN

```js
console.log(isNaN(NaN)); //true
console.log(isNaN(23)); //false
console.log(isNaN('ds')); //true
console.log(isNaN('32131sdasd')); //true
console.log(NaN === NaN); //false
console.log(NaN === undefined); //false
console.log(undefined === undefined); //false
console.log(typeof NaN); //number
console.log(Object.prototype.toString.call(NaN)); //[object Number]
```

2. 解释下浮点数计算的正确性问题

```js
console.log(0.1 + 0.2);   //0.30000000000000004
console.log(0.1 + 0.2 == 0.3);  //false
```

JavaScript 中的 number 类型是浮点型，采用IEEE-754 格式的规定，这是一种二进制表示法，这种二进制浮点数表示法并不能精确的表示类似0.1这样的简单的数字，对于 0.1 + 0.2 这样的运算，操作数会先被转成二进制，解决方案可以先升幂再降幂，或用 `toPrecision`,`toFixed`方法

```js
function add(num1, num2){
  let r1, r2, m;
  r1 = (''+num1).split('.')[1].length;
  r2 = (''+num2).split('.')[1].length;

  m = Math.pow(10,Math.max(r1,r2));
  return (num1 * m + num2 * m) / m;
}
console.log(add(0.1,0.2));   //0.3
console.log(add(0.15,0.2256)); //0.3756
```

3. 如何判断整数

可以将 x 转换成10进制，判断和本身是不是相等

```js
function isInteger(x) { 
    return parseInt(x, 10) === x; 
}
```

也可以用 ES6 的 `Number.isInteger`

JavaScript能够准确表示的整数范围在 -2^53 到 2^53 之间（不含两个端点），超过这个范围，无法精确表示这个值。ES6 引入了Number.MAX_SAFE_INTEGER 和 Number.MIN_SAFE_INTEGER这两个常量，用来表示这个范围的上下限，并提供了 Number.isSafeInteger() 来判断整数是否是安全型整数

4. 输出运算结果

```js
var arr1 = "john".split(''); j o h n
var arr2 = arr1.reverse(); n h o j
var arr3 = "jones".split(''); j o n e s
arr2.push(arr3);
console.log("array 1: length=" + arr1.length + " last=" + arr1.slice(-1));
console.log("array 2: length=" + arr2.length + " last=" + arr2.slice(-1));
```

__解答：__引用类型传值、`reverse`改变原数组、`slice`截取数组，不改变原数组

5. 如何快速浅拷贝一个数组

```js
var _arr = arr.concat()
```

6. 输出以下类型转换及数字运算结果

```js
console.log(1 +  "2" + "2"); // 122
console.log(1 +  +"2" + "2"); // 32
console.log(1 +  -"1" + "2"); // 02
console.log(+"1" +  "1" + "2"); // 112
console.log( "A" - "B" + "2"); // NaN2
console.log( "A" - "B" + 2); // NaN
```

__解答：__

多个数字和数字字符串混合运算时，跟操作数的位置有关

```js
console.log(2 + 1 + '3'); / /‘33’
console.log('3' + 2 + 1); //'321'
```

数字字符串之前存在数字中的正负号(+/-)时，会被转换成数字

```js
console.log(typeof '3');   // string
console.log(typeof +'3');  //number
```

同样，可以在数字前添加 ''，将数字转为字符串

```js
console.log(typeof 3);   // number
console.log(typeof (''+3));  //string
```

对于运算结果不能转换成数字的，将返回 NaN

```js
console.log('a' * 'sd');   //NaN
console.log('A' - 'B');  // NaN
```

7. 输出运算结果

```js
var a={},
    b={key:'b'},
    c={key:'c'};

a[b]=123;
a[c]=456;

console.log(a[b]); // 456
```

__解答：__当设置一个对象的属性时，会先对属性名 `stringify`，这里的属性名b和c都是对象（之前先定义），所以会转成 `[object Object]`，等于a[b]和a[c]实际是对同一个键名即`[object Object]`赋值


8. 输出运算结果

```js
var x;
console.log(x); // undefined
console.log(typeof y); // undefined
console.log(z); // ReferenceError: z is not defined
```

__解答：__JavaScript 未声明变量直接使用会抛出异常：var name is not defined，但是，使用typeof undeclared_variable并不会产生异常，会直接返回 undefined

## 用法

1. 给一个 DOM 元素，创建一个能访问该元素所有子元素的函数，并且要将每个子元素传递给指定的回调函数。

```js
function elecb(ele, cb){
  cb(ele)
  var list = ele.children
  for(var i = 0; i < list.length; i++){
    elecb(list[i], cb)
  }
}
```

2. 清空数组的方法

```
arr.length = 0;
arr = [];
arr.splice(0, arr.length);
```

3. 输出运算结果

```js
var y = 1;
if (function f(){}) {
    y += typeof f;
}
console.log(y); // 1undefined
```

__解答：__JavaScript中if语句求值其实使用 `eval` 函数，`eval(function f(){})` 返回 `function f(){}` 也就是 `true`，以上代码等同于

```js
var k = 1;
if (true) {
    eval(function foo(){});
    k += typeof foo;
}
console.log(k);
```

但最终结果是 `1undefined`，因为 `eval` 方法只接受原始字符串作为参数，如果参数不是原始字符串，那么该方法将不作任何改变地返回，注意上面代码和以下代码不同

```js
var k = 1;
if (1) {
    function foo(){};
    k += typeof foo;
}
console.log(k); // 1function
```

4. 声明一个 Date 对象的几种方法

```js
new Date('2017/01/01')
new Date(2017, 0, 1)
new Date(1483200000000)
new Date(Date.parse('2017/01/01'))
```
5. 输出运算结果

```js
Promise.resolve(1)
  .then(2)
  .then(Promise.resolve(3))
  .then(console.log) // 1
```

__解答：__then 或者 .catch 的参数期望是函数，传入非函数则会发生值穿透

6. 输出运算结果

```js
Promise.resolve()
  .then(() => {
    return new Error('error!!!')
  })
  .then((res) => {
    console.log('then: ', res)
  })
  .catch((err) => {
    console.log('catch: ', err)
  })
```

运行结果：then: Error: error!!!

__解答：__ then 或者 .catch 中 return 一个 error 对象并不会抛出错误

需要改成如下其中一种：

```js
return Promise.reject(new Error('error!!!'))
throw new Error('error!!!')
```

## 运算符

1. 输出运算结果

```js
var output = (function(x){
    delete x;
    return x;
})(0);
console.log(output); // 0
```

__解答：__`delete` 操作符是将object的属性删去的操作。但是这里的 x 是并不是对象的属性， delete 操作符无效

2. 输出运算结果

```js
var Employee = {
    company: 'xyz'
}
var emp1 = Object.create(Employee);
delete emp1.company
console.log(emp1.company); // 'xyz'
```

__解答：__delete 只能删除自身的属性，这里的 emp1 通过 prototype 继承了 Employee 的 company。所以 delete 操作符无效

3. 输出运算结果

```js
var trees = ["redwood","bay","cedar","oak","maple"];
delete trees[3];
console.log(trees); // undefined x 1
```
__解答：__使用 delete 操作符删除一个数组中的元素，这个元素的位置就会变成一个占位符。打印出来就是 undefined x 1。
注意如果用 `trees[3] === 'undefined × 1` 返回的是 false。因为它仅仅是一种打印表示，并不是值变为 undefined x 1。另外，delete 操作符并不是影响数组的长度

4. 输出运算结果

```js
var bar = true;
console.log(bar + 0); // 1
console.log(bar + "xyz"); // truexyz 
console.log(bar + true); // 2
console.log(bar + false); // 1
```

__解答：__遵循以下规则

```
Number + Number -> 加法
Boolean + Number -> 加法
Boolean + Boolean -> 加法
Number + String -> 连接
String + Boolean -> 连接
String + String -> 连接
```

5. 输出运算结果

```js
function foo(){ 
  return foo; 
}
console.log(new foo() instanceof foo); // false
```

__解答：__构建函数的返回值，如果无设置，则自动返回生成对象，除非手动返回一个引用类型，否则设置为其它值都会直接返回生成对象，比如上面代码返回的是构建对象本身，所以 instanceof 不成立
