title: JSON对象
keywords: JSON对象
savedir: articles
categories:
  - js
tags:
  - summary
date: 2015-11-28 13:27:26
updated: 2015-11-28 13:27:26
urlkey: json
---

ECMAScript 5对解析 JSON 的行为进行了规范，定义了全局对象 JSON，该对象有两个方法：`stringify()` 和 `parse()`。在最简单的情况下，这两个方法分别用于把 JavaScript 对象序列化为 JSON 字符串和把 JSON 字符串解析为原生 JavaScript

```js
var book = {
	title: "JavaScript高级程序设计",
	authors: [
		"Nicholas C. Zakas",
		"Oricla zen"
	],
	edition: 3,
	year: 2011
};
var jsonBook = JSON.stringify(book);
var objectBook = JSON.parse(jsonBook);
```
jsonBook 的值为

```
{"title":"JavaScript高级程序设计","authors":["Nicholas C. Zakas","Oricla zen"],"edition":3,"year":2011}
```


> 注意：序列化时，所有函数及原型成员会被忽略，值为 undefined 的任何属性也会被跳过，转成 JavaScript 对象时，如果传入字符串不是有效的 JSON，则会抛出错误

## 序列化

`JSON.stringify()` 除了传入要序列化的对象外，还可以接收另外两个参数，这两个参数用于指定不同的方式来序列化。第一个参数是过滤器，可以是一个数组或一个函数；第二个参数缩放选项，表示是否在 JSON 字符串中保留缩进

如果过滤器是数组，那么 `JSON.stringify()` 的结果中只包含数组中列出的属性

```js
var jsonText = JSON.stringify(student,["name","id"]);
```

如果过滤器是函数，则函数只接收两个参数，属性名和属性值。属性名只能是字符串，函数返回的值就是相应键的值，如果函数返回了undefined，那么相应的属性会被忽略

```js
var jsonText = JSON.stringify(student,jsonConvert);
function jsonConvert(key,value){
	switch (key){
		case "name":
		return "Lily";
		case "grade":
		return undefined;
		case "subject":
		return value.join(",");
		default :
		return value;
	}
}
```

> 注意：这里最后一定要提供 default 项，使其他的值都能够正常出现在结果中，不然就会出错

缩放参数用于控制结果中的缩进和空白符，只要传入有效的值，返回结果就会自动包含换行符

如果缩进参数是一个数值，那么它表示的是每个级别缩进的空格数，最大缩进空格数位10，所有大于10的值都会自定转换为10，

```js
var jsonBook = JSON.stringify(book,null,4);
```
返回结果为

```js
{
    "title": "JavaScript高级程序设计",
    "authors": [
        "Nicholas C. Zakas",
        "Oricla zen"
    ],
    "edition": 3,
    "year": 2011
}
```

如果缩进参数是一个字符串而非数值，则这个字符串将在 JSON 字符串中被用作缩进字符而不再使用空格

```js
var jsonBook = JSON.stringify(book,null,"+");
```

返回结果为

```js
{
+"title": "JavaScript高级程序设计",
+"authors": [
++"Nicholas C. Zakas",
++"Oricla zen"
+],
+"edition": 3,
+"year": 2011
}
```

## toJSON()方法

在对象上定义一个 toJSON() 方法，当该对象被序列化时，toJSON方法会执行并把返回值作为结果

```js
var book = {
	title: "JavaScript高级程序设计",
	edition: 3,
	year: 2011,
	toJSON: function(){
		return this.title + "-" + this.year;
	}
};
var jsonBook = JSON.stringify(book);
```

结果为

```js
"JavaScript高级程序设计-2011"
```

如果定义了 `toJSON()` 方法，并传入一个函数过滤器，则 `toJSON()` 方法返回的值会作为 value 再经过过滤器处理

```js
var jsonBook = JSON.stringify(book,function(key,value){
	return value + "hhh";
});
```

结果为

```js
"JavaScript高级程序设计-2011hhh"
```

## 解析

`JSON.parse()` 方法可以接收另一个参数，为一个函数，该函数同样接收两个参数，一个键一个值，如果返回 undefined，则表示从结果中删除相应的键；如果返回其他值，则将该值应用到结果中

```js
var student = {
	name: "Bill",
	birthDate: new Date(1990,8,4)
};
var jsonText = JSON.stringify(student);
var studentObject = JSON.parse(jsonText,function(key,value){
	if(key == "birthDate"){
		return new Date(value);
	}else{
		return value;
	}
});
```

以上案例原 birthDate 属性保存着一个 Date 对象，序列化后转成字符串了，在解析的时候利用第二个参数还原为 Date 对象