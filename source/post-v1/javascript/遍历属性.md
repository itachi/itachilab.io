title: 遍历属性
keywords: 遍历属性
savedir: articles
categories:
  - js
tags:
  - summary
date: 2015-11-28 13:27:26
updated: 2015-11-28 13:27:26
urlkey: loop-property
---

在JavaScript中,遍历一个对象的属性往往没有在其他语言中遍历一个哈希(有些语言称为字典)的键那么简单。这主要有两个方面的原因:一个是,JavaScript中的对象通常都处在某个原型链中,它会从一个或多个的上层原型上继承一些属性.第二个原因是,JavaScript中的属性不光有值，它还有一些除了值以外的其他特性，其中一个影响属性遍历的特性就是[[Enumerable]],如果该值为true，则称这个属性是可枚举的，否则反之。

可以把属性的遍历分为四种情况.

## 遍历可枚举的自身属性

可枚举的意思就是该属性的[[Enumerable]]特性为true，自身属性的意思就是该属性不是从原型链上继承下来的.

```js
(function () {
    var propertys = Object.keys(window);
    alert(propertys.length);         //3
    alert(propertys.join("\n"));     //window,document,InstallTrigger,除了最后一个是火狐私有的属性,原来window对象只有两个可枚举的自身属性.window属性指向window对象自身,一般没什么用.
})()
```

## 遍历所有的自身属性

特性为不可枚举的属性也并不是遍历不到,ES5给我们提供了getOwnPropertyNames方法,可以获取到一个对象的所有自身属性.

```js
(function () {
    var propertys = Object.getOwnPropertyNames(window);
    alert(propertys.length);       //72
    alert(propertys.join("\n"));   //Object,Function,eval等等
})()
```

## 遍历可枚举的自身属性和继承属性

继承属性怎么遍历，最常用的是用 `for in` 遍历

```js
(function () {
    var getEnumPropertyNames = function (obj) {
        var props = [];
        for (prop in obj) {
            props.push(prop);
        }
        return props;
    }
    var propertys = getEnumPropertyNames(window);
    alert(propertys.length);       //185
    alert(propertys.join("\n"));   //addEventListener,onload等等
})()
```

## 遍历所有的自身属性和继承属性

```js
(function () {
    var getAllPropertyNames = function (obj) {
        var props = [];
        do {
            props = props.concat(Object.getOwnPropertyNames(obj));
        } while (obj = Object.getPrototypeOf(obj));
        return props;
    }
    var propertys = getAllPropertyNames(window);
    alert(propertys.length);          //276
    alert(propertys.join("\n"));      //toString等
})()
```