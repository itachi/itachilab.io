title: this取值
keywords: this取值
savedir: articles
categories:
  - js
tags:
  - knowledge
date: 2017-06-03 10:49:35
updated: 2017-06-03 10:49:35
urlkey: this
---

一般情况下this的取值大概有四种情况，

> 注意：函数中this的取值，是在函数真正被调用执行的时候确定的，函数定义的时候确定不了。因为this的取值是执行上下文环境的一部分，每次调用函数，都会产生一个新的执行上下文环境。

## 构造函数

如果函数作为构造函数用，那么其中的this就代表它即将new出来的对象

```js
function Fn(){
	this.name = 'hello'
	this.value = 'world'
	console.log(this)
}
var f = new Fn()
console.log(f.name)
console.log(f.value)
```

其实，不仅仅是构造函数的prototype，即便是在整个原型链中，this代表的也都是当前对象的值。

> 注意：严格来说，所有的函数都可以new一个对象，但有些函数的定义是为了new一个对象，而有些函数则不是，以上情况只限于函数用于构造函数的情景下（即new一个对象）

## 函数作为对象的一个属性

如果函数作为对象的一个属性时，并且作为对象的一个属性被调用时，函数中的this指向该对象

```js
var obj = {
	x: 10,
	fn: function(){
		console.log(this) // Object { x: 10, fn: function}
		console.log(this.x) // 10
	}}
}
obj.fn()
```

但如果函数不作为对象的属性来调用，这种情况下this就不是指向该对象，而是指向全局对象，即window

```js
var obj = {
	x: 10,
	fn: function(){
		console.log(this) // window
		console.log(this.x) // undefined
	}}
}
var _fn = obj.fn
_fn()
```

## 函数用call或者apply调用

当一个函数被call和apply调用时，this的值就取传入的对象

```js
var obj = {
	x: 10
}
var fn = function(){
	console.log(this) // obj
	console.log(this.x) // 10
}
fn.call(obj)
```

## 全局或调用普通函数

在全局环境下，this永远是window（浏览器环境）

```js
console.log(this === window) // true
```

普通函数在调用时，其中的this也都是window

```js
var x = 10
var fn = function(){
	console.log(this) // window
	console.log(this.x) // 10
}
fn()
```

以下情况需要注意，函数f虽然是在obj.fn内部定义的，但是它仍然是一个普通的函数，this仍然指向window。

```
var obj = {
	x: 10,
	fn: function(){
		function f(){
			console.log(this) // window
			console.log(this.x) // undefined
		}
		f()
	}
}
obj.fn()
```