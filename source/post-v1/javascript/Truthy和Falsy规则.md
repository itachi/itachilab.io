title: Truthy和Falsy规则
keywords: Truthy和Falsy规则
savedir: articles
categories:
  - js
tags:
  - knowledge
date: 2015-10-03 17:44:37
updated: 2015-10-03 17:44:37
urlkey: truthy-falsy
---

javasript语言支持布尔类型的数据，可以指定值为 true 或 false 的变量，任何其它值可以转化为布尔类型，转化规则如下：

## 以下值转化为 false,称为 ”Falsy“

```
false
0 (zero)
"" (empty string)
null
undefined
NaN (a special Number value meaning Not-a-Number!)
```

其它任何值都是 true,称为 ”Truthy“，包括 "0","false",空函数，空数组，空对象

## Falsy之间的比较规则

0，false，"" 之间互相相等

```js
console.log(false == 0);             // true 
console.log(false == "");            // true 
console.log(0 == "");                // true  
```

null，undefined 和任何值都不相等，除了自己

```js
console.log(null == false);          // false 
console.log(null == null);           // true 
console.log(undefined == undefined); // true 
console.log(undefined == null);      // true  
```

NaN和任何值都不相等，包括自己

```js
console.log(NaN == null);            // false 
console.log(NaN == NaN);             // false  
```

要注意空数组([])和空对象({})

```js
console.log([] == false) //true
console.log({} == false) //false
console.log(Boolean([])) //true
console.log(Boolean({})) //true
```

 '0' == false 是成立的，因为在比较的过程中会把两者都转换为整数 0，所以对比就变成了 0 == 0
 console.log('0' == false) //true