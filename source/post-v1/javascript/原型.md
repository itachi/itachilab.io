title: 原型
keywords: 原型
savedir: articles
categories:
  - js
tags:
  - knowledge
date: 2017-06-03 15:23:28
updated: 2017-06-03 15:23:28
urlkey: prototype
---

## 数据类型

在JavaScript中，数据类型分为两类，原始类型（也叫值类型）和引用类型

### 原始类型

* boolean：布尔，值为true或false
* number：数字，值为任何整型会浮点数值
* string：字符串，值为由单引号或双引号括出的单个字符或连续字符（JavaScript不区分字符类型）
* null：空类型，其仅有一个值：nulll
* undefined：未定义，其仅有一个值：undefined

原始类型的值是直接保存在变量中，并可以用 typeof 进行检测。但是 typeof 对 null 的检测是返回 object,而不是返回 null

对于字符串、数字或者布尔值，其都有对应的方法，这些方法来自于对应的原始封装类型：String、Number和Boolean。原始封装类型将被自动创建。

```js
var name = "Pomy";
var char = name.charAt(0);
console.log(char);  //"P"
```

本质上等于

```js
var name = "Pomy";
var temp = new String(name);
var char = temp.charAt(0);
temp = null;
console.log(char);  //"P"
```

字符串对象的引用在用完之后立即被销毁，所以不能给字符串添加属性，并且 instanceof 检测对应类型时均返回 false

```js
var name = "Pomy";
name.age = 21;
console.log(name.age);   //undefined
console.log(name instanceof String);  //false
```

### 引用类型

保存为对象，实质是指向内存位置的引用，所以不在变量中保存对象。除了自定义的对象，JavaScript提供了6中内建类型

* Array：数组类型，以数字为索引的一组值的有序列表
* Date：日期和时间类型
* Error：运行期错误类型
* Function：函数类型
* Object：通用对象类型
* RegExp：正则表达式类型

这所以叫引用类型是因为变量保存的不是对象实例，而是一个指向内存中实际对象所在位置的指针（或者说引用），因为 typeof 对所有非函数的引用类型均返回 object，所以需要用 instanceof 来检测引用类型。

## 一切都是对象

undefined, number, string, boolean属于简单的值类型，函数、数组、对象、new Number(10)这些都是对象。他们都是引用类型，一切都是对象其实指的是一切引用类型都是对象，值类型就不是对象

javascript里的对象指的是若干属性的集合，只有属性，没有方法，方法也是一种属性，属性表示为键值对的形式

既然函数也是对象，当然也是可以添加属性的，比如 `$.trim()`

```js
var fn = function () {
    alert(100);
};
fn.a = 10;
fn.b = function () {
    alert(123);
};
fn.c = {
    name: "王福朋",
    year: 1988
};
```

## 属性操作

检测属性是否存在时，有两种方式：in 和 hasOwnProperty，前者会检测原型属性和自有(实例)属性，后者只检测自有(实例)属性

```js
var per1 = {
  age: 0
}
console.log("age" in per1);  //true
console.log(per1.hasOwnProperty("age"));  //true
console.log("toString" in per1);  //true
console.log(per1.hasOwnProperty("toString"));  //false
```

只想判断一个对象属性是不是原型，可以利用如下方法：

```js
function isPrototypeProperty(obj,name){
    return name in obj && !obj.hasOwnProperty(name);
} 
```

枚举对象的可枚举属性，有两种方式：for-in 循环和 Object.keys()，前者依旧会遍历出原型属性，后者只返回自有属性。所有可枚举属性的内部属性[ [Enumerable]] 的值均为 true

## 函数和对象的关系

函数就是对象的一种，这点通过instanceof函数可以判断

```js
var fn = function () { };
console.log(fn instanceof Object);  // true
```

而对象都是通过函数来创建的

```js
function Fn() {
    this.name = '王福朋';
    this.year = 1988;
}
var fn1 = new Fn();
```

平常定义对象或数组时

```js
var obj = { a: 10, b: 20 };
var arr = [5, 'x', true];
```

其实是一种便捷的写法，属于js的语法糖，本属上等下

```js
var obj = new Object();
obj.a = 10;
obj.b = 20;

var arr = new Array();
arr[0] = 5;
arr[1] = 'x';
arr[2] = true;
```

`Object` 和 `Array` 就是函数

## prototype原型

既然函数是对象，因此函数也是属性的集合，可以添加自定义属性。而javascript里函数有一个默认的属性：`prototype`，该属性的值是一个对象，默认的只有一个叫做 `constructor` 的属性，指向这个函数本身，这个 `prototype` 的值，但就是这个函数的原型

在函数的 `prototype` 里添加自定义的属性时，用这个函数 new 出来的对象便可以调用函数 `prototype` 中的属性，因为每个对象都有一个隐藏的属性 `__proto__`，这个属性引用了创建这个对象的函数的 `prototype`

## constructor指向

在重写原型时，需要注意constructor属性的指向问题。

```js
function Hello(name){
    this.name = name;
}
//重写原型
Hello.prototype = {
    sayHi:function(){
        console.log(this.name);
    }
};
var hi = new Hello("Pomy");
console.log(hi instanceof Hello);  //true
console.log(hi.constructor === Hello); //false
console.log(hi.constructor === Object); //true
```

如果constructor指向很重要，则需要在改写原型对象时手动重置其constructor属性

```js
Hello.prototype = {
    constructor:Hello,
    sayHi:function(){
        console.log(this.name);
    }
};
console.log(hi.constructor === Hello); //true
console.log(hi.constructor === Object); //false
```

## 隐式原型

每个对象都有一个 `__proto__` 属性，称为隐式原型，指向创建该对象的函数的 `prototype` ,这个 `__proto__` 是一个隐藏的属性，javascript不希望开发者用到这个属性值，但却是实际存在的，可以直接打印出来看

```js
var obj = {}
console.log(obj.__proto__)
```

### Object.prototype 的 __proto__

普通的对象本质上是被 Object 函数创建的，即 `obj.__proto__=== Object.prototype`，而 `Object.prototype` 也是一个对象，但它是一个特例，因为它的 `__proto__` 指向的是 null

### Function.prototype 的 __proto__

函数也是对象，因此也可以说函数是由函数创建的，这个函数便是 'Function'

```js
function fn(x){
	return x;
}
```

本质上等同于

```js
var fn = new Function('x', 'return x;');
```

所以函数的 `__proto__` 等于 `Function.prototype ，以下等式是成立的

```js
Object.__proto__ === Function.prototype
```

Function 也是一个函数，函数是一种对象，也有 `__proto__` 属性。既然是函数，那么它一定是被 Function 创建。所以 Function 是被自身创建的。它的 `__proto__` 指向了自身的 `Prototype`

## instanceof

Instanceof运算符的第一个变量是一个对象，暂时称为A；第二个变量一般是一个函数，暂时称为B

Instanceof的判断队则是：沿着A的__proto__这条线来找，同时沿着B的prototype这条线来找，如果两条线能找到同一个引用，即同一个对象，那么就返回true。如果找到终点还未重合，则返回false。

```js
function fn(){}
var f = new fn();

console.log(f instanceof fn); // true
console.log(f instanceof Object); // true
console.log(Object instanceof Function); // true
console.log(Function instanceof Object); // true
console.log(Function instanceof Function); // true
```

instanceof 表示的就是一种继承关系，或者原型链的结构

## 继承

访问一个对象的属性时，先在基本属性中查找，如果没有，再沿着 `__proto__` 这条链向上找，这就是原型链。javascript中的继承是通过原型链来体现的

```js
function fn(){}
var f = new fn();
f.a = 1;
fn.prototype.a = 100;
fn.prototype.b = 200;

console.log(f.a); // 1
console.log(f.b); // 200
```

举个例子，每个函数都有call，apply方法，都有length，arguments，caller等属性，是因为函数都是由Function函数创建的，继承了Function.prototype的属性（方法）

```js
console.log(Function.prototype.call);
```

for...in 循环会遍历出原型链上的可枚举属性，使用 `hasOwnProperty` 可以区分基本属性或从原型中找到的属性

```js
function fn(){
	this.c = 0;
}
var f = new fn();
f.a = 1;
fn.prototype.a = 100;
fn.prototype.b = 200;

for(var i in f){
	if(f.hasOwnProperty(i)){
		console.log(i);
	}
}
// a
```

> 注意：这里的 `hasOwnProperty` 方法 f 自身没有，fn.prototype 也没有，沿着 __proto__ 继续找，便是 fn.prototype.__proto__，等于 Object.prototype，因此这个方法是来自 Object.prototype