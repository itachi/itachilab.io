title: centos安装jenkins
keywords: centos安装jenkins
savedir: articles
categories:
  - development
tags:
  - ci
date: 2018-03-13 12:17:21
updated: 2018-03-13 12:17:21
urlkey: centos-jenkins-install
---

## war方式安装

```bash
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
java -jar jenkins.war
```

## yum安装

```bash
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
yum install jenkins
```

## 配置

```bash
vi /etc/sysconfig/jenkins
# 输入
JENKINS_USER="root"
JENKINS_PORT="8080"

vi /etc/init.d/jenkins
# 在 /usr/bin/java 之前添加jdk安装路径，比如：`/usr/local/java/jdk_1.8.0/bin/java`
```

## 相关目录

* Jenkins安装目录: /usr/lib/jenkins
* Jenkins工作目录: /var/lib/jenkins(对应于环境变量 JENKINS_HOME)
* 构建项目源码目录：/var/lib/jenkins/workspace
* 日志默认路径：/var/log/jenkins/jenkins.log

## 启动

```bash
service jenkins start
```
或
```bash
systemctl enable jenkins
```

## 开机启动

```bash
chkconfig jenkins on
```

## 登录及配置

* 地址：http://localhost:8080/
* 初始密码：`cat /var/lib/jenkins/secrets/initialAdminPassword`

## 日志文件体积过大问题

由于dns解析异常等问题，jenkins会不断写日志，很短时间就可以把磁盘写满

查看磁盘占用情况，可用

```bash
df -h
du -sh /*
```

jenkins日志默认路径为 `/var/log/jenkins/jenkins.log`，如果发现该文件多大占满磁盘空间了，先删了日志文件，删除文件后并不会马上释放空间，需要重启/关闭jenkins进程。

### 彻底解决该问题

* 在 `/etc/sysconfig` 目录下新建文件 `jenkins.logging.properties`，写入一行代码 `.level = INFO`
* 编辑文件jenkins配置文件 `/etc/sysconfig/jenkins`，加入一行代码 `JENKINS_JAVA_OPTIONS="-Djava.util.logging.config.file=/etc/sysconfig/jenkins.logging.properties"`
* 重启jenkins