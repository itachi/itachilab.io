title: centos安装redis
keywords: centos安装redis
savedir: articles
categories:
  - storage
tags:
  - redis
  - linux
date: 2016-01-02 18:42:16
updated: 2016-01-02 18:42:16
urlkey: centos-redis-install
---

## 安装及配置服务

```bash
cd /usr/local/src
wget http://download.redis.io/releases/redis-3.0.1.tar.gz
tar -zxvf redis-3.0.1.tar.gz
cd redis-3.0.1
make
cd src
make PREFIX=/usr/local/redis/ install

# 移动配置文件
cd ..
cp redis.conf /usr/local/redis/bin/

# 测试连接
cd /usr/local/redis/bin
./redis-server

# 修改配置文件，改为后台启动
vi /usr/local/redis/bin/redis.conf
# 把daemonize改为yes

# 添加全局变量
vi /etc/profile
export PATH=$PATH:/usr/local/redis/bin

# shell
redis-cli

# 安装服务
cd /usr/local/src/redis-3.0.1/utils
cp redis_init_script /etc/rc.d/init.d/redis
vi /etc/rc.d/init.d/redis

第二行添加 # chkconfig: 2345 80 90，设置EXEC，CLIEXEC，CONF，PIDFILE的值
EXEC=/usr/local/redis/bin/redis-server
CLIEXEC=/usr/local/redis/bin/redis-cli
PIDFILE=/var/run/redis.pid
CONF="/usr/local/redis/bin/redis.conf"

chkconfig --add redis

# 开启
service redis start

# 关闭
service redis stop
```

## bin下面的工具

* `redis-benchmark`：redis性能测试工
* `redis-check-aof`：检查aof日志的工
* `redis-check-dump`：检查rdb日志的工
* `redis-cli`：连接用的客户
* `redis-server`：redis服务进

## Redis的配置

* `daemonize`：如需要在后台运行，把该项的值改为ye
* `pdifile`：把pid文件放在/var/run/redis.pid，可以配置到其他地
* `bind`：指定redis只接收来自该IP的请求，如果不设置，那么将处理所有请求，在生产环节中最好设置该
* `port`：监听端口，默认为637
* `timeout`：设置客户端连接时的超时时间，单位为
* `loglevel`：等级分为4级，debug，revbose，notice和warning。生产环境下一般开启notic
* `logfile`：配置log文件地址，默认使用标准输出，即打印在命令行终端的端口
* `database`：设置数据库的个数，默认使用的数据库是
* `save`：设置redis进行数据库镜像的频
* `rdbcompression`：在进行镜像备份时，是否进行压
* `dbfilename`：镜像备份文件的文件
* `dir`：数据库镜像备份的文件放置的路
* `slaveof`：设置该数据库为其他数据库的从数据
* `masterauth`：当主数据库连接需要密码验证时，在这里设
* `requirepass`：设置客户端连接后进行任何其他指定前需要使用的密
* `maxclients`：限制同时连接的客户端数
* `maxmemory`：设置redis能够使用的最大内
* `appendonly`：开启appendonly模式后，redis会把每一次所接收到的写操作都追加到appendonly.aof文件中，当redis重新启动时，会从该文件恢复出之前的状
* `appendfsync`：设置appendonly.aof文件进行同步的频
* `vm_enabled`：是否开启虚拟内存支
* `vm_swap_file`：设置虚拟内存的交换文件的路
* `vm_max_momery`：设置开启虚拟内存后，redis将使用的最大物理内存的大小，默认为
* `vm_page_size`：设置虚拟内存页的大
* `vm_pages`：设置交换文件的总的page数
* `vm_max_thrrads`：设置vm IO同时使用的线程数

> 注意：虚拟机如果无法连接，有可能是防火墙的问题，执行 `systemctl stop firewalld.service` 关闭防火墙解决

## 防火墙的设置
```bash
#停止firewall
systemctl stop firewalld.service

#禁止firewall开机启动
systemctl disable firewalld.service
```

## 开放6379端口

centos安装好redis后，如果客户端连接不上，可以用telnet测试下端口是否开放（在“打开或关闭Windows功能”中添加“telnet客户端”就可以使用telnet命令了）

```bash
telnet 130.34.5.2 6379
```
如果显示连接失败，则在centos里开放6379端口

```bash
/sbin/iptables -I INPUT -p tcp --dport 6379  -j ACCEPT
/etc/rc.d/init.d/iptables save
```

重启centos（这里使用`/etc/init.d/iptables restart`也可以不重启）

如果提示iptables文件不存在，可以使用 `iptables -P OUTPUT ACCEPT` 随便写一条规则，使用 `service iptables save` 来保存