title: linux常用命令
keywords: linux常用命令
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-27 14:30:22
updated: 2018-03-27 14:30:22
urlkey: linux-cmd
---

## 软链接

```bash
ln -s /web/redis/3.0.1/bin/redis-cli /usr/local/bin/redis-cli
```

## 设置可执行权限

```bash
chmod +x /etc/rc.d/rc.local
```

## 添加全局变量

```bash
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
source /etc/bashrc
```

## 防火墙

### 停止firewall

```bash
systemctl stop firewalld.service
```

### 禁止firewall开机启动

```bash
systemctl disable firewalld.service
```

## 查看进程

```bash
ps -ef | grep nginx
```

## 拷贝

```bash
cp -a old new
```

## 查看端口状态

```bash
/etc/init.d/iptables status
netstat -lnp | grep 80
lsof -i :80
nmap 127.0.0.1
tcping 139.199.227.231 8004
```

## 清空文本内容

```bash
> log.txt
```

## 上传并覆盖文件

```bash
rz -y
```

## 查看日志

```bash
tail -100f access.log 
tail -f access.log
tail -f access.log | grep xxx
```

## 打包

```bash
cd public
tar -zcvf public.tar.gz *
```

## 查看进程启动路径

```bash
ps eho command -p 23374
```

## 查看进程网络状态

```bash
netstat -pan | grep 23374
```

## 查看分区情况

```bash
df -h
```

## 查看各目录的占用情况

```bash
du -sh /*
```

## 查看inode区域占用情况

```bash
df -ih
```

## 统计字数

```bash
wc -w acc.log
```

## 统计行数

```bash
wc -l acc.log
```