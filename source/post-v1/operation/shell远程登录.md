title: shell远程登录
keywords: shell远程登录
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-04-02 11:45:14
updated: 2018-04-02 11:45:14
urlkey: ssh-connect
---

## SSH简介

Secure Shell（缩写为SSH），由IETF的网络工作小组（Network Working Group）所制定；SSH为一项创建在应用层和传输层基础上的安全协议，为计算机上的Shell（壳层）提供安全的传输和使用环境

### 优点

1. SSH是目前较可靠，专为远程登录会话和其他网络服务提供安全性的协议。
2. 利用SSH协议可以有效防止远程管理过程中的信息泄露问题。
3. 通过SSH可以对所有传输的数据进行加密，也能够防止DNS欺骗和IP欺骗。
4. 其传输的数据可以是经过压缩的，所以可以加快传输的速度。

### 安全验证

在客户端来看，SSH提供两种级别的安全验证。

* 基于密码的安全验证，知道帐号和密码，就可以登录到远程主机，并且所有传输的数据都会被加密。但是，可能会有别的服务器在冒充真正的服务器，无法避免被“中间人”攻击
* 基于密匙的安全验证，需要依靠密匙，也就是你必须为自己创建一对密匙，并把公有密匙放在需要访问的服务器上。客户端软件会向服务器发出请求，请求用你的密匙进行安全验证。服务器收到请求之后，先在你在该服务器的用户根目录下寻找你的公有密匙，然后把它和你发送过来的公有密匙进行比较。如果两个密匙一致，服务器就用公有密匙加密“质询”（challenge）并把它发送给客户端软件。从而避免被“中间人”攻击

SSH是一种网络协议，用于计算机之间的加密登录，存在多种实现，既有商业实现，也有开源实现。这里指 OpenSSH ，它是自由软件，应用非常广泛，是 Linux 系统的标准配置。 

> 这里只讨论 SSH 在 Linux Shell 中的用法。如果要在 Windows 系统中使用 SSH，需要使用 PuTTY 等其它软件

## 远程登录

远程登录需要指定用户名和主机host，如果本地用户名与远程用户名一致，登录时可以省略用户名，默认端口是22，也可以使用p参数修改这个端口

```
ssh user@host
ssh host
ssh -p 2222 user@host
```

## 防中间人攻击

SSH采用了非对称加密，远程主机收到用户的登录请求后会把自己的公钥发给用户，用户使用这个公钥将登录密码加密后发送给远程主机，远程主机用自己的私钥解密登录密码，如果密码正确，就同意用户登录。 但不同于https协议，整个过程没有证书中心，公钥都是自己签发的。 所以会有中间人攻击风险，即截获了登录请求，然后冒充远程主机，将伪造的公钥发给用户

为了解决这个问题，SSH规定远程主机必须在自己的网站上贴出公钥指纹，以便用户自行核对，如果是第一次登录对方主机，系统会出现下面的提示

```bash
The authenticity of host 'host (12.18.429.21)' can't be established.
RSA key fingerprint is 98:2e:d7:e0:de:9f:ac:67:28:c2:42:2d:37:16:58:4d.
Are you sure you want to continue connecting (yes/no)?
```

用户确认后才接受这个公钥，开始输入密码登录。 当远程主机的公钥被接受以后，它就会被保存在文件 `$HOME/.ssh/known_hosts` 之中。下次再连接这台主机，系统就会认出它的公钥已经保存在本地了，从而跳过警告部分，直接提示输入密码

每个SSH用户都有自己的known_hosts文件，此外系统也有一个这样的文件，通常是/etc/ssh/ssh_known_hosts，保存一些对所有用户都可信赖的远程主机的公钥

## 公钥登录

使用密码登录，每次都必须输入密码，非常麻烦。 SSH还提供了公钥登录，可以省去输入密码的步骤。 就是用户将自己的公钥储存在远程主机上。登录的时候，远程主机会向用户发送一段随机字符串，用户用自己的私钥加密后，再发回来。远程主机用事先储存的公钥进行解密，如果成功，就证明用户是可信的，直接允许登录shell，不再要求密码。

用户首先生成公钥私钥对

```bash
ssh-keygen
# or
ssh-keygen -t rsa
# 可以带上注释，但验证时这部分是没有作用的，只是一个说明文本
ssh-keygen -C "glowd@gmail.com"
```

运行结束以后，在 `$HOME/.ssh/` 目录下，会新生成两个文件：id_rsa.pub和id_rsa。前者对应公钥，后者对应私钥

将公钥传送到远程主机上面

```bash
ssh-copy-id user@host
```

远程主机将用户的公钥，保存在登录后的用户主目录的 `$HOME/.ssh/authorized_keys` 文件中。公钥就是一段字符串，只要把它追加在 authorized_keys 文件的末尾就行了，因此上面的 `ssh-copy-id` 命令可以用下面的代替

```bash
ssh user@host 'mkdir -p .ssh && cat >> .ssh/authorized_keys' < ~/.ssh/id_rsa.pub
```

远程主机配置公钥登录，配置 `/etc/ssh/sshd_config` 这个文件

```bash
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
```

重启远程主机的ssh服务

```bash
# ubuntu系统
service ssh restart

# debian系统
/etc/init.d/ssh restart
```



