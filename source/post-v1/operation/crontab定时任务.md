title: 利用crontab定时执行任务
keywords: 利用crontab定时执行任务
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-22 16:01:49
updated: 2018-03-22 16:01:49
urlkey: crontab
---

cron服务是Linux的内置服务，但它不会开机自动启动。可以用以下命令启动和停止服务

```
/sbin/service crond start
/sbin/service crond stop
/sbin/service crond restart
/sbin/service crond reload
```

要把cron设为在开机的时候自动启动，在 /etc/rc.d/rc.local 脚本中加入 `/sbin/service crond start` 即可

## 限权

可对各用户使用定时任务进行管理

### 拒绝用户使用crontab任务

在 `/etc/cron.deny` 中添加要拒绝的用户名，格式如下：

```
listen
nobody
noaccess
```

### 允许用户访问crontab任务

在 `/etc/cron.allow` 中添加要允许的用户名，格式如下：

```
listen
nobody
noaccess
```

## 添加任务

```bash
crontab -e
```

也可以直接编辑 `/var/spool/cron/root` 文件，但是用 `crontab -e` 命令退出时可以检查语法

## 查看当前用户的定时任务

```bash
crontab -l
```

## 删除定时任务

```bash
crontab -r
或
crontab -i 删前会有提示
```

## 命令的书写格式

```
  *      *      *      *      *    /bin/sh /scripts/yy.sh
  分     时     日     月     周    命令和文件路径
(00-59) (0-23) (1-31) (1-12) (0-6) 
```

* `*` 星号表示每
* `-` 减号表示连续一段时间，如:00 17-19 * * * cmd  每天下午17点，18点，19点执行一次命令
* `,` 逗号表示多个时间段，如:00 10-11,17-19 * * * cmd 每天的上午10,11点整，下午的17,18,19点整执行一次命令
* `/n` n代表数字，表示每隔n时间，如: */2 * * * * cmd 每隔2分钟执行一次命令

举例：

`30 12-16/2 * * * cmd` 表示每天的中午12点到下午4点间，每隔2小时执行一次

### 注意

* 定时任务中带%无法执行，需要加\转义
* 如果时上有值，分钟上必须有值
* 日和周不要同时使用，会冲突
* `>>` 与 `>/dev/null 2>&1` 不要同时存在

## 服务器时间同步

### 手动同步

```bash
which ntpdate
或
/sbin/ntpdate time.windows.com
```

### 用定时任务同步

```bash
*/5 * * * * /sbin/ntpdate time.windows.com>/dev/null 2>&1
```

## 使用重定向

重定向到空可以避免碎片文件占用inode资源，重定向到一个指定log里，可以看任务是否执行

```bash
30 4 * * * /bin/shtar_mysql.sh > /dev/null 2>&1 
```

## 调试定时任务

cron执行的每一项工作都会被纪录到 `/var/log/cron` 这个日志文件中，可以从这个文件查看命令执行的状态

```bash
tail /var/log/cron
```

也可以重定向到一个日志里，查看任务执行情况

```bash
*/1 * * * * echo yangrong >> /var/log/yy 2>&1
```