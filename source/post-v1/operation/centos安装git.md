title: centos安装git
keywords: centos安装git
savedir: articles
categories:
  - tool
tags:
  - git
  - linux
date: 2018-03-13 12:17:21
updated: 2018-03-13 12:17:21
urlkey: centos-git-install
---
## 获取最新版本地址
[https://github.com/git/git/releases](https://github.com/git/git/releases)

## 安装依赖包
```bash
yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel
yum -y install gcc perl-ExtUtils-MakeMaker
```

## 卸载旧的git版本（如果之前有安装rpm包）
```bash
yum remove git
```

## 编译安装
```bash
cd /usr/local/src
wget https://github.com/git/git/archive/v2.16.0.tar.gz
tar -zxvf v2.16.0.tar.gz
cd git-2.16.0
make prefix=/usr/local/git all
make prefix=/usr/local/git install

vi /etc/profile
# 输入
export GIT_HOME=/usr/local/git
export PATH=$PATH:$GIT_HOME/bin

source /etc/profile
```