title: nginx
keywords: nginx
savedir: articles
categories:
  - operation
date: 2017-12-26 09:52:42
updated: 2017-12-26 09:52:42
urlkey: nginx
---

## 简介

Nginx 是一个高性能的 Web 和反向代理服务器, 它具有有很多非常优越的特性:

* __作为 Web 服务器：__相比 Apache，Nginx 使用更少的资源，支持更多的并发连接，体现更高的效率，这点使 Nginx 尤其受到虚拟主机提供商的欢迎。能够支持高达 50,000 个并发连接数的响应
* __作为负载均衡服务器：__Nginx 既可以在内部直接支持 Rails 和 PHP，也可以支持作为 HTTP代理服务器 对外进行服务。Nginx 用 C 编写, 不论是系统资源开销还是 CPU 使用效率都比 Perlbal 要好的多。
* __作为邮件代理服务器: __Nginx 同时也是一个非常优秀的邮件代理服务器(最早开发这个产品的目的之一也是作为邮件代理服务器)，Last.fm 描述了成功并且美妙的使用经验。

nginx有一个主进程和几个工作进程。 主进程的主要目的是读取和评估配置，并维护工作进程。工作进程对请求进行实际处理。 nginx采用基于事件的模型和依赖于操作系统的机制来有效地在工作进程之间分配请求。 工作进程的数量可在配置文件中定义，并且可以针对给定的配置进行修改，或者自动调整到可用CPU内核的数量

## 启动，停止和重新加载Nginx配置

运行可执行文件启动nginx。当nginx启动后，可以通过使用 `-s` 参数调用可执行文件来控制它。

```
nginx -s [signal]
```

指定配置文件

```
/web/nginx/sbin/nginx -c /web/nginx/conf/nginx.conf
```

### 正常关闭服务

```
nginx -s quit
```

正常关闭服务，会等待工作进程完成当前请求后再停止nginx进程

### 快速关闭服务

```
nginx -s stop
```

### 重新加载配置

```
nginx -s reload
```

当主进程收到要重新加载配置的信号，它将检查新配置文件的语法有效性，并尝试应用其中提供的配置。 如果这是成功的，主进程将启动新的工作进程，并向旧的工作进程发送消息，请求它们关闭。 否则，主进程回滚更改，并继续使用旧配置。 老工作进程，接收关闭命令，停止接受新连接，并继续维护当前请求，直到所有这些请求完成之后，旧的工作进程退出。

### 重新打开日志文件

```
nginx -s reopen
```

### 借助Unix工具

可以借助Unix工具(如kill utility)将信号直接发送到nginx进程，需要先获取进程ID

```
kill -s QUIT 1628
```

## 配置文件的结构

nginx的配置系统由一个主配置文件和其他一些辅助的配置文件构成。这些配置文件均是纯文本文件，全部位于nginx安装目录下的conf目录下。配置文件由配置项组成，每个配置项由指令和参数构成，

指令的参数使用一个或者多个空格或者TAB字符与指令分开。指令的参数有一个或者多个TOKEN串组成。TOKEN串之间由空格或者TAB键分隔。

TOKEN串分为简单字符串或者是复合配置块。复合配置块即是由大括号括起来的若干配置指令。如果一个配置指令的参数全部由简单字符串构成，则使用分号(;)结尾，如果参数有复合配置块，一般是简单TOKEN串放在前面，复合配置块位于最后并且以其结尾，不需要再添加分号

```
# 简单配置
error_page   500 502 503 504  /50x.html;

# 复杂配置
location / {
    root   /home/jizhao/nginx-book/build/html;
    index  index.html index.htm;
}
```

nginx.conf中的配置信息，根据其逻辑上的意义，对它们进行了分类，也就是分成了多个作用域，或者称之为配置指令上下文。不同的作用域含有一个或者多个配置项
，放置在任何上下文之外的指令都被认为是主上下文，通常叫顶级指令，适用于将不同流量类型的指令组合在一起

* [events](http://nginx.org/en/docs/ngx_core_module.html?&_ga=1.10035445.1509956953.1490042234#events) – 一般连接处理
* [http](http://nginx.org/en/docs/http/ngx_http_core_module.html?&_ga=1.10035445.1509956953.1490042234#http) – HTTP协议流量
* [mail](http://nginx.org/en/docs/mail/ngx_mail_core_module.html?&_ga=1.85614937.1509956953.1490042234#mail) – Mail协议流量
* [stream](http://nginx.org/en/docs/stream/ngx_stream_core_module.html?&_ga=1.85614937.1509956953.1490042234#stream) – TCP协议流量

## 静态内容服务

通过在http下定义server块来创建服务器，通常配置文件可以定义服务器监听端口和服务器名称来区分的几个server块，当nginx决定哪个服务器处理请求后，它会根据服务器块内部定义的location指令的参数测试请求头中指定的URI，如果有几个匹配的location块，nginx将选择最长前缀来匹配location，location可以使用正则表达式来匹配，正则表达式之前是区分大小写匹配的波形符号 `~`，或者不区分大小写匹配的波形符号 `~*`，使用 `^~` 修饰符可以对正则表达式更高的优先级，而 `=` 修饰符定义了URI完全匹配规则。如果找到则搜索停止

```
http {
    server {
        location / {
            root /data/www;
        }
        location /images/ {
            root /data;
        }
    }
}
```

## 代理服务器

使用proxy_pass来定义代理服务
```
server {
    listen 8080;
    location / {
        root /data/up1;
    }
}

server {
    location / {
        proxy_pass http://localhost:8080/;
    }

    location ~ \.(gif|jpg|png)$ {
        root /data/images;
    }
}
```

## 虚拟服务器

使用server_name来定义主机域名，可以是完整名称，通配符或正则表达式，如果匹配多个名称，则nginx通过以下顺序搜索名称并使用其找到的第一个主机，如果所有都不匹配，会默认使用第一个主机

```
server {
    listen       80;
    server_name vhost1.com www.vhost1.com;
    index index.html index.html;
    root  /data/www/vhost1;
    access_log  /var/log/vhost1.com.log;
}

server {
    listen       80;
    server_name vhost2.com www.vhost2.com;
    index index.html index.html;
    root  /data/www/vhost2;
    access_log  /var/log/vhost2.com.log;
}
```

## 变量

变量是在运行时计算的命名值，可用作指令的参数，变量由 `$` 开头，有许多预定义的变量，比如 `$remote_addr` 表示客户端IP地址，`$uri` 保存当前的URI值。也可以使用 `set`，`map` 和 `geo` 指令自定义变量

## 返回特定状态码

使用 `return` 指令可以立即返回具有特定错误或重定向代码的响应，第一个参数是响应代码。可选的第二个参数可以是重定向的URL或在响应体中返回文本
```
location /wrong/url {
    return 404;
}
location /permanently/moved/url {
    return 301 http://www.example.com/moved/here;
}
```

## URI重写

使用 `rewrite` 指令可以在请求处理期间多次修改请求URI，该指令具有一个可选参数和两个必需参数。 第一个(必需)参数是匹配请求URI的正则表达式。 第二个参数是用于替换匹配URI的URI。可选的第三个参数是决定执行完该指令后的后续动作

```
location /users/ {
    rewrite ^/users/(.*)$ /show?user=$1 break;
}
```

第二个参数的取值：

* `last` - 停止执行当前服务器或位置上下文中的重写指令，但是NGINX会搜索与重写的URI匹配的位置，并且应用新位置中的任何重写指令(URI可以再次更改，往下继续匹配)。
* `break` - 像break指令一样，在当前上下文中停止处理重写指令，并取消搜索与新URI匹配的位置。新位置(location)块中的rewrite指令不执行。

可以在location 和 server上下文中包含多个rewrite指令，nginx按照它们的顺序逐个执行，处理完server上下文中的rewrite指令（如果有定义）后，nginx会根据新的URI选择一个location上下文
，如果所选location块包含rewrite指令，则依次执行它们，处理完根据新的URI继续搜索新location块

```
server {
    rewrite ^(/download/.*)/media/(.*)\..*$ $1/mp3/$2.mp3 last;
    rewrite ^(/download/.*)/audio/(.*)\..*$ $1/mp3/$2.ra  last;
    return  403;
}
```

此示例配置区分两组URI。 诸如/download/some/media/file之类的URI更改为/download/some/mp3/file.mp3。由于最后一个标志，所以跳过后续指令(第二次rewrite和return指令)，但nginx继续处理该请求，该请求现在具有不同的URI。