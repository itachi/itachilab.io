title: linux安装软件的几种方法
keywords: linux安装软件的几种方法
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-12 09:29:38
updated: 2018-03-12 09:29:38
urlkey: linux-software
---

## bin文件

bin文件可以直接运行，如果权限不够提升权限即可

```bash
chmod +x soft.bin
./soft.bin
```

## rpm包

```bash
rpm –ivh software-1.2.3.rpm
```

卸载的时候需要用软件名

```bash
rpm –e software
```

## yum方式

yum能在线下载并安装rpm包，能更新系统，且还能自动处理包与包之间的依赖问题

```bash
yum install software
```

## deb包

dpkg是Debian Linux提供的一个包管理器，它与RPM十分类似，但只出现在Debina Linux中，其它Linux版本一般都没有

```bash
dpkg -i software-1.2.3.deb
```

卸载的时候需要用软件名

```bash
dpkg –e software
```

## apt方式

apt-get是debian，ubuntu发行版的包管理工具，与yum工具类似，能在线下载并安装deb包

```bash
apt-get install software
```

卸载

```bash
apt-get remove software
```

## tar源代码包安装

需要先下载tar源码包，可以用wget下载，一般tar包都会用gzip、bz2等压缩，如果是最常见的gz格式，可以直接解包，如果不是，则需要先解压软件，再对tar解包

### tar.gz格式

```bash
tar -zxvf soft.tar.gz
cd soft
./configure
make && make install
```

### 非gz格式，比如tar.bz2，bar.xz等

```bash
tar -Jxvf soft.tar.bz2
cd soft
./configure
make && make install
```

> 注意：执行 `make clean` 可以删除安装时产生的临时文件

对于xz格式文件，也可以先用xz解压得到tar，再对tar解包

```bash
xz -d soft.tar.xz
tar -xvf soft.tar
```

安装xz

```bash
yum search xz
yum install xz.i386
```

## 解压后直接运行

对于已经编译过的二进制包，解压后便可以直接运行目录里的可执行文件

```bash
tar -zxvf soft.tar.gz
# or
tar -Jxvf soft.tar.xz
```

如果可执行文件没有权限，需要手动提升

```bash
chmod +x soft
```

如果需要在全局环境使用，可以在/bin目录下建立连接

```bash
ln -s /soft /bin/soft
```

这里以安装nodejs为例（推荐使用nvm安装nodejs）

```bash
cd /web/nodejs/
wget https://nodejs.org/dist/v8.10.0/node-v8.10.0-linux-x64.tar.xz
tar -Jxvf node-v8.10.0-linux-x64.tar.xz
mv node-v8.10.0-linux-x64 8.10.0
vi /etc/profile

# 输入下面内容
export NODE_HOME=/web/nodejs/8.10.0
export PATH=$PATH:$NODE_HOME/bin
export NODE_PATH=$NODE_HOME/lib/node_modules

:wq
source /etc/profile
```