title: linux备忘
keywords: linux备忘
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-12 09:29:38
updated: 2018-03-12 09:29:38
urlkey: linux-meno
---

## 设置环境变量

### 在 `/etc/bashrc` 设置

```bash
export PATH=$PATH:/usr/local/git/bin
# or
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
```

### 在 `/etc/profile` 设置

```bash
export NODE_HOME=/web/nodejs/8.10.0
export PATH=$PATH:$NODE_HOME/bin
```
> 注意：这种方法要在 `/etc/bashrc` 里末尾添加一句 `source /etc/profile` 不然每次重启后会失效

## 文件编码格式问题

执行shell脚本时，报“/bin/bash^M: bad interpreter: No such file or directory”错误，原因是由于shell脚本文件是dos格式，即每一行结尾以\r\n来标识，而unix格式的文件行尾则以\n来标识。

查看脚本文件是dos格式还是unix格式的几种办法。

1. cat -A filename  从显示结果可以判断，dos格式的文件行尾为^M$，unix格式的文件行尾为$。
2. od -t x1 filename 如果看到输出内容中存在0d 0a的字符，那么文件是dos格式，如果只有0a，则是unix格式。
3. vi filename打开文件，执行 : set ff，如果文件为dos格式在显示为fileformat=dos，如果是unxi则显示为fileformat=unix。

解决方法：

1. 使用linux命令dos2unix filename，直接把文件转换为unix格式
2. 使用sed命令sed -i "s/\r//" filename  或者 sed -i "s/^M//" filename直接替换结尾符为unix格式
3. vi filename打开文件，执行 : set ff=unix 设置文件为unix，然后执行:wq，保存成unix格式。

## IP加端口访问返回503

使用egg部署应用，直接通过ip加端口的方式访问，返回503，无法进入到应用，如果用nginx配置域名代理访问则正常，使用 `tcping 139.199.227.231 8004` 扫描端口，显示端口closed，使用 `netstat -lnp | grep 8004` 显示网络状态，看到127.0.0.1:8004字样

原因：127.0.0.1是内网地址，端口监听在该地址上面，所以外网无法访问

解决：使用0.0.0.0或具体ip地址来监听端口