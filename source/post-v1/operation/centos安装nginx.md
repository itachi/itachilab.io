title: centos安装nginx
keywords: nginx
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-09 09:52:42
updated: 2018-03-09 09:52:42
urlkey: centos-nginx--install
---

## 更新包

```bash
yum update
```

## 依赖包安装

```bash
yum -y install gcc gcc-c++ autoconf automake libtool make cmake
yum -y install zlib zlib-devel openssl openssl-devel pcre-devel
```

## 安装PCRE

PCRE 作用是让 Nginx 支持 Rewrite 功能。

```bash
cd /usr/local/src
wget http://downloads.sourceforge.net/project/pcre/pcre/8.35/pcre-8.35.tar.gz
tar zxvf pcre-8.35.tar.gz
cd pcre-8.35
./configure
make && make install
```

查看pcre版本

```bash
pcre-config --version
```

## 安装 Nginx

```bash
cd /usr/local/src
wget -c http://nginx.org/download/nginx-1.10.3.tar.gz
tar zxvf nginx-1.10.3.tar.gz
cd nginx-1.10.3

./configure --prefix=/usr/local/nginx \
--with-http_ssl_module \
--with-pcre

make && make install
```

configure命令支持以下参数

* `--prefix = path` - 定义将保留服务器文件的目录。 这个同一个目录也将用于由configure(除了库源的路径)和nginx.conf配置文件中设置的所有相关路径。 它默认设置为/usr/local/nginx目录。
* `--sbin-path = path` - 设置nginx可执行文件的名称。此名称仅在安装期间使用。默认情况下文件名为 prefix/sbin/nginx。
* `--conf-path = path` - 设置nginx.conf配置文件的名称。 如果需要，nginx可以始终使用不同的配置文件启动，方法是在命令行参数-c file 指定。 默认情况下，该文件名为：prefix/conf/nginx.conf。
* `--pid-path = path` - 设置将存储主进程的进程ID的nginx.pid文件的名称。 安装后，可以使用pid指令在nginx.conf配置文件中更改文件名。 默认情况下，文件名为：prefix/logs/nginx.pid。
* `--error-log-path = path` - 设置主错误，警告和诊断文件的名称。 安装后，可以在nginx.conf配置文件中使用error_log指令更改文件名。 默认情况下，文件名为：prefix/logs/error.log。
* `--http-log-path = path` - 设置HTTP服务器主要请求日志文件的名称。 安装后，可以使用access_log指令在nginx.conf配置文件中更改文件名。 默认情况下，文件名为：prefix/logs/access.log。
* `--build = name` - 设置一个可选的nginx构建名称。
* `--user = name` - 设置非特权用户的名称，该用户的凭据将由工作进程使用。 安装后，可以使用user指令在nginx.conf配置文件中更改名称。 默认的用户名是：nobody。
* `--group = name` - 设置由工作进程使用其凭据的组的名称。 安装后，可以使用user指令在nginx.conf配置文件中更改名称。 默认情况下，组名称设置为非特权用户的名称。
* `--with-select_module` 和 `--without-select_module` — 启用或禁用构建允许服务器使用select()方法的模块。 如果平台似乎不支持更合适的方法(如kqueue，epoll或/dev/poll)，则会自动构建该模块。
* `--with-poll_module` 和 `--without-poll_module` — 启用或禁用构建允许服务器使用poll()方法的模块。 如果平台似乎不支持更合适的方法(如kqueue，epoll或/dev/poll)，则会自动构建该模块。
* `--without-http_gzip_module` - 禁用构建压缩HTTP服务器响应的模块。 需要zlib库来构建和运行此模块。
* `--without-http_rewrite_module` - 禁用构建一个允许HTTP服务器重定向请求并更改请求URI的模块。 需要PCRE库来构建和运行此模块。
* `--without-http_proxy_module` - 禁用构建HTTP服务器代理模块。
* `--with-http_ssl_module` - 可以构建一个将HTTPS协议支持添加到HTTP服务器的模块。 默认情况下不构建此模块。 OpenSSL库是构建和运行该模块所必需的。
* `--with-pcre = path` - 设置PCRE库源的路径。库发行版(4.4` - 8.40版)需要从PCRE站点下载并提取。 其余的由nginx的./configure和make完成。 该库是 location 指令和ngx_http_rewrite_module模块中正则表达式支持所必需的。
* `--with-pcre-jit` - 使用“即时编译”支持构建PCRE库。
* `--with-zlib = path` - 设置zlib库的源路径。 库分发(版本1.1.3` - 1.2.11)需要从zlib站点下载并提取。 其余的由nginx的./configure和make完成。 该库是ngx_http_gzip_module模块所必需的。
* `--with-cc-opt = parameters` - 设置将添加到CFLAGS变量的其他参数。 在FreeBSD下使用系统PCRE库时，应指定--with-cc-opt="-I /usr/local/include"。 如果需要增加select()所支持的文件数，那么也可以在这里指定，如：--with-cc-opt="-D FD_SETSIZE=2048"。
* `--with-ld-opt = parameters` - 设置链接过程中使用的其他参数。 当在FreeBSD下使用系统PCRE库时，应指定--with-ld-opt="-L /usr/local/lib"。

## 修改配置

```bash
cd /usr/local/nginx/conf
vi nginx.conf
```

## 全局变量

```bash
vi /etc/profile
export PATH=$PATH:/usr/local/nginx/sbin
source /etc/profile
```

## 启动Nginx程序、查看进程

```bash
nginx
ps -ef | grep nginx
```

## 停止

```bash
# 从容停止
kill -QUIT 主进程号
nginx -s quit
# 快速停止
kill -TERM 主进程号
nginx -s stop
# 强制停止
pkill -9 nginx
```

## 重启

```bash
nginx -s reload
```

## 测试运行

```bash
curl localhost
```

## 开机启动

```bash
vi /etc/rc.d/rc.local
```
增加一行 `/usr/local/nginx/sbin/nginx`
设置执行权限：`chmod +x /etc/rc.d/rc.local`

## 开吂80端口

```bash
/sbin/iptables -I INPUT -p tcp --dport 80 -j ACCEPT
/etc/init.d/iptables save
service iptables restart
```

也可以手动修改防火墙配置文件

```bash
vi /etc/sysconfig/iptables
加入
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT

service iptables restart
```

查看端口状态

```bash
netstat -lnp | grep 80
或
lsof -i :80
```

## 问题

如果访问如果提示403错误，有可能是nginx没有web目录的操作权限，可以修改web目录的读写权限

```bash
chmod -R 755 / var/www
```

或者把nginx的启动用户改成目录的所属用户（以root为例）

```bash
user  root;
```