title: git常用操作
keywords: git常用操作
savedir: articles
categories:
  - tool
tags:
  - git
date: 2016-01-03 12:17:21
updated: 2016-01-03 12:17:21
urlkey: git-use
---
## 临时切换到某个commit
```
git checkout <commitid>
```

这时候执行 `git branch` 会看到正处于一个临时的分支上面，如果想以这个提交作为基础开发新的功能，可以创建一个新的分支进行开发
```
git checkout -b <branchname> <commitid>
```

想回到之前的的分支只要checkout回去就好了，但如果在切回以前的commit的之前已经改了一些代码，则需要使用reset将修改了的代码丢弃，如果后续需要这些代码，可以使用 `stash`

## 撤销修改
使用 `git checkout -- file` 可以撤销对某文件的修改，分两种情况：

* 文件作了修改还没添加到暂存区，这时撤销修改就回到同版本库一样的状态
* 文件已经添加到暂存区后作了修改，这时撤销修改就回到添加到暂存区后的状态

如果作了修改并且已经添加到暂存区了，这时想撤销修改，需要分两步执行

1. `git reset HEAD file` 把暂存区的修改撤销掉，重新放回工作区
2. `git checkout -- file` 撤销修改回到版本库同步状态

## 撤销提交
git revert是用一次新的commit来回滚之前的commit，和git reset不同，git reset是把HEAD向后移动了一下，而git revert是HEAD继续前进
```
git revert HEAD
```

不产生新的commit
```
git revert HEAD -n
```

## 版本回退
使用 `git reset` 可以回到某个提交，或撤销某些文件的修改，有三种模式，`--soft` 把文件回退到暂存区，`--mixed` 默认模式，把文件回退到工作区，`--hard` 把记录全部清除

`HEAD` 表示当前版本，`HEAD^` 表示上一个版本，上上一个版本就是 `HEAD^^`，`HEAD~n` 表示上n个版本（n为数字），当然，也可以使用具体的版本id（版本id不用写完整，git会自动查找，当然也不能只写前一两位）

将最近一次提交的提交记录全部清除
`git reset --hard HEAD^`

回退到某个提交
`git reset --hard 3628164`

撤销所有文件的修改
`git reset --hard HEAD`

将暂存区的文件回退到工作区
`git reset HEAD a.js`


`git reset` 实际把 `HEAD` 指针重置到某个版本，`git log` 也只能查看到直至该版本的信息，如果reset后想恢复到新版本，需要用 `git reflog` 查看所有操作记录，找到想恢复的版本id，再根据该id去reset

## 删除文件
如果在资源管理器直接删除了文件，`git status` 会提示哪些文件被删除，需要使用 `git rm file` 确定删除文件，再用 `git commit -m` 提交（`git rm` 和 `git add` g一样，用于添加到暂存区）
如果只是误删文件了，可以用 `git checkout -- file` 恢复文件，该命令无论工作区是修改还是删除，都可以“一键还原”

## 同步远程库
1. 在线创建远程库，得到库地址
2. 本地初始化git仓库，添加远程，推送
```
echo "# git-test" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/Zohner/git-test.git
git push -u origin master
```

## 记住密码
作用于 http 协议的git地址（第一次需要输入用户名密码）
```
git config --local credential.helper store
```
该命令会在配置里写入 `credential.helper=store`，local 也可以改为 global

## 分支与合并
### 创建并切换到分支dev
```
git checkout -b dev
```

相当于以下两条命令
```
git branch dev
git checkout dev
```

### 查看本地所有分支（当前分支前面会标一个*号）
```
git branch
```

### 切换分支
```
git checkout master
```

### 合并指定分支到当前分支（默认使用快进模式）
```
git merge dev
```

### 删除分支
```
git branch -d dev
```

### 用普通模式合并，合并后生成一个新的commit，这样历史上就可以看出曾经做过合并
```
git merge --no-ff -m "merge with no-ff" dev
```

### 强行删除（可删除还没合并的分支）
```
git branch -D dev
```

## 拉取和推送
本地创建远程分支
```
git checkout -b dev origin/dev
```

本地分支拉取远程分支
```
git pull origin master
```
可以直接使用 `git pull`，如果提示“no tracking information”，可以设置本地分支和远程分支的对应关系
```
git branch --set-upstream dev origin/dev
```

指定本地分支推送到远程，Git会把该分支推送到远程库对应的远程分支上
```
git push origin master
```
当你从远程仓库克隆时，实际上Git自动把本地的master分支和远程的master分支对应起来了，并且，远程仓库的默认名称是origin

## 暂存修改
当修改了文件还不想提交，但这时又需要切换到其它分支工作，为了不丢失这些修改内容，可以先暂存起来
```
git status
```

这时工作区便是干净的，可以切到其它分支工作，完成后回来原来分支，查看已暂存的内容
```
git stash list
```

再恢复暂存的内容
```
git stash pop
```
使用 `git stash apply` 也可以恢复，但恢复后暂存区的记录还在，需要使用 `git stash drop` 删除

如果暂存区有多条记录，可以恢复指定的stash
```
git stash apply stash@{0}
```

## 配置别名
```
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.br branch
```

## 标签管理
标签就是指向某个commit的指针，它跟某个commit绑在一起，只是起了一个让人容易记住的有意义的名字
### 创建标签
```
git tag <name>
```

### 查看所有标签
```
git tag
```

### 在某个commit上打标签（默认标签是打在最新提交的commit上）
```
git tag <name> <commitid>
```

### 查看标签信息
```
git show <tagname>
```

### 推送标签
```
git push origin <tagname>
```

### 推送所有标签
```
git push origin --tags
```

### 删除本地标签
```
git tag -d <tagname>
```

### 删除远程标签
```
git push origin :refs/tags/<tagname>
```

## 迁移
1. 从原地址克隆一份裸版本库
```
git clone –bare git://github.com/username/project.git
```

2. 到新的 Git 服务器上创建一个新项目newproject

3. 以镜像推送的方式上传代码到新的项目中
```
cd project.git
git push –mirror git@example.com/username/newproject.git
```

4. 完成，可以从新的项目中clone，或者修改本地旧仓库的源地址
```
git remote set-url origin git@example.com/username/newproject.git
```

> 如果是gitlab这些有在线管理页面的，可以直接在后台操作

## TortoiseGit 管理 github
1. 安装 TortoiseGit
2. 创建 ssh key：TortoiseGit - Puttygen，Generate，Save private key到本地，(无需key passphrase)
3. 将上面创建的 key 添加到 github 帐号
4. clone 项目，使用 ssh 地址，设置 ueser.name user.email Load Putty Key 选择上面保存的key
5. 选择上面生成的key：setting - git - remote - putty - xxx.ppk
6. 另附[手动生成SSH key的方法](https://help.github.com/articles/generating-ssh-keys/)

