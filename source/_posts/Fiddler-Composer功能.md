title: Fiddler Composer功能
keywords: Fiddler Composer功能
savedir: articles
categories:
  - tool
date: 2015-12-03 23:07:47
updated: 2015-12-03 23:07:47
urlkey: fiddler-composer
---
Fiddler Composer的功能就是用来创建HTTP Request 然后发送。可以自定义一个 Request, 也可以手写一个 Request,还可以在Web会话列表中拖拽一个已有的 Request. 来创建一个新的 HTTP Request，发送后的 Request 会在左边会话列表里出现

{% img articleimg /postimg/fiddler-composer/001.png %}

## Fiddler Composer 比其他工具的优势
能创建发送HTTP Request的工具很多很多。但是Fiddler的功能有如下的优势。

1. 能从"Web会话列表"中 拖拽一个先前捕获到的 Request, 然后稍微修改一下
2. 发送 Request 后，还能设置断点，继续修改 Request.
3. 支持在 Request中上传文件
4. 支持发送多次 Request.

## Parsed和Raw两种编辑模式
Fiddler Composer 有两种编辑模式

{% img articleimg /postimg/fiddler-composer/002.png %}

* __Parsed模式__: 最常用, 把 Request 分为三个部分，Request line, Request Headesr, Request Body。很容易创建一个 Request
* __Raw模式__: 需要一行一行手动写一个 Request


## 实例: 模拟京东商城的登录

1. 启动 fiddler 监听
2. 打开京东，然后输入用户名和密码，登录。Fiddler 捕获到这个登录的 Request
3. 找出哪个 Request 是用来登录的，然后把它拖拽到 Composer 中
4. 在 Composer 可以看到，登录是使用 POST 方法把用户名和密码发送给服务器。可以修改 Composer 中的 request 内容，比如修改用户名或密码
5. 改好 request 后点击 Execute 按钮发送请求，这样便通过 fiddler 发送了一次京东登录的请求

> 如果按住 Shift 键的同时按 Execute. Fiddler 会自动给这个 Request 下断点
