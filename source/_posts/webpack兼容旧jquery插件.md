title: webpack兼容旧jquery插件
keywords: webpack兼容旧jquery插件
savedir: articles
categories:
  - automation
tags:
  - remind
date: 2018-06-12 16:45:30
updated: 2018-06-12 16:45:30
urlkey: webpack-jquery-plugin
---

## ProvidePlugin

ProvidePlugin插件用于自动加载模块，不需要手动引入，代码里可以直接使用$, jQuery，可以直接加载jquery插件
```
var providePlugin = new webpack.ProvidePlugin({
	$: 'jquery',
	jQuery: 'jquery',
	'window.jQuery': 'jquery',
	'window.$': 'jquery',
});
```

## expose-loader
可以将指定js模块export的变量声明为全局变量，用于适配`<script>`标签加载的jquery插件

```
{
  test: require.resolve('jquery'),
  loader: 'expose?$!expose?jQuery',
},
```

## externals
用来将某个模块替换为全局变量，和ProvidePlugin相反，不会自动加载该模块，用于适配`<script>`标签加载的jquery库，构建后不会打包jquery库
```
externals: {
  'jquery': 'window.jQuery',
},
```

## imports-loader
相当于手动版的ProvidePlugin，需要在模块上单独设置，如果有多个模块，需要设置多次
```
loaders: [
    {
        test: require.resolve("some-module"),
        loader: "imports?$=jquery&jQuery=jquery",
    }
]
```