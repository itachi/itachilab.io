title: 解决fonticon字体跨域问题
keywords: 解决fonticon字体跨域问题
savedir: articles
categories:
  - resolution
date: 2016-01-03 12:08:15
updated: 2016-01-03 12:08:15
urlkey: fonticon-cross
---
## 背景
当前页面与字体文件不同域时，会出现跨域加载不了字体的情况导致图标无法显示，可以在字体所在的服务器上开启[CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS)来解决

## ngnix 针对字体文件开启CORS
```
server {
    root   e:/www;
    # ...
    # ...
    location ~* \.(ttf|ttc|otf|eot|woff|svg)$ {
        add_header Access-Control-Allow-Origin "*";
    }
}
```

## ngnix 开启CORS完整配置

代码来源：[CORS on Nginx](http://enable-cors.org/server_nginx.html)

```
location / {
     if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        #
        # Om nom nom cookies
        #
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        #
        # Custom headers and headers various browsers *should* be OK with but aren't
        #
        add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
        #
        # Tell client that this pre-flight info is valid for 20 days
        #
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
     }
     if ($request_method = 'POST') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
     }
     if ($request_method = 'GET') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
     }
}
```