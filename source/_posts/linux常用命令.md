title: linux常用命令
keywords: linux常用命令
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-27 14:30:22
updated: 2018-03-27 14:30:22
urlkey: linux-cmd
---

## 软链接
```
ln -s /web/redis/3.0.1/bin/redis-cli /usr/local/bin/redis-cli
```

## 设置可执行权限
```
chmod +x /etc/rc.d/rc.local
```

## 添加全局变量
```
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
source /etc/bashrc
```

## 防火墙
### 停止firewall
```
systemctl stop firewalld.service
```

### 禁止firewall开机启动
```
systemctl disable firewalld.service
```

## 查看进程
```
ps -ef | grep nginx
```

## 拷贝
```
cp -a old new
```

## 查看端口状态
```
/etc/init.d/iptables status
netstat -lnp | grep 80
lsof -i :80
nmap 127.0.0.1
tcping 139.199.227.231 8004
```

## 清空文本内容
```
> log.txt
```

## 上传并覆盖文件
```
rz -y
```

## 查看日志
```
tail -100f access.log 
tail -f access.log
tail -f access.log | grep xxx
```

## 打包
```
cd public
tar -zcvf public.tar.gz *
```

## 查看进程启动路径
```
ps eho command -p 23374
```

## 查看进程网络状态
```
netstat -pan | grep 23374
```

## 查看分区情况
```
df -h
```

## 查看各目录的占用情况
```
du -sh /*
```

## 查看inode区域占用情况
```
df -ih
```

## 统计字数
```
wc -w acc.log
```

## 统计行数
```
wc -l acc.log
```