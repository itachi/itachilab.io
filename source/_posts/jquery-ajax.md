title: JQuery-ajax
keywords: JQuery-ajax
savedir: articles
categories:
  - jQuery
tags:
  - summary
date: 2015-10-03 19:48:13
updated: 2015-10-03 19:48:13
urlkey: jquery-ajax
---
jQuery对AJAX操作进行了封装，`$.ajax()` 属于最底层的方法 ，第二层是 `load()`，`$.get()`，`$.post()` 方法 ，第三层则是 `$.getScript()` 和 `$.getJSON()` 方法

## load()方法
选择目标容器DOM调用该方法，可能载入远程HTML内容追加到目标容器中，可以添加发送数据和定义回调函数。通常用于从服务器获取静态内容
```js
$("#restext").load("test.html");//把text.html的内容添加到id=restext的div中
```

支持对加载的HTML内容进行jQuery式的筛选，格式为在url后追加**选择器**，以空格隔开，如
```js
$("#restext").load("test.html .dd");//加载class为dd的元素内容
```

当不指定参数时默认为 GET 发送，如果指定参数则自动转换为 POST 发送
```js
$("#restext").load("test.html",{name:"apple",key:"1"});
```

回调函数有三个参数,代表请求返回的内容，请求状态和 XMLHttpRequest 对象.无论Ajax请求是否成功，只要请求完成就会触发回调函数。
```js
$("#restext").load("test.html",function(responseText,textStatus,XMLHttpRequest){
    //code
});
```

## $.get()方法
指定用GET方式来发送AJAX请求，可以指定url，数据，回调函数，返回数据类型。发送的数据以对象的形式（也可以是字符串，但要手动对字符进行编码）作为第二个参数传入，而不是手动追加在url后面，该方法会自动将数据附加到url。回调函数只有两个参数，data 代表返回的内容，可以是XML文档，json文件，HTML片段等等，textStatus 代表请求状态，有 success、error、nosmodified、timeout 4 种，只有当数据成功返回（success）时才触发回调函数，这点和 load() 不同，返回数据类型包括 xml、html、script、json、text 和 _default

### HTML片段
```js
$("#send").click(function(){
     $.get("getdata.php",{username:$("#uname").val()},function(data,textStatus){
          $("#restext").html(data);
     },"html");
});
```

### XML文档
```js
$("#send").click(function(){
     $.get("getdata.php",{username:$("#uname").val()},function(data,textStatus){
          var username = $(data).find("coment").attr("username");
          var txthtml = "<p class='un'>"+username+"</p>";
          $("#restext").html(txthtml);
     },"xml");
});
```

### JSON文件
```js
$("#send").click(function(){
     $.get("getdata.php",{username:$("#uname").val()},function(data,textStatus){
          var username = data.username;
          var txthtml = "<p class='un'>"+username+"</p>";
          $("#restext").html(txthtml);
     },"json");
});
```
> 注意：此时返回的是JSON文件，而不是JSON格式的字符串，所以不用对返回数据进行JSON解析，JSON文件对格式要求很严格，构建的JSON文件必须完整无误，否则会导致页面的脚本终止运行

## $.post()方法
使用上同 $.get() 方法一样，只不过发送方式被限定为 POST.(当 load() 方法带参数时也是以 POST 方式发送，这时两个方法是一样的功能)
```js
$("#send").click(function(){
     $.post("getdata.php",{username:$("#unmae").val()},function(data,textStatus){
          $("restext").append(data);
     },"html");
});
```

## $.getScript()方法
动态加载js文件并自动执行，支持回调函数，在js文件成功加载后执行
```js
$.getScript("http://jquery.com/view/trunk/color.js",function(){
    $("#dd").click(function(){
        //code
    });
});
```

另外，动态加载js文件还可以这样写
```js
$(document.createElement("script")).attr("src","test.js").appendTo("head");
$("<script type='text/javascript' src='test.js' />").appendTo("head");
```

## $.getJSON()方法
动态加载JSON文件，并在回调函数中对返回数据进行处理。使用该方法还可以加载其它服务器的JSON数据，实现跨域请求
```js
$("#send").click(function(){
     $.getJSON('text.json',function(data){
          $.each(data,function(index,item){
               $(".txg").html(item['content']);
          });
     })
})
```

## $.ajax() 
jQuery最底层的Ajax实现，只传递一个参数，该参数为一个对象，设置了请求的相应参数。前面的 load()、$.get()、$.post()、$.getScript()、$.getJSON() 都是基于 $.ajax 的方法创建的，都可以用 $.ajax() 代替，还可以设定 beforeSend（提交前），error（请求失败），success（请求成功），complete（请求完成）的回调函数

### 代替$.getScript()方法
```js
$.ajax({type:"GET",url:"test.js",dataType:"script"});
```

### 代替$.get()方法
```js
$.ajax({
	ype:"GET",
	url:"getdata.php",
	data:{username:$("#uname").val()},
	dataTpye:"html",
	success:function(data){
	     $("#restext").append(data);
	}
});
```

## ajax全局事件
只要发生ajax请求就会触发它们

	ajaxStart() 
	ajaxStop()
	ajaxComplete()
	ajaxError()
	ajaxSend()
	ajaxSuccess()

```js
$("#loading").ajaxStart(function(){
     $(this).show();
});
$("#loading").ajaxStop(function(){
     $(this).hide();
});
```

## serialize() 
将DOM元素内容序列化为字符串方便作为AJAX请求的数据发送，该方法会自动对字符进行编码
```js
$.get("getdata.php",$("#form").serialize(),function(data,textStatus){
     $("#restext").html(data);
});
```

## serializeArray() 
将DOM元素内容序列化为JSON格式的数据，返回的是对象
```js
var fields = $(":checkbox").serializeArray();
$.each(fields,function(i,field){
     $("#restext").append(field.value);
});
```

## $.param() 
对一个数组或对象按照key/value进行序列化
```js
var obj = {a:1,b:2};
var k = $.param(obj);
alert(k);//输出a=1&b=2
```