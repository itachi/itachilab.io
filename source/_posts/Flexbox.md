title: Flexbox
keywords: Flexbox
savedir: articles
categories:
  - css
tags:
  - summary
date: 2016-01-03 12:29:01
updated: 2016-01-03 12:29:01
urlkey: flexbox
---
## 简介
2009年，W3C 提出了一种新的方案：Flex布局，有别于 CSS 2.1 定义的四种布局模式（块布局、行内布局、表格布局、定位布局），Flexbox（伸缩布局）是为了呈现复杂的应用与页面而设计出来的，一种更加方便有效，能够在未知或者动态尺寸的情况下自由分配容器空间的布局方式。兼容方面主流浏览器及 IE10 以上支持，移动端基本都支持

## 参考
* [Flex 布局教程：语法篇 - 阮一峰](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)
* [Flex 布局教程：实例篇 - 阮一峰](http://www.ruanyifeng.com/blog/2015/07/flex-examples.html)

## 语法
### 定义容器
任何一个容器都可以指定为 Flex布局，包括行内元素，设为 Flex布局以后，子元素的 float、clear 和 vertical-align 属性将失效。
```css
div{
	display: flex;
}
span{
	display:inline-flex;
}
```
> 注意：为达到最大兼容性，需要添加 `-webkit` 等厂商前缀，旧版的语法为 `display: box`

### 容器属性
#### `flex-direction`
决定子元素的排列方向

```css
flex-direction: row | row-reverse | column | column-reverse;
```

* `row` : （默认值），从左往右
* `row-reverse` : 从右往左
* `column` : 从上到下
* `column-reverse` : 从下到上

#### `flex-wrap`
决定一条轴线排不下的情况下如何换行

```css
flex-wrap: nowrap | wrap | wrap-reverse;
```

* `nowrap` : （默认值），不换行
* `wrap` : 换行，第一行在上方
* `wrap-reverse` : 换行，第一行在下方

#### `flex-flow`
`flex-direction` 属性和 `flex-wrap` 属性的简写形式，默认值为 `row nowrap`

#### `justify-content`
决定水平方向的对齐方式

```css
justify-content: flex-start | flex-end | center | space-between | space-around;
```

* `flex-start` : （默认值），左对齐
* `flex-end` : 右对齐
* `center` : 居中
* `space-between` : 两端对齐，子元素间隔相等
* `space-around` :  子元素两侧的间隔相等（子元素之间的间隔比子元素与边框的间隔大一倍）

#### `align-items`
决定垂直方向的对齐方式

```css
align-items: flex-start | flex-end | center | baseline | stretch;
```

* `stretch` : （默认值），如果子元素未设置高度或设为 auto，将占满整个容器的高度
* `flex-start` : 上对齐
* `flex-end` : 上对齐
* `center` : 居中
* `baseline` : 子元素的第一行文字的基线对齐

#### `align-content`
定义了多根轴线的对齐方式。如果只有一根轴线，该属性不起作用

```css
align-content: flex-start | flex-end | center | space-between | space-around | stretch;
```

* `stretch` : （默认值），轴线占满整个交叉轴
* `flex-start` : 与交叉轴的起点对齐
* `flex-end` : 与交叉轴的终点对齐
* `center` : 与交叉轴的中点对齐
* `space-between` : 与交叉轴两端对齐，轴线之间的间隔平均分布
* `space-around` : 每根轴线两侧的间隔都相等。轴线之间的间隔比轴线与边框的间隔大一倍

### 子元素属性
#### `order`
子元素排列顺序。数值越小，排列越靠前，默认为 0

#### `flex-grow`
所占比例，默认为 0，如果所有子元素的 `flex-grow` 属性都为1，则将等分空间

#### `flex-shrink`
缩小比例，默认为 1，即如果空间不足，该项目将缩小，如果所有元素的 `flex-shrink` 属性都为1，当空间不足时，都将等比例缩小。如果一个项目的 `flex-shrink` 属性为 0，其他都为1，则空间不足时，前者不缩小

#### `flex-basis`
在分配多余空间之前，元素占据的空间。浏览器根据这个属性，计算是否有多余空间。它的默认值为 auto，即元素的本来大小。可以设为跟 width 或 height 属性一样的值（比如350px），表示占据固定空间。

#### `flex`
`flex-grow`, `flex-shrink` 和 `flex-basis` 的简写，默认值为 0 1 auto。后两个属性可选

#### `align-self`
允许单个元素有与其他元素不一样的对齐方式，可覆盖align-items属性。默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch

```css
align-self: auto | flex-start | flex-end | center | baseline | stretch;
```
该属性有6个值，除了 `auto`，其他都与 `align-items` 属性完全一致