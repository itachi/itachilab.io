title: mustache
keywords: mustache
savedir: articles
categories:
  - compile
tags:
  - templete
date: 2015-12-05 13:07:09
updated: 2015-12-05 13:07:09
urlkey: mustache
---
## 语法

### 变量
`{% raw %}{{value}}{% endraw %}` 表示渲染一个变量，当值为空或没有时返回空，默认会对 HTML 转义，使用 `{% raw %}{{{name}}}{% endraw %}` 或 `{% raw %}{{& name}}{% endraw %}` 可以不转义 HTML

#### 模板
```
* {{name}}
* {{age}}
* {{company}}
* {{{company}}}
```

#### 数据
```
{
  "name": "Chris",
  "company": "<b>GitHub</b>"
}
```

#### 结果
```
* Chris
*
* &lt;b&gt;GitHub&lt;/b&gt;
* <b>GitHub</b>
```

### 块
`{% raw %}{{#person}}{{/person}}{% endraw %}` 渲染文本块，如果值为 false 或空列表，文本块不显示，如果值是一个非空列表，则根据列表重复渲染文本块

#### 模板

```
{{#repo}}
  <b>{{name}}</b>
{{/repo}}
```

#### 数据

```
{
  "repo": [
    { "name": "resque" },
    { "name": "hub" },
    { "name": "rip" }
  ]
}
```

#### 结果

```
<b>resque</b>
<b>hub</b>
<b>rip</b>
```

除了传递一个列表，也可以传递回调函数，对渲染文本二次封装

#### 模板

```
{{#wrapped}}
  {{name}} is awesome.
{{/wrapped}}
```

#### 数据

```
{
  "name": "Willy",
  "wrapped": function() {
    return function(text, render) {
      return "<b>" + render(text) + "</b>"
    }
  }
}
```

#### 结果
```
<b>Willy is awesome.</b>
```

如果值不是 false 也不是列表，则只渲染一次

#### 模板
```
{{#person}}
  Hi {{name}}!
{{/person}}
```

#### 数据
```
{
  "person?": { "name": "Jon" }
}
```

#### 结果
```
Hi Jon!
```

### 空数据文本
`{% raw %}{{^person}}{{/person}}{% endraw %}` 表示当值为空列表，不存在或 false 时才渲染的文本块

#### 模板
```
{{#repo}}
  <b>{{name}}</b>
{{/repo}}
{{^repo}}
  No repos :(
{{/repo}}
```

#### 数据
```
{
  "repo": []
}
```

#### 结果
```
No repos :(
```

### 模板注释
`{% raw %}{{! ignore me }{% endraw %}` 加一个感叹号表示注释语法

### 内嵌
`{% raw %}{{> next_more}}{% endraw %}` 使用 `>` 可以内嵌后缀名为 mustache 的模板文件

#### 主模板
```
<h2>Names</h2>
{{#names}}
  {{> user}}
{{/names}}
```

#### 子模板-user.mustache
```
<strong>{{name}}</strong>
```

等同于

```
<h2>Names</h2>
{{#names}}
  <strong>{{name}}</strong>
{{/names}}
```

## 应用

* javascript - [mustache.js](https://github.com/janl/mustache.js)