title: git-rebase
keywords: git-rebase
savedir: articles
categories:
  - tool
tags:
  - git
date: 2017-11-05 12:17:21
updated: 2017-11-05 12:17:21
urlkey: git-rebase
---
git rebase用于把一个分支的修改合并到当前分支。和git merge不同的是，使用rebase后分支历史看起来像没有经过任何合并一样

```
git checkout dev
git rebase origin
```
这个命令会把你的dev分支里的每个commit取消掉，并且把它们临时保存为补丁(这些补丁放到".git/rebase"目录中)，然后把dev分支更新 为最新的origin分支，最后把保存的这些补丁应用到dev分支上

合并前
{% img articleimg /postimg/git-rebase/001.jpg %}

使用pull命令（实际上操作了git merge）
{% img articleimg /postimg/git-rebase/002.jpg %}

使用git rebase或git pull --rebase
{% img articleimg /postimg/git-rebase/003.jpg %}

当dev分支更新之后，它会指向这些新创建的commit，而那些老的提交会被丢弃，如果运行垃圾收集命令(pruning garbage collection)，这些被丢弃的提交就会删除（参考git gc)

### 使用git rebase也会影响commit的顺序

假设C3提交于9点，C5提交于10点，C4提交于11点，C6提交于12点

使用 `git merge` 来合并所看到的commit的顺序（从新到旧）是：C7,C6,C4,C5,C3,C2,C1
使用 `git rebase` 来合并所看到的commit的顺序（从新到旧）是：C6‘,C5',C4,C3,C2,C1

其中C6‘和C5'是C6和C5的克隆

### 解决冲突
在rebase的过程中如果出现冲突，git会停止rebase并会让你去解决冲突，在解决完冲突后，用 `git add` 命令去更新这些内容的索引，然后无需执行 `git-commit`，只要执行 `git rebase --continue` ，这样git会继续应用余下的补丁

在任何时候都可以用 `--abort` 参数来终止rebase的行动，并且分支会回到rebase开始前的状态
```
git rebase --abort
```

### 交互模式
这种方法通常用于在向别处推送提交之前对它们进行重写
```
git rebase -i origin/master
```