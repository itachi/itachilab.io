title: jsonpCallback处理多异步并发
keywords: jsonpCallback处理多异步并发
savedir: articles
categories:
  - resolution
date: 2015-10-03 15:58:19
updated: 2015-10-03 15:58:19
urlkey: jsonpcallback
---
jquery jsonp跨域使用 jsonpCallback 参数时，代码如下
```js
$.ajax({
	url: intfhref,
	type: "GET",
	dataType: "jsonp",
	jsonpCallback: 'getproduct',
	success: function(data) {
		//code
	}
});
```

当两个请求几乎同时发送时， 会报 getproduct is not defined 错误，原因是第一次请求时 jquery 检测到用户没有定义 `getproduct` 的函数，所以自己定义了一个，然后继续处理请求，在这期间同时发送了请求2，请求2判断到已经有 `getproduct` 函数了（请求1定义的），继续执行，这时请求1已经处理完了，jquery 会把先前定义的 `getproduct` 函数销毁掉，当请求2执行到 append 一个 script 时页面上页面没有 `getproduct` 函数了， 所以就报错了，解决方法是不让 jquery 去定义回调函数，因为它处理完会自动销毁掉， 而选择自己定义回调函数，修改后如下

```js
function getproduct(data) {
	//code
}
$.ajax({
	url: intfhref,
	type: "GET",
	dataType: "jsonp",
	jsonpCallback: 'getproduct'
});
```

还有一种情况， 回调函数可能会有几种处理情况，比如

```js
function postajax(type) {
	$.ajax({
		url: intfhref,
		type: "GET",
		dataType: "jsonp",
		jsonpCallback: 'getproduct',
		success: function(data) {
			if (type == 1) {
				//code
			} else {
				//code
			}
		}
	});
}
```

这种场景不适合把回调函数定义在函数里，比如

```js
function postajax(type) {
	getproduct = function(data) {
		if (type == 1) {
			//do
		}
	}
	$.ajax({
		url: intfhref,
		type: "GET",
		dataType: "jsonp",
		jsonpCallback: 'getproduct'
	});
}
```

这样写虽然 `getproduct` 可以拿到 type 的值，但因为请求的异步特性，当几个请求同时发生时，`getproduct` 里拿到的 type 都是最终的值， 等于每个请求都是同一个回调了， 虽然传进去的 type 不一样， 这已经和预想的不一样了，解决方案如下

```js
function getproduct1(data) {}
function getproduct2(data) {}
function postajax(type) {
	var callback = "";
	if (type == 1) {
		callback = "getproduct1";
	} else {
		callback = "getproduct2"
	}
	$.ajax({
		url: intfhref,
		type: "GET",
		dataType: "jsonp",
		jsonpCallback: callback
	});
}
```