title: jQuery ajax方法备记
keywords: jQuery ajax方法备记
savedir: articles
categories:
  - jQuery
date: 2015-11-05 22:18:16
updated: 2015-11-05 22:18:16
urlkey: jquery-ajax-bak
---
## 回调函数
处理 `$.ajax()` 得到的数据，需要使用回调函数

### beforeSend(xhr)
在发送请求之前调用，接收一个xhr对象作为参数，可以在函数里修改xhr对象，比如添加自定义HTTP头，如果返回 `false` 可以取消本次 ajax 请求

### error(XMLHttpRequest, textStatus, errorThrown)
请求出错时调用，传入xhr对象，描述错误类型（字符串）以及一个异常对象（如果有的话），其中错误类型（第二个参数）除了得到null之外，还可能是 "timeout", "error", "notmodified" 和 "parsererror"

### dataFilter(data, type)
请求成功之后调用。给 Ajax 返回的原始数据的进行预处理的函数。提供 `data` 和 `type` 两个参数：`data` 是 Ajax 返回的原始数据，`type` 是调用 `jQuery.ajax` 时提供的 `dataType` 参数，传入返回的数据以及 `dataType` 参数的值。必须返回新的数据（可能是处理过的）交由 jQuery 进一步处理，最终再传递给 success 回调函数

### success(data, textStatus, xhr)
请求成功后的回调函数，其中第一个参数 `data` 是由服务器返回，并根据 `dataType` 参数进行处理后的数据

### complete(xhr, textStatus)
请求完成后回调函数，请求成功或失败之后均调用

## 数据类型
`$.ajax()` 对返回的数据有几种不同的处理方式，除了单纯的XML，还可以是 `html`、`json`、`jsonp`、`script` 或者 `text`。处理方式通过 `dataType` 来指定，如果不指定，jQuery 将自动根据 HTTP 包 MIME 信息来智能判断

其中，text和xml类型返回的数据不会经过处理。数据仅仅简单的将 XMLHttpRequest 的 responseText 或 responseHTML 属性传递给 success 回调函数，

如果指定为html类型，返回纯文本 HTML 信息，但内嵌的 script 标签会在HTML作为一个字符串返回之前执行。类似的，指定script类型的话，也会先执行服务器端生成JavaScript，然后再把脚本作为一个文本数据返回

如果指定为 `json` 类型，则会把获取到的数据作为一个JavaScript对象来解析，并且把构建好的对象作为结果返回（内建JSON解析器）

如果指定为 `jsonp` 类型，会创建一个查询字符串参数 `callback=随机函数名` 并加在请求的URL后面

如果指定了 `script` 或者 `jsonp` 类型，那么当从服务器接收到数据时，实际上是用了 `<script>` 标签而不是 XMLHttpRequest 对象。这种情况下，`$.ajax()`不再返回一个 XMLHttpRequest 对象，并且也不会传递事件处理函数，比如 `beforeSend`。`complete`

> 注意：使用过程需要确保服务器报告的MIME类型与选择的 `dataType` 所匹配。比如XML的话，服务器端就必须声明 `text/xml` 或者 `application/xml` 来获得一致的结果

## 发送数据
通过 `data` 参数设置发送的数据，可以包含一个查询字符串，比如 key1=value1&key2=value2 ，也可以是一个映射，比如 `{key1: 'value1', key2: 'value2'}`，如果为数组，jQuery 将自动为不同值对应同一个名称。如 `{foo:["bar1", "bar2"]}` 转换为 `"&foo=bar1&foo=bar2"` 

> 注意：如果传递一个映射，数据在发送前也会被转换成查询字符串，如果想禁用自动转换，可以设置 `processData` 为 `false`。如果想发送一个XML对象，还需要改变 `contentType` 选项的值，用其他合适的MIME类型来取代默认的 `application/x-www-form-urlencoded`

## 参数
### accepts[Map]
内容类型请求头，告诉服务器什么样的响应会接受返回

### async[Boolean]
是否异步，默认为 `true`

### cache[Boolean]
是否缓存，默认为 `true`，`dataType` 为 `script` 和 `jsonp` 时默认为 `false`

### contentType[String]
内容编码类型，默认为 "application/x-www-form-urlencoded"

### context[Object]
设置 Ajax 相关回调函数的上下文，让回调函数内 `this` 指向这个对象，默认情况下 `this` 指向调用本次AJAX请求时传递的 options 参数

### crossDomain[map]
默认情况下同域为 `false`，跨域为 `true`，如果在同域的情况下需要强制使用跨域，可设置为 `true`，在一些服务器重定向的场景下有一定作用

### global[Boolean]
是否触发全局 AJAX 事件，默认为 `true`

### ifModified[Boolean]
仅在服务器数据改变时获取新数据。使用 HTTP 包 Last-Modified 头信息判断，默认为 false

### jsonp[String]
在一个 jsonp 请求中回调函数的参数名，默认为 `callback`

### jsonpCallback[String]
在一个 jsonp 请求中回调函数的名，默认是一个随时数

### scriptCharset[String]
只有当请求时 `dataType` 为 `jsonp` 或 `script`，并且 `type` 是 `GET` 才会用于强制修改 `charset`。通常只在本地和远程的内容编码不同时使用。

### statusCode[map]
一组数值的HTTP代码和函数对象，当响应时调用了相应的代码
```js
$.ajax({
	statusCode: {404: function() {
		alert('page not found');
	}
});
```

### timeout[Number]
设置请求超时时间（毫秒）。此设置将覆盖全局设置