title: centos安装mongodb
keywords: centos安装mongodb
savedir: articles
categories:
  - storage
tags:
  - mongodb
  - linux
date: 2016-01-02 18:42:16
updated: 2016-01-02 18:42:16
urlkey: centos-mongodb
---

## 安装及配置服务
```
cd /usr/local
mkdir mongodb
cd mongodb
mkdir -p data/db
mkdir -p data/logs
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-amazon-3.6.3.tgz
tar -zxvf mongodb-linux-x86_64-amazon-3.6.3.tgz
mv mongodb-linux-x86_64-amazon-3.6.3 3.6.3
cd 3.6.3
cd bin
vi mongodb.conf

# 添加以下几行代码
port = 27017
dbpath = /web/mongodb/data/db
logappend = true
fork = true
logpath = /web/mongodb/data/logs/mongpdb.log
bind_ip = 0.0.0.0

# 添加全局变量
vi /etc/profile
export PATH=$PATH:/usr/local/mongodb/3.6.3/bin

# 测试开启
mongod -f mongodb.conf

# 关闭
pkill mongod
# 或者进入mongo shell运行`db.shutdownServer()`

# shell
mongo

# 安装服务
vi /etc/rc.d/init.d/mongodb

# 添加以下内容
#!/bin/sh

# chkconfig: 2345 80 90
# description: Mongodb database.
# processname: mongod
# Source function library
  
EXEC=/usr/local/mongodb/3.6.3/bin/mongod
CONF="/usr/local/mongodb/3.6.3/bin/mongodb.conf"


OPTIONS=" -f "
#mongod
#mongod="/usr/local/mongodb/bin/mongod"
lockfile=/var/lock/subsys/mongod

start()
{
 echo -n $"Starting mongod: "
 $EXEC $OPTIONS $CONF
 #daemon $mongod $OPTIONS
 RETVAL=$?
 echo
 [ $RETVAL -eq 0 ] && touch $lockfile
}
  
stop()
{
 echo -n $"Stopping mongod: "
 # killproc $EXEC -QUIT
 pkill mongod
 RETVAL=$?
 echo
 [ $RETVAL -eq 0 ] && rm -f $lockfile
}
  
restart () {
    stop
    start
}
ulimit -n 12000
RETVAL=0
  
case "$1" in
 start)
  start
  ;;
 stop)
  stop
  ;;
 restart|reload|force-reload)
  restart
  ;;
 condrestart)
  [ -f $lockfile ] && restart || :
  ;;
 status)
  status $EXEC
  RETVAL=$?
  ;;
 *)
  echo "Usage: $0 {start|stop|status|restart|reload|force-reload|condrestart}"
  RETVAL=1
esac
exit $RETVAL

# 添加服务
chkconfig --add mongodb

# 开启
service mongodb start

# 关闭
service mongodb stop
```

> 如果报如下错误: ERROR: child process failed, exited with error number 1，很可能是 mongodb.conf 中配置的路径不一致
> 如果报如下错误: ERROR: child process failed, exited with error number 100，很可能是没有正常关闭导致的，那么可以删除 mongod.lock 文件