title: webview调试环境
keywords: webview调试环境
savedir: articles
categories:
  - development
tags:
  - debug
date: 2016-06-22 17:19:03
updated: 2016-06-22 17:19:03
urlkey: webview-debug
---
## chrome-devtools移动端模式
适合单纯web页面(无调用app注入方法)，如果页面有作ua判断，可手动添加自定义设备

## 真机 + Chrome inspect

{% img articleimg /postimg/webview-debug/001.png %}

1. Android 测试机连接到电脑，开启 USB 调试
2. 在 PC 的 Chrome 上打开 `Chrome://inspect`，找到测试机设备
3. 测试机进入 webview 页面，在 Chrome 调试台上调试

## 真机 + weinre

{% img articleimg /postimg/webview-debug/002.png %}
{% img articleimg /postimg/webview-debug/003.png %}

1. 安装 [weinre](http://people.apache.org/~pmuellr/weinre/docs/latest/Home.html) `npm install -g weinre`
2. 开启 weinre `weinre --httpPort 8888 --boundHost -all-`
3. 浏览器打开 `localhost:8888`
4. 页面注入 weinre 调试js，测试机进入 webview 页面
5. 浏览器进入 weinre 调试地址进行调试

## Genymotion + Chrome

{% img articleimg /postimg/webview-debug/004.png %}

1. Genymotion模拟器安装app，进入页面
2. 启用 Chrome inspect，直接调试页面

## Genymotion + Fiddler
适合调试线上页面

{% img articleimg /postimg/webview-debug/005.png %}

1. 开启 Fiddler
2. Genymotion 设置代理
3. 映射文件，调试