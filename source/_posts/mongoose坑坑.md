title: mongoose坑坑
keywords: mongoose坑坑
savedir: articles
categories:
  - storage
tags:
  - mongodb
  - mongoose
date: 2018-01-25 16:26:41
updated: 2018-01-25 16:26:41
urlkey: mongoose-experience
---
 ## createConnection和connect
 常规连接使用 `connect`，使用多个连接时使用 `createConnection`，区别在于使用 `createConnection` 后创建model需要在返回的connection实例上，而使用 `connect` 是在mongoose实例上

 ```
 var db = mongoose.createConnection(setting.host, setting.db);  
 db.once('open',function(){  
     var schema = mongoose.Schema({  
         title:String  
     });  
     var model = db.model('all',schema);  
     var kit = new model({title: 'Fuck'});  

     kit.save(function(err, date, num){  
         if(err) console.log(err);  
         else if(date) console.log(date);  
         else console.log(num);  
     })  
 })
 ```
 或
 ```
 mongoose.connect('mongodb://localhost/natsu');  
 var db = mongoose.connection;  
 db.once('open',function(){  
     var schema = mongoose.Schema({  
         title:String  
     });  
     var model = mongoose.model('all',schema);  
     var kit = new model({title: 'Fuck'});  
     kit.save(function(err, date, num){  
         if(err) console.log(err);  
         else if(date) console.log(date);  
         else console.log(num);  
     })  
 })
 ```
 如果不正常使用，会导致无法写入数据等问题

 ## Model.update不返回promise
 使用update更新数据时，如果找不到对应_id代码会直接抛出错误，如果该update是放在promise.all里的，则不会触发promise.all的catch，因为直接报错了，没有返回promise对象，解决方案是用findOneAndUpdate替代update