title: AJAX
keywords: AJAX
savedir: articles
categories:
  - request
tags:
  - summary
date: 2015-10-03 16:44:19
updated: 2015-10-03 16:44:19
urlkey: ajax
---
## 传统交互方式
用户单击提交按钮提交表单，整个表单提交到服务器，转发给处理表单的脚本，脚本执行完返回全新页面，返回时浏览器重新刷新绘制，屏幕短暂空白，用户需要等待,整个过程围绕表单进行。

## AJAX交互方式
用户填写表单或其它动作，通过事件触发，整合需要发送的表单数据发送到js代码而不是直接发送到服务器，js获取表单数据并发送到服务器，服务器返回信息，触发在js中定义的处理事件，对返回数据进行处理。这些js代码主要使用 XMLHttpRequest 对象，整个过程是异步的，页面无需刷新，用户继续其它操作，js在幕后与服务器通信而不受用户干预

## 流程

1.  创建XMLHttpRequest对象
2.  获取数据
3.  建立连接
4.  定义回调函数
5.  发送请求

## 应用举例
```js
//1.创建XMlHttpRequest 对象
var xhr = null;
if (window.ActiveXObject) {
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
} else if (window.XMLHttpRequest) {
	xhr = new XMLHttpRequest();
}

//2.创始请求
var url = "ajax_check_pname.php?pname=" + pname.value + "&r=" + Math.random();
xhr.open("get", url);
//如果请求类型为POST,设置http请求头信息
//xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

//3.发送请求
xhr.send(null);

//捕获请求状态和请求结果
xhr.onreadystatechange = function() {
	if (4 == xhr.readyState && 200 == xhr.status) {
		if (parseInt(xhr.responseText)) {
			//code
		}
	}
}
```
> 注意：open 方法只是定义了一个连接，并没有打开该连接，更没有任何数据通信，如果发送方式为 GET,数据已经附加在url上，所以 send 函数参数设为 null

## HTTP就绪状态
AJAX的每一次请求都伴随着HTTP就绪状态的改变，每次改变都会调用 `onreadystatechange` 定义的回调函数，HTTP就绪状态有如下几种

* 0：请求没建立，（调用 `open()` 之前）
* 1：请求建立但没发出，（调用 `send()` 之前）
* 2：请求发出正在处理中，响应中可以得到内容头部
* 3：请求已处理，响应中可以得到部分数据，但服务器没完成响应
* 4：响应完成，可以使用响应数据

> 注意：不同浏览器就绪状态不同，有些直接跳过0跟1，从2开始，有些有多次状态1，有些则从0到4，所以判断时只判断状态为4

## HTTP状态码
如果服务器响应了请求并完成了处理，但报告了一个错误，这样就算HTTP就绪状态为4也得不到预期的数据，因为除了判断HTTP就绪状态外还需要判断HTTP状态码，在网络世界HTTP代码可以处理请求中发生的任何问题，状态为200表示一切正常。

## HEAD请求
当不确定服务器端返回的响应数据，或不需要真正处理返回的信息，不想浪费大量的带宽在响应内容上，可以只发送一个HEAD请求，这时服务器会返回资源的头 header，然后从该头信息取得有用的相关信息，达到某种特殊的用途。例如检查url，查看返回内容的长度或类型。确认某个文件是否被修改过。
```js
request.open("HEAD',url,true);
alert(request.getAllRequestHeaders());
alert(request.getAllRequestHeaders("Content-Lenght"));
```

## XMLHttpRequest 和 XML
XMLHttpRequest 对象虽然名字有个XML，但其实跟XML没半点关系，该对象只不过为客户端代码（如javascript）提供一种发送HTTP请求的方法而已。XML只是一种数据格式，而HTTP是一种协议，两者毫无关系。当然AJAX中的X仍然是指XML的重要性，体现在两个方面，以XML格式向服务器发送请求或以XML格式从服务器接收数据。但两者的数据量都会比文本大，所以如非必要，使用文本会更快更高效。

### 从客户端到服务端的XML
将请求的格式设置为XML，用XML发送请求，在数据发送之前将数据处理成XML格式。
假定服务器端脚本要求的格式为
```xml
<address>
<firstname></firstname>
<lastname></lastname>
</address>
```

代码如下
```js
var fname = document.getElementById("firstname").value;
var lname = document.getElementById("lastname").value;
var xmlstring = "<address>" +
	"<firstname>" + escape(fname) + "</firstname>" +
	"<lastname" > +escape(lname) + "</lastname>" +
	"</address>";
var url = "index.php";
xmlhttp.open("POST", url, true);
xmlhttp.setRequestHeader("Content-Type", "text/xml");
xmlhttp.send(xmlstring);
```
> 因为构建XML的复杂性，并且用XML作为请求并没有任何优点好处（跟接收XML不同），所以除非服务器端脚本只接受XML数据，否则还是尽量用普通文本发送数据。

### 从服务端到客户端的XML
客户端本身并没有较为标准且方便快捷的方法来处理除了简单纯文本的服务器响应信息（如健值对），这时服务端选择XML作为响应数据的格式是常见的解决方案。

服务端的XML响应基本有两种格式，一种是格式化为XML格式的纯文本，如 `<address><firstname>name</firstname></address>`，这种情况用 `responseText` 属性接收，还要对数据进行相应的字符串处理。另一种是作为一个XML文档（也是一个DOM Document对象）返回，这种情况用 `responseXML` 属性接收，得到一个 DOM Document 对象，再用DOM的相关API函数处理。

```js
var xmldoc = xmlhttp.responseXML;
var adds = xmldoc.getElementByTabName("address");
for(var i=0; i < adds.lenght; i++){
     var fname = adds[i].childNodes[0].value;
}
```
返回XML的PHP脚本

```php
header("Content-Type:text/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo "<address>";
while ($row = mysql_fetch_array($rlt)) {
	$fname = $row["firstname"];
	echo "<firstname>".$name.
	"</firstname>";
}
echo "</address>";
```

## 使用json格式传输数据
json格式可表示除简单健值对之外的更复杂的数据结构，且数据是动态的，可输出或修改。最大的方便利用 JavaScript 对象和字符串值之间的快速转换，而且，不管使用 GET 还是 POST，由于 JSON 只是文本。不需要特殊格式的转码而且每个服务器端脚本都能处理文本数据，所以可以轻松利用 JSON 并将其应用到服务器