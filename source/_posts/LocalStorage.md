title: LocalStorage
keywords: LocalStorage
savedir: articles
categories:
  - storage
tags:
  - summary
date: 2015-12-26 16:22:49
updated: 2015-12-26 16:22:49
urlkey: LocalStorage
---
允许在本地存储 5M 的数据（只支持字符串），移动端及主流浏览器均支持，IE 系列下 IE8 及以上支持。HTML5 对 LocalStorage 进行了扩展，添加了新的元素，在 HTML5 中，本地存储是一个 window 的属性，包括 localStorage 和 sessionStorage，二者用法完全相同，前者是一直存在本地的，后者只是伴随着 session，窗口一旦关闭就没了

## 使用
可以用操作对象的方式来操作 localStorage，还可以使用 `setItem`，`getItem` 方法，清除用 `removeItem`，清除所有的键值对，用 `clear()`。另外，HTML5 还提供了一个 `key()` 方法，直接用索引来获取键名
```js
//set
localStorage.a = "xx";
localStorage["a"] = "xx";
localStorage.setItem("a","xx");
//get
var a = localStorage["a"];
var a = localStorage.a;
var a = localStorage.getItem("a");
//remove
localStorage.removeItem("a");
//clear
localStorage.clear();
//key
for (var i = 0; i < localStorage.length; i++) {
	var key = localStorage.key(i);
	var value = localStorage,getItem(key);
}
```

HTML5 的本地存储，还提供了一个 storage 事件，可以对键值对的改变进行监听，事件变量 e 是一个 StorageEvent 对象，提供了一些实用的属性，可以很好的观察键值对的变化
```js
window.addEventListener("storage",handle_storage,false);
function handle_storage(e){
	if(!e){e=window.event;}
	cosole.log(e);
}
```