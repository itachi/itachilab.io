title: delete操作符
keywords: delete操作符
savedir: articles
categories:
  - js
tags:
  - remind
date: 2015-10-03 14:51:02
updated: 2015-10-03 14:51:02
urlkey: javascript-delete
---
## delete 操作符
每一个执行上下文中声明的变量及函数和当前上下文的变量对象的关系是属性关系，作为变量对象（不管是全局对象还是激活对象）的属性，具有 DontDelete 特性，是不能 delete 的，而作为其它对象创建的属性不具有 DontDelete 特性，因此可以 delete

var创建的全局变量（任何函数之外的程序中创建）是不能被删除的通过。无var创建的隐式全局变量（无视是否在函数中创建）是能被删除的。

```js
// 可以 delete
var o = { x: 1 };
delete o.x; // true
o.x; // undefined

var global = this;
global.x = 1;
delete global.x; // true
global.x; // undefined

// 不可以 delete
var x = 1;
delete x; // false
x; // 1

function x(){}
delete x; // false
typeof x; // "function"
```

特殊情况一：未声明赋值的变量可以删除
```js
bar = 1;
delete bar; // true
typeof bar; // "undefined"
```

特殊情况二：函数内部的 arguments 及参数不能删除
```js
function foo(x){
	delete arguments; // false
	typeof arguments; // "object"
	delete x; // false
	typeof x; // "string"
}
```

特殊情况三：eval 内的代码虽然和调用 eval 的代码是同一个上下文，但 eval 内声明的变量是可以删除
```js
eval("var x = 1;"); 
delete x; // true
x; // undefined
```

> 注意：delete 只会删除属性本身，不会删除属性指向的对象，这点和 C++ 的 delete 是不同的
```js
var o = {};
var a = { x: 10 };
o.a = a;
delete o.a; // o.a属性被删除
o.a; // undefined
a.x; // 10
```