title: redis
keywords: redis
savedir: articles
categories:
  - storage
tags:
  - redis
date: 2018-01-18 09:44:03
updated: 2018-01-18 09:44:03
urlkey: redis
---
## 下载&安装

* 官网下载地址：[http://redis.io/download](http://redis.io/download)
* github下载地址：[https://github.com/MicrosoftArchive/redis/releases](https://github.com/MicrosoftArchive/redis/releases)

下载到本地后解压即可，还可以把该目录添加到path环境变量，后续操作就不到到该目录

## 启动

进入到redis目录，运行 `redis-server` 启动服务器（需保持窗口）

没有指定配置文件会使用默用的配置文件，也可以手动指定
```
redis-server redis.windows.conf
```

## 关闭
```
redis-cli shutdown
```

## 命令行

redis自带cli工具，进入到redis目录后运行 `redis-cli` 可进行命令行模式，不带参数则使用默认的端口和ip，也可以手动指定
```
redis-cli -h 127.0.0.1 -p 6379
```

如果设置了密码，需要先验证才能进行其它操作
```
AUTH password
```

## 设置服务
以下命令可以把redis设置为服务，不用每次都手动启动
```
redis-server --service-install redis.windows-service.conf --loglevel verbose
```

常用的redis服务命令

* 卸载服务：`redis-server --service-uninstall`
* 开启服务：`redis-server --service-start`
* 停止服务：`redis-server --service-stop`

## 设置密码
```
config get requirepass
config set requirepass test123
```