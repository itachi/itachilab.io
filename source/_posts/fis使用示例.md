title: fis使用示例
keywords: fis使用示例
savedir: articles
categories:
  - automation
tags:
  - fis
date: 2016-12-02 14:38:04
updated: 2016-12-02 14:38:04
urlkey: fis-demo
---
## 发布到远端机器
服务器端需要接收脚本：[php实现](https://github.com/fex-team/fis-command-release/blob/master/tools/receiver.php)、[node实现](https://github.com/fex-team/receiver)
```
fis.match('*', {
  deploy: fis.plugin('http-push', {
    receiver: 'http://cq.01.p.p.baidu.com:8888/receiver.php',
    to: '/home/work/htdocs' // 注意这个是指的是测试机器的路径，而非本地机器
  })
})
```

## 替代内置Server
假设本地 Web Server 的根目录是 `/Users/my-name/work/htdocs`
```
fis.match('*', {
  deploy: fis.plugin('local-deliver', {
    to: '/Users/my-name/work/htdocs'
  })
})
```
或者直接执行
```
fis3 release -d /Users/my-name/work/htdocs
```

## 对除了html、css、js 三种语言以外的其它语言使用fis内置语法
```
fis.match('*.tmpl', {
  isJsLike: true
});
fis.match('*.sass', {
  isCssLike: true
});
fis.match('*.xxhtml', {
  isHtmlLike: true
})
```