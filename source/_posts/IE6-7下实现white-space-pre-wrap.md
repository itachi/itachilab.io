title: 'IE6,7下实现white-space:pre-wrap'
keywords: 'IE6,7下实现white-space:pre-wrap'
savedir: articles
categories:
  - css
tags:
  - experience
date: 2015-12-26 16:51:57
updated: 2015-12-26 16:51:57
urlkey: ie-whitespace
---
HTML 中的“空白符”包括空格 (space)、制表符 (tab)、换行符 (CR/LF) 三种。在默认情况下，HTML 正文（行内元素之间）的空白符均被显示为空格，并且连续的多个空白符会被视为一个，或者说，连续的多个空白符会被合并。如果希望HTML正文中（行内元素之间）的多个连续空格在网页浏览器中可以真实地呈现，或者需要源码中的换行符能起到真正的换行作用。可以使用 `<pre>` 标签，也可以通过 `white-space` 属性来控制达到相同效果。

### white-space 属性
CSS 中的 `white-space` 属性用于设置文本空白符的处理规则，这其中包括：是否合并空白符、是否保留换行符、是否允许自动换行。因此，如果需要某个容器元素具有类似 `<pre>` 元素的空白符处理行为，则为它设置 `white-space: pre` 样式即可。各属性值的不同行为如下所示：

    属性值     空白符     换行符     自动换行    最早出现于
    normal      合并       忽略       允许         CSS 1
    nowrap      合并       忽略       不允许       CSS 1
    pre         保留       保留       不允许       CSS 1
    pre-wrap    保留       保留       允许         CSS 2.1
    pre-line    合并       保留       允许         CSS 2.1

> 注：在 CSS1/2 下，white-space 属性只可应用于块级元素；在 CSS 2.1 下，可应用于所有元素。

### 对 pre-wrap 的需求

1. 文本流按照文本方向排版保留空格和回车，又能够自动换行时，如显示一些代码又不想有滚动条影响阅读。
2. 显示由 textarea 输入的内容，还原回车，而不需要在服务器端将换行转换成 `<br>`

### 兼容IE6 7

IE6 7不支持 `pre-wrap`,兼容方法是先用 `white-space: pre` 保留空格及回车，再用 `word-wrap: break-word` 强制换行。

```css
.content {
    white-space: pre-wrap;
    *white-space: pre;
    *word-wrap: break-word;
}
```