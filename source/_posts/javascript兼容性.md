title: javascript兼容性
keywords: javascript兼容性
savedir: articles
categories:
  - js
date: 2016-01-02 18:53:49
updated: 2016-01-02 18:53:49
urlkey: javascript-diffent
---
## IE6/7/8中parseInt第一个参数为非法八进制字符串且第二个参数不传时返回值为0
```js
parseInt('08') // IE678返回0，其它浏览器返回8
parseInt('09') // IE678返回0，其它浏览器返回9
```