title: egg使用passport实现登录鉴权
keywords: egg使用passport实现登录鉴权
savedir: articles
categories:
  - eggjs
date: 2018-12-21 17:13:38
updated: 2018-12-21 17:13:38
urlkey: egg-passport
---

## local 登录

### 安装相应插件，配置
```
// plugin.js
exports.passportLocal = {
  enable: true,
  package: 'egg-passport-local',
};

// config.default.js
exports.passportLocal = {
  // usernameField: 'username',
  // passwordField: 'password',
};
```

### 设置鉴权中间件
```
module.exports = (option, app) => {
  return async function (ctx, next) {
    if (ctx.isAuthenticated()) {
      await next()
    } else {
      if (ctx.acceptJSON) {
        ctx.throw(401, '401 Unauthorized')
      } else {
        return ctx.redirect(`/user/login?redirect=${ctx.url}`)
      }
    }
  }
}
```

### 设置鉴权路由
```
const localStrategy = app.passport.authenticate('local', { failWithError: true, successReturnToOrRedirect: null })
userRouter.post('/login', localStrategy, 'user.loginHandle')
```
failWithError设为true，认证失败后会抛出一个401异常供程序捕获，如果设为false，会自动响应一个401的status
successReturnToOrRedirect设为false，认证成功后不自动跳转，继续执行后面的逻辑，如果设置一个url会跳转到设置的url，不设置的情况下会自动中转到'/'

### 设置鉴权逻辑
```
const LocalStrategy = require('passport-local').Strategy;

module.exports = app => {
  // 处理用户信息
  app.passport.verify(async (ctx, user) => {
    if (username === password) {
      return username
    } else {
      ctx.throw(401, '401 Unauthorized')
    }
  })
  app.passport.serializeUser(async (ctx, user) => {
    return user.username
  })
  app.passport.deserializeUser(async (ctx, user) => {
    return '你好' + user
  })
}
```
> 注意：认证错误信息，需要使用session记录供登录页面使用，最好使用flash信息

### config.default.js配置自定义错误处理401异常
```
  config.onerror = {
    html(err, ctx) {
      if (err.status === 401) {
        ctx.flash('error', err.message || '登录异常')
        ctx.redirect('/user/login')
      } else {
        ctx.redirect('/500')
      }
    }
  }
```

### 获取个人信息
```
ctx.user
```

### 退出登录逻辑
```
async logout () {
  const { ctx } = this
  ctx.logout()
  ctx.redirect(ctx.get('referer') || '/')
}
```

> 注意: local登录验证最好使用异步接口供前端调用，前端处理跳转逻辑，如果使用form表单的形式，后端需要处理错误信息并传递到登录页面展示

## oauth 登录
### 接入中间件auth.js验证登录
同上

### 安装相应插件，配置
```
// plugin.js
module.exports.passportGithub = {
  enable: true,
  package: 'egg-passport-github'
}

// config.default.js
config.passportGithub = {
  key: '6876124bfxxxxx2af16',
  secret: '1db1f0c5692d035bd0xxxxxxb8ab2826abdeee',
  proxy: true
}
```

### 设置鉴权路由
```
app.passport.mount('github', {
  successRedirect: '/',
  failureRedirect: '/user/login?oauth=error'
})
// 上面的 mount 是语法糖，等价于
// const github = app.passport.authenticate('github', {});
// router.get('/passport/github', github);
// router.get('/passport/github/callback', github);
```

### 设置鉴权逻辑
```
app.passport.verify(async (ctx, user) => {
  // const user = {
  //   provider: 'github',
  //   id: profile.id,
  //   name: profile.username,
  //   displayName: profile.displayName,
  //   photo: profile.photos && profile.photos[0] && profile.photos[0].value,
  //   accessToken,
  //   refreshToken,
  //   params,
  //   profile,
  // }
  const { profile } = user
  let existUser = await ctx.service.user.getUser({ githubId: profile.id })
  if (existUser.success && existUser.data) {
    return existUser.data
  } else {
    const newUser = await ctx.service.user.addUser({
      username: uuid.v4(),
      password: '',
      githubId: profile.id,
      githubUsername: profile.username,
      githubAccessToken: profile.accessToken || '',
      nickname: profile.username,
      avatar: profile._json.avatar_url || '',
      email: (profile.emails && profile.emails[0] && profile.emails[0].value) || ''
    })
    return newUser.data
  }
});
```

## jwt 登录
### 安装相应插件，配置
```
// plugin.js
exports.passportJwt = {
  enable: true,
  package: 'egg-passport-jwt'
}

// config.default.js
config.passportJwt = {
  secret: 'xx'
}
```

### 设置鉴权中间件
```
module.exports = (option, app) => {
  const jwt = app.passport.authenticate('jwt', { session: false, successReturnToOrRedirect: null })
  return jwt
}
```

### 设置鉴权逻辑
```
module.exports = app => {
  app.passport.verify(async (ctx, user) => {
    // check user
    assert(user.provider, 'user.provider should exists');
    assert(user.payload, 'user.payload should exists');

    // find user from database
    const existsUser = await ctx.model.User.findOne({ id: user.payload.sub });
    if (existsUser) return existsUser

    // or you could create a new user
  })
}
```

### 发放令牌
```
const jwt = require('jsonwebtoken')
jwt.sign({ data }, jwtConfig.secret, Object.assign({}, jwtConfig.sign, option))
```

> 注意: jwt也可以使用 [egg-jwt](https://github.com/okoala/egg-jwt) 实现