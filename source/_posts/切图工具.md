title: 切图工具
keywords: 切图工具
savedir: articles
categories:
  - tool
date: 2015-12-26 16:33:17
updated: 2015-12-26 16:33:17
urlkey: photoshop-cut
---
## [Size Marks](https://github.com/romashamin/Size-Marks-PS)

一款快速标识尺寸的photoshop script插件，可以审图及构思时顺便标下重要间距的尺寸

1. 下载：[Size Marks.jsx](https://cdn.rawgit.com/romashamin/Size-Marks-PS/master/Size-Marks.jsx-v0.1.1.zip)
2. 安装：复制到 script 目录：`E:\Program Files\Adobe\Photoshop CS6\Presets\Scripts` 
3. 设置快捷键：编辑->键盘快捷键，设置为`Ctrl+Shift+D`，（不用快捷键也可以通过 文件->脚本->Size Marks 来触发
4. 使用：先用选区工具选择选区，触发工具，则自动生成尺寸图层

## [马克鳗](https://www.getmarkman.com/)

一款收费（60元/年）设计稿标注、测量工具，需要安装[Adobe AIR 运行时](http://get.adobe.com/cn/air/)

1. 长度标记
2. 坐标和矩形标记
3. 色值标记
4. 文字标记

## [PxCook像素大](http://www.fancynode.com.cn/)

一款可以脱离photoshop集测量、标记、切图于一身的工具，需要安装[Adobe AIR 运行时](http://get.adobe.com/cn/air/)