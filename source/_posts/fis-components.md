title: fis-components
keywords: fis-components
savedir: articles
categories:
  - automation
tags:
  - fis
date: 2015-11-28 13:20:39
updated: 2015-11-28 13:20:39
urlkey: fis-components
---
## 简介
1. 使用相对严格的[组件规范](https://github.com/fis-components/spec) 解决使用困惑和文件杂乱的问题
2. 采用 CommonJs 作为模块规范，与 node.js 一致，同时支持 amd 异步 require，满足浏览器端按需加载问题
3. 结合 [travis-ci](http://travis-ci.org/fis-components/components) 和 [modules配置](https://github.com/fis-components/components/tree/master/modules), 将现有流行的组件自动转化成 CommonJs 规范，为 fis-components 平台贡献组件
4. 短路径支持，只需要组件名就能引用，满足组件间依赖引用，无需关心组件被安装在什么目录
5. 多平台支持，FIS 的组件存放平台可以是 github、gitlab 或者 lights，可以满足私有化需求

## 组件规范
一个模块可以由 HTML/TPL、JS、CSS、Images、Fonts 或其他前端资源的任意组合组成，每个模块必须包含一个 component.json 文件，可以指定依赖，加载其它模块。模块内部资源引用使用相对路径，模块中的 JS 要用 COMMONJS 规范或者 [AMD](https://github.com/amdjs/amdjs-api) 规范来解决,JS 的引用通过模块名来引用

## component.json
compnent.json 分两种，一种是组件里面的，用来说明组件的信息，一种放在 fis 项目中根目录下面，用来说明当前 fis 项目要依赖哪些组件，本身这个项目不是组件，也基本不会发布成组件
···
{
	"name": "dialog",
	"description": "一个简单的 dialog 组件",
	"version": "1.0.0",
	"main": "index.js",
	"protocol": "github",
	"github": {
		"author": "fis-components"
	},
	"dependencies": [
		"jquery@~1.9.0",
		"jquery-ui@*",
		"otherAuthor/xxxx",
		"gitlab:fis-dev/fis-report-record@master",
		"lights:pc-demo@latest"
	],
	"mapping": "./mapping.config.js",
	"shim": {
		"index.js": {
			"deps": ["a", "b"],
			"exports": "window.dialog"
		}
	}
}
···

### name
必填，模块名，只能包含小写英文字母、数字、中划线和下划线，它必须满足该正则 `/^[0-9a-z-_]+$/`

### description
必填，模块信息

### version
必填，采用 3 组数字组成，如：1.2.3，如果一个模块托管在 git 仓库，发布此模块的时应当创建版本号同名的 tag

### keywords
可以提供几个关键字协助平台搜索
```
"keywords": [
	"fisp",
	"pagination"
]
```

### main
模块包含 JS 时，应当通过 main 来指定主文件，此文件将会通过 require('模块名') 的时候被引用。默认值为 index.js

### dependencies
模块依赖，每个依赖应当按照以下形式指定，用中括号括起来的部分为选填
```
[GIT 平台:][作者名/]模块名[@版本]
[lights:][lights平台域名/]仓库名称[@版本]
```
···
"dependencies": [
	"github:xiangshouding/glob.js@0.1.9",
	"gitlab:fis-dev/fis-report-record@latest"
]
···

### protocol
平台名称，默认为 github，取决于将此组件存放于什么平台。当依赖中没有指定组件平台时，默认将采用此处所设置的值。

### github.author
当默认平台名称设置为 github 时，设置 github 仓库默认的用户名，默认值为 [fis-components](https://github.com/fis-components)。当依赖中省略了 github 用户名时，默认将采用此处所设置的值。类似的还有 `gitlab.author` `lights.repos`

### mapping
如果仓库中文件存放比较多或希望最终安装后路径与原存放路径不一致，可以通过指定此属性来设定。由于 mapping 的规则比较复杂，不适合用 json 文件来描述，所以需要借助一个 js 文件来配置
```
{
	"mapping": "./mapping.config.js"
}
```
mapping.config.js 中 `reg` 用来匹配仓库原文件路径，支持正则与 [glob](https://github.com/isaacs/node-glob) 两种规则。
`release` 用来指定安装后的存放路径，当 `reg` 为正则表达式时，可以通过 `$数字` 来代替分组捕获。
```
module.exports = function(options) {
	// options 为 components.json 的值。
	// 可以添加一些逻辑，最终返回一个 mapping 配置列表。
	return [
		{
			reg: /^\/dist\/(.*)$/i,
			release: "$1"
		}
	];
}
```

### shim
如果由于某种原因组件不能修改成 commonJS 规范或者 amd 规范，应该通过设定此属性来显式的指定组件依赖以及暴露对象
```
"shim": {
	"index.js": {
		"deps": ["a", "b"],
		"exports": "window.dialog"
	}
}
```

## 安装
1. 通过 `install` 命令行安装：`fis install jquery`
2. 根据当前或者父级 component.json 文件中  dependencies 数组来安装

### 路径说明
组件支持 lights、github 和 gitlab 三种平台，完整路径应该是这样的
```
github:fis-components/jquery@1.9.1
gitlap:fis-components/jquery@1.9.1
lights:lightjs.duapp.com/jquery@1.9.1
```
默认的平台为 github，用户名为 fis-compoennts，版本号默认是最新版，如果安装的是 fis 官方维护组件且最新版，则可以简化为 `jquery`

### 安装说明
目标组件将会安装在 `/comonents` 目录下面，如果组件已安装且版本正确，则不会重复安装，如果未安装或版本不正确，则会重新安装，相应的依赖也会同样检查并安装

### 根目录
fis install 执行时，可以不在 fis 项目目录，通过 --root 来设定。如果没有设定 --root，将会按以下规则来决定哪个目录为项目目录
1. 从当前目录开始向父级查找 fis-conf.js 文件，如果在某个目录下面查找到此文件，则把该目录认为项目目录
2. 所有的父级都没有找到，则把 cwd 命令行所在目录作为项目目录

### fis-conf.js 配置
fis install 也会读取 fis-conf.js 配置文件。有以下配置项：
```
component.dir
component.protocol
component.github.author
component.gitlab.author
component.lights.repos
```
component.dir 默认为 /components 即 fis 项目下面的 components 目录，其它和 component.json 中配置一样，只是 component.json 中配置优先级更高

## 使用
1. 根据组件名安装组件 `fis install jquery`. 默认被安装到了当前项目的 commonponents/jquery 目录下面
2. 在 js 里引用组件
```
var $ = require('jquery');
```
3. fis 编译，自动把路径变成编译后生成的组件路径

## 平台贡献
FIS 目前把一些常用的的组件，结合 travis-ci 自动转换成了 CommonJs 规范的组件，去掉了多余的文件。贡献组件时只需要提交[模块配置文件](https://github.com/fis-components/components/tree/master/modules)即可
```
module.exports = (function() {
    return [{
        // 目标组件的地址
        repos: 'https://github.com/defunkt/jquery-pjax.git',
        // 需要转换的版本
        version: 'v1.9.4',
        // 在 fis-components 中的名称
        name: 'jquery-pjax',
        // 主文件，请查看规范说明。当组件名引用的时候，默认引用的是此文件
        main: 'jquery.pjax.js',
        // 设置此组件的依赖。
        dependencies: [
            "jquery@>=1.8"
        ],
        // 配置文件信息，只把需要的文件 copy 过来
        mapping: [
            {
                reg: /\.min\.(js|css)$/,
                release: false
            },
            {
                reg: /^\/jquery\.pjax\.js$/,
                release: 'jquery.pjax.js'
            },
            {
                reg: /^\/README\.md$/,
                release: '$&'
            },
            {
                reg: '*',
                release: false
            }
        ],
        // 当目标不是 amd/umd 规范时，需要设置 shim 信息。
        shim: {
            "jquery.pjax.js": {
              "deps": ["jquery"]
            }
        }
    }]
})();
```

### 配置说明
* __repos__：目标组件的地址，只支持 http 地址
* __version__：版本号，目标组件必须存在这个 tag
* __name__：该组件的名称，可以与原仓库名不一致
* __main__：主文件，模块引用的是此文件。可选，默认是 index.js
* __dependencies__：数组格式，指定该组件需要的依赖，支持 [semverion](https://github.com/npm/node-semver)
* __mapping__：结合 `reg` 和 `release` 配置需要的文件
* __shim__：如果目标组件代码内部没有标记依赖，则需要此属性来转换成带带依赖的代码，比如 bootstrap 他本身是依赖 jquery，但是他的代码里面没用 require 进来。如果不配置此属性，当使用 bootstrap 的时候，不能自动的把 jquery 加载进来。
```
shim: [
    "bootstrap.js": {
      "deps": ["jquery"]
    }
]
```
* __extend__：当新加一个版本时，如果大部分的配置与之前配置的基本一致，可以通过 extend 关键字来继承

### shim 规则
shim 的 key 为文件的路径。 shim 的 value 中的属性说明
* `deps` 组件依赖列表
* `vars` 默认组件依赖不会赋值给任何局部变量。如果设置了 var 属性则会把依赖的结果赋值给局部变量。
* `exports` 设置暴露的内容。 比如：$， 当 require('bootstrap') 的时候，返回值就是 jquery
* `init` 可选， Function 类型，如果设置了 init 则将 此 function 的返回值作为 exports 的内容
* `content` 如果设置了此属性，整个文件的内容都会被替换成所设置的。以上三种配置都无效。
```
{
    shim: [
        'core/core.js': {
            "deps": ["jquery"],
            "vars": ["jQuery"],

            "replace": {
                "from": /window\.jQuery/g,
                "to": "jQuery"
            }
        }
    ]
}
```

### 本地验证
```
node test.js modules/xxx.js
```
