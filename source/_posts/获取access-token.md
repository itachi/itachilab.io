title: 获取access token
keywords: 获取access token
savedir: articles
categories:
  - weixin
date: 2016-01-02 18:30:16
updated: 2016-01-02 18:30:16
urlkey: access-token
---
## 获取access token
### 参数
```
grant_type: client_credential
appid: xxx
secret: xxx
```

### 接口
```
https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=xxx&secret=xxx
```

### 返回
```
{
access_token: "_6iJ0BMz-GOcVo0hdGBo9rFCE96-Hvl1NhE2u_PWV3ugs247qtKBiYl93kXnNQ0WUCqt_KWNnClQ2Ql6gHIQCZX2oZHUfVA0BOX6rSx_2l4VIQeAGAGTB",
expires_in: 7200
}
```

## 获得jsapi_ticket

### 参数
```
type: jsapi
access_token: xxx
```

### 接口
```
https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=xxx&type=jsapi
```

### 返回
```
{
errcode: 0,
errmsg: "ok",
ticket: "sM4AOVdWfPE4DxkXGEs8VM2lG1ypPxDvt5kH8iuv_rayrwHbtGZq-WWaMJjyaK3qCcF-OLM602S58kaJsD5pog",
expires_in: 7200
}
```

## 生成签名

### 参数
```
noncestr: Wm3WZYTPz0wzccnW（随机字符串）
jsapi_ticket: xxx
timestamp: （时间戳）
url: http://mp.weixin.qq.com?params=value（当前网页的URL，不包含#及其后面部分）
```

对所有参数按照字段名的 ASCII 码从小到大排序后，使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串，注意所有参数名均为小写字符。字段名和字段值都采用原始值，不进行 URL 转义，得到以下字符串
```
jsapi_ticket=xxx&noncestr=Wm3WZYTPz0wzccnW&timestamp=1414587457&url=http://mp.weixin.qq.com?params=value
```

对该字符串行sha1签名，得到签名
```
0f9de62fce790f9a083d5c99e95740ceb90c27ed
```

## 注意事项
* 签名用的 noncestr 和 timestamp 必须与 wx.config 中的 nonceStr 和 timestamp 相同。
* 签名用的 url 必须是调用JS接口页面的完整 URL。
* 出于安全考虑，开发者必须在服务器端实现签名的逻辑。

## 工具
[在线生成地址](http://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=jsapisign)