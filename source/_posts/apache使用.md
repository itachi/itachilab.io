title: apache使用
keywords: apache使用
savedir: articles
categories:
  - operation
date: 2015-12-01 00:43:34
updated: 2015-12-01 00:43:34
urlkey: apache-use
---
### 设置别名
当项目文件不在网站根目录又必须让其它机访问时，可以使用别名，如通过 http://localhost/svn 来访问 F:\svn，但 localhost 的根目录却是 E，可在 httpd.conf 或 http-vhosts.conf 文件中添加下面代码

```
alias /svn "F:/svn/" 
<directory "F:/svn/">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride all
    Order Allow,Deny
    Allow from all
</directory>
```

> 注意：需要在 httpd.conf 注释掉 `LoadModule alias_module modules/mod_alias.so`

### 权限控制
现有目录结构：Dropbox/Test，不想 Dropbox 公开，不过 Dropbox 里的测试目录 test 又要对外公开，这样可以在 Dropbox 里添加一个 .htaccess 文件，内容为

```
order deny,allow
Deny from all
Allow from 127.0.0.1
Allow from 192.168.50.144
```

这样只允许本地和一个IP访问，然后再在 Dropbox/test 目录里添加一个 .htaccess 文件，其内容是

```
order allow,deny
Allow from all
```

这样所有人都可以访问 Dropbox/test 目录了
