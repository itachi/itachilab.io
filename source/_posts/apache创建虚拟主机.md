title: apache创建虚拟主机
keywords: apache创建虚拟主机
savedir: articles
categories:
  - operation
date: 2015-11-29 23:18:34
updated: 2015-11-29 23:18:34
urlkey: apache-virtual-host
---
创建虚拟主机时，先使用 `NameVirtualHost` 指令配置服务器上的IP地址，可以用 `*` 指定所有IP地址。不同虚拟机由 `<VirtualHost>` 段配置，。`<VirtualHost>` 的参数与 `NameVirtualHost` 的参数必须是一样才能建立起关联，在每个 `<VirtualHost>` 段中，至少要有一个 `ServerName` 指令指定主机名和一个 `DocumentRoot` 指令指定主机路径。

当一个请求到达的时候，服务器会首先检查它是否使用了一个能和 `NameVirtualHost` 相匹配的IP地址。如果能够匹配，它就会查找每个与这个IP地址相对应的 `<VirtualHost>` 段，并尝试找出一个与请求的主机名相同的 ServerName` 或 `ServerAlias` 配置项。如果找到了，它就会使用这个服务器。否则，将使用符合这个IP地址的第一个列出的虚拟主机。

> 注意：当一个IP地址与NameVirtualHost指令中的配置相符时，主服务器中的DocumentRoot将永远不会被用到

```
NameVirtualHost *:80
<VirtualHost *:80>
ServerName localhost
DocumentRoot "D:/localhost/www/htdocs"
<Directory "D:/localhost/www/htdocs">
Options FollowSymLinks IncludesNOEXEC Indexes Includes
DirectoryIndex index.html index.htm default.htm index.php
AllowOverride all
Order Deny,Allow
Allow from all
</Directory>
</VirtualHost>

<VirtualHost *:80>
ServerName xxx.com
DocumentRoot "D:/localhost/www/htdocs/xxx"
<Directory "D:/localhost/www/htdocs/xxx">
Options FollowSymLinks IncludesNOEXEC Indexes Includes
DirectoryIndex index.html index.htm default.htm index.php
AllowOverride all
Order Deny,Allow
Allow from all
</Directory>
</VirtualHost>
```