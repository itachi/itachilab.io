title: linux查看日志常用命令
keywords: linux查看日志常用命令
savedir: articles
categories:
  - operation
tags:
  - memo
date: 2018-08-02 11:12:56
updated: 2018-08-02 11:12:56
urlkey: linux-log-command
---

## cat

一次显示整个文件
```
cat access.log
```

显示行号
```
cat -n access.log
```

将几个文件合并为一个文件
```
cat file1 file2 > file
```

清空文件
```
cat /dev/null > access.log
```

## tac

tac 是将 cat 反写过来，所以他的功能就跟 cat 相反。 cat 是由第一行到最后一行连续显示在萤幕上，而 tac 则是由最后一行到第一行反向在萤幕上显示出来

一次显示整个文件，从最后行开始
```
tac access.log
```

## head

显示文件的开头的内容。默认情况下只显示头10行内容
```
head access.log
```

显示头20行内容
```
head -n 20 access.log
```

显示头20个字符
```
head -c 20 access.log
```

## tail
显示文件的尾部的内容。默认情况下只显示最后10行内容
```
tail access.log
```

显示最后20行内容（-n可以省略）
```
tail -n 20 access.log
```

从第20行显示到最后（-n可以省略）
```
tail -n +20 access.log
```

监听文件变化
```
tail -n 100 -f access.log
或
tail -100f access.log
```

## more

以分页的形式展示所有内容，ctrl+f或空格键显示下一页，ctrl+b返回上一页，q退出，enter滚动一行，v进入vi
```
more access.log
```

从第100行开始显示
```
more +100 access.log
```

每页显示10行
```
more -10 access.log
```

## grep
对内容进行筛选

必须同时满足三个条件
```
grep word1 access.log | grep word2 | grep word3
```

满足任一条件
```
grep -E "word1|word2|word3" accees.log
```

## 配合使用

从第3000行开始，显示1000行。即显示3000~3999行
```
tail -n +3000 access.log | head -n 1000
```

显示1000行到3000行
```
head -n 3000 access.log | tail -n +1000
```

过滤内容并分页
```
cat access.log | grep "log" | more
```