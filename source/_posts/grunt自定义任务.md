title: grunt自定义任务
keywords: grunt自定义任务
savedir: articles
categories:
  - automation
tags:
  - grunt
date: 2015-11-28 15:23:06
updated: 2015-11-28 15:23:06
urlkey: grunt-task
---
## 单任务
```js
module.exports = function(grunt) {
    grunt.registerTask('mytask', '一个最简单的任务演示，根据参数打印不同的输出.', function(arg1, arg2) {
        if (arguments.length === 0) {
            grunt.log.writeln('任务' + this.name + ", 没有传参数");
        } else if (arguments.length === 1) {
            grunt.log.writeln('任务' + this.name + ", 有一个参数是" + arg1);
        } else {
            grunt.log.writeln('任务' + this.name + ", 有两个参数分别是" + arg1 + ", " + arg2);
        }
    });
};
```
输入 `grunt mytask`、`grunt mytask:var1`、`grunt mytask:var1:var2`查看结果

## 多任务
```js
module.exports = function(grunt) {
    grunt.initConfig({
        log: {
            t1: [1, 2, 3],
            t2: 'hello world',
            t3: false
        }
    });
 
    grunt.registerMultiTask('log', 'Log stuff.', function() {
        grunt.log.writeln(this.target + ': ' + this.data);
    });
};
```
输入  `grunt log`、`grunt log:t1`查看结果

## 任务交互
```js
module.exports = function(grunt) {
    grunt.registerTask('mytask', '一个最简单的任务演示，根据参数打印不同的输出.', function(arg1, arg2) {
        if (arguments.length === 0) {
            grunt.log.writeln('任务' + this.name + ", 没有传参数");
        } else if (arguments.length === 1) {
            grunt.log.writeln('任务' + this.name + ", 有一个参数是" + arg1);
        } else {
            grunt.log.writeln('任务' + this.name + ", 有两个参数分别是" + arg1 + ", " + arg2);
        }
    });
 
    grunt.registerTask('default', '默认的任务', function() {
        grunt.task.run('mytask:param1:param2')
    })
};
```
输入  `grunt` 查看结果

调用多个任务，以逗号分隔传给run方法即可，或者以数组形式
```js
grunt.registerTask('default', '默认的任务', function() {
    grunt.task.run('mytask1', 'mytask2')
    // 或者
    grunt.task.run(['mytask1', 'mytask2'])   
})
```