title: swig
keywords: swig
savedir: articles
categories:
  - templetecompile
tags:
  - templete
date: 2015-12-05 13:13:51
updated: 2015-12-05 13:13:51
urlkey: swig
---
## 安装

```
npm install swig --save
```

## node 端使用

```
var swig = require('swig');

// 先编译，再渲染
var tpl = swig.compileFile('/path/to/template.html');
console.log(tpl({ article: { title: 'Swig is fun!' }}));

// 立即渲染
console.log(swig.render('{% if foo %}Hooray!{% endif %}', { locals: { foo: true }}));
```

## 前端使用

```
<script src="swig.min.js"></script>
<script>
var data = {
	name: "hello"
}
var tpl = $("#tpl").html();
var html = swig.render(tpl,data);
$("#output").html(html);
</script>
```

前端很难通过路径名去查找对应文件，因此不能很好地处理 `extends` `include` 指令，解决方案是先把需要加载的模板先编译成js，指定一个全局函数名，引入该js文件，预加载，正常运行，以下以加载 `./myfile.html` 为例

### 命令行工具预编译
```
$ swig compile myfile.html --method-name=myfile > myfile.js
```

### 加载js
```html
<script src="swig.min.js"></script>
<script src="myfile.js"></script>
```

### 预加载
```js
swig.run(myfile, {}, '/myfile.html');
```

### 使用
```js
var tpl = '{% extends "./myfile.html" %}{% block content %}{{ stuff }}{% endblock %}';
var output = swig.render(tpl, { filename: '/tpl', locals: { stuff: 'awesome' }});
document.querySelector('#foo').innerHTML = output;
```

## 语法

### 变量
未定义的变量不会渲染，空串，`null`, `false`, `0` 会直接输出，变量可以接过滤器，也可以是一个函数

```
{{ title }}
{{ title|escape }}
{{ foo.bar }}
{{ foo['bar'] }}
{{ func() }}
```

### 逻辑块
每一个逻辑块需要用 end 闭合，[详细列表](http://paularmstrong.github.io/swig/docs/tags/)

```
{% block tacos %}
  //...
{% endblock tacos %}
```

### 注释

```
{#
This is a comment.
#}
```

### 空白符控制
默认渲染空白符，在逻辑块前/后添加 `-` 字符可以去掉空白符

```
// seq = [1, 2, 3, 4, 5]
{% for item in seq -%}{{ item }}
{%- endfor %}
// => 12345
```

### 模板继承
使用 `extends`、`block` 实现模板继承

#### layout.html
```
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>{% block title %}My Site{% endblock %}</title>
	{% block head %}
	<link rel="stylesheet" href="main.css">
	{% endblock %}
</head>
<body>
	{% block content %}{% endblock %}
</body>
</html>
```

#### index.html
```
{% extends 'layout.html' %}
{% block title %}My Page{% endblock %}
{% block head %}
	{% parent %}
	<link rel="stylesheet" href="custom.css">
{% endblock %}
{% block content %}
<p>This is just an awesome page.</p>
{% endblock %}
```

### 过滤器
多个过滤器可以一起使用，[过滤器列表](http://paularmstrong.github.io/swig/docs/filters/)

```
{{ names|striptags|join(', ')|title }}
```

## 参考
* [官网](http://paularmstrong.github.io/swig/)
* [文档](http://paularmstrong.github.io/swig/docs/)
* [fis-parser-render-swig](https://www.npmjs.com/package/fis-parser-render-swig)