title: mouseenter与mouseover
keywords: mouseenter与mouseover
savedir: articles
categories:
  - dom
date: 2017-12-11 16:48:05
updated: 2017-12-11 16:48:05
urlkey: mouseenter-mouseover
---
## 区别
`mouseenter` 不支持冒泡，鼠标进入目标元素后触发 `mouseenter`，离开目标元素触发 `mouseleave`，鼠标进入目标元素的子元素不会触发这两个事件，`mouseover` 支持冒泡，因此鼠标进入目标元素后会触发 `mouseover`，进入目标元素子元素也再触发 `mouseover`，从子元素离开会触发 `mouseout`，从本身离开也会触发 `mouseout`

{% img articleimg /postimg/mouseenter-mouseover/001.png %}

## relatedTarget
`relatedTarget` 事件属性返回与事件的目标节点相关的节点。

* 对于 `mouseover` 事件，该属性是鼠标指针移到目标节点上时所离开的那个节点
* 对于 `mouseout` 事件，该属性是离开目标时，鼠标进入的节点
* 对于其他类型的事件来说，这个属性没有用

{% img articleimg /postimg/mouseenter-mouseover/002.png %}

上图中，对于ul上添加的 `mouseover` 事件来说，`relatedTarget` 只可能是

* ul的父元素wrap（移入ul时,此时也是触发 `mouseenter` 事件的时候）
* ul元素本身（在其子元素上移出时）
* 子元素本身（直接从子元素A移动到子元素B）

## 模拟mouseenter和mouseleaver
可以对 relatedTarget 的值进行判断：如果值不是目标元素，也不是目标元素的子元素，就说明鼠标已移入目标元素而不是在元素内部移动。根据这个原理可以模拟 mouseenter

```
let contains = function (parent, node) {
  return parent !== node && parent.contains(node)
}
let emulateEnterOrLeave = function (callback) {
  return function (e) {
    let relatedTarget = e.relatedTarget
    if (relatedTarget !== this && !contains(this, relatedTarget)) {
      callback.apply(this, arguments)
    }
  }
}

let callback = function() {

}
document.querySelector("dom").addEventListener('mouseover', emulateEnterOrLeave(callback), false)
document.querySelector("dom").addEventListener('mouseout', emulateEnterOrLeave(callback), false)
```