title: centos安装jenkins
keywords: centos安装jenkins
savedir: articles
categories:
  - development
tags:
  - ci
date: 2018-03-13 12:17:21
updated: 2018-03-13 12:17:21
urlkey: jenkins-install
---

### war方式安装
```
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
java -jar jenkins.war
```

### yum安装
```
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
yum install jenkins
```

### 配置
vi /etc/sysconfig/jenkins
```
JENKINS_USER="root"
JENKINS_PORT="8080"
```

vi /etc/init.d/jenkins
在“/usr/bin/java”之前添加jdk安装路径，比如：`/usr/local/java/jdk_1.8.0/bin/java`

### 相关目录
Jenkins安装目录: /usr/lib/jenkins
Jenkins工作目录: /var/lib/jenkins(对应于环境变量 JENKINS_HOME)
构建项目源码目录：/var/lib/jenkins/workspace
日志默认路径：/var/log/jenkins/jenkins.log

### 启动
```
service jenkins start
```
或
```
systemctl enable jenkins
```

### 开机启动
```
chkconfig jenkins on
```

### 登录及配置
地址：http://localhost:8080/
初始密码：`cat /var/lib/jenkins/secrets/initialAdminPassword`

