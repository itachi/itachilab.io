title: Fiddler代理
keywords: Fiddler代理
savedir: articles
categories:
  - tool
tags:
  - experience
date: 2016-01-03 12:35:23
updated: 2016-01-03 12:35:23
urlkey: Fiddler-remote
---
Fiddler以代理服务器的形式工作，使用代理地址:127.0.0.1（即本机），默认端口:8888，能够记录所有http(S)通讯

## 开启
1. Tools-> Fiddler Options->Connections，勾选"Allow remote computers to connect" 
{% img articleimg /postimg/Fiddler-remote/001.jpg %}

2. Tools-> Fiddler Options->Connections，勾选"Capture HTTPS CONNECTs"，再勾选"Decrypt HTTPS traffic"、"Ignore server certificate errors"
{% img articleimg /postimg/Fiddler-remote/002.jpg %}

3. 手机安装HTTPS证书
假设本机IP为192.168.11.8，手机访问http://192.168.11.8:8888，点"FiddlerRoot certificate" 然后安装证书

> 如果不需要捕获HTTPS请求，可省略2，3步