title: chrome设置--disable-web-security解决跨域
keywords: chrome设置--disable-web-security解决跨域
savedir: articles
categories:
  - tool
tags:
  - experience
date: 2018-05-07 10:57:53
updated: 2018-05-07 10:57:53
urlkey: chrome-disable-web-security
---

通过使用chrome命令行启动参数可以改变chrome浏览器的设置，这里使用 `--disable-web-security` 参数。这个参数可以降低chrome浏览器的安全性，禁用同源策略，利于开发人员本地调试。

使用步骤如下：

1. 关闭所有的chrome进程
2. 新建一个chrome快捷方式，右键“属性”，“快捷方式”选项卡里选择“目标”，添加  `--args --disable-web-security --user-data-dir`，比如 `"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir`
3. 启动chrome，可以看到“您使用的是不受支持的命令行标记：--disable-web-security，稳定性和安全性会有所下降“，设置成功