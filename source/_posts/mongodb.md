title: mongodb
keywords: mongodb
savedir: articles
categories:
  - storage
tags:
  - mongodb
date: 2016-01-02 18:42:16
updated: 2016-01-02 18:42:16
urlkey: mongodb
---
## 下载 & 安装
选择合适的版本下载，[官方地址](https://www.mongodb.org/downloads)，运行 `.msi` 文件安装

## 启动 MongoDB
命令行模式下进入到MongoDB安装目录下的bin目录，运行
```
mongod --dbpath "d:\mongodb\data\db"
```
MongoDB需要一个目录来存储数据，这里以 `d:\mongodb\data` 为例

## 关闭 MongoDB
```
use admin
db.shutdownServer()
```

## MongoDB Shell
MongoDB Shell是MongoDB自带的交互式Javascript shell，用来对MongoDB进行操作和管理的交互式环境

命令行模式下进入到 MongoDB 安装目录下的bin目录，运行
```
mongo
```
默认会链接到test数据库
> 注意：需要先开启 MongoDB 后才能连接并使用，期间保持 `mongod` 运行

## 安装 MongoDB 服务
新建一个配置文件 `C:\mongodb\mongod.cfg` (目录可自定义)，配置文件中需要指定日志目录和数据目录(相应目录如没有需要手动创建)，格式如下
```
systemLog:
    destination: file
    path: c:\data\log\mongod.log
storage:
    dbPath: c:\data\db
```

命令行模式下进入到 MongoDB 安装目录下的 bin 目录，运行
```
mongod --config C:\mongodb\mongod.cfg --install
or
mongod --serviceName MongoDB --serviceDisplayName MongoDB --logpath c:\MongoDB.Log --dbpath c:\MongoDB --directoryperdb --install
```

## 卸载服务
```
mongod --remove
```

## 开启服务
```
net start MongoDB
```

## 停止或删除服务
```
net stop MongoDB
```

另外，也可以使用系统自带 `sc` 指令来创建 MongoDB 服务，指令如下
```
sc.exe create MongoDB binPath= "C:\mongodb\bin\mongod.exe --service --config=\"C:\mongodb\mongod.cfg\"" DisplayName= "MongoDB" start= "auto"
```