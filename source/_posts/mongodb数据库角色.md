title: mongodb数据库角色
keywords: mongodb数据库角色
savedir: articles
categories:
  - storage
tags:
  - mongodb
date: 2016-01-02 18:42:16
updated: 2016-01-02 18:42:16
urlkey: mongodb-role
---

### 内建的角色
* __数据库用户角色__：read、readWrite;
* __数据库管理角色__：dbAdmin、dbOwner、userAdmin；
* __集群管理角色__：clusterAdmin、clusterManager、clusterMonitor、hostManager；
* __备份恢复角色__：backup、restore；
* __所有数据库角色__：readAnyDatabase、readWriteAnyDatabase、userAdminAnyDatabase、dbAdminAnyDatabase
* __超级用户角色__：root // 这里还有几个角色间接或直接提供了系统超级用户的访问（dbOwner 、userAdmin、userAdminAnyDatabase）
* __内部角色__：__system

### 角色说明
* __Read__：允许用户读取指定数据库
* __readWrite__：允许用户读写指定数据库
* __dbAdmin__：允许用户在指定数据库中执行管理函数，如索引创建、删除，查看统计或访问system.profile
* __userAdmin__：允许用户向system.users集合写入，可以找指定数据库里创建、删除和管理用户
* __clusterAdmin__：只在admin数据库中可用，赋予用户所有分片和复制集相关函数的管理权限。
* __readAnyDatabase__：只在admin数据库中可用，赋予用户所有数据库的读权限
* __readWriteAnyDatabase__：只在admin数据库中可用，赋予用户所有数据库的读写权限
* __userAdminAnyDatabase__：只在admin数据库中可用，赋予用户所有数据库的userAdmin权限
* __dbAdminAnyDatabase__：只在admin数据库中可用，赋予用户所有数据库的dbAdmin权限。
* __root__：只在admin数据库中可用。超级账号，超级权限