title: DOM事件模型
keywords: DOM事件模型
savedir: articles
categories:
  - dom
date: 2015-10-03 16:55:28
updated: 2015-10-03 16:55:28
urlkey: domEvent
---
## 原始事件模型
注册原始事件模型有两种方式，一种是通过定义 HTML 属性来设置事件句柄，一种是在 javascript 设置事件属性，因为文档上的元素都有对应的一个DOM对象，这个对象映射了绝大部分元素属性，包括事件句柄属性，但两种方式使用方法稍有不同，事件句柄接的是 javascript 语句，以分号隔开，而事件属性接的是一个函数，如下
```html
<input type="text" onclick = "valid();" />
```

```javascript
function func(){ //code }
ele.onclick = func;
ele2.onclick = function(){ //code }
```

## 主动调用
因为事件属性是一个函数，所以可以通过主动调用去触发事件，但这种情况不能完全模拟真实环境，所以只会执行定义为那个属性的函数，但不能触发浏览器的默认操作，比如主动触发定义在a标签上的一个单击事件，只执行函数，页面不会跳转，当需要模拟真实情况时，可以这样做：

```javascript
var oldhander = ele.onclick;
function newhander() { //code }
ele.onclick = function(){ oldhander(); newhander();}
ele.onclick();
```

## 2级事件模型
2级事件模型中，事件传播分为三个阶段，第一，捕捉阶段，事件从 Document 对象沿着文档树向下传播给目标节点，如果某一祖先元素（不是目标本身）注册了专门的捕捉事件句柄，则在事件传播过程中会运行这些句柄，第二个阶段是发生在目标节点身上，执行事件句柄函数，第三个阶段叫冒泡，这个阶段事件从目标元素向上传播回 Document 对象，中途可能会再次触发（如果有祖先元素定义了相同事件类型句柄）。注意：所有事件都有捕捉阶段，但冒泡都不一定，一般来说原始输入事件起泡，高级语义事件不泡冒。

高级事件模型的传播阶段看起来比较复杂，但应用在某些场景却可以明显提高效率，比如需要为每个 p 绑定一个 mouseover 事件，不必每一个 p 都注册一个，只需要在 Document 对象上注册一个 mouseover，然后在事件起冒阶段作相应处理即可。这种定义事件的方法也叫事件代理。

### 注册方法
2级事件模型是调用对象的 `addEventListener()` 方法来注册事件，第一个参数为事件类型，第二个为处理函数，该函数只接收一个参数，为 Event 对象，第三个参数为布尔值，决定了该事件句柄用于捕捉或是常规事件，值为 `true` 为捕捉事件，为 `false` 时为常规事件
```javascript
ele.addEventListener("mousedown",func,false);
```

### 多个处理函数
2级模型是调用方法来注册事件，而不是通过元素属性来注册事件，所以本质上是不同的，通过属性定义事件句柄只能绑定一个处理函数，因为属性值只有一个，而用 `addEventListener()` 可以注册多个处理函数，另外要注意：当为一事件注册了多个处理函数时，不一定是按注册的顺序来执行。

### this关键字
和原始事件模型不同，2级DOM明确了事件监听器是对象而不是简单的函数，并且没有指明 this 关键字的值，虽然句柄被调用时 this 的确引用了注册了这个句柄的对象，但这种引用有不确定性，所以可以使用 Event 对象的属性 `target` 来替代。

### 混合事件模型
为了向后兼容，支持2级事件模型的浏览器都支持0级事件模型，即0级事件模型也可以使用 Event 对象，具体表现为：当通过 HTML 属性定义事件句柄时，该句柄被隐式地转换为一个函数，这个函数有一个名叫隐性的参数，引用事件对象，相当于在该事件处理函数中可以通用这个隐性参数直接拿到 Event 对象。当通过定义 HTML 属性来设置事件句柄时，这时指定的是js语句，此时无法获取 Event 对象，但还是有其它方法来获取 Event 对象。
```javascript
ele.onclick = function(e){ e.target }
```

## IE事件模型及Event对象
IE事件模型同样定义了 Event 对象，但不是把该对象作为参数传递给事件句柄，而是通过 window 对象的 event 属性来引用，意味着用 `window.event 或直接用 `event` 来引用，IE中的 Event 对象和标准的 Event 对象有大量相似的属性，比如 `srcElement` 对应 `target` 属性等。为了拿到 Event 对象，兼容的写法通常是以下两种：
```javascript
ele.onclick = function(e){
	var e = event || window.event;
	// or if(!e) e = window.event;
}
```

### 注册方法
IE事件模型提供了 `attachEvent()` 方法和 `detachEvent()` 方法来注册来取消事件，因为IE事件模型不支持捕捉阶段，所以 `attachEvent()` 只有两个参数，即事件类型和处理函数，另外要注意的是用 `attachEvent()` 注册的函数将作为全局函数调用，所以 this 指向的是 window 对象，并且 `attachEvent()` 允许同一个处理函数注册多次，当事件触发时会重复调用，而标准事件模型则会忽略掉重复的处理函数。
```javascript
ele.attachEvent("onclick",hander);
```

### 事件冒泡
IE没有事件捕捉阶段，但对于原始或输入事件还是有冒泡阶段，和标准事件型不同的只是阻止事件冒泡的方法，标准事件模型用的是 `e.stopPropagation()` 方法，而IE中是 `window.event.cancelBubble = true`

### this关键字
前面提过 `attachEvent()` 定义的处理函数中 this 指向的是 window 对象，而IE事件对象中的 `srcElement` 等价于 `target` 属性而不是 `currentTarget`，`currentTarget` 指向的是真正绑定了事件的元素，而IE Event 对象是没有提供指向该元素的属性的，所以，当处理有冒泡情况的事件并需要拿到真正绑定事件元素时，可以用以下方法（该方法在IE6下有内存漏露的问题）
```javascript
function hander(){ //这里可以用this关键字引用元素自身 }
	ele.onmouseover = hander;
	ele.attachEvent("onmouseover",function(){
	     hander.call(ele,event);
})
```

## 铵键事件
有3种招按键事件类型，keydown，keypress，keyup，一次典型的按键会产生这三种事件，有时候在 keydown 和 keyup 之间会有多次 keypress 事件，这取决于浏览器和操作系统。

 keypress 会在相关的事件对象包含产生的实际字符编码，而 keydown 和 keyup 是较为底层的，事件对象中包含的是一个和硬件编码相关的*按键码*，对于ASCII字符集中的字母和数字集，这个*按钮码*和ASCII码相同，但不会完全处理，比如当同时按下 shift 和 2， keydown 得到 *shift+2* 的按钮事件，而 keypress 得到一个可打印的 *@*。

不能打印的功能按键，如 Backspace，Enter，它们会产生 keydown 和 keyup 事件，一些浏览器会产生 keypress 事件，但IE中，只有当按键有一个ASCII码时，它才会产生 keypress 事件。

Alt，Ctrl，Shift 和一个按钮一起按下，通过事件对象的 altKey，ctrlKey 和 shiftKey 来表示，只有一个例外，Alt 按键组合在IE中被认为是无法打印的，因为不会产生 keypress 事件

在 firefox，按键事件对象中定义了两个属性，keyCode 存储了一个按键码，和 ASCII 对应，charCode 存储了实际产生的编码，在IE中，只有一个 keyCode，它的解释取决于事件的类型，对于 keydown 事件是一个按键码，对于 keypress 是一个实际字符码。另外，firefox 有 which 属性，作用和 keyCode 类似

当想获取铵键名时，可用 `String.fromCharCode(keyCode)`
```javascript
function keyUp(e) {
	var currKey = 0,e = e || event;
	currKey = e.keyCode || e.which || e.charCode;
	var keyName = String.fromCharCode(currKey);
	alert("按键码: " + currKey + " 字符: " + keyName);
}
document.onkeyup = keyUp;
```

代码示例(js)
```javascript
document.onkeydown = function(e) {
	var e = e || window.event;
	var code = e.keyCode;
	if (code == 37) {
		alert("left");
	}
	if (code == 39) {
		alert("right");
	}
}
```

代码示例(jquery)
```javascript
$(document).keydown(function(e) {
	var code = e.which;
	if (code == 37) {
		alert("left");
	}
	if (code == 39) {
		alert("right");
	}
})
```