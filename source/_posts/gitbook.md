title: gitbook
keywords: gitbook
savedir: articles
categories:
  - thirdparty
tags:
  - git
date: 2016-01-03 12:05:36
updated: 2016-01-03 12:05:36
urlkey: gitbook
---
## 简介
[GitBook](https://www.gitbook.com/) 用于使用 markdown 快速书写电子书，可在线编辑内容或本地利用 git 更新，提供在线阅读地址，也可以绑定自定义域名，具体使用查看[官方文档](https://www.gitbook.com/book/gitbookio/documentation)

## 注册
访问 [GitBook](https://www.gitbook.com/) 注册帐号，也可以直接用 github 帐号登录，如果使用 github 帐号登录，登录后需要在 settings 里填字 email 并通过验证，还需要设置密码以便在本地使用 git 拉取项目

## 创建book
提供四种方式创建book，成功创建后获得一个 git 地址，如 `https://git.gitbook.com/user/bookname.git`

* basic：具有基本初始化文件的空项目
* science：内建语法支持，适用于编写学术文章或数字类电子书
* github：需要和一个 github 仓库建立关联并获取权限，可以利该 github 项目来同步内容
* import：利用另一个 git 仓库来导入并创建项目

> 注意：最好使用 basic 创建项目，初始化文件后再和 github 仓库建立关联

## 项目结构
一个book项目由根目录下几个文件来定义

### README.md
定义书的简介，也可以使用其它文件来代替它，需要在 `book.json` 里定义
```
{
    "structure": {
        "readme": "myIntro.md"
    }
}
```

### SUMMARY.md
定义书的章节，由链接列表组成
```
# Summary
* [Part I](part1/README.md)
    * [Writing is nice](part1/writing.md)
    * [GitBook is nice](part1/gitbook.md)
* [Part II](part2/README.md)
    * [We love feedback](part2/feedback_please.md)
    * [Better tools for authors](part2/better_tools.md)
```

### LANGS.md
定义多语言
```
* [English](en/)
* [French](fr/)
* [Español](es/)
```

### GLOSSARY.md
定义术语，gitbook 会根据这个文件为定义的术语建立索引并在文章里自动高亮
```
# term
Definition for this term

# Another term
With it's definition, this can contain bold text and all other kinds of inline markup .
```

### book.json
项目配置文件，可配置 `title` `description` `styles` 等字段，[详细文档](https://help.gitbook.com/format/configuration.html)

### .gitignore
定义忽略的文件，.bookignore 和 .ignore 文件起同样作用

## 设置封面
封面为 `cover.jpg` 和 `cover_small.jpg` 决定，尺寸最好为 1800x2360 和 200x262

## 编写内容
每一页对应一个 md 文件，并在 SUMMARY.md 里手动添加链接，文章内容由 markdown 格式编写，查看[语法](https://help.gitbook.com/format/markdown.html)

编辑文章时可以使用模板语言，GitBook 使用 [Nunjucks](https://mozilla.github.io/nunjucks/) and [Jinja2](http://jinja.pocoo.org/) 语法，查看[语法](https://help.gitbook.com/format/templating.html)

在文章内容中可以引用其它页的内容，甚至另一本书的内容，并支持继承语法，查看[语法](https://help.gitbook.com/format/conrefs.html)
```
{% include "./test.md" %}
{% include "git+https://github.com/GitbookIO/documentation.git/README.md#0.0.1" %}
```

## 使用插件
利用第三方插件增强功能，[插件列表](http://plugins.gitbook.com/)
```
{
    "plugins": ["myPlugin", "anotherPlugin"]
}
```

## 自定义样式
通过配置文件 `styles` 字段来自定义文章样式
```
{
    "styles": {
        "website": "styles/website.css",
        "ebook": "styles/ebook.css",
        "pdf": "styles/pdf.css",
        "mobi": "styles/mobi.css",
        "epub": "styles/epub.css"
    }
}
```

## 构建生成
通过本地 git 推送或在线编辑后，GitBook 会自动为 website json epub pdf 四种方式并生成历史
```
---从新创建---
touch README.md SUMMARY.md
git init
git add README.md SUMMARY.md
git commit -m "first commit"
git remote add gitbook https://git.gitbook.com/{{UserName}}/{{Book}}.git
git push -u gitbook master
---或利用已有仓库---
git remote add gitbook https://git.gitbook.com/{{UserName}}/{{Book}}.git
git push -u gitbook master
```

如果项目关联了 github 项目，但 github 有推送时也会同步并自动构建

## 整合github
### 授权
gitbook 提供四种授权方式来和 github 建立关联（在account setting中设置）

* __Default permissions__: 默认授权，允许日常登录
* __Access to webhook__: 允许设置 [webhook](https://help.gitbook.com/github/index.html#webhooks)
* __Access to public repositories__: 允许使用在线编辑器修改仓库
* __Access to private repositories__: 允许使用在线编辑器修改私人仓库

### 链接到 github 仓库
在 book 管理面板的 GitHub 项可以设置关联到某一 github 仓库，链接后使用在线编辑器更改的内容会自动同步到 github 仓库，该 github 仓库有内容推送后也会自动同步到 gitbook

### 导出内容
利用[在线工具](https://import.github.com/new)可以将内容导到某一 github 仓库

## 命令行工具
可以使用 node 工具 [gitbook](https://www.npmjs.com/package/gitbook#output-formats) 来本地构建

### 安装
```
npm install gitbook-cli -g
```

### 命令
```
gitbook init
gitbook serve
gitbook build
```