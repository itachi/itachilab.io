title: exporess坑坑
keywords: exporess坑坑
savedir: articles
categories:
  - nodejs
tags:
  - experience
date: 2018-01-25 16:26:41
updated: 2018-01-25 16:26:41
urlkey: express-experience
---
 ## 错误捕获
 中间件函数中如果有js报错，会被错误中间件捕获，可以看作是中间件函数默认是包侌在try中运行的

 ## 重复请求
 写一个简单的路由，直接返回一个字符串，发现每次在浏览器刷新页面后，路由函数都执行了两次，网上说是304缓存问题，F12看下请求确实是304状态，禁用缓存
```
res.header('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1
res.header('Pragma', 'no-cache') // HTTP 1.0
res.header('Expires', '0'); // Proxies
```
问题依然存在，证明和缓存无关

怀疑是chrome问题，试下firefox，正常，百度关键词改为chrome两次请求，得到解决方案，是插件jsonview导致中，需要修改插件的配置，将Use safe method to parse HTTP response(*) 的勾去掉