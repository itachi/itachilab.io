title: SSH免密登录
keywords: SSH免密登录
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-04-02 11:45:14
updated: 2018-04-02 11:45:14
urlkey: ssh-login
---

## 客户端连接服务端

客户端-生成公钥和私钥
```
ssh-keygen -t rsa
```
也可以直接用 `ssh-keygen`，执行完后默认在 ~/.ssh目录生成id_rsa和id_rsa.pub两个文件

可以带上注释，但验证时这部分是没有作用的，只是一个说明文本
```
ssh-keygen -C "glowd@gmail.com"
```

服务端-设置权限
```
cd ~
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

客户端-添加公钥到服务器
```
ssh-copy-id [-P port] root@192.168.208.136
或
scp [-P port] ~/.ssh/id_rsa.pub root@192.168.208.136:~
cat ~/id_rsa.pub >> ~/.ssh/authorized_keys（服务端执行）
```

连接
```
ssh root@192.168.208.136
```
可以在本地~/.ssh下新建config文件，内容为
```
Host 192.168.208.136
User root 
Port 22
```
这样就可以直接用 `ssh 192.168.208.136` 连接

## 服务端连接其它服务端
openssh-server的功能主要是作为一个服务运行在后台，如果这个服务开启，我们就可以用一些远程连接工具来连接centos。因为minimal版本自带openssh-server，所以XShell可以连上centos，而openssh-client的功能是可以作为一个客户端连接上openssh-server，但是Centos6.4的minimal版本不包括openssh-client，所以要先安装

```
yum -y install openssh-clients
```

以连接github为例，服务端生成公钥私钥对，将公钥添加到github，连接github
```
ssh -T git@github.com
```
因为现在ssh还不能识别这个密匙key，会提示`Permission denied (publickey).`，需要手动加上上面生成的key
```
ssh-add ~/.ssh/id_rsa
ssh-add -l
```
成功连接会提示`Hi Glowdable! You've successfully authenticated, but GitHub does not provide shell access.`，因为GitHub没有提供shell访问，但是不影响git操作

## 问题
如果提示 `It is required that your private key files are NOT accessible by others`，是因为证书权限问题，需要改为600
```
chmod 600 ~/.ssh/*
```

如果操作正常还是提示需要登录密码，可以把客户端.ssh目录下的known_hosts删掉再重新连接