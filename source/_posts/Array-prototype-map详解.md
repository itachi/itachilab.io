title: Array.prototype.map详解
keywords: Array.prototype.map详解
savedir: articles
categories:
  - js
tags:
  - knowledge
date: 2016-01-02 18:51:17
updated: 2016-01-02 18:51:17
urlkey: array-prototype-map
---
## 语法
```
array.map(callback[, thisArg]);
```
给数组中的每个元素都按顺序调用一次 callback 函数。callback 每次执行后的返回值组合起来形成一个新数组。 callback 函数只会在有值的索引上被调用；那些从来没被赋过值或者使用 delete 删除的索引则不会被调用。

callback 函数会被自动传入三个参数：数组元素，元素索引，原数组本身。

如果 thisArg 参数有值，则每次 callback 函数被调用的时候，this 都会指向 thisArg 参数上的这个对象。如果省略了 thisArg 参数,或者赋值为 null 或 undefined，则 this 指向全局对象 

当一个数组运行 map 方法时，数组的长度在调用第一次 callback 方法之前就已经确定。在 map 方法整个运行过程中，不管 callback 函数中的操作给原数组是添加还是删除了元素。map 方法都不会知道，如果数组元素增加，则新增加的元素不会被 map 遍历到，如果数组元素减少，则 map 方法还会认为原数组的长度没变，从而导致数组访问越界。如果数组中的元素被改变或删除，则他们被传入 callback 的值是 map 方法遍历到他们那一刻时的值。

> 注意：map 方法属于 es5 标准，在 IE8 及以下浏览器不支持，可使用 [es5-shim](https://github.com/es-shims/es5-shim) 或 underscore 解决

## 案例
在一个String上使用map方法获取字符串中每个字符所对应的 ASCII 码组成的数组
```
var result = Array.prototype.map.call("Hello world", function(x, index, arr) {
	console.log(arr);
	return x.charCodeAt(0);
});
//Outputs: [72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100] 
```

不能用 `var result = ["1", "2", "3"].map(parseInt);` ，因为 map方 法的 callback 函数接收三个参数，而 parseInt 最多只能接收两个参数，以至于第三个参数被直接舍弃，与此同时，parseInt 把传过来的索引值当成进制数来使用.从而返回了 NaN，正确使用方式如下
```
var result = ["1", "2", "3"].map(function(val) {
    return parseInt(val, 10);
});
//Outputs: [1, 2, 3] 
```