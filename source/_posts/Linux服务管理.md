title: Linux服务管理
keywords: Linux服务管理
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-28 09:29:44
updated: 2018-03-28 09:29:44
urlkey: linux-service
---

## 服务分类
Linux系统的服务按管理方式主要有两大类：stand-alone和super-daemon，即独立管理服务和统一管理服务。

### stand-alone
这种类型的服务机制较为简单，可以独立启动服务。典型的stand-alone服务有：httpd、和ftp。其特点是

* 可以自行独立启动，无需通过其他机制的管理
* 一旦启动加载到内存后，就会一直占用内存空间和系统资源，知道该服务被停止
* 对client的请求有更快的响应速度


### super-daemon
这种管理机制通过一个统一的daemon来负责启动、管理其他服务。典型的super-daemon服务有：telnet等。在 CentOS6.X 中这个super-daemon就是xinetd这个程序。特点有：

* 所有的服务由xinetd控管，因此对xinetd可以有安全控管的机制，如网络防火墙
* clinet请求前，所需服务是未启动的；直到client请求服务时，xinetd才会唤醒相应服务；一旦连接结束后，相应服务会被关闭。所以super-daemon方式不会一直占用系统资源
* 既然有请求才会去启动服务，所以server端的响应速度自然不如stand-alone方式来得快

> 注意：xinetd服务本身是一个独立的服务（stand-alone），它用于管理其他服务

## 启动方式

Linux中的不同的服务都有个自的启动脚本，以在服务启动前进行环境的检测、配置文件的分析、PID文件的规划等相关操作。stand-alone方式和super-daemon方式的启动脚本放置位置有所不同，启动方式自然也是有区别的。

### stand-alone
stand-alone方式的启动脚本位于/etc/init.d/目录下，可以直接调用，直接调会显示服务的使用方法
```
/etc/init.d/crond
Usage: /etc/init.d/crond {start|stop|status|restart|condrestart|try-restart|reload|force-reload}
```

加上对应的方法名便可以使用该服务了，比如开启服务
```
/etc/init.d/crond start
```

还可以使用service这个脚本，不过service只能用来管理stand-alone的服务，而且不是所有Linux发行版都有
```
# 显示服务使用方法
service crond

# 开启/重启/停止
service crond start / restart / stop

# 查看服务状态
service crond status

# 查看所有stand-alone服务的状态
service --status-all
```

### super-daemon
super-daemon方式的启动脚本放在了/etc/xinetd.d/中，启动方法是先编辑启动脚本，将需要开启的服务disable一项改为no，然后重启xinetd：/etc/init.d/xintd restart

查看服务的启动脚本
```
[root@localhost xinetd.d]# grep -i 'disable' /etc/xinetd.d/*
......
/etc/xinetd.d/daytime-dgram:    disable     = yes
/etc/xinetd.d/daytime-stream:   disable     = yes
/etc/xinetd.d/discard-dgram:    disable     = yes
/etc/xinetd.d/discard-stream:   disable     = yes
/etc/xinetd.d/echo-dgram:   disable     = yes
/etc/xinetd.d/telnet:   disable = no
......
```

使用chkconfig可以看到xinetd based services一项中服务的启动情况
```
chkconfig
xinetd based services:
    echo-dgram:     off
    echo-stream:    off
    rsync:          off
    tcpmux-server:  off
    telnet:         on
```

## 开机启动
### 软链接方式
在Linux中有7种运行级别（可以在/etc/inittabe文件中设置），每种运行级别分别对应着/etc/rc.d/rc[0-6].d 这7个目录。 7个目录中，每个目录分别存放着对应运行级别加载时需要关闭或启动的服务，K开头的脚本文件代表运行级别加载时需要关闭的，S开头的代表需要启动执行的。当需要开机启动某脚本时，在/etc/rc[级别].d 中建立软链接即可
```
ln -s /etc/init.d/sshd /etc/rc.d/rc3.d/S100ssh
```

这种方式比较繁琐，如果需要在多个运行级别下设置自启动，则需要建立多个软链接，适用于自定义的服务脚本

### chkconfig方式
chkconfig方式多用于系统已存在的服务。比如ftp、samba、ssh、httpd等等。在默认情况下，chkconfig会自启动2345这四个级别，如果想自定义可以加上 `–level` 选项。`–list` 选项课查看指定服务的启动状态，chkconfig不带任何选项则查看所有服务的状态。

```
chkconfig 服务名  on 
chkconfig 服务名  off
chkconfig --level 35 服务名 on
chkconfig --list 服务名
```

> 注意：chkconfig只能查看RPM包安装的服务，因为chkconfig读取的是默认的安装目录（/etc/init.d），而源码包安装的服务，安装位置是可以自己指定的，一般在/usr/local/下

### 源码包的自启动
在 /etc/rc.d/rc.local 加入服务的启动命令
```
/usr/local/apache2/bin/apachectl start
```

让源码包的服务能被service命令管理识别，可以创建一个软连接
```
ln -s /usr/local/apache2/bin/apachectl /etc/init.d/apache
```

### ntsysv方式
ntsysv和chkconfig其实是一样的，只不过加上了图形而已。启动ntsysv用两种方式，一是直接在命令行中输入ntsysv，二是使用setup命令，然后选择系统服务

默认情况下，当前运行级别为多少，在ntsysv中设置的启动服务的级别便是多少，如果想自定义运行级别可使用 `ntsysv –level 2345` 方式
```
ntsysv --level 2345
```


## 服务脚本
编写服务脚本，通常需要start|stop|restart|status功能，将脚本复制至/etc/init.d文件夹中，并添加至chkconfig，这样在2345的运行级别下服务便可以自启动，而且可以在任意目录用service命令启动服务
```
cp myservice /etc/init.d
chkcofig --add myservice
chkconfig myservice on
service myservice start
```