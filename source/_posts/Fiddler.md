title: Fiddler
keywords: Fiddler
savedir: articles
categories:
  - tool
tags:
  - summary
date: 2015-12-03 23:22:26
updated: 2015-12-03 23:22:26
urlkey: fiddler
---
## 工作原理
Fiddler 以代理web服务器的形式工作，代理地址:127.0.0.1, 端口:8888. 启动 Fiddler 后会自动设置IE代理， 退出后自动注销代理

{% img articleimg /postimg/fiddler/001.png %}
{% img articleimg /postimg/fiddler/002.png %}

能支持HTTP代理的任意程序的数据包都能被 Fiddler 嗅探到，Fiddler 的运行机制其实就是本机上监听8888端口的HTTP代理。 Fiddler 启动的时候默认把IE的代理设为了127.0.0.1:8888，如果其它程序不是默认使用IE的代理的，需要手动设置代理才可以监听数据

> 注意：如果 Fiddler 非正常退出，这时候因为 Fiddler 没有自动注销，可能会造成网页无法访问。解决的办法是重新启动下 Fiddler.

## 捕获HTTPS会话
默认下，Fiddler 不会捕获 HTTPS 会话，需要打开菜单 Tool -> Fiddler Options -> HTTPS tab 完成以下三步设置（后两步选择Yes）

{% img articleimg /postimg/fiddler/003.png %}
{% img articleimg /postimg/fiddler/004.png %}
{% img articleimg /postimg/fiddler/005.png %}

## QuickExec命令行
Fiddler 的左下角有一个命令行工具叫做 QuickExec，允许直接输入命令

{% img articleimg /postimg/fiddler/006.png %}

常见得命令有

* __help__: 打开官方的使用页面介绍，所有的命令都会列出来
* __cls__: 清屏（Ctrl+x 也可以清屏）
* __select__: 选择会话的命令
* __?.png__: 用来选择png后缀的图片
* __bpu__: 截获 request


## 设置Request断点
通过设置断点，可以修改 httpRequest 的任何信息包括 host, cookie 或者表单中的数据

{% img articleimg /postimg/fiddler/007.png %}

设置断点有两种方法

* __第一种__: 菜单 Rules -> Automatic Breakpoint -> Before Requests，这种方法会中断所有的会话，菜单 Rules-> Automatic Breakpoint -> Disabled 用来清除
* __第二种__: 在命令行中输入命令: `bpu www.baidu.com`，这种方法只会中断 www.baidu.com，命令 `bpu` 用来清除中断

### 实例，模拟博客园的登录

1. 打开博客园的登录界面  https://passport.cnblogs.com/login.aspx（该链接是https请求，需要先设置捕获https请求）
2. 打开Fiddler,  在命令行中输入 `bpu http://passport.cnblogs.com/login.aspx`，设置该链接断点，这时再刷新该页面会被 fiddler 中断

{% img articleimg /postimg/fiddler/008.png %}

3. 输入错误的用户名和密码，提交表单，这样同样会被中断，但请求的数据会被 fiddler 捕获
4. Fiddler 已经了中断这次会话，选择被中断的会话，点击 Inspectors tab 下的 WebForms tab ，可以看到发送过去的表单信息

{% img articleimg /postimg/fiddler/009.png %}

5. 在 fiddler WebForms tab下手动修改用户名密码为正确值，然后点击继续运行

{% img articleimg /postimg/fiddler/010.png %}

6. 结果：页面提交了并正确登录

## 设置Response断点
Fiddler 同样能修改 Response 的信息，应用思路和上面一样

{% img articleimg /postimg/fiddler/011.png %}

也有两种方式

* __第一种__: 菜单 Rules -> Automatic Breakpoint -> After Response，这种方法会中断所有的会话，Rules -> Automatic Breakpoint -> Disabled 用于清除中断
* __第二种__: 在命令行中输入命令: `bpafter www.baidu.com`，这种方法只会中断www.baidu.com，命令 `bpafter` 用于清除中断

## 自带编码小工具
点击 Fiddler 工具栏上的 TextWizard, 这个简单的文本编辑器提供编码功能

{% img articleimg /postimg/fiddler/012.png %}

## script系统
fiddler 复杂的脚本系统，可以自己编写脚本去运行

首先先安装 SyntaxView 插件，Inspectors tab -> Get SyntaxView tab -> Download and Install SyntaxView Now

{% img articleimg /postimg/fiddler/013.png %}

安装成功后 Fiddler 就会多了一个 Fiddler Script tab 用来编写脚本

## 常见问题
### 调试本地网页
默认 Fiddler 是不能嗅探到 localhost 的网站。不过只要在 localhost 后面加个点号，Fiddler 就能嗅探到，比如原本地址是 `http://localhost/Default.aspx`，加个点号后，变成 `http://localhost./Default.aspx` 就可以了

### Response乱码
有时看到 Response 中的HTML是乱码的，这是因为 HTML 被压缩了，可以通过两种方法去解压缩

1. 点击 Response Raw 上方的 "Response is encoded any may need to be decoded before inspection. click here to transform"
2. 选中工具栏中的 Decode，这样会自动解压缩。

{% img articleimg /postimg/fiddler/014.png %}