title: grunt-init
keywords: grunt-init
savedir: articles
categories:
  - automation
tags:
  - grunt
date: 2015-11-28 13:30:41
updated: 2015-11-28 13:30:41
urlkey: grunt-init
---
grunt-init 是一个用于自动创建项目的脚手架工具。它会基于当前工作环境和几个问题的答案，构建一个完整的目录结构。但是这依赖于模板的选择，它会根据所提问题的答案，来创建更精确的文件和内容。

## 安装
```
npm install -g grunt-init
```
## 使用
使用模板目录下的模板创建项目
```
grunt-init TEMPLATE
```
使用其它目录的模板创建项目
```
grunt-init /path/to/TEMPLATE
```
> 注意：这里的模板目录是指 %USERPROFILE%\.grunt-init\ 目录，该目录默认是没有的，需要手动创建(`md .grunt-init`)

## 安装模板
Grunt 官方维护五套模板，可以下载到模板目录直接调用

* [grunt-init-commonjs](https://github.com/gruntjs/grunt-init-commonjs)：创建一个包含Nodeunit单元测试的commonjs模块
* [grunt-init-gruntfile](https://github.com/gruntjs/grunt-init-gruntfile)：创建一个基本的Gruntfile
* [grunt-init-gruntplugin](https://github.com/gruntjs/grunt-init-gruntplugin)：创建一个包含Nodeunit单元测试的Grunt插件
* [grunt-init-jquery](https://github.com/gruntjs/grunt-init-jquery)：创建一个包含QUnit单元测试的jQuery插件
* [grunt-init-node](https://github.com/gruntjs/grunt-init-node)：创建一个包含Nodeunit单元测试的Node.js模块

## 自定义模板
自定义模板必须遵循下列文件结构

* `my-template/template.js` - 主模板文件
* `my-template/rename.json` - 模板特定的重命名规则，用于处理模板
* `my-template/root/` - 要复制到目标位置的文件

> 注意：可以把自定义模板放到模板目录，这样可以直接调用`grunt-init my-template`

## 模板语法
当执行初始化模板时, 只要模板使用init.filesToCopy和init.copyAndProcess方法，任何位于root/子目录中的文件都将被复制到当前目录。所有被复制的文件都会被处理为模板，并且所有暴露在props数据对象集合中的{% raw %}{% %}{% endraw %}模板都会被处理，除非设置了noProcess选项

1. 处理root目录的模板文件，多使用{% raw %}{% %}{% endraw %}语法
2. 编写template.js，获取用户录入信息，编写处理逻辑（变量处理、输出package.json等）
3. 编写rename.json（如果需要），设置重命名规则

## 重命名或者排除模板文件
rename.json用于复制过程中对文件重命名，格式为`{src:dest}`，src是相对于root/目录要被复制的文件路径，dest值可以包含{% raw %}{% %}{% endraw %}模板，也可以为false，当为false时该文件不会复制

## 指定初始化提示默认值
新建defaults.json文件来设置默认选项的值，该文件需要放在.grunt-init文件夹中
```
{
  "author_name": "\"Cowboy\" Ben Alman",
  "author_email": "none",
  "author_url": "http://benalman.com/"
}
```

## 内置提示
内置的一些提示可以转成package.json的相应属性，比如author_email用于package.json中的作者邮箱地址

具体使用参考[文档](http://www.gruntjs.org/docs/project-scaffolding.html#init.addlicensefiles)及官方案例。