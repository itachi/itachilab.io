title: 使用Fiddler调试网页
keywords: 使用Fiddler调试网页
savedir: articles
categories:
  - tool
date: 2015-12-03 23:17:58
updated: 2015-12-03 23:17:58
urlkey: fiddler-debug
---
1，找到需要修改的文件session

{% img articleimg /postimg/fiddler-debug/001.png %}

2，保存该文件，如果本地有这个文件可跳过此步

{% img articleimg /postimg/fiddler-debug/002.png %}

3，开启请求自动重定向功能

{% img articleimg /postimg/fiddler-debug/003.png %}

4，创建重定向规则，将目标的 HTTP 请求重定向到本地文件，可以通过 Add 按钮手动添加规则，不过这个 URL 已经出现在 session 列表中，可以直接拖动过来

{% img articleimg /postimg/fiddler-debug/004.png %}

5，修改这个规则，选择本地的文件作为返回的body内容

{% img articleimg /postimg/fiddler-debug/005.png %}

6，完成

{% img articleimg /postimg/fiddler-debug/005.png %}
