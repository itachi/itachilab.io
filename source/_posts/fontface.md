title: fontface
keywords: fontface
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-05 17:19:03
updated: 2015-12-05 17:19:03
urlkey: font-face
---
## 简介
font-face 是 CSS3 中的一个模块，用于网页中使用用户自定义的字体而不局限于安全字体，使用前需要先引入字体，引入后便可以在其它地方设置 `font-family` 了。引入 font-face 语法为

```
@font-face {
	font-family: <fontname>;
	src: <source> [<format>][,<source> [<format>]]*;
	[font-weight: <weight>];
	[font-style: <style>];
}
```

其中 `fontname` 为自定义的字体名，`source` 为字体存放路径，`format`是字体的格式，有 truetype,opentype,truetype-aat,embedded-opentype,avg 等类型，`weight` 定义字体是否为粗体，`style` 主要定义字体样式，如斜体

## 兼容

不同的浏览器对字体格式支持是不一致的（IE4起就已经支持eot格式的字体），以下是几种常见字体的说明

### TureTpe(.ttf)格式：
`.ttf` 字体是Windows和Mac的最常见的字体，是一种RAW格式，因此他不为网站优化,支持这种字体的浏览器有【IE9+,Firefox3.5+,Chrome4+,Safari3+,Opera10+,iOS Mobile Safari4.2+】；

### OpenType(.otf)格式：
`.otf` 字体被认为是一种原始的字体格式，其内置在TureType的基础上，所以也提供了更多的功能,支持这种字体的浏览器有【Firefox3.5+,Chrome4.0+,Safari3.1+,Opera10.0+,iOS Mobile Safari4.2+】；

### Web Open Font Format(.woff)格式：
`.woff` 字体是Web字体中最佳格式，他是一个开放的TrueType/OpenType的压缩版本，同时也支持元数据包的分离,支持这种字体的浏览器有【IE9+,Firefox3.5+,Chrome6+,Safari3.6+,Opera11.1+】；

### Embedded Open Type(.eot)格式：
`.eot` 字体是IE专用字体，可以从TrueType创建此格式字体,支持这种字体的浏览器有【IE4+】；

### SVG(.svg)格式：
`.svg` 字体是基于SVG字体渲染的一种格式,支持这种字体的浏览器有【Chrome4+,Safari3.1+,Opera10.0+,iOS Mobile Safari3.2+】。

> 实际使用中至少需要`.woff`，`.eot` 两种格式字体，甚至还需要 `.svg` 等字体达到更多种浏览版本的支持

```css
@font-face {
	font-family: 'YourWebFontName';
	src: url('YourWebFontName.eot?') format('eot');/*IE*/
	src:url('YourWebFontName.woff') format('woff'), url('YourWebFontName.ttf') format('truetype');/*non-IE*/
}
```

## 资源

* [阿里巴巴](http://www.iconfont.cn/)
* [Google Web Fonts](http://www.google.com/webfonts)
* [Dafont.com](http://www.dafont.com/)
* [Font-Awesome](http://fortawesome.github.io/Font-Awesome/)

## 工具

* 转换字体格式-[Font Squirrel](http://www.fontsquirrel.com/tools/webfont-generator)
* 在线生成字体 - [Fontomas](http://nodeca.github.com/fontomas/)