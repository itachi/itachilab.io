title: git服务端配置
keywords: git服务端配置
savedir: articles
categories:
  - tool
tags:
  - git
date: 2018-03-30 17:37:55
updated: 2018-03-30 17:37:55
urlkey: git-server
---

这种使用场景主要是针对多人协同开发的情况，使用一个git帐户专门管理git仓库，如果只有一个root用户，则不用创建git用户，直接导入公钥并使用ssh协议即可，当然前提是服务端开启ssh登录。

## 服务端-创建git用户
```
groupadd git
adduser git -g git
passwd git
su git
cd ~
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

## 禁用git用户shell登录
```
vi /etc/passwd
git:x:1001:1001:,,,:/home/git:/bin/bash
改为
git:x:1001:1001:,,,:/home/git:/usr/local/git/bin/git-shell
```

## 服务端-创建裸库
```
mkdir /git-res
chown git:git /git-res
su git
mkdir -p /git-res/testgit.git
cd /git-res/testgit.git
git init --bare --shared
```
运行 git init 命令，并加上 –shared 选项，那么 Git 会自动修改该仓库目录的组权限为可写，连接地址为：`ssh://git@<host_ip>:/git-res/testgit.git`（目录名通常以.git结尾）

## 客户端-创建SSH密钥对
```
ssh-keygen -t rsa
```

## 服务端-导入公钥
```
su git
vi ~/.ssh/authorized_keys
输入公销（一行一个）
```

也可以用ssh-copy-id
```
ssh-copy-id [-P port] root@192.168.208.136
```

## 服务端-问题&调试
查看日志文件:/var/log/secure

如果提示“bash: git-upload-pack: command not found”，可以创建软链接 `ln -s /usr/local/git/bin/git-upload-pack /usr/bin/git-upload-pack` 解决，也可以用下面的方法
```
vi /etc/ssh/sshd_config
PermitUserEnvironment yes
cd ~
env > .ssh/environment
service sshd restart
```

如果无法连接，尝试以下操作
```
vi /etc/ssh/sshd_config

RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
service sshd restart
```