title: jenkins日志文件体积过大问题
keywords: jenkins日志文件体积过大问题
savedir: articles
categories:
  - solution
tags:
  - experience
date: 2018-08-01 14:20:02
updated: 2018-08-01 14:20:02
urlkey: jenkins-log-conf
---

### 问题描述
由于dns解析异常等问题，jenkins会不断写日志，很短时间就可以把磁盘写满

查看磁盘占用情况，可用
```
df -h
du -sh /*
```

jenkins日志默认路径为 `/var/log/jenkins/jenkins.log`，如果发现该文件多大占满磁盘空间了，先删了日志文件，删除文件后并不会马上释放空间，需要重启/关闭jenkins进程。

### 彻底解决该问题
* 在 `/etc/sysconfig` 目录下新建文件 `jenkins.logging.properties`，写入一行代码 `.level = INFO`
* 编辑文件jenkins配置文件 `/etc/sysconfig/jenkins`，加入一行代码 `JENKINS_JAVA_OPTIONS="-Djava.util.logging.config.file=/etc/sysconfig/jenkins.logging.properties"`
* 重启jenkins