title: HACK汇总
keywords: HACK汇总
savedir: articles
categories:
  - css
tags:
  - summary
date: 2015-12-26 16:54:57
updated: 2015-12-26 16:54:57
urlkey: hack
---
## IE系列

**条件注释**

	<!--[if IE]>
	<![endif]-->

	lt        [if lt IE 5.5               低于
	lte       [if lte IE 6]               低于或等于
	gt        [if gt IE 5]                高于
	gte       [if gte IE 7]               高于或等于
	!()       [if !(IE 7)]                除了
	&         [if (gt IE 5)&(lt IE 7)]    并且
	|         [if (IE 6)|(IE 7)]          或者

**IE5 IE7**

	*+html select {}

**IE6**

	* html select {}
	*html select {}
	{ _property }
	{ property!important; property }

**IE7**

	*+html select { property!important }
	html > body select { *property }

**IE6 IE7**

	{ *property }

**IE 6-10**

	{ property\9 }

**IE 8-10**

	{ property\0 }

**IE9-10**

	{ property\9\0 }

## 其它浏览器

**Firefox专属**

	@-moz-document url-prefix()
	{ select {} }

**Safari专属**

	@media screen and (-webkit-min-device-pixel-ratio:0)
	{ select {} }

**Opera专属**

@media all and (-webkit-min-device-pixel-ratio:10000), not all and (-webkit-min-device-pixel-ratio:0)
{ head~body select {} }