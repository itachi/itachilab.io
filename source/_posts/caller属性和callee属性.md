title: caller属性和callee属性
keywords: caller属性和callee属性
savedir: articles
categories:
  - js
tags:
  - knowledge
date: 2015-10-03 16:40:19
updated: 2015-10-03 16:40:19
urlkey: caller-callee
---
`arguments.caller`：返回一个函数的引用， 该函数调用了当前函数，对于函数来说，`caller` 属性只有在函数执行时才有定义。 如果函数是在最外层调用的， 那么 `caller` 值是`null`
```js
function callerDemo() {
	if (arguments.caller) {
		var a = callerDemo.caller.toString();
		alert(a);
	} else {
		alert("this is a top function");
	}
}
function handleCaller() {
	callerDemo();
}
handleCaller();
```

`arguments.callee`：对函数对象本身的引用，这有利于匿名函数的递归或者保证函数的封装性。此外，`arguments.length` 是实参长度，`arguments.callee.length` 是形参长度

常规递归如下，其中函数内部包含了对sum自身的引用，此时函数名是一个变量名
```js
var sum = function(n) {
	if (n == 1){
		return 1;
	}else{
		return n + sum(n - 1);
	}
}
```

用 `callee` 改写的递归
```js
var sum = function(n) {
	if (n < = 0){
		return 1;
	}else{
		return n + arguments.callee(n - 1)
	}
}
```