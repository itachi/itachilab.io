title: window.name跨域
keywords: window.name跨域
savedir: articles
categories:
  - request
tags:
  - knowledge
date: 2015-10-03 17:41:56
updated: 2015-10-03 17:41:56
urlkey: windowname-cross
---
原理：window窗口（包括iframe）在加载新页面时，其 name 属性的值是保持不变的，该值可以保存2M大小的数据（IE 或 firefox 是32M）

步骤一：跨域页面组织数据并保存到其 name 属性  
```js
window.name = 'data';
```
步骤二：在当前页面创建一个 iframe，将其 src 指向跨域页面

步骤三：改变 iframe 的 src ，使其和当前页面同域，可以预先准备一个空文件 blank.html 或直接指向 favicon.ico，拿到数据。整个过程通过监听 iframe 的 load 事件来完成
```js
var state = 0,
	iframe = document.createElement('iframe'),
	loadfn = function() {
		if (state === 1) {
			var data = iframe.contentWindow.name;
			alert(data);
		} else if (state === 0) {
			state = 1;
			iframe.contentWindow.location = "http://a.com/proxy.html"; // 设置的代理文件
		}
	};
iframe.src = 'http://b.com/data.html';
if (iframe.attachEvent) {
	iframe.attachEvent('onload', loadfn);
} else {
	iframe.onload = loadfn;
}
document.body.appendChild(iframe);
```

步骤四：销毁 iframe
```js
iframe.contentWindow.document.write('');
iframe.contentWindow.close();
document.body.removeChild(iframe);
```