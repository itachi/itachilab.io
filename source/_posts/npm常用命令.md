title: npm常用命令
keywords: npm常用命令
savedir: articles
categories:
  - nodejs
date: 2015-11-28 15:27:08
updated: 2015-11-28 15:27:08
urlkey: npm-cil
---
### npm install <name>
安装nodejs的依赖包，默认安装最新版本，也可以通过在后面加版本号的方式安装指定版本，如 `npm install express@3.0.6`

### npm install <name> -g
将包安装到全局环境中

### npm install <name> --save
安装的同时，将信息写入 package.json 中，项目路径中如果有 package.json 文件时，可以直接使用 `npm install` 方法根据 dependencies 的配置安装所有的依赖包，这样代码提交到 github 时，就不用提交 node_modules 这个文件夹了。

### npm install <name> --save-dev
安装的同时添加 devDependencies

### npm init
引导创建一个package.json文件，包括名称、版本、作者这些信息等

### npm remove <name>
移除

### npm update <name>
更新

### npm ls
列出当前目录安装的所有包

### npm root
查看当前包的安装路径

### npm root -g
查看全局的包的安装路径

### npm adduser
添加npm帐户

### npm whoami
查看当前npm帐户

### npm publish
发布当前包，需要先 `adduser` ，代码改变后重新发布需要修改版本号

### npm unpublish xxx --force
删除包

### npm unpublish xxx -g
删除全局包