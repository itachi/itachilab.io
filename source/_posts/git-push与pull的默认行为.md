title: git-push与pull的默认行为
keywords: git-push与pull的默认行为
savedir: articles
categories:
  - tool
tags:
  - git
date: 2017-11-28 15:43:51
updated: 2017-11-28 15:43:51
urlkey: git-pull-push
---
## git push
git的全局配置中，有一个 `push.default` 属性，其决定了git push操作的默认行为。在Git 2.0之前，这个属性的默认被设为 `matching`，2.0之后则被更改为了 `simple`

push.default 有以下几个可选值：nothing, current, upstream, simple, matching，可以通过 `git config --global push.default option` 改变push.default的默认行为

* _nothing_ - push操作无效，除非显式指定远程分支，例如git push origin develop（我觉得。。。可以给那些不愿学git的同事配上此项）。
* _current_ - push当前分支到远程同名分支，如果远程同名分支不存在则自动创建同名分支。
* _upstream_ - push当前分支到它的upstream分支上（这一项其实用于经常从本地分支push/pull到同一远程仓库的情景，这种模式叫做central workflow）
* _simple_ - simple和upstream是相似的，只有一点不同，simple必须保证本地分支和它的远程
* _upstream_ -  分支同名，否则会拒绝push操作。
* _matching_ - push所有本地和远程两端都存在的同名分支。

当新建分支并推送到远程时
```
git push origin develop
```
本地有了新的commit，此时执行git push命令时，（Git 2.0之后）会push失败，并收到git如下的警告
```
fatal: The current branch new has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin develop
```
因为2.0之后，push.default = simple，如果没有指定当前分支的upstream分支，就会收到上文的fatal提示

## upstream & downstream
每一个本地分支都相对地可以有一个远程的upstream分支（这个upstream分支可以不同名，但通常都会使用同名分支作为upstream），相应的，本地分支便是远程分支的downstream，这是种相对关系

初次提交本地分支并不会定义当前本地分支的upstream分支，需要使用 `--set-upstream` 或 `-u` 手动指定当前分支的upstream
```
git push --set-upstream origin develop
git push -u origin develop
```

> 注意：push.default = current 可以在远程同名分支不存在的情况下自动创建同名分支，有些时候这也是个极其方便的模式，比如初次push可以直接输入 `git push` 而不必显示指定远程分支

## git pull
当未指定当前分支的upstream时，通常git pull操作会得到如下的提示：
```
There is no tracking information for the current branch.
Please specify which branch you want to merge with.
See git-pull(1) for details

    git pull <remote> <branch>

If you wish to set tracking information for this branch you can do so with:

    git branch --set-upstream-to=origin/<branch> new1
```

git pull的默认行为和git push完全不同。当我们执行git pull的时候，实际上是做了 `git fetch + git merge` 操作，fetch操作将会更新本地仓库的remote tracking，也就是refs/remotes中的代码，并不会对refs/heads中本地当前的代码造成影响。

当进行pull的第二个行为merge时，对git来说，如果没有设定当前分支的upstream，它并不知道要合并哪个分支到当前分支，所以需要通过下面的代码指定当前分支的upstream：

```
git branch --set-upstream-to=origin/<branch> develop
// 或者git push --set-upstream origin develop 
```

实际上，如果我们没有指定upstream，git在merge时会访问git config中当前分支(develop)merge的默认配置，可以通过配置下面的内容指定某个分支的默认merge操作

```
[branch "develop"]
    remote = origin
    merge = refs/heads/develop
```

这样当在develop分支git pull时，如果没有指定upstream分支，git将根据的config文件去merge origin/develop；如果指定了upstream分支，则会忽略config中的merge默认配置。

[参考](https://segmentfault.com/a/1190000002783245)