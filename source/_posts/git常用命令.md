title: git常用命令
keywords: git常用命令
savedir: articles
categories:
  - tool
tags:
  - git
date: 2016-01-03 12:27:46
updated: 2016-01-03 12:27:46
urlkey: git-cmd
---
## 配置-config
### 系统级别（/etc/gitconfig）
```
git config --system user.name "John"
```

### 用户级别（系统用户目录/.gitconfig）
```
git config --global user.name "John"
```

### 项目级别（项目目录/.git/config）
```
git config user.name "John"
```

### 查看配置
```
git config --list
```

## 仓库-init
### 创建仓库
```
git init
```

### 克隆仓库-clone
```
git clone git://github.com/schacon/grit.git
git clone git://github.com/schacon/grit.git mygrit
git clone D:/git/project E:/work/project
```

### 更新-pull
```
git pull origin master
```

### 提交（需要先暂存文件，该命令运行完会启动文本编辑器以便输入版本说明）
```
git commit
```

可以带 `-m` 参数后跟版本说明，`-a` 暂存文件来一步提交
```
git commit -a -m 'info'
```

### 修改最后一次提交（需要先暂存文件）
```
git commit --amend -m 'info'
```

### 源仓库更新
```
git reset --hard
```

### 查看历史
```
git log
```

### 文件
### 文件状态
```
git status
```

### 暂存文件
```
git add .
```

### 忽略文件（一行代表一条规则，可用glob正则）
```
touch .gitignore
or
copy con .gitignore
```

### 文件比较
```
git diff
```

### 移除文件
```
git rm
```

### 重命名
```
git mv file_from file_to
```

## 分支
### 查看分支
```
git branch
```

### 建立分支
```
git branch 'name'
```

### 切换分支
```
git ckeckout 'name'
```

### 合并分支
```
git merge 'name'
```

### 删除分支
```
git branch -d 'name'
```

### 强制删除分支
```
git branch -D 'name'
```

### 推送分支
```
git push origin 'name'
```
## 更新
### 完整写法：`git pull 远程主机 远程分支:本地分支`
```
git pull origin v3:v3
```

### 省略本地分支，表示和当前分支合并
```
git pull origin v3
```

### 省略远程分支，表示当前分支与对应的追踪分支合并
```
git pull origin
```

### 本地删除远程已经删除的分支（默认在更新时不会删除远程已经删除的分支）
```
git pull -p
```

## 推送
### 完整写法：`git push 远程主机 本地分支:远程分支`
```
git push origin v3:v3
```

### 省略远程分支名，表示将本地分支推送到与之对应的远程分支（通常两者同名），如果该远程分支不存在，则会被新建
```
git push origin v3
```

### 省略本地分支名，表示删除指定的远程分支
```
git push origin origin :v3
等同于
gut push origin --delete v3
```

### 省略分支名，表示推送当前分支到远程对应分支
```
git push origin
```

### 推送所有分支，不管远程有没有对应
```
git push --all origin
```

### 强制推送，覆盖远程版本
```
git push --force origin
```

### 推送标签（默认不会推送）
```
git push origin --tags
```

## 远程
### 查看远程
```
git remote
```

### 查看远程址址
```
git remote -v
```

### 远程详细信息
```
git remote show 'name'
```

### 添加远程
```
git remote add 'name' 'adress'
```

### 删除远程
```
git remote rm 'name'
```

### 重命名
```
git remote rename 'oldname' 'newname'
```

### 设置url
```
git remote set-url origin url
```

## 取消
### 取消上一版修改
```
git revert HEAD -n
```

### 回滚到上一个版本
```
git reset --hard HEAD^
```

### 回滚到某个版本
```
git reset --hard 'commit-id'
```

### 回滚最近3次的提交
```
git reset --hard HEAD~3
```

### 取消最近的reset
```
git reset --hard ORIG_HEAD
```