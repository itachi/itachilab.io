title: Visual Studio Code 常用快捷键
keywords: Visual Studio Code 常用快捷键
savedir: articles
categories:
  - tool
tags:
  - experience
date: 2018-05-09 09:57:32
updated: 2018-05-09 09:57:32
urlkey: vscode-keymap
---

### 编辑器与窗口管理
* 打开一个新窗口： `Ctrl+Shift+N`
* 新建文件： `Ctrl+N`
* 文件之间切换： `Ctrl+Tab`
* 分割一个新的编辑器（最多3个）： `Ctrl+\`

### 格式
* 代码行缩进： ``Ctrl+[` `Ctrl+]``
* 区域代码折叠展开： ``Ctrl+Shift+[` `Ctrl+Shift+]``
* 代码格式化： `Shift+Alt+F`
* 上下移动一行： `Alt+Up 或 Alt+Down`
* 向上向下复制一行： `Shift+Alt+Up 或 Shift+Alt+Down`
* 在当前行下边插入一行： `Ctrl+Enter`
* 在当前行上方插入一行： `Ctrl+Shift+Enter`

### 光标
* 移动到定义处： `F12` 或 `Ctrl+点击具体方法`
* 扩展/缩小选取范围： `Shift+Alt+Left` 和 `Shift+Alt+Right`
* 多行编辑(列编辑)： `Alt+Shift+鼠标左键` 和 `Ctrl+Alt+Down/Up`
* 选择从光标到行尾： `Shift+End`
* 选择从行首到光标处： `Shift+Home`
* 删除光标右侧的字： `Ctrl+Delete`
* 删除当前行： `Ctrl+Shift+K`
* 同时选中所有匹配： `Ctrl+Shift+L`
* 匹配花括号的闭合处，跳转： `Ctrl+Shift+\`

### 代码重构
* 格式化代码： `Alt+Shift+F`
* 到所有的引用： `Shift+F12`
* 重命名方法： `F2`

### 其它
* 上传配置： `Shift+Alt+U`
* 下载配置： `Shift+Alt+D`