title: 函数节流在Underscore下的实现
keywords: 函数节流在Underscore下的实现
savedir: articles
categories:
  - thirdparty
date: 2015-12-05 16:41:21
updated: 2015-12-05 16:41:21
urlkey: underscore-throttle
---
## 背景

所谓函数节流，就是控制函数不被频繁地调用，节省客户端及服务器资源。常见的两种方式有：

* __debounce__：关注函数执行的间隔，即函数两次的调用时间不能小于指定时间
* __throttle__：关注函数的执行频率，即在指定频率内函数只会被调用一次

> 注意：以下示例代码基于 [Underscore.js](http://www.css88.com/doc/underscore/)

## 场景一：搜索自动提示

用户在文本框输入文本，自动查询匹配的关键字并提示给用户，实现方式是绑定文本框的 keypress 事件，当输入框内容发生变化时，查询匹配关键字并展示，当用户输入速度比较快时，比如在一秒内输入了 “windows phone”，包含13个字符，在这1秒内便会调用13次查询方法，实际的情况我们只希望用户已经输入完成，或者正在等待提示的时候才去查询匹配关键字，解决方案是在用户暂停输入200毫秒后再进行查询，这样用户不断输入的情况下不会一直请求

```js
var query = _(function() {
	// 在这里进行查询操作
}).debounce(200);
$('#search').bind('keypress', query);
```

## 场景二：滚动获取内容

当用户拖动浏览器滚动条时需要请求服务器获取最新内容，用常规方法绑定 `window.onscroll` 事件后，实际出来的效果用户拖动一次滚动条可能会触发几十次甚至上百次 onscroll 事件，解决方案是每两次查询的间隔不少于500毫秒，如果用户拖动了1秒钟， 可能会触发200次 onscroll 事件，但最多只进行2次查询

```js
var query = _(function() {
	// 在这里进行查询操作
}).throttle(500);
$(window).bind('scroll', query);
```

> 注意：这里不用上面场景一的解决方案，是因为用户希望在拖动的过程中也能看到新内容的变化，而不是当用户拖动滚动条完毕后再查询新的内容