title: jQuery插件开发
keywords: jQuery插件开发
savedir: articles
categories:
  - jQuery
date: 2015-10-03 19:17:21
updated: 2015-10-03 19:17:21
urlkey: jquery-plug
---
## 类级别的插件
在jQuery对象上面添加全局方法
```js
jQuery.foo = function(){}
// 或
jQuery.extend({
    foo: function(){};
})
// 或
jQuery.myplugin = {
    foo: function(){}
}
```
使用 `jQuery.foo()` 或 `jQuery.myplugin.foo()` 调用

## 对象级别的插件
附加到 jQuery.fn 对象上，所以jquery对象可以调用
```js
jQuery.fn.foo = function(){}
// 或
jQuery.fn.extend({
    foo: function(){}
})
```
使用 `$("#id").foo()` 调用

## 默认参数
```js
jQuery.fn.foo = function(options) {
     var defaults = {
        color: "#333"
    }
    var c = jQuery.extend(defaults,options);
})
// 或
jQuery.fn.foo = function(options) {
    var c = jQuery.extend({},jQuery.foo.defaults,options);
}
jQuery.foo.defaults = {
    color: "#333"
}
```