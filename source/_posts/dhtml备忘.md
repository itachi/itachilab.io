title: dhtml备忘
keywords: dhtml备忘
savedir: articles
categories:
  - dom
date: 2015-10-03 14:57:29
updated: 2015-10-03 14:57:29
urlkey: dhtml-memo
---
## querySelector 和 querySelectorAll
IE8(含) 以上，主流浏览器支持。Document、DocumentFragment、Element 都实现了 NodeSelector 接口。即这三种类型的元素都拥有这两个方法。

querySelector 返回的是一个对象（集合里的第一个元素），querySelectorAll 返回的一个集合 (NodeList)，如果没有匹配项，则返回 null 或空的 nodelist

> 注意：IE8中的 selector string 只支持 css2.1 的

## createElement 与 createDocumentFragment
* createElement 创建的元素可以使用 innerHTML，createDocumentFragment 创建的元素使用 innerHTML 并不能达到预期修改文档内容的效果，只是作为一个属性而已。两者的节点类型完全不同，并且 createDocumentFragment 创建的元素在文档中没有对应的标记，因此在页面上只能用js中访问到。
* createElement 创建的元素可以重复操作，添加之后就算从文档里面移除依旧归文档所有，可以继续操作，但是 createDocumentFragment 创建的元素是一次性的，添加之后再就不能操作了
```
var p = document.createElement('p');
p.innerHTML = '测试DocumentFragment';
var fragment = document.createDocumentFragment();
fragment.appendChild(p);
document.body.appendChild(fragment);
var outer = $('outer');
outer.appendChild(fragment); //报错，因为此时文档内部已经能够不存在fragment了
```

## 回车提交表单
* 如果表单里有一个 type=”submit” 的按钮，回车提交表单
如果表单里只有一个 type=”text” 的 input，不管按钮是什么 type，回车提交表单，取消可用 `<input type='text' name='name' onkeydown='if(event.keyCode==13) return false;'/>`
* 有多个 type=”text” 的 input 按下回车将不会自动提交，需要提交时可用 `<input type='text' name='name' onkeydown='if(event.keyCode==13){gosubmit();}' />`

## document.all用法
* `document.all` 是页面内所有元素的一个集合，`document.all(0)` 表示页面内第一个元素
* `document.all` 可以判断浏览器是否是IE
* 可以给某个元素设置id属性（id=aaaa），然后用 `document.all.aaaa` 调用该元素

## 定位到锚点
```js
location.href = location.href + "#wrap";
location.replace(location.href + "#wrap");
```
这两种情况都不会刷新页面，只会定位到锚点

## iframe重复加载问题
iframe未加载完成时发生位移会触发onload事件，导致重复加载

## 属性存放html实体
属性值里有 `&lt;` 这些转义的实体，用 `getAttribute` 拿到后是未转义的 `<`，会按 html 正常解析，而直接存放 `<`，拿到的依然是 `<`，所以这类字符尽量不要存放在属性里

## this.href 和 this.value
input 元素的 `this.value` 返回的是输入框中的值，链接元素的 `this.href` 返回的是绝对URL。如果需要用到这两个网页元素的属性准确值，可以用 `this.getAttribute('value')` 和 `this.getAttibute('href')`

## 连续字母超出容器
* `word-wrap`：允许长单词或 URL 地址换行到下一行，默认为 `normal`。值为 `break-word` 时在长单词或 URL地址内部进行换行。该属性不会强行拆开单词，除非当某单词的长度超过容器宽度时才会被拆行，此时该单词是另起一行
* `word-break`：规定自动换行的处理方法。默认为 `normal`。值为 `break-all` 时允许在单词内换行，值为 `keep-all` 只能在半角空格或连字符处换行。该属性会强行拆开单词
* 处理连续字母超出容器时，如果不需要强制拆开单词，只使用 `word-wrap:break-word`

## defer
IE从IE6起支持 defer 标签，这个标签会让IE并行下载js文件，并且把其执行排到了整个DOM装载完毕，不会阻塞后续DOM的的渲染，多个 defer 的 `<script>` 在执行时也会按照其出现的顺序来运行
```html
<script defer type="text/javascript" src="alert.js" ></script>  
```
> 延伸：HTML5加入了一个异步载入javascript的属性：`async`，无论赋什么样的值，只要它出现，就开始异步加载js文件，但载入后会马上执行，所以无法控制执行的次序和时机

## 使用 cssText 注意问题
cssText 会把原有的 style 样式清掉，所以最好使用累加的方法，而且，cssText（假如不为空）在IE中最后一个分号会被删掉，所以最好在前面添加一个分号
```
ele.style.cssText += ‘;width:100px;height:100px;top:100px;left:100px;’
```