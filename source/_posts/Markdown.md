title: Markdown
keywords: Markdown
savedir: articles
categories:
  - development
tags:
  - knowledge
date: 2015-11-28 15:47:31
updated: 2015-11-28 15:47:31
urlkey: markdown
---
## 宗旨

Markdown 的宗旨是实现易读易写，用一些简单的符号来表示常用的一些样式，而不需要一系列的标签，Markdown 语法的目标就是成为一种适用于网络的书写语言，使用它更快速更便捷得书写文档。

## 兼容 HTML

在Markdown文档里可以直接用HTML语法，比如`<span>`、`<cite>`、`<del>`，甚至`<a>` 或 `<img>` 标签，即使markdown本身有这两个标签的替代用法。使用块状元素比如 `<div>`、`<table>`、`<pre>`、`<p>` 等标签时，必须在前后加上空行与其它内容隔开，还要求它们的开始标签与结尾标签不能用制表符或空格来缩进，比如

    这是一个普通段落。
    
    <table>
        <tr>
            <td>Foo</td>
        </tr>
    </table>
    
    这是另一个普通段落。

> 注意：在HTML块状元素里使用Markdown格式语法是无效的，但在行内元素里使用有效

## 特殊字符自动转换

在Markdown文档里可以直接书写字符 `<` 或 `&`，最终会根据具体环境自动转换为`&lt;`和`&amp;`

## 段落和换行

一个 Markdown 段落是由一个或多个连续的文本行组成，它的前后要有一个以上的空行（空行的定义是显示上看起来像是空的，便会被视为空行。比方说，若某一行只包含空格和制表符，则该行也会被视为空行）。普通段落不该用空格或制表符来缩进。如果有空行或缩进，生成HTML时也会被忽略掉，并且markdown的语法也会被忽略，想实现换行时则是在插入处先按入两个以上的空格然后回车。

## 标题

在行首插入 1 到 6 个 `#` ，对应到标题 1 到 6 ，可以在后面加上 `#` 号来闭合标题，这样只是为了美观，也可以不加，并且不一定要和前面的 `#` 数量相对应，此外，一级标题二级标题还可以在下面用不限数量的 `=` 号和 `-` 号表示

    This is an H1
    =============
    This is an H2
    -------------
    # 这是 H1
    ## 这是 H2 ##

    ### 这是 H3 ######


## 引用

Markdown语法使用类似 email 中用 `>` 的引用方式来表示引用块，具体用法是在每行的最前面加上 `>` ，或整个段落的第一行最前面加上 `>`，嵌套引用只要根据层次加上不同数量的 `>` 即可。另外，_在引用块里面是可以使用其他的 Markdown 语法的_

    > > This is nested blockquote.
    >
    > Back to the first level.

## 强调

Markdown 使用星号 `*` 和底线 `_` 作为标记强调字词的符号，被 `*` 或 `_` 包围的字词会被转成用 `<em>` 标签包围，用两个 `*` 或 `_` 包起来的话，则会被转成 `<strong>`，例如

    *single asterisks*
    _single underscores_
    **double asterisks**
    __double underscores__

## 列表

无序列表使用 `*` 、`+` 或是 `-` 作为列表标记：有序列表则使用数字接着一个 `.` ：

    *   Red
    *   Green
    *   Blue
    1.  Bird
    2.  McHale
    3.  Parish

注意，列表上使用的数字和实际输出的效果是没关系的，

    3.  Bird
    1.  McHale
    8.  Parish

或者

    1.  Bird
    1.  McHale
    1.  Parish

生成的结果都是

    <ol>
    <li>Bird</li>
    <li>McHale</li>
    <li>Parish</li>
    </ol>

效果是一样的

如果列表项目间用空行分开，在输出 HTML 时 Markdown 就会将项目内容用 `<p>` 标签包起来

    *   Bird
    
    *   Magic

会转换为

    <ul>
    <li><p>Bird</p></li>
    <li><p>Magic</p></li>
    </ul>

列表项可以包含多个段落，每个段落都必须缩进 4 个空格或是 1 个制表符：

    1.  This is a list item with two paragraphs. Lorem ipsum dolor
        sit amet, consectetuer adipiscing elit. Aliquam hendrerit
        mi posuere lectus.

        Vestibulum enim wisi, viverra nec, fringilla in, laoreet
        vitae, risus. Donec sit amet nisl. Aliquam semper ipsum
        sit amet velit.

    2.  Suspendisse id sem consectetuer libero luctus adipiscing.

如果要在列表项目内放进引用，那 `>` 就需要缩进：

    *   A list item with a blockquote:
    
        > This is a blockquote
        > inside a list item.

如果要放代码区块的话，该区块就需要缩进两次，也就是 8 个空格或是 2 个制表符：

    *   一列表项包含一个列表区块：
    
            <代码写在这>

## 代码块

建立代码块只要简单地缩进 4 个空格或是 1 个制表符，Markdown 就会用 `<pre>` 和 `<code>` 标签来把代码区块包起来，一个代码区块会一直持续到没有缩进的那一行（或是文件结尾）

    这是一个普通段落：
        这是一个代码区块。

会生成

    <p>这是一个普通段落：</p>
    
    <pre><code>这是一个代码区块。
    </code></pre>

## 分隔线

可以在一行中用三个以上的星号、减号、底线来建立一个分隔线，行内不能有其他东西。但可以在星号或是减号中间插入空格

    * * *
    ***
    *****
    - - -

## 链接

Markdown 支持两种形式的链接语法： **行内式和参考式**两种形式，不管是哪一种，链接文字都是用方括号来标记。

**行内式：**方括号紧跟圆括号并插入网址链接， title 文字位于网址后面用引号包起来

    This is [an example](http://example.com/ "Title") inline link.

会生成

    <p>This is <a href="http://example.com/" title="Title">
    an example</a> inline link.</p>

**参考式：**方括号紧跟方括号，第二个方括号为在任意其它地方定义好的链接标识，两个方括号之间可以有空格，链接标识的定义语法为：方括号标识名，冒号空格，链接，title用引号包围，_注意，标识名是不区分大小写的_

    I get 10 times more traffic from [Google][1] than from
    [Yahoo][2] or [MSN] [3].
    [1]: http://google.com/        "Google"
    [2]: http://search.yahoo.com/  "Yahoo Search"
    [3]: http://search.msn.com/    "MSN Search"

另外，定义链接时链接也可以用尖括号包起来，网址太长时，还可以把title属性放到下一行（可以加缩进）

    [id]: <http://example.com/>  "Optional Title Here"
    [id]: http://example.com/longish/path/to/resource/here
        "Optional Title Here"

如果链接文本和要定义的链接标识名一样时，可以用空方括号来引用标识，比如

    [Google][]
    [Google]: http://google.com/

## 代码

如果要标记一小段行内代码，可以用反引号把它包起来 `` ` ``

    Use the `printf()` function.

会产生：

    <p>Use the <code>printf()</code> function.</p>

如果需要在代码区段内正常使用反引号，可以用多个反引号来开启和结束代码区段，反引号前后可以放一个空格

    ``There is a literal backtick ` here.``
    A backtick-delimited string in a code span: `` `foo` ``

## 图片

Markdown 使用一种和链接相似的语法来标记图片，同样有**行内式和参考式**。

**行内式：**`![Alt text](imagepath "title")`

**参考式：**

    ![Alt text][id]
    [id]: imagepath  "title"

> 注意：Markdown 没有办法指定图片的宽高，如果需要的话可以使用普通的 `<img>` 标签。

## 自动链接

对网址和电子邮件信箱用尖括号包起来， Markdown 会自动把它转成链接。链接文字和链接地址一样，如果是一个邮件地址，Markdown 还会有一个编码转换的过程，把文字字符转成 16 进位码的 HTML 实体

    <http://example.com/>

会转为：

    <a href="http://example.com/">http://example.com/</a>

##反斜杠

Markdown 利用反斜杠来插入一些在语法中有其它意义的符号

    \   反斜线
    `   反引号
    *   星号
    _   底线
    {}  花括号
    []  方括号
    ()  括弧
    #   井字号
    +   加号
    -   减号
    .   英文句点
    !   惊叹号

## GFM

GFM（github-flavored-markdown） 是 SM（standard Markdown）的一个超集，对标准 MarkDown 进行了一些扩展

* 二级标题自动带有下划线
* 取消用单个 `_` 包围的字词来表示斜体
* 自动链接不需要加尖括号会自动转换
* 加入删除线样式 ：`~~Mistaken text.~~`
* 代码块的优化：使用 `` ``` `` 包围并支持高亮，代码不需要缩进

        ```javascript
        function test() {
          console.log("notice the blank line before this function?");
        }
        ```

* 加入对表格的支持

        First Header  | Second Header
        ------------- | -------------
        Content Cell  | Content Cell
        Content Cell  | Content Cell
* 可以引用系统中的用户：`@chengn`
* 可以引用 issue：`#9`
* 支持任务列表

        [] task #1
        [] task item
        [x] complete

## 文档

<http://wowubuntu.com/markdown/>

## 工具

<http://markdownpad.com/>  
<http://dillinger.io/>  
<http://maxiang.info/>  
<http://markable.in/editor/>  

## 结合github

结合github使用会比较方便，github可以在线直接观看效果，并保存md文件

## 结合evernote

**第一种方式：**在 markdownpad 等 Markdown 编辑器里编写文档，得到预览效果，复制到 Evernote 里，保存 .md 源文件，并把这个文件当做附件拖到 Evernote 里，编辑笔记时，点击 Evernote 中附件的 quick look 按钮，点击窗口的右上角用默认的 Markdown editor 进行编辑，完成编辑后再复制回 Evernote 里（通常是只复制你编辑的那一小部分），Evernote 会自动把更新过的 .md 文件保存起来

**第二种方式：**使用马克飞象，可以在线编写 markdown 文本，支持快捷键、工具样、高亮主题、本地储存，完成后可以同步到 evernote（需要先绑定 evernote 帐号），虽然没有保存 md 源文件，但同步到 evernote 的笔记会自带一个链接，可以返回到马克飞象重新编辑
第三种方式：使用 Evernote for Sublime Text 插件，地址：<https://sublime.wbond.net/packages/Evernote> 可以在 sublime 编辑 markdown 并保存到 evernote，同样需要绑定帐号






