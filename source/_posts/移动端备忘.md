title: 移动端备忘
keywords: 移动端备忘
savedir: articles
categories:
  - dom
date: 2016-01-02 18:02:52
updated: 2016-01-02 18:02:52
urlkey: mobile-bak
---
## 定义浏览器点击行为
```html
<a href="tel:020-10086">打电话给:020-10086</a>
<a href="sms:10086">发短信给: 10086</a>
<a href="mailto:me@22278.club">发送邮件: me@22278.club</a>
```

## 定义上传文件类型和格式
```html
<input type=file accept="image/*">
```

上面的文件上传框中，`accept` 可以限制上传文件的类型，参数为 `image/*` 是所有图片类型，点击会弹出图库，也可以指定图片格式，参数设置成 `image/png` 则可以限制图片类型为png；参数如果为 `video/*` 则是选择视频的意思；`accept` 还可以设置多个文件格式，语法为 `accept="image/gif, image/jpeg"`

## IOS 为 WebApp 设置桌面图标和启动画面
基于 `apple-mobile-web-app-capable` 设置为 `yes`
```html
<link rel="apple-touch-icon" href="touch-icon-iphone.png">
<link rel="apple-touch-startup-image" href="/startup.png">
```
> 注意：ios7 以下系统默认会对图标添加特效（圆角及高光），如果不希望系统添加特效，则可以用 `apple-touch-icon-precomposed.png` 代替 `apple-touch-icon.png`

## devicePixelRatio
window.devicePixelRatio 是设备上物理像素和设备独立像素(device-independent pixels (dips))的比例。
设备独立像素（device independent pixels）与屏幕密度有关。可以用来辅助区分视网膜设备还是非视网膜设备。比如：
在视网膜屏幕的iphone 4s上，屏幕物理像素640像素，独立像素还是320像素，因此，window.devicePixelRatio等于2，devicePixelRatio 大于1的都可以视为高清屏

当页面设置了 `<meta name="viewport" content="width=device-width">` 时候，document.documentElement.clientWidth 在大部分浏览器下，得到的是布局视区的宽度，等同于设备独立像素（dips）

对于screen.width的值：
在iOS视网膜设备上，screen.width返回dips宽
在Android设备上，screen.width返回的是物理像素宽度
因此
在iOS设备，screen.width乘以devicePixelRatio得到的是物理像素值。
在Android以及Windows Phone设备，screen.width除以devicePixelRatio得到的是设备独立像素(dips)值