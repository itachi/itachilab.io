title: promise备忘
keywords: promise备忘
savedir: articles
categories:
  - javascript
tags:
  - remind
date: 2017-12-18 16:12:35
updated: 2017-12-18 16:12:35
urlkey: promise-remind
---
## 创建promise对象
`new Promise(fn)` 返回一个promise对象，在fn中指定异步等处理，处理结果正常的话，调用 `resolve(处理结果值)`，处理结果错误的话，调用 `reject(Error对象)`
```js
function getURL(URL) {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        req.open('GET', URL, true);
        req.onload = function () {
            if (req.status === 200) {
                resolve(req.responseText);
            } else {
                reject(new Error(req.statusText));
            }
        };
        req.onerror = function () {
            reject(new Error(req.statusText));
        };
        req.send();
    });
}
```

## promise对象处理方法
被resolve后的处理，promise对象变为FulFilled状态，可以在 `.then` 方法中传入想要调用的函数。被reject后，promise对象变为Rejected状态，可以在 `.then` 的第二个参数或者是在 `.catch` 方法中设置想要调用的函数（推荐使用catch方法）
```js
var URL = "http://httpbin.org/status/500"; 
getURL(URL).then(function onFulfilled(value){
    console.log(value);
}).catch(function onRejected(error){ 
    console.error(error);
});
```
> 注意：catch只是 `promise.then(undefined, onRejected)` 的别名而已， 如下代码也可以完成同样的功能
```
getURL(URL).then(onFulfilled, onRejected);
```

## Promise.resolve
`Promise.resolve(value)` 的返回值也是一个promise对象，可以认为是 `new Promise()` 方法的快捷方式

比如 `Promise.resolve(42)` 可以认为是以下代码的语法糖
```
new Promise(function(resolve){
    resolve(42);
});
```

在这段代码中的 `resolve(42)` 会让这个promise对象立即进入确定（即resolved）状态，并将 42 传递给后面then里所指定的 onFulfilled 函数，当然也可以对其返回值进行 `.then` 调用
```
Promise.resolve(42).then(function(value){
    console.log(value);
});
```

`Promise.resolve` 方法另一个作用就是将thenable对象转换为promise对象。thenable对象是一个具有 `.then` 方法的对象，比如 `jQuery.ajax()`。这种将thenable对象转换为promise对象的机制要求thenable对象所拥有的then方法应该和Promise所拥有的then方法具有同样的功能和处理过程，在将thenable对象转换为promise对象的时候，还会巧妙的利用thenable对象原来具有的then方法

```
var promise = Promise.resolve($.ajax('/json/comment.json'));// => promise对象
promise.then(function(value){
   console.log(value);
});
```

## Promise.reject
`Promise.reject(error)` 是和 `Promise.resolve(value)` 类似的静态方法，是 `new Promise()` 方法的快捷方式

比如 `Promise.reject(new Error("出错了"))` 就是下面代码的语法糖
```
new Promise(function(resolve,reject){
    reject(new Error("出错了"));
});
```

## Promise的异步操作
为了防止同步调用和异步调用同时存在导致的混乱，Promise的回调都是异步的，即使马上resolve
```
var promise = new Promise(function (resolve){
    console.log("inner promise"); // 1
    resolve(42);
});
promise.then(function(value){
    console.log(value); // 3
});
console.log("outer promise"); // 2
```

## then
then可以链式调用，每个then方法的return的值作为下一个then接收的参数，return的值会由 `Promise.resolve(return的返回值)` 进行相应的包装处理，因此不管回调函数中会返回一个什么样的值，最终then的结果都是返回一个新创建的promise对象，也就是说then不仅仅是注册一个回调函数那么简单，它还会将回调函数的返回值进行变换，创建并返回一个promise对象

```
function taskA() {
    console.log("Task A");
    throw new Error("throw Error @ Task A")
}
function taskB() {
    console.log("Task B");// 不会被调用
}
function onRejected(error) {
    console.log(error);// => "throw Error @ Task A"
}
function finalTask() {
    console.log("Final Task");
}

var promise = Promise.resolve();
promise
    .then(taskA)
    .then(taskB)
    .catch(onRejected)
    .then(finalTask);
 ```

 因为then或catch是每次都创建一个新的promise对象，所以如果多个then需要链式操作，下面的代码是错误的示范
 ```
var aPromise = new Promise(function (resolve) {
    resolve(100);
});
aPromise.then(function (value) {
    return value * 2;
});
aPromise.then(function (value) {
    return value * 2;
});
aPromise.then(function (value) {
    console.log("1: " + value); // => 100
})
 ```

 这种写法中的 then 调用几乎是在同时开始执行的，而且传给每个 then 方法的 value 值都是 100

 ```
 function badAsyncCall() {
    var promise = Promise.resolve();
    promise.then(function() {
        // 任意处理
        return newVar;
    });
    return promise;
}
```
这种写法有很多问题，首先在 promise.then 中产生的异常不会被外部捕获，此外，也不能得到 then 的返回值，即使其有返回值，需要改为
```
function anAsyncCall() {
    var promise = Promise.resolve();
    return promise.then(function() {
        // 任意处理
        return newVar;
    });
}
```

## Promise.all
`Promise.all` 接收一个promise对象的数组作为参数，当这个数组里的所有promise对象全部变为resolve或reject状态的时候，它才会去调用 `.then` 方法
```
function getURL(URL) {
    return new Promise(function (resolve, reject) {
    	// code
    });
}
var request = {
        comment: function getComment() {
            return getURL('http://azu.github.io/promises-book/json/comment.json').then(JSON.parse);
        },
        people: function getPeople() {
            return getURL('http://azu.github.io/promises-book/json/people.json').then(JSON.parse);
        }
    };
function main() {
    return Promise.all([request.comment(), request.people()]);
}

main().then(function (value) {
    console.log(value);
}).catch(function(error){
    console.log(error);
});
```
在上面的代码中，request.comment() 和 request.people() 会同时开始执行，而且每个promise的结果（resolve或reject时传递的参数值），和传递给 Promise.all 的promise数组的顺序是一致的
```
function timerPromisefy(delay) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve(delay);
        }, delay);
    });
}
var startDate = Date.now();
Promise.all([
    timerPromisefy(1),
    timerPromisefy(32),
    timerPromisefy(64),
    timerPromisefy(128)
]).then(function (values) {
    console.log(Date.now() - startDate + 'ms');
    // 約128ms
    console.log(values);    // [1,32,64,128]
});
```

## Promise.race
只要有一个promise对象进入 FulFilled 或者 Rejected 状态的话，就会继续进行后面的处理。
```
var winnerPromise = new Promise(function (resolve) {
        setTimeout(function () {
            console.log('this is winner');
            resolve('this is winner');
        }, 4);
    });
var loserPromise = new Promise(function (resolve) {
        setTimeout(function () {
            console.log('this is loser');
            resolve('this is loser');
        }, 1000);
    });
// 第一个promise变为resolve后程序停止
Promise.race([winnerPromise, loserPromise]).then(function (value) {
    console.log(value);    // => 'this is winner'
});
```
> 注意：Promise.race 在第一个promise对象变为Fulfilled之后，并不会取消其他promise对象的执行

## then or catch
```
function throwError(value) {
    // 抛出异常
    throw new Error(value);
}
// <1> onRejected不会被调用
function badMain(onRejected) {
    return Promise.resolve(42).then(throwError, onRejected);
}
// <2> 有异常发生时onRejected会被调用
function goodMain(onRejected) {
    return Promise.resolve(42).then(throwError).catch(onRejected);
}
// 运行示例
badMain(function(){
    console.log("BAD");
});
goodMain(function(){
    console.log("GOOD");
});
```
这段代码中，badMain是不太好的实现方式，因为即使 throwError 抛出了异常，onRejected 指定的函数也不会被调用（即不会输出"BAD"字样），而 oodMain 的代码则遵循了 throwError→onRejected 的调用流程。 这时候 throwError 中出现异常的话，在会被方法链中的下一个方法，即 .catch 所捕获，进行相应的错误处理。

`.then` 方法中的onRejected参数所指定的回调函数，实际上针对的是其promise对象或者之前的promise对象，而不是针对 `.then` 方法里面指定的第一个参数，即onFulfilled所指向的对象，这也是 then 和 catch 表现不同的原因。

## reject or throw
Promise的构造函数，以及被 then 调用执行的函数基本上都可以认为是在 try...catch 代码块中执行的，所以在这些代码中即使使用 throw ，程序本身也不会因为异常而终止。

如果在Promise中使用 throw 语句的话，会被 try...catch 住，最终promise对象也变为Rejected状态。因此，想把 promise对象状态 设置为Rejected状态的话，使用 reject 方法则更显得合理。
```
var promise = new Promise(function(resolve, reject){
    reject(new Error("message"));
});
promise.catch(function(error){
    console.error(error);// => "message"
})
```

如果在then中想reject又不想用throw方法时，可以这样
```
var promise = Promise.resolve();
promise.then(function () {
    return Promise.reject(new Error("this promise is rejected"));
}).catch(onRejected);
```
或者直接 `throw new Error('error!!!')`