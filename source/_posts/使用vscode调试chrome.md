title: 使用vscode调试chrome
keywords: 使用vscode调试chrome
savedir: articles
categories:
  - development
tags:
  - debug
date: 2018-10-30 15:32:52
updated: 2018-10-30 15:32:52
urlkey: vscode-debug-chrome
---

使用 Debugger for Chrome 插件，可以对在 vscode 上调试运行在chrome上的页面，支持两种调试模式：launch和attach

## launch模式
### 添加配置
```
{
    "version": "0.1.0",
    "configurations": [
        {
            "name": "Launch localhost",
            "type": "chrome",
            "request": "launch",
            "url": "http://localhost:7000",
            "webRoot": "${workspaceFolder}"
        },
        {
            "name": "Launch index.html",
            "type": "chrome",
            "request": "launch",
            "file": "${workspaceFolder}/index.html"
        }
    ]
}
```
这种模式会自动启动一个chrome浏览器，并访问url字段配置的地址或者file字段配置的文件

> 注意：如果启动调试之前已经运行了chrome，这样后面打开的chrome是不会在调试模式的，这种情况下会以其它用户的身份打开chrome，这个chrome是比较纯净的chrome，没有加载安装过的扩展（因为是新的用户），为了避免这种情况，在启动调试之前最好关了所有chrome

## attach模式
### 添加配置
```
{
    "version": "0.1.0",
    "configurations": [
        {
            "name": "Attach Chrome",
            "type": "chrome",
            "request": "attach",
            "port": 9222,
            "webRoot": "${workspaceFolder}"
        }
    ]
}
```

### 用remote-debugging模式打开chrome
找到chrome的快捷方式，右键属性，在“目标”字段后面添加 ` --remote-debugging-port=9222`，也可以在终端直接带参数启动chrome
```
cd C:\Program Files (x86)\Google\Chrome\Application && start chrome.exe --remote-debugging-port=9222
```

这种模式可以附加到已经存在的chrome，避免上面那种找不到扩展的问题，但一样需要启动之前关闭所有chrome

> 注意：如果提示 `connect ECONNREFUSED 127.0.0.1:9222` 错误，可能是chrome没有在debug模式启动，或者启动的端口号和配置不一致