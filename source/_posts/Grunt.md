title: Grunt
keywords: Grunt
savedir: articles
categories:
  - automation
tags:
  - grunt
date: 2015-11-28 15:29:37
updated: 2015-11-28 15:29:37
urlkey: grunt
---
## 安装 Grunt 命令行
将Grunt命令行（CLI）安装到全局环境中，以后就可以在任何目录下执行 grunt 命令
```
npm install -g grunt-cli
```
> 注意：安装 grunt-cli 并不等于安装了 Grunt，它的作用是调用与 Gruntfile 在同一目录中 Grunt，即同一个系统可以同时安装多个版本的 Grunt

## 创建 Grunt 项目
一个 Grunt 项目要求有两个文件：package.json 和 Gruntfile。

package.json：此文件源于 npm ，用于存储项目的元数据以便将此项目发布为npm模块  
Gruntfile： 此文件被命名为 Gruntfile.js 或 Gruntfile.coffee，用来配置或定义任务（task）并加载 Grunt 插件的

### package.json
可通过三种方式来创建该文件

1. 大部分 grunt-init 模版都会自动创建特定于项目的 package.json 文件。
2. npm init命令会创建一个基本的 package.json 文件。
3. 手动创建

```
{
  "name": "my-project-name",
  "version": "0.1.0",
  "devDependencies": {
    "grunt": "~0.4.1",
    "grunt-contrib-jshint": "~0.6.0",
    "grunt-contrib-nodeunit": "~0.2.0",
    "grunt-contrib-uglify": "~0.2.2"
  }
}
```

### Gruntfile
Gruntfile.js 或 Gruntfile.coffee 文件是有效的 JavaScript 或 CoffeeScript 文件,由以下几部分构成：

* "wrapper" 函数
* 项目与任务配置
* 加载grunt插件和任务
* 自定义任务

#### "wrapper" 函数
每一份 Gruntfile （和grunt插件）都遵循同样的格式，书写的 Grunt 代码必须放在此函数内：
```js
module.exports = function(grunt) {
};
```

#### 项目和任务配置
大部分的Grunt任务都依赖某些配置数据，这些数据被定义在一个对象内，并传递给 grunt.initConfig 方法
```js
grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	uglify: {
		options: {
			banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
		},
		build: {
			src: 'src/<%= pkg.name %>.js',
			dest: 'build/<%= pkg.name %>.min.js'
		}
	}
})
```
在上面的案例中，`grunt.file.readJSON('package.json')`将存储在 package.json 文件中的JSON元数据引入到 grunt config 中。 由于`<% %>`模板字符串可以引用任意的配置属性，因此可以通过这种方式来指定诸如文件路径和文件列表类型的配置数据，从而减少一些重复的工作。

> 注意：此配置主要是以任务名称命名的属性，也可以包含其他任意数据。一旦这些代表任意数据的属性与任务所需要的属性相冲突，就将被忽略。此外，由于这本身就是JavaScript，你不仅限于使用JSON；还可以使用任意的有效的JS代码

#### 加载 Grunt 插件和任务
已经安装的 Grunt 插件都可以在 Gruntfile 中以简单命令的形式使用
```
grunt.loadNpmTasks('grunt-contrib-uglify');
```

也可以使用 matchdep 来一次性加载所有插件，在`grunt.initConfig`前面加上
```js
require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
```
> 注意：matchdep 也是一个 grunt 插件，需要出现在 package.json 并安装

#### 自定义任务
通过定义 default 任务，可以让Grunt默认执行一个或多个任务。
```
grunt.registerTask('default', ['uglify']);
```
在上面的这个案例中，执行 grunt 命令时如果不指定一个任务的话，将会执行uglify任务。这和执行grunt uglify 或者 grunt default的效果一样。

如果Grunt插件中的任务（task）不能满足你的项目需求，你还可以在Gruntfile中自定义任务（task）。例如，在下面的 Gruntfile 中自定义了一个default 任务，并且他甚至不依赖任务配置：
```
grunt.registerTask('default', 'Log some stuff.', function() {
	grunt.log.write('Logging some stuff...').ok();
})
```
## 安装 Grunt 和 grunt插件
```
npm install <module> --save-dev
```
此命令不光安装了<module>，还会自动将其添加到 devDependencies 配置段中，所以用该命令来安装 Grunt 和 grunt插件，安装完同样会记录到 package.json 文件里，比如安装 Grunt 最新版本到项目目录中如下
```
npm install grunt --save-dev
```
安装完下载的文件会出现在项目目录下node_modules文件夹里，还有另一种安装方式，先在 package.json 文件里定义好需要的插件，在项目目录下执行以下命令
```
npm install
```

## Gruntfile 配置对象
当运行一个任务时，Grunt会自动查找配置对象中的同名属性。多任务（multi-task）可以通过任意命名的“目标（target）”来定义多个配置，同时指定任务（task）和目标（target），例如grunt concat:foo或者grunt concat:bar，将只会处理指定目标（target）的配置，而运行grunt concat将遍历所有目标（target）并依次处理。
```js
grunt.initConfig({
	concat: {
		foo: {
			// concat task "foo" target options and files go here.
		},
		bar: {
			// concat task "bar" target options and files go here.
		},
	},
	uglify: {
		bar: {
			// uglify task "bar" target options and files go here.
		},
	},
});
```
在一个任务配置中，options属性可以用来指定覆盖内置属性的默认值。此外，每一个目标（target）中还可以拥有一个专门针对此目标（target）的options属性。目标（target）级的平options将会覆盖任务级的options。
```js
grunt.initConfig({
	concat: {
		options: {
			// 这里是任务级的Options，覆盖默认值 
		},
		foo: {
			options: {
				// "foo" target options may go here, overriding task-level options.
			},
		},
		bar: {
			// No options specified; this target will use task-level options.
		},
	},
});
```

## 文件
由于大多的任务都是执行文件操作，Grunt 有一个强大的抽象层用于声明任务应该操作哪些文件。并有几种方式来表达这些文件映射关系，比如最常用的是以下这种文件数组格式，另外还有简洁格式、文件对象格式等
```js
grunt.initConfig({
	concat: {
		foo: {
			files: [
				{src: ['src/aa.js', 'src/aaa.js'], dest: 'dest/a.js'},
				{src: ['src/aa1.js', 'src/aaa1.js'], dest: 'dest/a1.js'}
			]
		},
		bar: {
			files: [
				{src: ['src/bb.js', 'src/bbb.js'], dest: 'dest/b/', nonull: true},
				{src: ['src/bb1.js', 'src/bbb1.js'], dest: 'dest/b1/', filter: 'isFile'}
			]
		}
	}
})
```

> 注意：所有的文件格式都支持 src 和 dest 属性，表示源文件和目标文件，此外简洁格式和文件数组格式还支持一些额外的属性，比如 filter nonull dot 等

### 额外filter属性
filter属性可以给你的目标文件提供一个更高级的详细帮助信息。只需要使用一个有效的fs.Stats 方法名。下面的配置仅仅清理一个与模式匹配的真实的文件：
```js
clean: {
	foo: {
		src: ['tmp/**/*'],
		filter: 'isFile',
	},
}
```
也可以自定义 filter 函数，根据文件是否匹配来返回true或者false。下面的例子将仅仅清理一个空目录：
```js
clean: {
	foo: {
		src: ['tmp/**/*'],
		filter: function(filepath) {
			return (grunt.file.isDir(filepath) && require('fs').readdirSync(filepath).length === 0);
		}
	}
}
```

### 通配符模式
Grunt通过内置支持 [node-glob}(https://github.com/isaacs/node-glob) 和 [minimatch](https://github.com/isaacs/minimatch) 库来匹配文件名

* `*` 匹配任意数量的字符，但不匹配 /
* `?` 匹配单个字符，但不匹配 /
* `**` 匹配任意数量的字符，包括 /，只要它是路径中唯一的一部分
* `{}` 允许使用一个逗号分割的“或”表达式列表
* `!` 在模式的开头用于排除一个匹配模式所匹配的任何文件

比如：foo/*.js 将匹配位于 foo/ 目录下的所有的 .js 结尾的文件；而 foo/**/*js 将匹配 foo/ 目录以及其子目录中所有以 .js 结尾的文件

此外，Grunt 允许在一个数组里定义这些路径或路径模式，匹配时按顺序处理
```js
// 指定单个文件：
{src: 'foo/this.js', dest: ...}
// 指定一个文件数组：
{src: ['foo/this.js', 'foo/that.js', 'foo/the-other.js'], dest: ...}
// 使用一个匹配模式：
{src: 'foo/th*.js', dest: ...}

// 一个独立的node-glob模式：
{src: 'foo/{a,b}*.js', dest: ...}
// 也可以这样编写：
{src: ['foo/a*.js', 'foo/b*.js'], dest: ...}

// foo目录中所有的.js文件，按字母顺序排序：
{src: ['foo/*.js'], dest: ...}
// 首先是bar.js，接着是剩下的.js文件，并按字母顺序排序：
{src: ['foo/bar.js', 'foo/*.js'], dest: ...}

// 除bar.js之外的所有的.js文件，按字母顺序排序：
{src: ['foo/*.js', '!foo/bar.js'], dest: ...}
// 按字母顺序排序的所有.js文件，但是bar.js在最后。
{src: ['foo/*.js', '!foo/bar.js', 'foo/bar.js'], dest: ...}
```

### 动态文件对象
需要批量处理文件时，一些附加的属性可以用来动态地构建文件列表。expand 设为 true 用于启用以下选项：

* cwd 所有 src 的匹配都是相对于当前路径，但不包括当前路径。
* src 相对于 cwd 路径的匹配模式。
* dest 目标路径的前缀。
* ext 使用这个属性值替换生成的 dest 路径中所有文件的扩展名。
* extDot 用于指定扩展名点号的位置。可以设为 'first' (扩展名始于文件名的第一个点号)或 'last' (扩展名始于文件名的最后一个点号)，默认为 'first'。[0.4.3 新增]
* flatten 从生成的 dest 路径中移除所有的路径部分。
* rename 在执行 ext 和 flatten 之后，对每个匹配的 src 文件调用这个函数。传入 dest 和匹配的 src 路径，返回一个新的 dest 值。如果返回相同的 dest 不止一次，每个使用它的 src 来源都将被添加到一个数组中。

在下面的例子中，static_mappings 和 dynamic_mappings 两个目标中查找到的文件映射列表是相同的，这是因为任务运行时 Grunt 会自动将 dynamic_mappings 文件对象扩展为与 static_mappings 一致的静态文件映射。
```js
grunt.initConfig({
	uglify: {
		static_mappings: {
			// 静态文件映射，
			// 添加或删除文件时需要更新 Gruntfile。
			files: [
				{src: 'lib/a.js', dest: 'build/a.min.js'},
				{src: 'lib/b.js', dest: 'build/b.min.js'},
				{src: 'lib/subdir/c.js', dest: 'build/subdir/c.min.js'},
				{src: 'lib/subdir/d.js', dest: 'build/subdir/d.min.js'},
			],
		},
		dynamic_mappings: {
			// 动态文件映射，
			// 当任务运行时会自动在 "lib/" 目录下查找 "**/*.js" 并构建文件映射，
			// 添加或删除文件时不需要更新 Gruntfile。
			files: [{
				expand: true, // 启用动态扩展
				cwd: 'lib/', // 源文件匹配都相对此目录
				src: ['**/*.js'], // 匹配模式
				dest: 'build/', // 目标路径前缀
				ext: '.min.js', // 目标文件路径中文件的扩展名
				extDot: 'first' // 扩展名始于文件名的第一个点号
			}, ],
		},
	},
});
```
### 模板
使用`<%= %>`可以读取配置对象中的其它数据，不管是什么类型，还可以使用 grunt 以及它的方法，比如`<%= grunt.template.today('yyyy-mm-dd') %>`，`<% %>`可以执行任意内联的 JavaScript 代码。对于控制流或者循环来说是非常有用的。
```js
grunt.initConfig({
	concat: {
		sample: {
			options: {
				banner: '/* <%= baz %> */\n', // '/* abcde */\n'
			},
			src: ['<%= qux %>', 'baz/*.js'], // [['foo/*.js', 'bar/*.js'], 'baz/*.js']
			dest: 'build/<%= baz %>.js', // 'build/abcde.js'
		}
	}
	//用于任务配置模板的任意属性
	foo: 'c',
	bar: 'b<%= foo %>d', // 'bcd'
	baz: 'a<%= bar %>e', // 'abcde'
	qux: ['foo/*.js', 'bar/*.js'],
})
```