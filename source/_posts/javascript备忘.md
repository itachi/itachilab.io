title: javascript备忘
keywords: javascript备忘
savedir: articles
categories:
  - js
tags:
  - remind
date: 2015-10-03 14:51:02
updated: 2015-10-03 14:51:02
urlkey: js-remind
---
## 字符串转数字
`var num = Number(str)` 这种强制类型转换只对以10为基数有效，并不允许数字中间有空白（开头和结尾的空白是允许的），要更灵活地转化数字，可以使用 `parseInt()` 和 `parseFloat()`，这两个函数可以从字符串开始处转换数字，还可以传一个表示基数的参数，如果转换不成功，则返回 NaN，如
```js
parseInt("ab12")==NaN
```

## substring(start,end) 和 substr(start,length)
* substring 传递两个索引，以较小的那个为起始，如 `substring(0,3)` 和 substring(3,0)` 结果一样，如果 start 或 end 为负数或 NaN，则自动转化为0。最终返回的结果不包括 end。
* substr 传递索引和长度，如果长度为0或负数，则返回空串，如果长度不指定，则返回剩下全部

## split(str|reg,limit) 和 slice(start,end) 和 splice(start,count,[item1,item2])
* split：根据传入的 str 或 reg 将字符串分隔成字符串数组，str 或 reg 本身不返回，如果不传入 str 或 reg，则返回包含整个字符串的单一元素数组，limit 可以限制返回数组的数量
* slice：返回数组从 start 到 end 的部分，不包含 end，如果 start 或 end 为负数，则按 length+start 或 length+end 处理，如果不传入 end，则一直复制到结尾
* splice：从数组移除一个或多个元素，可以在移除的位置插入新元素，返回移除元素组成的新数组

## 构造函数返回值
构造函数默认自动返回 this，如果手动指定返回数据，当该数据不是引用类型时会被忽略，比如返回字符串，结果拿到的还是 this

## for-in遍历对象
for in 在循环对象的属性时也会遍历原型链，但不会读取不可枚举属性，如数组的 length 属性。
```js
Array.prototype.oo = "xx";
var arr = ["a","b","c"];
for(var i in arr){
  console.log(arr[i]); //输出"a","b","c","xx"
}
```

hasOwnProperty 是 Javascript 中唯一一个可以处理对象属性而不遍历原型链的方法。可以使用该属性在遍历时过滤掉原型链属性
```js
for(var i in arr){
  if(arr.hasOwnProperty(i)){
    console.log(arr[i]); //输出"a","b","c"
  }
}
```

> 提示：因为 for in 循环总是遍历整个原型链，所以当遍历多继承的对象时效率较低。

## 获取子元素节点
`childNodes` 方法和 `firstChild` 方法在现代浏览器中都会把元素标签中的空白节点检测出来
```
<div>
    <p>123</p>
</div>
```

```
var oDiv=document.getElementByTagName("div")[0];
alert(oDiv.firstChild.nodeName)
```
在ie9以下，alert出来的是p（p标签名字），但是在现代浏览器下，比如Chrome,FF，ie11等等会alert出#text（由于空白节点是属于text文本节点）

解决方案：用 `firstElementChild` 代替 `firstChild`，但该方法不兼容ie678，需要考虑兼容可以用 `children`
```
var first=document.getElementByTagName("div")[0].children[0]
```

## 严格模式下的this
严格模式下，顶层作用域的this值是 `undefined`，而不是指向 `window` 

```
(function() {
    // "use strict";
    var item = {
        document: "My document",
        getDoc: function() {
            return this.document;
        }
    }
    var getDoc = item.getDoc;
    console.log(getDoc());
})();
```

这段代码在严格模式下会报错，`Uncaught TypeError: Cannot read property 'document' of undefined`

## forEach 跳出循环
forEach 无法使用 break 或 return false 来跳出循环，如果需要，可以用 try 包住，通过抛出异常来实现中断循环，另外，for循环是可以用break来跳出循环的

## forEach 和 map
forEach 只是对数组的每个元素执行一遍callback，返回的结果没什么作用，而map是用每一个callback返回的结果作为新数组的元素，包括undefined

## map
map是类似对象的一种数据类型，其键可以是任何类型的值（对象只能是字符串或synbol），对map遍历可以使用foreach或forof
let iterable = new Map([["a", 1], ["b", 2], ["c", 3]]);

iterable.forEach((value, key) => {
  console.log(value) // a, b, c
})

for (let entry of iterable) {
  console.log(entry);
}
// [1, 'a']
// [2, 'b']
// [3, 'c']

for (let [key, value] of iterable) {
  console.log(key);
}
// 1
// 2
// 3