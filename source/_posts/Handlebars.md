title: Handlebars
keywords: Handlebars
savedir: articles
categories:
  - compile
tags:
  - templete
date: 2015-12-05 12:54:56
updated: 2015-12-05 12:54:56
urlkey: Handlebars
---
## 模板
模板表达式使用 `{% raw %}{{{% endraw %}` 开始，`{% raw %}}}{% endraw %}` 闭合。[表达式语法参考](http://handlebarsjs.com/expressions.html)

### 基本表达式
在当前上下文读取属性或 helper，可以用 `.` 或 `/` 读取下一级，`../` 读取上一级
```
<h1>{{title}}</h1>
<h1>{{article.title}}</h1>
<h1>{{article/title}}</h1>
{{articles.[10].[#comments]}} 等同于 articles[10]['#comments']
{{../permalink}}
```

### Helpers
在基本表达式后面追加空格和任意个参数，便成为一个 Helpers，参数可以是基本表达式、字符串、`=`号键值对（作为最后一个参数），脚本里使用 `Handlebars.registerHelper` 定义 Helpers，函数里使用 `Handlebars.escapeExpression` 和 `Handlebars.SafeString` 返回 HTML 字符
```
{{{link "See more..." story.url}}}
----------------------
Handlebars.registerHelper('link', function(text, url) {
  url = Handlebars.escapeExpression(url);
  text = Handlebars.escapeExpression(text);
  return new Handlebars.SafeString(
    "<a href='" + url + "'>" + text + "</a>"
  );
});

{{{link "See more..." href=story.url class="story"}}}
----------------------
Handlebars.registerHelper('link', function(text, options) {
  var attrs = [];
  for (var prop in options.hash) {
    attrs.push(
        Handlebars.escapeExpression(prop) + '="'
        + Handlebars.escapeExpression(options.hash[prop]) + '"');
  }
  return new Handlebars.SafeString(
    "<a " + attrs.join(" ") + ">" + Handlebars.escapeExpression(text) + "</a>"
  );
});
```

参数支持字符串、布尔值、null、undefined 类型
```
{{agree_button "My Text" class="my-class" visible=true counter=4}}
```

在 Helpers 可使用 this 来引用当前上下文
```
<ul>
  {{#each items}}
  <li>{{agree_button}}</li>
  {{/each}}
</ul>
----------------------
var context = {
  items: [
    {name: "Handlebars", emotion: "love"},
    {name: "Mustache", emotion: "enjoy"},
    {name: "Ember", emotion: "want to learn"}
  ]
};
Handlebars.registerHelper('agree_button', function() {
  var emotion = Handlebars.escapeExpression(this.emotion),
      name = Handlebars.escapeExpression(this.name);
  return new Handlebars.SafeString(
    "<button>I agree. I " + emotion + " " + name + "</button>"
  );
});
```

### 块表达式
在 Helpers 添加 `#` 并使用 `/` 闭合，便成为一个块表达式，块表达式拥有单独的上下文，[详细查看](http://handlebarsjs.com/block_helpers.html)
```
{{#list people}}{{firstName}} {{lastName}}{{/list}}
----------------------
{
  people: [
    {firstName: "Yehuda", lastName: "Katz"},
    {firstName: "Carl", lastName: "Lerche"},
    {firstName: "Alan", lastName: "Johnson"}
  ]
}
----------------------
Handlebars.registerHelper('list', function(items, options) {
  var out = "<ul>";
  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }
  return out + "</ul>";
});
```

### 子表达式
可以在表达式里调用其它表达式的结果作为参数
```
{outer-helper (inner-helper 'abc') 'def'}
```

### 空白符
在表达式中添加 `~` 可以去掉空白符
```
<a href="{{url}}">
  {{~title}}
</a>
```

### HTML 转译
默认会 HTML 转译，使用 `\` 或 `{% raw %}{{{{% endraw %}` 可以输出 HTML 字符
```
\{{escaped}}
{{{{raw}}}
  {{escaped}}
{{{{/raw}}}}
```

### 注释
使用 `{% raw %}{{!-- --}}{% endraw %}` 或 `{% raw %}{{! }}{% endraw %}` 注释
```
{{!-- only output author name if an author exists --}}
```

## 编译
先定义模板，调用 `Handlebars.compile` 编译模板，再传入数据，得到渲染结果
```
1----------------------
<script id="entry-template" type="text/x-handlebars-template">
  <div class="entry">
    <h1>{{title}}</h1>
    <div class="body">
      {{body}}
    </div>
  </div>
</script>
2----------------------
var source   = $("#entry-template").html();
var template = Handlebars.compile(source);
3----------------------
var context = {title: "My New Post", body: "This is my first post!"};
var html    = template(context);
```

`template` 函数支持第二个参数，[详细查看](http://handlebarsjs.com/execution.html)

## 预编译模板
为了节省客户端渲染时间，可以使用工具预先编译好模板，页面只使用引入一个 runtime 版本的 handlebars 库，[具体查看](http://handlebarsjs.com/precompilation.html)
```
npm install handlebars -g
handlebars <input> -f <output>
----------------------
<script src="/libs/handlebars.runtime.js"></script>
```

## 子模板
可使用子模板使用共用模板，[详细查看](http://handlebarsjs.com/partials.html)
```
<div class="post">
  {{> userMessage tagName="h1" }}

  <h1>Comments</h1>

  {{#each comments}}
    {{> userMessage tagName="h2" }}
  {{/each}}
</div>
----------------------
Handlebars.registerPartial('userMessage',
    '<{{tagName}}>By {{author.firstName}} {{author.lastName}}</{{tagName}}>'
    + '<div class="body">{{body}}</div>');
var context = {
  author: {firstName: "Alan", lastName: "Johnson"},
  body: "I Love Handlebars",
  comments: [{
    author: {firstName: "Yehuda", lastName: "Katz"},
    body: "Me too!"
  }]
};
```

## 内建 Helpers
Handlebars 内置了一些 Helpers，[详细查看](http://handlebarsjs.com/builtin_helpers.html)
### if
```
{{#if author}}
  <h1>{{firstName}} {{lastName}}</h1>
{{/if}}
```

### unless
```
{{#unless license}}
<h3 class="warning">WARNING: This entry does not have a license!</h3>
{{/unless}}
```

### each
```
<ul class="people_list">
  {{#each people}}
    <li>{{this}}</li>
  {{/each}}
</ul>
```

### with
```
<div class="entry">
  <h1>{{title}}</h1>

  {{#with author}}
  <h2>By {{firstName}} {{lastName}}</h2>
  {{/with}}
</div>
```

### lookup
```
{{#each bar}}
  {{lookup ../foo @index}}
{{/each}}
```

### log
```
{{log "Look at me!"}}
```