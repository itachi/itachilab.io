title: Genymotion无法安装Google Play的解决方法
keywords: Genymotion无法安装Google Play的解决方法
savedir: articles
categories:
  - tool
tags:
  - experience
date: 2016-01-03 12:11:29
updated: 2016-01-03 12:11:29
urlkey: genymotion-install
---
## 背景
新版 Genymotion 移除了 Google 市场。也一并移除了 ARM library 的支持。有的APP用到了 ARM 的 SO 库，安装这些 App 时，会报 `「INSTALL_FAILED_CPU_ABI_INCOMPATIBLE」` 错误，比如微信

## 解决方法
1. 下载 [ARM Translation Installer v1.1](http://forum.xda-developers.com/showthread.php?p=47502902#post47502902)
2. 将第1步下载到的 zip 文件拖进虚拟机安装，安装完重启虚拟机
3. 下载对应系统的 [GApps](http://forum.xda-developers.com/showthread.php?p=47502902#post47502902)
4.  将第3步下载到的 zip 文件拖进虚拟机安装，安装完重启虚拟机