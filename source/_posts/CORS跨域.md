title: CORS跨域
keywords: CORS跨域
savedir: articles
categories:
  - request
tags:
  - knowledge
date: 2015-12-26 16:34:35
updated: 2015-12-26 16:34:35
urlkey: cors
---
CORS：跨域资源共享（Cross-Origin Resource Sharing），是 W3C 中一项较新的跨域解决方案，主流浏览器都支持，IE系列只 IE9 以上支持。值得一提的是，CORS在移动终端的支持很不错。

## 使用CORS

被请求的接口需要添加一个返回头`Access-Control-Allow-Origin`，这样该接口就可以接受其它域的请求并返回数据

如果跨域需要携带 cookie，接口需要返回响应头 `Access-Control-Allow-Credentials:true` 以及 `Access-Control-Allow-Origin: request.headers['origin']`，显式指定请求头的 origin 字段，不需要携带 cookie 的，只需要返回`Access-Control-Allow-Origin: *`

PHP示例
```php
header("Access-Allow-Origin: *");
```
`*`表示接受任何域名的请求，也可以把`*`换成具体某个域名
```php
header("Access-Allow-Origin: http://www.text.com");
```

> 注意：设置了CORS的接口在头信息是可以看到`Access-Control-Allow-Origin`标记的，可以利用这个判断该接口是否可以直接`POST`跨域