title: CentOS中的环境变量配置文件
keywords: CentOS中的环境变量配置文件
savedir: articles
categories:
  - operation
tags:
  - linux
date: 2018-03-13 17:49:21
updated: 2018-03-13 17:49:21
urlkey: centos-path
---
CentOS的环境变量配置体系是一个层级体系，这与其他多用户应用系统配置文件是类似的，有全局的，有用户的，有shell的，另外不同层级有时类似继承关系。下面以PATH变量为例。

### 影响当前终端（临时）
在终端执行以下命令
```
export PATH=/someapplication/bin:$PATH
```

### 影响当前用户使用的bash shell
修改 `~/.bashrc`

### 影响所有用户使用的bash shell
修改 `/etc/bashrc`（Ubuntu和Debian中是 `/etc/bash.bashrc`）

在bash shell打开时运行。这里bash shell有不同的类别：

* 登录shell和非登陆shell，登录shell需要输入用户密码，非登陆shell不会执行任何profiel文件
* 交互shell和非交互shell，提供命令提示符等待用户输入命令的是交互shell模式，直接运行脚本文件是非交互shell模式，一般情况下非交互shell模式不执行任何bashrc文件。

根据以上情况，选择是否修改 `/etc/bashrc`。

### 影响当前用户（首选）
修改 `~/.bash_profile` 添加
```
export PATH=/someapplication/bin:$PATH
```

### 影响全局
修改 `/etc/environment` 添加
```
PATH=/someapplication/bin:$PATH
```

### 影响全局（所有用户）
修改 `/etc/profile` 添加
```
export PATH=/someapplication/bin:$PATH
```

要使修改生效，可以重启系统，或者执行
```
source /etc/profile
echo $PATH
```

`/etc/environment` 文件与 `/etc/profile` 文件的区别是：`/etc/environment` 设置的是系统的环境，而 `/etc/profile` 设置的是所有用户的环境，即 `/etc/environment` 与用户无关，在系统启动时运行

> 注意：CentOS和大多Linux系统使用 `$` 访问环境变量，环境变量PATH中使用冒号:分隔。而Windows中使用两个 `%` 访问环境变量，PATH使用分号;分隔，例如：
```
set PATH=E:\someapplication\bin;%PATH%
```