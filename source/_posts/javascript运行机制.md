title: javascript运行机制
keywords: javascript运行机制
savedir: articles
categories:
  - js
date: 2017-06-03 10:49:35
updated: 2017-06-03 10:49:35
urlkey: javascript-execution
---
## 执行环境(执行上下文)(Execution Context)
当javascript解释器初始化代码后，默认会进入全局的执行环境，之后每调用一个函数，javascript解释器会创建一个新的执行环境

执行环境的分类：

* 全局执行环境：一个程序只有一个全局对象即window对象，全局对象所处的执行环境就是全局执行环境
* 函数执行环境：函数调用过程会创建函数的执行环境
* Eval执行环境：eval代码特定的环境

## 执行栈(Context Stack)
解释器使用执行栈来存储代码运行时的执行环境，执行栈遵循先进后出的原理

javascript初始化完代码后，首先会创建全局执行环境并推入当前的执行栈，当调用一个函数时，javascript引擎会创建新的执行环境并推到当前执行栈的顶端，直到再无新函数调用。最上方的函数执行完成后，它的执行环境便从当前栈中弹出，并将控制权移交到当前执行栈的下一个执行环境，直到全局执行环境。当程序或浏览器关闭时，全局环境也将退出并销毁。

## 创建执行环境
执行器会分为两个阶段来创建执行环境，分别是创建阶段和激活(执行)阶段。而由于规范的不同，每个阶段执行的过程有很大的不同。

### ES3 规范
创建阶段：

* 1. 创建作用域链
* 2. 创建变量对象VO(包括参数，函数，变量)
* 3. 确定this的值

激活/执行阶段：

* 完成变量分配，执行代码

### ES5 规范
创建阶段：

* 1. 确定 this 的值
* 2. 创建词法环境(LexicalEnvironment)
* 3. 创建变量环境(VariableEnvironment)

激活/执行阶段：

* 完成变量分配，执行代码

## Variable Object(VO: 变量对象)，Active Object(AO: 活动对象)
VO 和 AO 是ES3规范中的概念，在创建执行环境的第一阶段会创建变量对象，即VO.它是用来存放执行环境中可被访问但是不能被 delete 的函数标识符，形参，变量声明等，也就是说这些会被挂在这个OV对象上，对象的属性对应它们的名字

有了变量对象存每个上下文中的东西，进入执行后这个执行上下文儿中的变量对象就被激活，变成激活对象（Activation object），即AO和VO之间区别就是AO是一个激活的VO，仅此而已

### 创建VO对象

* 1.创建arguments对象
* 2.扫描上下文的函数声明(而非函数表达式),为发现的每一个函数，在变量对象上创建一个属性——确切的说是函数的名字——其有一个指向函数在内存中的引用。如果函数的名字已经存在，引用指针将被重写
* 3.扫描上下文的变量声明,为发现的每个变量声明，在变量对象上创建一个属性——就是变量的名字，并且将变量的值初始化为undefined。如果变量的名字已经在变量对象里存在，将不会进行任何操作并继续扫描

注意> 整个过程可以大概描述成： 函数的形参=>函数声明=>变量声明， 其中在创建函数声明时，如果名字存在，则会被重写，在创建变量时，如果变量名存在，则忽略不会进行任何操作

## LexicalEnvironment(词法环境)，VariableEnvironment（变量环境）
词法环境和变量环境是ES5以后提到的概念,简单的理解，词法环境是一个包含标识符变量映射的结构。

### 词法环境(lexicalEnvironment)
词法环境由两个部分组成

* 环境记录(enviroment record)，存储变量和函数声明
* 对外部环境的引用(outer)，可以通过它访问外部词法环境

### 变量环境(objectEnvironment)
变量环境也是个词法环境，主要的区别在于lexicalEnviroment用于存储函数声明和变量（ let 和 const ）绑定，而ObjectEnviroment仅用于存储变量（ var ）绑定。

### 伪代码展示
```
let a = 20;  
const b = 30;  
var c;

function d(e, f) {  
 var g = 20;  
 return e * f * g;  
}

c = d(20, 30);
```
```
// 全局环境
GlobalExectionContext = {

  this: <Global Object>,
    // 词法环境
  LexicalEnvironment: {  
    EnvironmentRecord: {  
      Type: "Object",  // 环境记录分类： 对象环境记录
      a: < uninitialized >,  // 未初始化
      b: < uninitialized >,  
      d: < func >  
    }  
    outer: <null>  
  },

  VariableEnvironment: {  
    EnvironmentRecord: {  
      Type: "Object",  // 环境记录分类： 对象环境记录
      c: undefined,  // undefined
    }  
    outer: <null>  
  }  
}
// 函数环境
FunctionExectionContext = {  
   
  this: <Global Object>,

  LexicalEnvironment: {  
    EnvironmentRecord: {  
      Type: "Declarative",  // 环境记录分类： 声明环境记录
      Arguments: {0: 20, 1: 30, length: 2},  // 函数环境下，环境记录比全局环境下的环境记录多了argument对象
    },  
    outer: <GlobalLexicalEnvironment>  
  },

  VariableEnvironment: {  
    EnvironmentRecord: {  
      Type: "Declarative",  // 环境记录分类： 声明环境记录
      g: undefined  
    },  
    outer: <GlobalLexicalEnvironment>  
  }  
}
```