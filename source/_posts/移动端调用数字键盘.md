title: 移动端调用数字键盘
keywords: 移动端调用数字键盘
savedir: articles
categories:
  - dom
date: 2015-12-05 13:20:52
updated: 2015-12-05 13:20:52
urlkey: number-keyboard
---
## type="tel"

```html
<input type="tel">
```

iOS 和 Android 键盘表现差不多，数字下面会显示字母

{% img articleimg /postimg/number-keyboard/001.png %}

## type="number"

```html
<input type="number">
```
Android 是真正的数字键盘（数字下面没有字母），iso 不是九宫格键盘，输入不方便

{% img articleimg /postimg/number-keyboard/002.png %}

旧版 Android（包括微信所用的X5内核）在输入框后面会有超级鸡肋的小尾巴，Android 4.4.4 以后给去掉了

{% img articleimg /postimg/number-keyboard/003.png %}

可以用 webkit 私有的伪元素给 fix 掉

```css
input[type=number]::-webkit-inner-spin-button,  
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        appearance: none; 
        margin: 0; 
    }
```

## pattern
pattern 用于验证表单输入的内容，通常 HTML5 的 type 属性，比如 email、tel、number、data类、url 等，已经自带了简单的数据格式验证功能了，加上 pattern 后，前端部分的验证更加简单高效了。

```html
<input type="number" pattern="[0-9]*">
```

ios 中，只有 [0-9]* 才可以调起九宫格数字键盘，\d 无效，Android 4.4.4 以上，只认 type 属性，也就是说，如果上面的代码将 type="number" 改为 type="text" ，将调起全键盘而不会是九宫格数字键盘

### pattern兼容性

{% img articleimg /postimg/number-keyboard/004.png %}