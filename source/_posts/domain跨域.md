title: domain跨域
keywords: domain跨域
savedir: articles
categories:
  - request
tags:
  - knowledge
date: 2015-10-03 17:39:36
updated: 2015-10-03 17:39:36
urlkey: domain-cross
---
解决主域相同而二级域名不同的两个页面的通讯，这里以 `http://www.a.com/a.html` 和 `http://script.a.com/b.html` 为例

步骤一：设置同一主域，两个页面分别加上 
```js
document.domain = ‘a.com’
```

步骤二：a.html 创建 iframe，指向 b.html
```js
var ifr = document.createElement('iframe');
ifr.src = 'http://script.a.com/b.html';
ifr.style.display = 'none';
document.body.appendChild(ifr);
```
步骤三：通过 iframe 的 contentDocument 获取 b.html 的内容
```js
ifr.onload = function(){
    var doc = ifr.contentDocument || ifr.contentWindow.document;
    alert(doc.getElementsByTagName("h1")[0].childNodes[0].nodeValue);
};
```
> 注意：某一页面的 domain 默认等于 window.location.hostname。主域名是不带 www 的域名，如 www.a.com 是二级域名，domain 只能设置为自已的主域名，比如不可以在 b.a.com 中将 domain 设置为 c.a.com，也不可以设置为 c.com，会报错
