title: JQuery备忘
keywords: JQuery备忘
savedir: articles
categories:
  - jQuery
date: 2015-10-03 19:38:01
updated: 2015-10-03 19:38:01
urlkey: jquery-memo
---
## 位置与尺寸
* innerWidth和innerHeight：宽或高加上padding
* outerWidth和outerHeight：宽或高加上padding加上border

> 注意，这四个属性可以带一个布尔值，默认为false，当手动设为true，结果会加上margin

* position：元素相对有定位的父级元素的偏移（即父元素设置了relative或absolute），当父元素没有设置定位时，结构和offset一样
* offset：元素相当文档的偏移

## 阻止冒泡
JQuery 提供了两种方式来阻止事件冒泡
### 方式一
```js
$("#div1").mousedown(function(event) {
event.stopPropagation();
})
```

### 方式二
```js
$("#div1").mousedown(function(event) {
return false;
})
```
 
区别：`return false` 不仅阻止了事件往上冒泡，而且阻止了事件默认行为。`event.stopPropagation()` 则只阻止事件往上冒泡，不阻止事件本身。

## 按钮事件
jQuery中对键盘事件进行了修正,通过事件的 `which` 可以找到键码，当有组合键的时候还需要注意一下，如 `ctrl+enter` 键，enter键的键码不是始终为13，在IE6中为10
```js
$(document).keypress(function(e){
     if(e.ctrlKey && e.which == 13 || e.which == 10) {
          $("#btn").click();
     } else if (e.shiftKey && e.which==13 || e.which == 10) {
          $("#btnv").click();
     }
})
```