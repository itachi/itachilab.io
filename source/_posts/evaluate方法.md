title: evaluate方法
keywords: evaluate方法
savedir: articles
categories:
  - dom
date: 2016-01-03 12:13:33
updated: 2016-01-03 12:13:33
urlkey:
---
## 简介
`evaluate()` 是 DOM 元素上的一个方法，用于计算一个 XPath 表达式

## XPath 表达式
XPath 是 W3C 的一个标准，是一种表达式语言，它的返回值可能是节点，节点集合，原子值，以及节点和原子值的混合等，语法详细参考[W3C文档](http://www.w3.org/TR/xpath20/)

> chrome，firefox 开发者工具元素右键菜单是可以直接复制出 XPath 的

## 语法

```
evaluate(xpathText, contextNode, namespaceURLMapper, resultType, result)
```

返回的是一个枚举集合，可以使用 `while` 循环来枚举元素
```
var result = document.evaluate("//a[@href]", document, null, XPathResult.ANY_TYPE, null);
var nodes;
while (nodes = result.iterateNext()){
    // code
}
```

> 注意：在IE中语法是不同的，IE使用 `document.selectNodes(xpath)` ，用 `for` 循环直接遍历元素

## 参数说明

* __`xpathText`__: XPath 表达式
* __contextNode__: 关联上下文节点， XPath 查询只返回包含在这个元素内的元素，一般为 `document`
* __namespaceURLMapper__: 只在工作在 application/xhtml+xml 类型的页面上有效，一般设为 `null`
* __resultType__: 值为 `XPathResult` 对象所定义的常量，`XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE` 表示结果以随机的方式返回。与页面出现的顺序无关；`XPathResult.ORDERED_NODE_SNAPSHOT_TYPE` 表示按页面出现的顺序返回
* __result__: 传一个 XPathResult 对象，以便把结果集合并到一起，没有则设为 `null`

## 应用举例

获取所有拥有 title 属性的元素
```
var allElements = document.evaluate('//*[@title]', document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
for (var i = 0; i < allElements.snapshotLength; i++) {
	var ele = allElements.snapshotItem(i);
	switch (ele.nodeName.toUpperCase()) {
		case 'A':
		// this is a link, do something
		break;
		case 'IMG':
		// this is an image, do something else
		break;
		default:
		// do something with other kinds of HTML elements
	}
}
```

jquery封装
```
$.dealXpath = function(xpathstr, dom){
	var xpathResult, elem, rlt = [], dom = dom || document;
	xpathResult = dom.evaluate(xpathstr, dom, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
	while(elem = xpathResult.iterateNext()){
		rlt.push(elem);
	}
	return $([]).pushStack(rlt);
}
// 使用：$.dealXpath('/html/body/div[11]/div[2]/img')
```