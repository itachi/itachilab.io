title: mongodb常用命令
keywords: mongodb常用命令
savedir: articles
categories:
  - storage
tags:
  - mongodb
date: 2016-01-02 18:42:16
updated: 2016-01-02 18:42:16
urlkey: mongodb-cli
---

### 常用
```
show dbs  #显示数据库列表 
show collections  #显示当前数据库中的集合（类似关系数据库中的表）
show users  #显示用户
use <db name>  #切换当前数据库，如果数据库不存在则创建数据库。 
```

### 删除
```
use mydb
db.dropDatabase()  #删除数据库
db.mycollection.drop()  #删除集合
```

### 关闭
```
use admin
db.shutdownServer()
```

### 添加用户
```
use admin
db.createUser({user: 'root', pwd: 'admin123-', roles: ['root']})
db.createUser({user: 'root', pwd: 'admin123-', roles: [{db: 'yapi', role: 'readWrite'}]})
```

### 认证
```
db.auth('username', 'password')
```
> 返回1表示认证成功

### 修改密码
```
db.changeUserPassword('username','password')
```