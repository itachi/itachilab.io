title: javascript数据类型
keywords: javascript数据类型
savedir: articles
categories:
  - javascript
tags:
  - knowledge
date: 2018-01-10 11:03:04
updated: 2018-01-10 11:03:04
urlkey: js-typeof
---
## 数据类型

js 目前有以下几大数据类型

* `undefined`
* `null`
* `string`
* `boolean`
* `number`
* `symbol (ES6新增)`
* `object`

### undefined 类型
原始值类型. 表示某个变量已经声明, 但是未分配内存空间给予该变量，以下情况会出现undefined
```
var o = {foo: 'foo'}
console.log(o.bar); /* 在对象中寻找不存在的属性 */

var foo; 
console.log(foo); /* 申明变量, 却没有赋值 */

function bar (a) {
  console.log(a); /* 函数形参声明, 却没有对应实参赋值 */
}
bar();

function foo2 () {}
var bar2 = foo2(); /* 函数无return 或者, return 不带任何返回值 */

var o = {foo: 'foo'}
console.log(o.bar); /* 在对象中寻找不存在的属性 */
```

### null 类型
原始值类型. 声明变量此时为一个空的原始值. 由于历史原因, 使用typeof null 返回 object表示为一个对象.如果要验证一个变量是否为null, 可以有以下几种方式
```
function is_null1 (o) {
  return o === null;
}

function is_null2 (o) {
  return Object.prototype.toString.call(o) === '[object Null]';
}
```

### number 类型
原始值类型. 存储一切数值类型, 包括正负数, 整数, 小数, 科学计数.
```
typeof NaN // number
typeof Infinity // number
typeof 0 // number
typeof 1e2 // number
typeof -0.5 // number
```

检查一个变量是否为合法的 number 类型
```
window.isNaN('12') // false
window.isNaN('foo') // true
```

检查number是否为有限的number(非无穷)值
```
window.isFinite('123') // true
window.isFinite(Infinity) // false
```

### object类型
引用类型. js中常见的引用对象 有 `Array`, `Object`, `Date`.
```
var arr = [];
var o = {};
var date = new Date()
typeof o // object
typeof arr // object
typeof date // object
```

函数也属于对象类型, 它是一种可调用的对象, 并且能创建调用栈执行内部代码. 只是函数在执行typeof 时,返回的是function
```
function Foo() {
}

typeof Foo // function 
Foo instanceof Object // true
```

## 包装类型
为了方便`boolean`, `number`, `string` 基本类型的处理及调用, 当我们在执行 `toString()`, `valueOf()`, `+操作符` 以及函数方法的时候, 会隐世转换为Boolean,Number, String对象类型. 执行后返回的又是基本类型。
```
var bool = true
var number = 1
var string = 'hello world'

console.log(bool + number) // 2
// 相当于
console.log(Boolean(bool).valueOf() + number) // 2

console.log(bool + string) // true hello world
// 相当于
console.log(Boolean(bool).toString() + string) // true hello world

string.charAt(0) // h
// 相当于
console.log(String(string).charAt(0)） // h

// 基础类型不属于包装的对象类型
bool instanceof Boolean // false
number instanceof Number // false
string instanceof String // false
```