title: JSON
keywords: JSON
savedir: articles
categories:
  - development
tags:
  - knowledge
date: 2015-11-28 12:14:51
updated: 2015-11-28 12:14:51
urlkey: json
---
JSON（JavaScript Object Notation） 是一种轻量级的数据交换格式，完全独立于语言，由两种基础结构组成

1. “键/值”对的集合：用大括号包围，键与值之间用冒号隔开，“键/值”对之间用逗号隔开。
2. 值的有序列表：用中括号包围，用逗分分隔

## javascript 中使用JSON
1. [下载扩展API包](https://github.com/douglascrockford/JSON-js)
2. 解压后得到文件：`json2.js`、`json.js`、`json_parse_state.js`、`json_parse.js`、`cycle.js`

### json2.js
定义了一个全局的对象JSON，带有两个方法 `stringify` 和 `parse` 来实现转换或解析 json 格式

#### JSON.stringify( value, replacer, space )

```js
var jsonobj = new Object();
jsonobj.username = "uname";
jsonobj.password = "pwd";
var jsonstr = JSON.stringify(jsonobj);
```

结果：`{"username":"uname","password":"pwd"}`  
如果不转换会直接输出`[object Object]`

#### JSON.parse(text, reviver )

```js
var resjson = '{"username":"uname","password":"pwd"}';
var resjson = JSON.parse(resjson);
document.getElementById("output").innerHTML=resjson;
```

结果：`[object Object]`，此时 json 格式的字符串被解析成对象或数组，方便程序调用。

### json.js
跟 `json2.js` 基本一样，也是定义了JSON 对象，同时还给 Object 对象扩展了两个方法 `toJSONString` 和 `parseJSON`，方便变量随时转换或解析 json 格式。  
使用案例（结果同上）：

```js
var jsonobj = new Object();
jsonobj.username="uname";
jsonobj.password="pwd";
var jsonstr = jsonobj.toJSONString();
document.getElementById("output").innerHTML=jsonstr;

var resjson = '{"username":"uname","password":"pwd"}';
var resstr = resjson.parseJSON();
document.getElementById("output").innerHTML=resstr;
```

其它文件定义了特殊需求的方法，详细说明查看[官方说明](https://github.com/douglascrockford/JSON-js)  

## PHP 中使用json
从5.2版本开始，PHP原生提供 `json_encode()` 和 `json_decode()` 函数，用于转换或解析 json 格式,由于 json 只接受 utf-8 编码的字符，所以 `json_encode()`的参数必须是 utf-8 编码，否则会得到空字符或者 null。

`json_encode` 对索引数组转换成数组形式，对关联数组或混合型数组转换成对象形式，将其中的数字索引转换为数字字符串

```php
$arr = array("a","b");
echo json_encode($arr);     //输出：["a","b"]

$arr = array("a"=>"aa","b"=>"bb");
echo json_encode($arr);     //输出{"a":"aa","b":"bb"}

$arr = array("a"=>"aa","b",3);
echo json_encode($arr);     //输出{"a":"aa","0":"b","1":3}
```

对刚申明对象进行转换时，只会转换对象所关联的类的公共变量

```php
class Foo {
     const ERROR_CODE = '404';
     public $public_ex = 'this is public';
     private $private_ex = 'this is private!';
     public function getErrorCode() {
          return self::ERROR_CODE;
     }
}

$foo = new Foo;
$foo_json = json_encode($foo);
echo $foo_json;//输出{"public_ex":"this is public"}
```

通常情况下，`json_decode()` 总是返回一个PHP对象，而不是数组，如果想要强制生成PHP关联数组，`json_decode()`需要加一个参数 true

```php
$arr = '{"a":"aa","b":"bb"}';
$rlt = json_decode($arr);
echo $rlt->a;//输出aa
//var_dump($rlt);

$arr = '{"a":"aa","b":"bb"}';
$rlt = json_decode($arr,true);
print_r($rlt);//输出Array ( [a] => aa [b] => bb )
```

json只能用来表示对象（object）和数组（array），如果对一个字符串或数值使用 `json_decode()`，将会返回 null。

```php
var_dump(json_decode("Hello World")); //返回null
```
注意：json的分隔符（delimiter）只允许使用双引号，“键/值”对的"键"必须使用双引号。最后一个值之后不能添加逗号，所以以下三种写法都是错的

```php
$bad_json = "{ 'bar': 'baz' }";
$bad_json = '{ bar: "baz" }';
$bad_json = '{ "bar": "baz", }';
```
### 处理中文
php5.2新增的 `json_encode` 对中文的处理有以下问题

1. 不能处理GB编码，所有的GB编码都会替换成空字符.
2. utf8 编码的中文被编码成 unicode 编码，相当于 javascript 的 `escape` 函数处理的结果.

__解决方法__：  
采用utf8编码，用以下的包装类来编码解码，解码后可按实际需要转为其它编码

```php
class Json{ 
public static function encode($str){ 
$code = json_encode($str); 
return preg_replace("#\\\u([0-9a-f]+)#ie", "iconv('UCS-2', 'UTF-8', pack('H4', '\\1'))", $code); 
} 

public static function decode($str){ 
return json_decode($str); 
} 
}

Json::encode($code); 
Json::decode($code);
```
对要发送到客户端的json数据，可以直接用 `json_encode` 编码（前提是utf-8编码），然后在客户端用 javascript 的 `unescape` 函数来对 unicode 编码的中文进行解码，还原成正确的中文
